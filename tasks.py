from atelier.invlib import setup_from_tasks
ns = setup_from_tasks(
    globals(),
    use_dirhtml=True,
    # tolerate_sphinx_warnings=True,
    # blogref_url="http://hw.lino-framework.org",
    revision_control_system='git',
    # multiple_blog_entries_per_day=True,
    cleanable_files=[
        'docs/rss_entry_fragments/*'],
    intersphinx_urls=dict(docs="https://hw.saffre-rumma.net"))
