=======================
The Human World project
=======================

This is the `Sphinx <http://sphinx-doc.org/>`_ source code of the content
published at <https://lsaffre.gitlab.io/hw/> and <https://hw.saffre-rumma.net>

The **Human World** website is a collection of my
personal opinions about God and the world.  Nobody pays me to write them.
They are not finished and may evolve as long as I live.
I appreciate your feedback.
Feel free to `contact me
<http://luc.lino-framework.org/about/index.html>`__
if you have suggestions or questions.
