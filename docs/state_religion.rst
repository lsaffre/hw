==============
State religion
==============

I believe that national governments should choose and support a given religion
as their `state religion <https://en.wikipedia.org/wiki/State_religion>`_,
as being the "recommended choice" for people living in that country.

Of course I don't mean a government should *enforce* any such choice. That would
be a restriction of the freedom of faith.

Governments must not ignore the importance of religious cultures.

Having no religious education at all is even worse than believing in
some obsolete teaching.


This implies two kinds of recognitions:

- recognize that certain teachings of an existing religion have become
  **obsolete**.
