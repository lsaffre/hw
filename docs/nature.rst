=======================
About nature and beyond
=======================


.. glossary::

  nature

    The part of :term:`reality` that exists outside of human brains.

  natural

    A part of :term:`nature`, or related to :term:`nature`.

  supernatural

    Something beyond nature.


[T]he very notion of supernatural--in opposition to the natural--is a Western
invention. The "supernatural realm" only came into being as a thought form after
we began to understand things in a natural, scientific way. Only when the
concept of "the natural" emerged was it deemed necessary by some to speak of
"the supernatural": that which was imagined to be above or outside of nature.
-- Benson Saler (via :doc:`/blog/2020/0905`).


Paul Broks (in `Are coincidences real?
<https://aeon.co/essays/how-should-we-understand-the-weird-experience-of-coincidence>`__)
writes that he is "an unequivocal rationalist" and yet still wants "to see
something strange and wonderful in life's weird coincidences". He gives a few
beautiful examples of those coincidences, "some of which had a distinctly
supernatural feel" and explains how and why he refuses to give in to the
temptation of believing in a magical, supernatural world. "I am a naturalist,
but coincidences give me a glimpse of what the supernaturalist sees, and my
worldview is briefly challenged." My comment: that's why "supernatural" is a
misleading word to use when speaking about God. God does not act against the
laws of nature.
