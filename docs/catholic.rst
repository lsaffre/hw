===============
About Catholics
===============


.. glossary::

  Catholic Church

    The world's largest Christian religious denomination.

    https://en.wikipedia.org/wiki/Catholic_Church

.. glossary::

  Holy Mass

    The central liturgical rite in the :term:`Catholic Church`.

    https://en.wikipedia.org/wiki/Mass_in_the_Catholic_Church

  Catechism

    The *Catechism of the Catholic Church*, published by the :term:`Catholic
    Church` in 1992. It sums up the beliefs of the Catholic faithful.

    https://en.wikipedia.org/wiki/Catechism_of_the_Catholic_Church

  Second Vatican Council

    https://en.wikipedia.org/wiki/Second_Vatican_Council

  Synod of Bishops

    See https://sinod.katoliku.ee/en/1/



Jokes about Catholics
=====================

A Coca Cola representative visits the Pope and asks: I will donate $1,000,000 if
you manage to add "and Coca-Cola" after the "daily bread" in Our Lord's prayer.
The Pope does not even answer, he picks the phone, says something in Italian,
and two Swiss guards come and throw the representative out of Vatican city.  Two
weeks later the representative comes back and says "Okay I spoke with my bosses.
We offer a billion.". The Pope picks up his phone and when the other end picked
up, he says "Please bring me from the archives our agreement with the bakers."

Old Andrzej was a minister in a small Polish town. He had always been a good man
and lived by the Bible. One day God decided to reward him, with the answer to
any three questions Andrzej would like to ask. Old Andrzej did not need much
time to consider, and the first question was: "Will there ever be married
Catholic priests?" God promptly replied: "Not in your life-time." Andrzej
thought for a while, and then came up with the second question: "what about
female priests then, will we have that one day?" Again God had to disappoint Old
Andrzej: "Not in your life-time, I'm afraid." Andrzej was sorry to hear that,
and he decided to drop the subject. After having though for a while, he asked
the last question: "Will there ever be another Polish pope?" God answered
quickly and with a firm voice: "Not in my life-time." (my mother, a great fan of
pope John Paul II, told me this once at the phone, somewhere around 2006
(shortly after John Paul II had passed away). I found it back `here
<https://www.netfunny.com/rhf/jokes/90q1/polpope.368.html>`__).
