=====================
About the free market
=====================

.. glossary::

  free market

    A system in which the prices for goods and services are self-regulated by
    buyers and sellers negotiating in an open market.  -- simplified from
    `Wikipedia <https://en.wikipedia.org/wiki/Free_market>`__


See also :doc:`/blog/2020/0816`.
