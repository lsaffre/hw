=============
About privacy
=============

The :term:`right to privacy` is the right to have secrets.

.. glossary::

  right to privacy

    The right to have secrets, the right to be left alone. The right to have
    some information about you that is protected from public scrutiny.

  privacy

    The state of being let alone, of having some part of your life hidden to
    certain other people.


Privacy means, unlike loneliness, that you actually chose it, that you *want* to
be let alone.

Privacy is partly the opposite of community.  Other people usually want to know
"everything" about you.  At least when they are interested in you, when they
love you.  You also want to know "everything" about people you are interested
in.

Choosing privacy for a given aspect of your life does not mean that you keep it
fully secret. It just means that you do not want it to be known to *everybody*.
You want to communicate about it *only with selected* people.

"The concept of universal individual privacy is a modern concept primarily
associated with Western culture and remained virtually unknown in some cultures
until recent times. Most cultures, however, recognize the ability of individuals
to withhold certain parts of their personal information from wider society, such
as closing the door to one's home." `Wikipedia
<https://en.wikipedia.org/wiki/Privacy>`__

participate in society without having other individuals and organizations
collect information about them
