===============
About atonement
===============

The Jewish people assumed that God rewards those who obey the divine laws, and
that he punishes those who fail to obey.  If not directly, then up to several
generations later.  This is divine justice.

On the other hand, many texts of the Old Testament praise God for his mercy
(e.g. `Ps 51 <https://www.bibleserver.com/ESV/Ps51>`__, Ps 103:8-13, Ps 130).

Mercy is the opposite of justice.
How can we imagine a God who rules the whole world in justice and at the same
time, at least sometimes, forgives sins?   How can we speak about divine justice
when those who do evil don't get punished as they deserve?
Something was missing.
Until Jesus came.

The answer to this theological dilemma of justice and mercy is that Jesus died
on the cross as an *atonement* for our sins.  By his death, Jesus "pays back our debt" so
that God can be merciful without being unjust.

This is why Christians worship the cross (see :doc:`/talks/cross`) and call Jesus the
*Lamb of God*, *Redeemer* or *Saviour* (see :doc:`saviour`).

The whole idea is also called `Penal Substitution
<https://en.wikipedia.org/wiki/Penal_substitution>`__ theory, which "derives
from the idea that divine forgiveness must satisfy divine justice, that is, that
God is not willing or able to simply forgive sin without first requiring a
satisfaction for it."

A hypertrophy of the Penal Substitution theory is the question
:doc:`/talks/my_sins`.

Some Bible verses about atonement:

- "Christ Jesus, whom God put forward as a sacrifice of atonement by his blood"
  (Romans 3:25)

- "For our paschal lamb, Christ, has been sacrificed." (1 Corinthians 5:7)

- "Christ loved us and gave himself up for us, a fragrant offering and sacrifice
  to God" (Ephesians 5:2)

- "But as it is, he has appeared once for all at the end of the age to remove sin
  by the sacrifice of himself" (Hebrews 9:26)

- "Christ had offered for all time a single sacrifice for sins" (Hebrews 10:12)

- "Jesus Christ the righteous, and he is the atoning sacrifice for our sins" (1
  John 2:2)

- "In this is love, not that we loved God but that he loved us and sent his Son to
  be the atoning sacrifice for our sins. (1 John 4:10)
