==========
Friendship
==========

Let us learn to appreciate our friends.
Let us care for each other.

Sometimes it is me who guides my friends, sometimes I need their help
for finding the right way.

Your friends help you to become a better human.


What is a good friend?

A good friend helps you as much as possible when you need help.

A good friend does not stop being your friend if you behave wrongly.

A good friend does not lie to you.

A good friend does not remain quiet if you need to learn something
unpleasant.
