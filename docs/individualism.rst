===================
About individualism
===================

.. glossary::

  individualism

    The belief that :term:`faith` is everybody's private business and should not
    get regulated in any way.

    The belief that an individual human can grasp the fullness of life.

    Causes a lack of motivation to discuss about :term:`faith questions <faith
    question>` with people who disagree.

    When you over-estimate your personal :term:`beliefs <belief>` in comparison
    with traditional answers to :term:`faith questions <faith question>`.

  relativism

    The belief that no :term:`faith statement` is worth to fight for. 
