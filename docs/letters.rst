================
Letters from God
================

Don't misunderstand the heading. I don't claim to have a direct mail connection
with God. The texts in this section are just another style of writing. I
sometimes *imagine* what God would tell us if He would write letters...

.. toctree::
    :maxdepth: 1

    letter
    letter2
    letter3
    letter4
    letter5
