=================
A letter from God
=================

My dear children!

I love you. You've done quite some good work so far. But now let's get to the
next level. Here is my draft for your next tasks in my big plan of running this
world.

You just entered the digital era. You can split huge amounts of information into
series of zeroes and ones, store them on some device, then restore and
reassemble them on another device. Congratulations. A great method for
remembering facts. Anyway your brains have never been very good at this purpose.
You might start using them for more important things.
But keep in mind that there might come times when you need to get along without any computers.
I don't promise that you will have these machines at your disposal till the end of times.

Remember 1811 when you created your first limited liability corporations?
[#llc]_
These corporations are a great medium for ideas that survive the lifetime of individual humans.
You know that they are spirits, don't you? A spirit is an idea that exists outside of a human brain.
These spirits are incredibly powerful and virtually immortal.
Yes, it's fun to watch them do their jobs.
They bring clean water and electricity into your houses. They bring bananas to tables in regions where no banana can grow.
They produce cars, telephones, computers, music, movies.
Feel free to enjoy.

Some of you start to realize that these spirits started doing their jobs more
efficiently than what would be useful. And that they are doing it
*independently*. You are the sorcerer apprentice realizing that the spirits you
called are out of control.

Yes, please realize this fully. And then don't panic. You just need to review
some of your laws.  Keep in mind that you created these laws and that it's you
and nobody else who will change them.

Just to give you some ideas: review your copyright laws, limit the power of
limited liability corporations, share your natural resources among all.

Yes, it will take you some time. But don't fear, I am with you.

Your Father in Heaven


.. rubric:: Footnotes

.. [#llc]  The world's first modern limited liability law was enacted by the state of New York in 1811. The modern world is built on two centuries of industrialisation. Much of that was built by equity finance. Which is built on limited liability.
  Limited liability corporations are the key to industrial capitalism.
  `economist.com <https://www.economist.com/finance-and-economics/1999/12/23/the-key-to-industrial-capitalism-limited-liability>`__,
  `Wikipedia <https://en.wikipedia.org/wiki/Limited_liability>`_
