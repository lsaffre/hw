===========
About faith
===========

Every human has a :term:`faith`. Faith is the result of what a human has
:ref:`learned <about_learning>` during their personal history.

.. contents::
   :depth: 1
   :local:


What is faith?
==============

.. glossary::

  faith

    The individual set of :term:`convictions <conviction>` of a human. The sum
    of beliefs you rely on, the result of what you have learned during your
    personal history.

    Also called "individual knowledge".

Faith is our connection to the :term:`invisible world`.  It lies at least partly
beyond rational considerations and decisions. It is "stored" in your
:term:`heart` (obviously not in the medical meaning of the word):

Religion is about right relationship to :term:`reality`, not about the
:term:`supernatural`. -- Michael Dowd (via :doc:`/blog/2020/0905`)

"Faith has nothing to do with beliefs; it's about trust. Trust that reality is
okay just as it is. Reality is not too tough for me; I was made for reality!
This trust gives meaning to our lives.  For me to look into the awe-filling
fullness of life and pronounce the name "God" means a commitment of my life to
reality-based living. That's why I say, Reality is my God, evidence is my
scripture, and integrity (living in right relationship with reality and helping
others do the same) is my religion. Life as it really is, with all its warts and
glory, this is the primary object of my trust, my loyalty, my love." -- Michael
Dowd (via :doc:`/blog/2020/0905`).


"I have one life and one chance to make it count for something ... I'm free to
choose what that something is, and the something I've chosen is my faith. Now,
my faith goes beyond theology and religion and requires considerable work and
effort. My faith demands ---this is not optional--- my faith demands that I do
whatever I can, wherever I am, whenever I can, for as long as I can with
whatever I have to try to make a difference." --- Jimmy Carter


.. glossary::

  heart

    The parts of our body where our :term:`faith` is stored.

    This is your brain and other parts of your body, including very unconscious
    parts.


"In the depths of his conscience, man detects a law which he does not impose
upon himself, but which holds him to obedience. Always summoning him to love
good and avoid evil, the voice of conscience when necessary speaks to his heart:
do this, shun that. For man has in his heart a law written by God; to obey it is
the very dignity of man; according to it he will be judged." -- `Gaudium et spes
<https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19651207_gaudium-et-spes_en.html>`__,
no 16.

Your :term:`heart` is the physical storage device used by your soul during your
lifetime.

Your faith is an integral part of your identity. You tend to believe very
strongly in what your faith tells you to believe.

Your faith is a rather stable and immutable part of your personality. It is
influenced by your body, your :term:`temperament`, your metabolism, your
culture, your education, your personal history.

  "[H]uman beings are notoriously difficult to sway from their beliefs" -- Bob
  Yirka via `phys.org
  <https://phys.org/news/2021-11-social-scientists-replication-capable-beliefs.html>`__

On the other hand every faith can evolve, grow or eventually experience
revolutionary changes ("conversions") as long as we live. Faith is never
finished as long as you live. Faith is the result of a life-long learning
process.

Faith becomes more and more fixed with age. This effect is natural and
unavoidable and similar to why children can learn a language more easily than
adults.

Faith is our personal simplified picture of the world. Every faith --of course--
reduces :term:`reality`, because reality is more complex than any :term:`heart`
can store. The faith of Albert Einstein was probably more "elaborate" than the
faith of my neighbour here in our village, because Einstein's education was more
elaborate, but my neighbour's faith might me more useful in many a situation of
daily life.


.. glossary::

  faith culture

    A :term:`culture` based on a given set of
    answers to a set of :term:`faith questions <faith question>`.  This can be a
    formal :term:`religion` or a less formal culture.

  faith question

    A question for which there is no scientifically valid answer and which
    therefore can have controversial answers.

  faith statement

    A reproducible answer to a :term:`faith question`. Where "reproducible"
    means that has been formulated in some way and is used for identification.

.. What is the relation between :term:`faith culture` and :term:`world view`?


Faith does not mean that you blindly assume as true a series of answers to a
series of `faith questions <faith question>`__ that do not reflect your personal
thinking, experience and conviction. Such an attitude would be worse than
useless: it would be harmful, because it would destroy your trust in God. Faith
means to draw the right conclusions from certain experiences. You cannot prove
these conclusions and you cannot get them from others.  You must live them
yourself.\ [#Albrecht]_

Faith is not limited to humans. Wild goats living in a mountain region where
thunderstorms can be very violent, learn quickly that certain constellations of
clouds, wind, smells and noises indicate a storm coming by. A wild goat, when it
feels that a thunderstorm is arriving, will focus on finding a shelter. This
knowledge about how to predict a thunderstorm is stored in their brain. It is
faith.  It is the same kind of knowledge as the belief that showing
homosexuality in public is okay, or that you should never spend more money than
you have, or that it is okay to use a cheat sheet in a school exam, or that Pope
Francis is a good leader for Church.

Faith is what makes your :term:`inner voice` talk.

.. glossary::

  inner voice

    The "voice" within you that tells you intuitively how to decide in a given
    situation.

Your inner voice might be wrong because you are not perfect. That's why in case
of conflict between your inner voice and that of other people, I would
--theoretically-- prefer to rather "die" (give in) than hurt these other people.
Of course that's just theory; reality shows that we can fail to give in when we
should, or that we hurt other people because we didn't care enough.


Faith versus religion
=====================

Don't mix up :term:`faith` and :term:`religion`.
Faith is an *individual* characteristic of a human,
while religion is a collective :term:`teaching` cultivated by a community.

Faith is not limited to "religious" beliefs and convictions. Steve Lohr
describes the University of Chicago as "long the high church of free-market
absolutism, whose ideology has guided antitrust court decisions for years" (in
`Paul Romer: Once tech's favorite economist, now a thorn in its side
<https://www.moneycontrol.com/news/trends/features-2/paul-romer-once-techs-favorite-economist-now-a-thorn-in-its-side-6924421.html/amp>`__)

:term:`Kalda20211031` gives many examples (in Estonian) to explain that the
so-called "least religious nation in the world" is nothing but a religion.

I'd bet that it would be easy to find two humans C1 and C2 who call themselves
"Christian" and two humans A1 and A2 who call themselves "atheist", and then
observe that the faith of C1 is more close to the faith of A1 than the faith of
C2.

There are no two absolutely identical faith's on Earth. No faith is absolutely
"evil" (against :term:`God's plan`) or absolutely :term:`good` (in harmony with
God's plan).



.. todo figure representing different faith cultures (religious and non-religious)

Four classes of "faithful"
==========================

As a naive :term:`Roman Catholic` I classify humans into four classes:

.. glossary::

  Roman Catholic

    Those who adhere to the teachings of the :term:`Catholic Church`.

  Non-Catholic Christian

    Those who use the :term:`Bible` as their :term:`Holy Scripture` but don't
    adhere to the :term:`Catholic Church`.

  Non-Christian

    Those who refuse to use the :term:`Bible` as their :term:`Holy Scripture`.

  non-religious

    Those who refuse to use any :term:`Holy Scripture` for cultivating their
    faith.




"Those who don't follow any religion can be more eager in worshipping their
image of me than those who follow some religion." (God in :doc:`letter5`)

"I don't believe in God, I live God" (`Maurice Zundel
<https://en.wikipedia.org/wiki/Maurice_Zundel>`__)

.. _about_learning:

About learning
==============

Learning happens by perceiving and classifying information messages:

- real-life experiences : a burning candle is hot; a gold fish dies when it
  jumps out of the aquarium and nobody puts it back; father gets angry when you
  disobey

- information perceived directly from other humans during a meeting (verbal and
  non-verbal messages)

- information produced by other humans and received via a media canal
  everything I hear, read or watch in books, radio, TV or Internet.

- ideas: you perceive some problem, think about it and then decide how to react



.. rubric:: Footnotes

.. [#Albrecht] Adapted from *Oliver Albrecht, Lebensthemen, Grundkurs biblische
   Theologie, 2013 Deutsche Bibelgesellschaft*, p. 224)
