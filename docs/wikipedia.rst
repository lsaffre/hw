=========
Wikipedia
=========

Beautiful pictures should be available to everybody
===================================================

Consider sharing your pictures on Wikimedia Commons before sharing a link on
Facebook.

How to get started
==================

- Click on `Create Account
  <https://commons.wikimedia.org/w/index.php?title=Special:CreateAccount&returnto=Main+Page>`__
  to create a free user account on Wikimedia Commons.

- Log in and click on the `Upload file
  <https://commons.wikimedia.org/wiki/Special:UploadWizard>`_ link in the sidebar.

- Unlike with Facebook you will need to give a name to each file. That's the
  biggest obstacles for beginners. The name can be in your native language. You
  must also give a description in English. This description can be changed
  later. The name and the description are required to avoid Wikimedia getting
  spammed with gigabytes of useless content. Don't give up here too quickly.

- Read the docs:

  - French: `Importer un fichier <https://fr.wikipedia.org/wiki/Aide:Importer_un_fichier>`__,
    `Droits d'auteur <https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Droit_d%27auteur>`__,
    `Accueil <https://fr.wikipedia.org/wiki/Aide:Accueil>`__

Why
===

Sharing beautiful pictures on Facebook is so easy! And it gives me immediate
positive feedback of many people! Why should I choose a method that is a little
bit less comfortable?

- You will get more than a number of "likes". You will get another type of
  feedback, less immediate but more valuable: other people are going to actually
  use your picture, include it in a Wikipedia article, classify it into a category
  so that people can find it back even in five years, even in fifty years.

- Are you aware that Facebook *does* have a price? Facebook is incredibly happy
  about your activity because every picture uploaded to their servers increases
  their capital. With every upload you give FB the irrevocable and perpetual
  right to use your picture. If your picture gets so famous that commercial
  publishers want to reproduce it, they are more likely to ask Facebook, not
  you, for the permission to do so.

You might say that anyway money isn't your motivation for sharing your pictures.
Thank you for this noble attitude. But in that case, giving your pictures to FB
isn't what you actually want in your heart.


Common questions
================

Les photos restent bien notre propriété donc?

Oui, chaque photo que tu ajoutes à Wikipédia reste ta propriété. Tu donnes à
tout le monde un meme droit de l'utiliser. La Wikimedia Foundation (la personne
juridique derrière Wikipédia) utilise alors ce droit pour publier ta photo sur
Wikipédia. Avec FB aussi tu reste le propriétaire, mais la licence est
différente (tu donnes à FB le droit d'utilser ta photo, mais pas à tout le
monde). (Disclaimer: je ne suis pas un expert en droit et mes explications ne
remplacent pas ce qui est écrit dans la licence que tu choisiras).

Background
==========


Wikipedia is for me a sign of hope that we will manage to govern this planet,
that a human world is possible, that humans rule, not money.

"Something about Wikipedia is working better than the rest of the internet, and
we can learn from it."
"[T]he content of a popular Wikipedia page is actually the most reliable form of
information ever created. Think about it—a peer-reviewed journal article is
reviewed by three experts (who may or may not actually check every detail), and
then is set in stone. The contents of a popular Wikipedia page might be reviewed
by thousands of people. If something changes, it is updated."
(`Wikipedia: The Most Reliable Source on the Internet?
<https://www.pcmag.com/news/wikipedia-the-most-reliable-source-on-the-internet>`__,
S.C. Stuart for pcmag.com in June 2021)
