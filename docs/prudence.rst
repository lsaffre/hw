======================
About prudence
======================


.. glossary::

  parrhesia

    A style of speech where you clearly criticise while also asking forgiveness
    for doing so.  Often translated as boldness.

    For example Acts 4:13 says "Now when they saw the boldness [την παρρησίαν]
    of Peter and John and realized that they were uneducated and ordinary men,
    they were amazed and recognized them as companions of Jesus." (via
    `Wikipedia <https://en.wikipedia.org/wiki/Parrhesia>`__)

  prudence

    The virtue of being prudent, careful.

  bull in a china shop

    "One who is aggressively reckless and clumsy in a situation that requires
    delicacy and care." (`thefreedictionary.com
    <https://idioms.thefreedictionary.com/bull+in+a+china+shop>`__) or
    "a person who breaks things or who often makes mistakes or causes damage in
    situations that require careful thinking or behavior" (`Merriam-Webster
    <https://www.merriam-webster.com/dictionary/bull%20in%20a%20china%20shop>`__)
