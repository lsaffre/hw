===================
About homosexuality
===================

.. contents::
   :depth: 1
   :local:

I find the mere idea of having erotic acts with another man disgusting. But I
also find `Escargots de Bourgogne
<https://fr.wikipedia.org/wiki/Escargots_de_Bourgogne>`__ disgusting. And if I
would be forced to choose between eating a portion of *Escargots* and sharing my
bed with a homosexual man for a night, I would choose the latter.

Humanity had no scientific understanding about homosexuality until some
generations ago. People were afraid of this phenomenon in ancient times.  And
some parts of our :term:`Holy Scriptures <Holy Scripture>` reflect that fear.

But human science has explained homosexuality well enough. It is not more
particular than being a twin, being left-handed or having read hair.  We cannot
call it a disease.  Not even a :term:`disorder`.   Let's get rid of ancient
beliefs that have become obsolete.


When we assume that the main purpose of a human is to reproduce, then the
optimal :term:`sexual orientation` for a man is to perceive women as attractive
and for a woman to perceive men as attractive.  This optimal orientation avoids
individuals wasting energy in sexual efforts towards a partner of same sex,
which would be useless.

If you continue this thought with a politically right-wing mind, you come to the
conclusion that a healthy civilization should repress :term:`homosexuality`
because it is at best a suboptimal development and at worst a sin against divine
laws, which subvert and potentially harm human civilization.

But things get more complex when you consider that :doc:`the meaning of life
</meaning>` is more than having children, and that increasing our population
(the number of individual humans living on earth) is not currently a primary
concern of our species, and that probably even the opposite might be true: God
might want more people to be homosexual in order to prevent overpopulation.


Same-sex marriage
=================

The :term:`Catholic Church` has no problem with marrying a :term:`couple` who
know that they won't have any biological children together, e.g. for health
reasons. Such a couple may have other kinds of "children": they can adopt
orphaned children, they can work together on spiritual "children" (a project or
a vision) or invest into material "children" (a house or a car).

This shows that a marriage makes sense also without sex and without biological
children.

So I see no problem to allow for same-sex marriage as well. I can imagine that
the :term:`Church` will one day recognize same-sex marriage as a variant that
deserves the same benediction as any other :term:`marriage`.  Of course we are
not yet there.

There are :term:`homosexual <homosexuality>` people who agree to form a
:term:`couple` and to adopt and raise children in an :term:`oikos`. I think that
there is nothing wrong with this. It is great and deserves our support and
benediction. Because children having a homosexual couple as parents are still
far better off than children who grow up with only one parent. Homosexual people
should have a right to form a :term:`civil union`, to marry and to adopt and
raise children.

There is a theory that homosexuality might be "nature's population control so
that we don't overpopulate."  `Richard Dawkins
<https://en.wikipedia.org/wiki/Richard_Dawkins>`__ responded to this idea on
`Darwin Day 2015
<https://www.richarddawkins.net/2015/03/darwin-day-2015-questions-is-homosexuality-natures-population-control-4/>`__.
He explains that evolution "doesn't work like this" because it works on the
level of the genes of an individual. He concludes with a --for a Britain--
surprisingly clear verdict: "So I am afraid that it couldn't be true that
homosexuality is an adaptation to keep the population size down."

- https://www.exposingtruth.com/homosexuality-is-natural/
- https://liberapedia.wikia.org/wiki/Homosexuality_as_a_natural_population_control

There are still people who insist that the term :term:`marriage` can be used
only for couples of a man and a woman. `N.T. Wright on gay marriage
<https://www.firstthings.com/blogs/firstthoughts/2014/06/n-t-wrights-argument-against-same-sex-marriage>`__
confirms to me that those who disagree with my definition of :term:`marriage`
won't change their mind during their lifetime.


See also
========

- :doc:`family`
- :doc:`/talks/clear`
- :doc:`/blog/2021/0317`


TODO
====

- https://de.wikipedia.org/wiki/Bibeltexte_zur_Homosexualit%C3%A4t

- "When it comes to homosexuality, many Christians are being stupid and unfair
  to both Pope Francis and Saint Paul." -- `Homosexuality, Francis & the Bible
  <https://www.patheos.com/blogs/messyinspirations/2020/10/homosexuality-francis-the-bible/>`__, Patheos, October 2020.

- "Homophobia forces many Christians to disproportionately apply ethical gravity
  to two obscure Greek terms while neglecting other biblical vices." -- `Ethical
  Consistency & Homosexuality
  <https://www.patheos.com/blogs/messyinspirations/2020/11/ethical-consistency-homosexuality/>`__,
  Patheos, November 2020.

- `Quora: Could my horse be gay? <https://www.quora.com/Could-my-horse-be-gay>`__

- http://blog.thomashieke.de/blog/bibel-und-homosexualitat/

- `Thérapies de conversion : « Aucune orientation sexuelle n’empêche de suivre le Christ »
  <https://www.lavie.fr/actualite/societe/therapies-de-conversion-aucune-orientation-sexuelle-nempeche-de-suivre-le-christ-78100.php>`__

- `When Holding Hands Isn't So Simple
  <https://www.psychologytoday.com/intl/blog/inclusive-insight/202206/when-holding-hands-isnt-so-simple>`__

- Parts of this page need revision after :doc:`/blog/2020/1116`


Did you know?
=============

- The gay Catholic journalist and author `Michael O'Loughlins
  <https://www.mikeoloughlin.com/>`__ received the Pope's blessing per letter in
  October 2021. (Source: `Papst Franziskus segnet per Brief homosexuellen
  US-Katholiken
  <https://www.katholisch.de/artikel/31991-papst-franziskus-segnet-per-brief-homosexuellen-us-katholiken>`__)

- `Nadezhda Filaretovna von Meck
  <https://en.wikipedia.org/wiki/Nadezhda_von_Meck>`__ is known for her artistic
  relationship with `Pyotr Ilyich Tchaikovsky
  <https://en.wikipedia.org/wiki/Pyotr_Ilyich_Tchaikovsky>`__, who was
  homosexual.

- `Robert Baden-Powell
  <https://en.wikipedia.org/wiki/Robert_Baden-Powell,_1st_Baron_Baden-Powell>`__
  was "gay and repressed it. Men were beautiful to him but he was a romantic,
  not a practising, homosexual." (Tim Jeal, via `bbc.com
  <https://www.bbc.com/news/uk-england-dorset-53007902>`__


Roman-Catholic church teachings about homosexuality
===================================================

2004-04-02 The `Compendium of the social doctrine of the Church
<https://www.vatican.va/roman_curia/pontifical_councils/justpeace/documents/rc_pc_justpeace_doc_20060526_compendio-dott-soc_en.html>`__
says (simplified excerpts):

  227. De facto unions are based on a false conception of an individual's freedom
  to choose and on a privatistic vision of marriage and family. Marriage is not a
  simple agreement to live together but a relationship with a social dimension
  that is unique with regard to all other relationships, since the family is the
  principal instrument for making each person grow in an integral manner and
  integrating him positively into social life.

  Making "de facto unions" legally equivalent to the family would discredit the
  model of the family, which cannot be brought about in a precarious relationship
  between persons but only in a permanent union originating in marriage, that is,
  in a covenant between one man and one women, founded on the mutual and free
  choice that entails full conjugal communion oriented towards procreation.

  228. Connected with de facto unions is the problem concerning (...) legal
  recognition of unions between homosexual persons. Only an anthropology
  corresponding to the full truth of the human person can give an appropriate
  response to this problem (...). The light of such anthropology reveals “how
  incongruous is the demand to accord ‘marital' status to unions between persons
  of the same sex. It is opposed, first of all, by the objective impossibility
  of making the partnership fruitful through the transmission of life according
  to the plan inscribed by God in the very structure of the human being. Another
  obstacle is the absence of the conditions for that interpersonal
  complementarity between male and female willed by the Creator at both the
  physical-biological and the eminently psychological levels. It is only in the
  union of two sexually different persons that the individual can achieve
  perfection in a synthesis of unity and mutual psychophysical completion”.

  Homosexual persons are to be fully respected in their human dignity and
  encouraged to follow God's plan with particular attention in the exercise of
  chastity. This duty calling for respect does not justify the legitimization of
  behaviour that is not consistent with moral law, even less does it justify the
  recognition of a right to marriage between persons of the same sex and its
  being considered equivalent to the family.

2016-03-19 `Amoris laetitia
<https://www.vatican.va/content/francesco/en/apost_exhortations/documents/papa-francesco_esortazione-ap_20160319_amoris-laetitia.html>`__
says:

  "[E]very person, regardless of sexual orientation, ought to be respected in
  his or her dignity and treated with consideration, while ‘every sign of unjust
  discrimination’ is to be carefully avoided, particularly any form of
  aggression and violence. [Their] families should be given respectful pastoral
  guidance, so that those who manifest a homosexual orientation can receive the
  assistance they need to understand and fully carry out God's will in their
  lives."

2021-02-22 The `Responsum of the Congregation for the Doctrine of the Faith to a
dubium regarding the blessing of the unions of persons of the same sex
<https://www.vatican.va/roman_curia/congregations/cfaith/documents/rc_con_cfaith_doc_20210222_responsum-dubium-unioni_en.html>`__

  The declaration of the unlawfulness of blessings of unions between persons of
  the same sex is not (...) a form of unjust discrimination, but rather a
  reminder of the truth of the liturgical rite and of the very nature of the
  sacramentals, as the Church understands them.
