=======
Saviour
=======

Jesus is called **Saviour** because he saves us. And *from what* does he save
us? A traditional answer is "from our sins". But I'd like to give some
explanation here because this formulation can get misunderstood.

You might say "He did't do a good job, then, as we continue to repeat that we
sinners".

First of all, note that it says "He *does* save us", not "He *did* save us".
Jesus is eternal. The process of salvation is a constant *work in progress* in
every single human.

Then I'd add that our traditional formulation is maybe a bit too abbreviated.
Jesus doesn't save us from our sins by removing them. He doesn't even say that
we must *get rid* of them. He saves us from *the Jewish idea about sins* itself,
i.e. from the idea that *not doing them* is a condition for making it into
:term:`Heaven`.

This idea was a serious theological problem of the :term:`Old Testament`. That's
why you shouldn't dive too deep into the Old Testament without your torch, i.e.
you must read it through the light of the :term:`New Testament`.

Jesus came to save the world from this idea. This is illustrated in the story
where he heals a man born blind `John 9:1-18
<https://www.bibleserver.com/ESV/John9%3A1-18>`__. Paul later confirms it: "For
I will be merciful toward their iniquities, and I will remember their sins no
more." (`Hebrews 8:12 <https://www.bibleserver.com/ESV/Hebrews8%3A12>`_) and
"There is therefore now no condemnation for those who are in Christ Jesus."
(`Romans 8:1 <https://www.bibleserver.com/ESV/Romans8%3A1>`_).

This idea is one of the explanations why Christians worship the cross (see
:doc:`/talks/cross`).

A hypertrophy of this idea is :term:`saviorism`.

.. glossary::

  saviorism

    A set of Christian teachings that hypertrophies the penal
    substitution theory (see :doc:`/talks/cross`).

Saviorism summarizes Christian faith as follows:

  Humans are sinners. We all deserve :term:`Hell`. But Jesus came to save us.
  Those who give their life to Jesus will go to Heaven. Consequently we who
  follow Jesus have the holy duty to convince others to follow Jesus because
  otherwise they will be going to Hell.

Saviorism is based on the belief that you are in :term:`Hell` because you
*deserve* it. Seen like this, saviorism is the opposite of the Gospel, which
says that we *are* saved through Jesus and that the :term:`Kingdom of God` is
already among us.


Some Bible verses about this:

- "Christ Jesus, whom God put forward as a sacrifice of atonement by his blood"
  (Romans 3:25)

- "For our paschal lamb, Christ, has been sacrificed." (1 Corinthians 5:7)

- "Christ loved us and gave himself up for us, a fragrant offering and sacrifice
  to God" (Ephesians 5:2)

- "But as it is, he has appeared once for all at the end of the age to remove sin
  by the sacrifice of himself" (Hebrews 9:26)

- "Christ had offered for all time a single sacrifice for sins" (Hebrews 10:12)

- "Jesus Christ the righteous, and he is the atoning sacrifice for our sins" (1
  John 2:2)

- "In this is love, not that we loved God but that he loved us and sent his Son to
  be the atoning sacrifice for our sins. (1 John 4:10)
