========================
About the United Nations
========================

.. glossary::

  UN
    United Nations

    See `Wikipedia <https://en.wikipedia.org/wiki/United_Nations>`__.

  UDHR

    The Universal Declaration of Human Rights, adopted in Paris on 10 December
    1948.

    `Text of the declaration
    <https://www.un.org/en/about-us/universal-declaration-of-human-rights>`__.
    See `Wikipedia
    <https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights>`__.
