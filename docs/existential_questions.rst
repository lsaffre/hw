=====================
Existential questions
=====================

.. glossary::

  existential question

    A question about the essence of my existence.

An existential crisis is a moment at which an individual questions the very
foundations of his or her life: whether his or her life has any meaning, purpose
or value (Ron Kolinie on Quora).

The most basic existential question is probably "What is the meaning of life?"
(see :doc:`meaning`).  There are many "derived" questions.  What they all have
in common is that there is no scientific answer. They are *out of the scope* of
science; science *does not even try* to answer them.

The fact that there is no scientific answer does not make them less important.

Here is my personal collection of existential questions (inspired by `52
Existential Questions – Deep, thought-provoking questions.
<https://www.mantelligence.com/existential-questions/>`__, `What are existential
questions? <https://www.quora.com/What-are-existential-questions>`__ and
others).


What are the important things in life?
How to live in peace with myself and the others?

What can make me fulfilled?
What does it mean to live a good, fulfilled life?
What protects me from terribleness?

Is there a life after death?
What happens to me when I die?
Am I an eternal soul in a temporal human body?

Will I pass a trial that decides whether I go to "Hell" or to "Heaven"?
How can I get into Heaven?
What protects me from terribleness?

What is this longing I have?
Why does life seem somewhat tragic somehow?

Is there a God?
Is our thinking controlled by some metaphysical supernatural power?

Why do I get out of bed in the morning?
What is the purpose of my life?
Is my purpose written for me or do I have the power to change it?

What is a life worth living?
Are some creatures more entitled to life than others?
Why is it okay to kill animals and plants but not okay to kill other humans?
Are humans the most advanced beings in the universe?
Why have we not been able to find other life forms?

Is my sense of right and wrong innate?
What part of it has been cultivated by my parents, friends and society?

Does history have a direction?
Is life on Earth evolving towards a next level of existence?
