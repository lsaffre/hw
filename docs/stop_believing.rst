================================
Stop believing that you know God
================================

One of the greatest features of the Christian religion is that it not based on a
*book* but on a *person*, the person of :term:`Jesus Christ`.  This person is
more than historical. If our faith would rely solely on the historically proven
facts about Jesus from Nazareth or the Bible as our :term:`Holy Scripture`, our
faith would be simplistic.

This feature has some important consequences.

A young woman from Latvia wrote after a week in a "Shalom" community:

  Jesus told to his disciples: "By this all men will know that you are my
  disciples, if you have love for one another" (John 13:35). Jesus did not say
  that others will know that we are His disciples by serving, praying or even
  gifts of Holy Spirit, but by love for each other. This seems to be some sort
  of mystery for me which I feel I need to discover more and more in my life.
  But there are no better witness for this broken world as the love God is
  giving us to share among each other. -- Olga from Latvia, `Jesus is worthy...
  And it is worthy to surrender your life to Him
  <https://comshalom.org/en/jesus-is-worthy-and-it-is-worthy-to-surrender-your-life-to-him/>`__.


In his article `What Lies About God Do You Believe?
<https://www.patheos.com/blogs/freelancechristianity/what-lies-about-god-do-you-believe/>`__,
Vance Morgan reports some verbal battles between :term:`progressive <progressive
faith>` and :term:`conservative <conservative faith>` Christians.  "Sacred
waters can turn into land mines whenever [we] believe that we understand what is
sacred and what is not, [and] think we know where to draw sharp boundaries
between the two."  I think: Yes, oh my God, will there ever be peace between
those two camps!? Morgan answers in his conclusion: "The more my faith evolves,
the more it becomes a matter of orthopraxy (right action) than of orthodoxy
(right belief). What if that is what Jesus intended all along?"

This page started as a copy of :doc:`/blog/2020/0914`.
