==========================
The future of Christianity
==========================

I believe that Christian faith has the potential of evolving into a universally
acceptable :term:`religion`. Which does not mean that every human should talk,
think, pray and act in the same ways as today's Christians do. It rather
requires clarification about some historically grown misconceptions about
Christian faith, and a reconciliation at theological level between all religions
of the world.  I believe that :term:`biblicism` and :term:`comminorism` are
more disturbing than useful with this regard.

The future world population needs a "world government", an institution carried
by all peoples, and institution with given power, which "rules" in "peace" over
the :term:`visible world`.

Will the United Nations be this world government?
Or something completely different?
Will it be a democratic structure at all?

We do not know the answer.
Maybe the answer is there, right in front of our door, but hidden.
Humanity is not yet ready for the answer.

But one thing seems clear to me:  world peace is not only required, it is
*inevitable*. It *will* come because it is God's plan.  It depends on us whether
mankind will be part of this plan.


Governing the earth in a peaceful way is one of the big challenges of
humanity.  Humans felt it already thousands of years ago when they
wrote texts like "and God said unto them, Be fruitful, and multiply,
and replenish the earth, and subdue it: and have dominion over the
fish of the sea, and over the fowl of the air, and over every living
thing that moveth upon the earth." (`Gen 1:28
<http://www.kingjamesbibleonline.org/Genesis-1-28/>`_).

While most humans agree more or less with the idea that it is our
vocation and duty to rule the world, the question on *how* to do it
has caused quite some discussion since then.

See also :doc:`democracy`.
