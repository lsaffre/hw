============
About family
============

On this website I use an unconventional definition of the word :term:`family`.
Many existing documents still use the word :term:`family` where they actually
mean an :term:`oikos`. I suggest the following definitions.



.. glossary::

  family

    A group of humans who have a biological relationship: parents and their
    children. They do not necessarily live in a same :term:`oikos`.

  oikos

    A group of humans who live together in a same household, forming an
    economical entity that provides a secure environment where its members can
    rest, where children can grow up, sick people can recover and old people can
    die.

    The plural of "oikos" is "oikoi". Also called "life groups".



Most human civilizations use the approach of growing up their children in
:term:`oikoi <oikos>`. This approach is probably optimal for humans because we
have a relatively long childhood; humans need about 15 years before they are
biologically able to reproduce, and almost twenty years before they are socially
able to live on their own and form themselves a family.  An oikos is the best
environment for humans to learn social rules, develop their emotional skills and
cultivate their individual faith. That's why providing community support to
oikoi is a basic part of most human cultures.

National law systems need to define what exactly is to be considered an
:term:`oikos` and what not. Saying that the only valid form of oikos is "a man,
a woman and their children" is obviously not enough as definition. What happens
when one of the parents died or changed their mind and formed a new
:term:`couple` with another partner? Or when one of the partners turns out to be
(secretly) part of several oikoi at once? What would happen with the children of
a divorced couple? How to handle existing cases of patchwork oikoi, polygamic
oikoi and homosexual oikoi?



.. glossary::
  couple

    An exclusive union of two humans who decide to build an :term:`oikos` as
    partners of same rank.

    Two partners having the same rank is a fundamental difference to most other
    forms of organizations.

    A same partner cannot be member of multiple couples at the same time.

  civil union

    The state of being a legally confirmed :term:`couple`, which includes legal
    community support to protect their :term:`oikos` regarding heritage,
    privacy, common belongings and other things.

    The partners decide to hold together as a :term:`couple` and receive in
    turn certain legal privileges to support that decision.

  divorce

    The legally recognized end of a :term:`civil union`.

  marriage

    A life-long :term:`couple`, i.e. a couple in which both partners engage to
    remain a couple until death separates them.

  to marry

    To start a :term:`marriage`.

  celibate

    A life-long decision of being unmarried and living in :term:`sexual
    abstinence`.

    (Definition inspired by `Wikipedia
    <https://en.wikipedia.org/wiki/Celibacy>`__, `Albanian sworn virgins
    <https://en.wikipedia.org/wiki/Albanian_sworn_virgins>`__)

  sexual abstinence

    The fact of not entering into sexual relation with another human.



Oikoi in the Bible
=====================

The Bible contains texts that have been written during a period of several
thousand years and therefore documents different :term:`law systems <law
system>` regarding oikoi, including polygamic ones.

- https://www.biblestudytools.com/topical-verses/polygamy-in-the-bible/
- https://medium.com/belover/christianitys-polygamy-problem-9bca6b4ada8e

Most systems documented in the Bible are `patriarchal
<https://en.wikipedia.org/wiki/Patriarchy>`__.

Jesus didn't care much about his :term:`family`. Some quotes:

  If anyone comes to me and
  does not hate his own father and mother and wife and children and brothers and
  sisters, yes, and even his own life, he cannot be my disciple." (`Luke 14:26
  <https://www.bibleserver.com/ESV/Luke14%3A26>`__

  But he replied to the man who told him, "Who is my mother, and who are my
  brothers?" And stretching out his hand toward his disciples, he said, "Here are
  my mother and my brothers! For whoever does the will of my Father in heaven is
  my brother and sister and mother."
  (`Matthew 12:48-50 <https://www.bibleserver.com/ESV/Matthew12%3A48-50>`_)

These observations make me say that :doc:`Christian values </christian_values>`
are not about what is a valid marriage and what not, they are about how to find
peace on Earth given the fact that humanity consists of many cultures. This is
why we cannot say that same-sex marriage is against "Christian values".

Every religious organization has its clear definition about what a family or a
marriage is.  For example the Catholic Church defines them quite clearly in its
teachings and doctrines. See `Marriage in the Catholic Church
<https://en.wikipedia.org/wiki/Marriage_in_the_Catholic_Church>`__. But even the
teachings and doctrines of the Roman Catholic church --the biggest religious
organization on Earth-- cannot be considered "divine" as long as there is one
human on Earth who refuses to follow them.

Until death separates you
=========================

Two girls are talking about their love stories. One of them says "I am so lucky!
Last week I got two different marriage proposals! I just can't yet decide with
whom to start..."

Yes, national laws allow to "try again". The :term:`Roman Catholic` refuses this
idea of a second chance. This rule makes sense because when a man and a woman
have children, they share a life-long common responsibility for their children,
independently of whether they want it or not, whether they acknowledge it or
not, whether they actually manage to assume it or not. I call a marriage when
they want, acknowledge and celebrate this common responsibility. The Church
knows cases where a marriage gets recognized as "invalid" or "cancelled". But
even these cases are not "divorces", they rather recognize the fact that the
marriage never existed.

I use to say that if my own marriage would fail, I hope that I would manage to
live as a single and wouldn't do the same mistake once more.

The difference between a :term:`marriage` and a :term:`civil union` is that the
former lasts "until death separates them" (i.e. until one of the partners dies)
while the latter can be divorced.

Most national laws cannot regulate :term:`marriage` (as I define it) because
they don't allow for lifelong agreements.  Human laws should call it a
:term:`civil union`.  I find it disturbing when the word :term:`marriage` is
used in legal texts, because this makes people think that a :term:`marriage` is
the same as a :term:`civil union`.

People tend to ignore this difference.  I wonder why they make a ceremony and
call it a marriage when they are actually just saying "hooray, we agreed to live
together *for some time*, as long as it suits both of us". For me this is just a
:term:`civil union`, just a temporary legal agreement between two individuals. I
perceive it as a degradation of language when people call this a "marriage".
This wrong wording might cause us to loose the original meaning.

Yes, my definitions of marriage and family differ (slightly IMO) from those of
the Catholic Church. I suggest that marriage should not require a plan to have
children, i.e. a marriage should be allowed to be *platonic*. This definition
would allow same-sex marriage.


Message to young couples
========================

Modern cultures seem to have no problem with couples who refuse the idea of
:term:`marriage` (as I defined it) and still want to have children.

But please consider what this means to us, the other humans.

Raising a child takes almost twenty years.  That's a long time for a human. Not
many humans agree to do this job for other people's children and without being
paid for it.

A child growing up in community with only one adult is a bit like a human with
only one leg.  It is a sub-optimal education environment. Yes, there are ways to
work around such a handicap, but these solutions need more energy and money than
the optimal solution of a family.

So when you make a child and then stop living together as a family, you create
yet another sub-optimal education environment. You are shuffling off some of
your responsibility upon us, the other humans.

Living together as a couple is a challenge because having two partners at the
same rank is a fundamental difference to most other forms of organizations.

A :term:`marriage` in the Catholic Church (i.e. not just a :term:`civil union`)
is a promise to bind yourself, to hold on even in difficult times. This promise
can give you undreamed-of energy when needed. Without this promise your couple
faces a bigger danger of breaking apart before your children are adult.

That's why I ask you: If you know that you love each other and want to have a
:term:`family` together, then wait with having sex until you also agree that
this means a life-long binding. And when you agree to be bound until the end of
your life, then don't wait any longer, tell this good news to the world and get
married. If you fear the costs of a wedding ceremony, just choose a more decent
way to celebrate your decision.

.. Doing it the other way round, i.e. you play around and when "it happened", i.e.
   when she is pregnant, you start to think about whether you are ready to marry.


TODO:

- `Papst Franziskus: „Habt keine Angst vor (Ehe-) Krisen“
  <https://www.vaticannews.va/de/papst/news/2021-11/papst-franziskus-krise-ehe-familie-audienz-retrouvaille-pastoral.html>`__

- `Papst an Ehepaare: „Lebt eure Berufung intensiv“
  <https://www.vaticannews.va/de/papst/news/2021-12/papst-ehepaare-brief-familienjahr-amoris-laetitia-berufung-covid.html>`__
