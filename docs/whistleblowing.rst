====================
About whistleblowing
====================

.. glossary::

  whistleblower

    A human who decides to disclose information that is considered private by
    some :term:`corporation`.

Private corporations should be deprived of certain rights they currently benefit
from (see :doc:`stgg`).  One of these rights is the :term:`right to privacy`.

Note that :term:`public corporations <public corporation>`, unlike
:term:`private corporations <private corporation>`, may benefit from this right
for security reasons.  For example the police or army are :term:`public
corporations <public corporation>`.

Note that *refusing to private corporations the right to privacy* does not mean
*privacy is forbidden*.  It just means that we don't protect their privacy.
:term:`Private corporations <private corporation>` usually do have private
knowledge which they don't want to share for various reasons (including the fact
that they regard it as their business secret or as a strategic decision). And
they may expect from their employees and business partners to respect their
privacy, and they may legally write this as a condition in their contracts.

When one of their employees or partners shares some data they wanted to remain
secret, and when they discover it, they may terminate the relationship and even
sue the employee for contract breach.  But they cannot sue him or her for
whistleblowing.

Whistleblowing becomes an act of public interest, done by an individual who
decides to disclose a secret. The whistleblower is ready to suffer by being
dismissed and getting accused of infidelity because he or she believes that this
is their duty towards humanity.

Note that *having no right to privacy* does not mean *duty to publish your
secrets*.  Perfect transparency is not possible.

Private corporations *may* have secrets, and they *may* protect them, but they
must do this themselves and at their own cost.  They must not get free support
of a public legal system.
