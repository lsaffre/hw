==============================
Intellectual property is a sin
==============================

The Synodal Church will be the first Christian institution to declare that
:term:`intellectual property` is a :term:`collective sin`.

It is a sin because it creates a social and economic system that limits the
freedom of some members, causing them harm. It causes social disorder. It causes
the stronger to become even stronger, and the weak to become even weaker. It
leads owners of proprietary software to use malicious strategies in order to
increase their power.

These ideas are not new. Already in 1985 `Richard M. Stallman
<https://en.wikipedia.org/wiki/Richard_Stallman>`__ designated intellectual
property as a seductive mirage. Another software developer,  `Eric S. Raymond
<https://en.wikipedia.org/wiki/Eric_S._Raymond>`__ did similar work. Both
remained focused on software, and both also caused :term:`controversial battles
<controversial battle>` about unrelated topics.

In 2010 Lawrence Lessig extended the idea to all forms of published content.
Both failed to consider the :term:`Church` as their ally, and the Church failed
to realize their work as important. :term:`Fratelli Tutti <FT>` started to point
at it, still without naming it.

See also :doc:`/lutsu/index`.
