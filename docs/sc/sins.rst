===========================
My teachings about sin
===========================

I suggest that we review our teachings about :term:`sin` in order to clarify
certain things and to make these teachings understandable for everybody.

Introduction: :doc:`/talks/confess`.


.. contents::
   :depth: 1
   :local:

Definitions
===========

I suggest the following definitions.

.. glossary::

  sin

    An act, statement, thought, attitude, position, belief, habit or behaviour
    that we assume to be *wrong*.

  personal sin

    A :term:`sin` carried out by an individual human.  Also called *individual* sin.

  collective sin

    A :term:`sin` carried out by a group of humans.
    Also called *social* or *institutional* sin.

  original sin

    An unavoidable :term:`sin`. Something the sinner cannot change, something we must
    accept as such and which we have to live with. Something we cannot expect
    the sinner :term:`to repent`.

You can carry out a sin without knowing it, or you can be aware of your sin
without seeing how to repent.



Sin versus crime
================

In historic texts the word :term:`sin` is sometimes mixed up with the notion of
:term:`crime`. But a crime makes sense only in :term:`common law`, formulated in
human language.  It makes no sense to speak about "a crime against the
:term:`divine law`" because we are not God.

.. glossary::

  crime

    An act that is forbidden and punishable by :term:`common law`.

  guilt

    The fact of being legally responsible for a :term:`sin`.

    Note the differences between *feeling guilty* and actually *being guilty*.

  accusation

    The fact of saying that somebody is :term:`guilty <guilt>`.



Recognizing sins
================

Only God knows whether something is a :term:`sin` or not. But in our desire to
avoid sin, we try to define rules for recognizing them. Some examples:

  A sin is when you deliberately harm somebody.

  A sin is when you turn the mirror of your heart away from God.

Classifying sins
================

The following criteria apply to both :term:`personal <personal sin>` and
:term:`collective <collective sin>` sins but can have different conclusions
depending on whether they are personal or collective.

- Which law is being offended (civil? moral? natural? divine? ...)
- Who suffers (the sinner himself? somebody else? many others?  ...)
- How severe is the problem? How big is the damage done?
- How much personal responsibility the sinner has (:term:`guilt`).

We define :term:`sin` as everything that is against God's plan. We can recognize
a sin by the fact that it harms somebody\ [#harm]_. :term:`Personal <personal
sin>` sins can be a single action, a recurring behaviour, an attitude or belief.
:term:`Collective <collective sin>` sins can be laws and teachings.

Christians dare to speak about sins because we believe that God forgives them.
This applies to our own sins as well as to those of other people. Our own sins
are the only ones we can --sometimes-- change directly, but they are the most
difficult to see. The sins of other people are theoretically "not our business",
but speaking about them is important because they can show us our own sins.

.. rubric:: Footnotes

.. [#harm] Note the difference between "to harm" and "to hurt". Your dentist can
  *hurt* you by causing a temporary pain when removing the actual cause of your pain.
