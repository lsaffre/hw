======================
Chronological overview
======================

In **September 2021** the `Bishop
<https://en.wikipedia.org/wiki/Philippe_Jean-Charles_Jourdan>`__ asked me
whether I'd agree to be the Synodal Contact Person. And I accepted
(:doc:`/blog/2021/0914`). I then called a number of people, asking them whether
they would join a team to help me with this job. This team (I call it the
:term:`sinoditiim` in Estonian) received benediction and was officially
presented to the public on 2021-10-17 in the Cathedral
(:doc:`/blog/2021/1017_1800`).

In **September and October 2021**, the first big job of the :term:`sinoditiim`
was to produce a **website** where we invited people to participate in the
consultation phase (https://sinod.katoliku.ee). Consenting on the content of
this website took us much energy. I was grateful and proud about the result.

In **November 2021** it had become clear that the :term:`sinoditiim` and I had
difficulties to dialogue because we had a number of opposing convictions. See
:doc:`/sc/camps`. I felt as if I was talking to a wall.  During a meeting on
2021-11-09 the Bishop made clear that he considered my plans for the
consultation phase as useless individual endeavour. I started to unconsciously
abandon the hope that this team would produce something useful and gradually
:doc:`gave up dialogue </talks/giving_up_a_discussion>`.

One week later I had a vision (:doc:`vision`) and started to tell people about
it. It caused controversial emotions but helped me to enter into dialogue with
non-catholic people.

In **December 2021** I had the idea of using an *incremental* method: I
published a "draft" and told people "This is what I would write to the Vatican
if I would have to write my synthesis right now. Tell me what's wrong or what's
missing."  Several people reacted irritatedly, saying "How dare you write a
synthesis before the consultation phase has finished!". But many people gave
concrete feedback, both critical and complimentary.

Between **January and March 2022**, I formed an "alternative" team and lead a
project of writing an *inter-confessional* consultation report based on my
draft. I submitted this document to the *sinoditiim* on 2022-03-31 as our report
of the consultation phase. See :doc:`report`.

Together with "my" report, the :term:`sinoditiim` received a total of 17
documents, 9 of which were meeting reports, 8 were individual responses.

In April 2022, the *sinoditiim* started the discernment phase, where our job was
to *synthesize* all reports. I had had my plans for this phase, I imagined to
finally get the other members to collaborate with me, using my methodology. But
another team member took leadership, and I did not fight back. One can say that
I `quiet quitted <https://en.wikipedia.org/wiki/Quiet_quitting>`__ the
sinoditiim during the first meeting in April.  My remaining interactions with
them were just :term:`work-to-rule`.  In :doc:`my comment <comment>`, I tried to
summarize what "we" did during this phase.

On 2022-05-27 the new leader of the *sinoditiim* had produced a first internal
draft of a 10 pages report, which reflected what they believed "good" Catholics
in Estonia would say at the Synod.  It was a selection of what they had liked in
the reports. Not a single idea expressed in the *inter-confessional* report had
made it into this synthesis. All members of the *sinoditiim* --except me--
agreed to this synthesis without discussion. I suggested to start a new
synthesis from scratch where the authors of the 17 reports would participate.
But I had to admit that time was running out. And of course a team that uses the
Waterfall approach cannot imagine to start from scratch when you are close to
the end. The Bishop then found a compromise: I would write my personal
"comment", which he would include into his letter along with the synthesis.

In June 2022 I worked on :doc:`this comment </sc/comment>`, where I concluded
that the "fact of having *failed to reach a consensus* between the two teams
might be our most valuable contribution to the Synod".

In July 2022 I had final discussions with the Bishop about minor details and on
2022-07-22 I finally sent "our" synthesis and the Bishop's letter to the
Secretariat of the Synod of Bishops. Done.

In August 2022 I read that the Bishop changed his mind and gave permission to
publish the Estonian synthesis (:doc:`/blog/2022/0817_0344`).

On October 4th, 2022 I informed the Bishop that I won't work any more as the
Synodal Contact Person (:doc:`/blog/2022/1004_2200`).

..
  We did not show this document to the authors of the 17 reports, and did not ask
  them to confirm whether this document reflects what they have to say. See also
  :doc:`/blog/2022/0601_0417`.
  :doc:`/blog/2022/0608_2332`
