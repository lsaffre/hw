======================
A name for a big issue
======================

Since September 2021 I have been listening to a series of :term:`faithful` who
responded to the questionnaire of the :term:`Synod on Synodality`.

I hear a clear tenor resonating from all aspects of the project: the
:term:`Church` is experiencing a :term:`controversial battle` between two
"camps" or "paradigms".

Already the names of these camps are a problem. Most often they are labelled
"traditional-conservative" and "liberal-progressive". I was obviously born in
the "liberal-progressive" camp. But confusingly I feel *very* traditional and
conservative about many topics, and I *do not* perceive faith as something
"liberal" (meaning an individual choice).

I personally became aware of this battle about 20 years ago when I moved from
Belgium to Estonia. Since then I have written some fervent blog entries against
"the other camp". There were times when I wrote "If this is the Christian faith, then I am not a
Christian!" \ [#blog]_ But many of these blog entries caused harm to others and increased my
suffering. I did not find any satisfying solution.

The first step in every dialogue is to clearly define what we are talking about.
We cannot fix a problem by refusing that it exists.

I believe that we *do* have a problem. A fundamental one. There *are* two
"types" of Christians, who *do* have very opposing understandings of the Gospel,
leading to fundamentally opposing views about how the Church should "live and
operate". And these views *do exclude* each other, there is no way to embrace
both. Jesus refers to such situations when he says "No one can serve two
masters, for either he will hate the one and love the other, or he will be
devoted to the one and despise the other. You cannot serve God and money."
(`Matthew 6:24 <https://www.bibleserver.com/ESV/Matthew6%3A24>`__, `Luke 16:13
<https://www.bibleserver.com/ESV/Luke16%3A13>`__), or "I came to cast fire on
the earth, and would that it were already kindled!" (`Luke 12,49
<https://www.bibleserver.com/ESV/Luke12%3A49>`__)

That's why I was fascinated when discovering the word "synodal", a `neologism
<https://sinod.katoliku.ee/en/3/#a-neologism>`__ that seems to be the right name
for the problem.  It is interesting to note that the word "synodal" has no
antonym. Synodality reminds me the slogan `Unity in Diversity
<https://en.wikipedia.org/wiki/Unity_in_diversity>`__, a prominent principle of
the Baháʼí Faith, which also has become the `Motto of the European Union
<https://en.wikipedia.org/wiki/Motto_of_the_European_Union>`__. Synodality also
reminds me the `code of conduct
<https://en.wikipedia.org/wiki/Code_of_conduct>`__ and the `Conflict of interest
policies
<https://meta.wikimedia.org/wiki/Code_of_conduct_and_conflict_of_interest_policies>`__
of the Wikimedia Foundation, or the `Ubuntu philosophy
<https://en.wikipedia.org/wiki/Ubuntu_philosophy>`__ ("A collection of values
and practices that people of Africa or of African origin view as making people
authentic human beings").

The Synod on Synodality gave me hope that we can change this battle into a
:term:`dialogue <controversial dialogue>`. It is going to make a few things
clear. [#clear]_ We are about to have a shift in `paradigm
<https://en.wikipedia.org/wiki/Paradigm>`__.

My general impression is that quite some convinced Christians feel reluctant or
even offended by the vision of the Synodal Church, which is propagated by Pope
Francis and which shines through in the `questionnaire of the Synod
<https://sinod.katoliku.ee/en/nuclei/>`__.  Their reactions reveal a series of
fears. The message "The church is synodal!" is for them like that call
"Liberté!" in the short film :term:`FreedomWithin`.

..
  Or in other words: there are unfortunate people who believe to understand the
  Gospel but who actually understood something that is rather its opposite. And
  maybe I am one of them!
  I believe of course that "my" camp is the "right" one.


It's good to have a name for an important topic. But what does "synodality"
mean *in practice*?  The big task of the Synod on Synodality is to write clear
answers to this question.   The Church is about to make a big choice. In the
following sections I try to contribute my two cents by formulating my personal
answer, knowing that it is incomplete and naive, and maybe even fundamentally
wrong.


..

..
  But every idea needs its opposite, every dialogue needs a name for *both* camps.
  Every business agreement needs to define the two partners, for example one is
  the "customer" and the other is the "provider".

..
  We need a name for what is *not* "synodal". I suggest to call our two dialogue
  partners "synodal" and "presynodal".


.. rubric:: Footnotes

.. [#clear] It won't make *everything* clear, and it won't make things *definitively*
  clear: there will always be weeds among the crop.

.. [#blog] For example :doc:`/blog/2019/1022`, :doc:`/blog/2020/0311`,
  :doc:`/blog/2020/0419`, :doc:`/blog/2020/1222`.
