=============================
About the prayer of the Synod
=============================

The `Adsumus Sancte Spiritus <https://sinod.katoliku.ee/en/adsumus/>`__ prayer
is a powerful text, which reminds us about a few challenges that we are going to
face when we come together. Yet it is only a text. It is a poem, not a law. Each
of us will understand it their way. Here are some examples about how I
understand it.


- *With You alone to guide us, make Yourself at home in our hearts; Teach us the
  way we must go and how we are to pursue it.*

  Give me courage and patience when my boss asks me something that differs
  from what you ask me. Let me trust my heart rather than my boss.

  On the other hand I need to learn, maybe even :term:`to repent`. Teach me!

- *We are weak and sinful*

  Help me to never forget that my personal opinions may be wrong and that I need
  to ask for help from others.

- *do not let us promote disorder*

  Every day brings new discoveries and scientific studies,
  sheds new light on :term:`reality`.
  :term:`Teachings <teaching>` that were justified yesterday, may promote disorder today.
  We must not leave obsolete teachings as they are.
  We must constantly update our teachings in order to keep them understandable for every human.

  The pace of evolution itself has increased dramatically in the digital era.

- *Let us find our unity in You*

  That is, not in the name of any teaching.
  Every teaching, even the most venerable, is made by humans.

- *You, who are at work in every place and time*

  Not only in the Vatican, and not only in the past.
