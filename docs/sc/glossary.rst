========
Glossary
========

Here is how I understand certain words when I use them on this website.
These definitions need to be reviewed and confirmed by experts.

.. glossary::

  teaching

    A written text about the :term:`Good News` that has been produced and
    published by the :term:`Church` and may be translated into different human
    languages.

  sacrament

    A Christian rite recognized as of particular importance and significance.

  sensus fidei

    A universal consent in matters of faith and morals, manifested by the whole
    people of God, from the bishops to the last of the :term:`faithful`.

    https://en.wikipedia.org/wiki/Sensus_fidelium

  common sense

    Der gesunde Menschenverstand.

  Synodality

    Synodality (from Greek *συν* together + *ὁδός* way, journey) is the name
    used by the :term:`Roman Catholic` church to describe its particular
    organization model (*modus vivendi et operandi*), which it has been using
    since its beginnings, but which is not yet documented in a systematic way.

    While the word "synod" has been widely used since ancient times to designate
    a church council, the adjective form "synodal" and the derived noun
    "synodality" are neologisms that emerged during the pontificate of Pope
    Francis.

    The first known public use of the word in this meaning is by Pope Francis
    during a speech on :term:`20151017`.  It is described by the International
    Theological Commission as "the decision to journey together" and "a
    prophetic sign for the human family, which needs a shared project capable of
    pursuing the good of all". (:term:`20180302`)

  Synod on Synodality

    The 16th Ordinary General Assembly of the Synod of Bishops, to be held in
    Rome in October 2023. The goal of this synod is to document more clearly the
    "modus vivendi et operandi" of the Church: How it works in daily life, how
    it is governed, how decisions are made, how it collaborates with the public.

    See the `Official website <https://www.synod.va/en.html>`__ or
    the short explanation elaborated `in Estonia <https://sinod.katoliku.ee/en/1>`__.


  Synodal Church

    The future :term:`church institution` that will be founded and lead by Pope
    Francis (if :doc:`my vision <vision>` becomes real).

  church institution

    A :term:`legal person` in the :term:`visible world` that represents and
    teaches her particular interpretation of the :term:`Gospel`.

  benediction

    A short invocation for divine help, blessing and guidance.

  Baptized

    An individual human who has declared before a community of other Baptized
    that she wants to follow the teachings of the Church. An the community has
    confirmed that they trust this declaration.

    A "certified Christian".
