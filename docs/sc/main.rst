=========================
Historic material
=========================

The following pages are no longer maintained because I integrated them into my
report where they continue to evolve.

.. toctree::
  :maxdepth: 1

  disclaimer
  why
  teachings
  other
  procare
  queer
  ip
  rites
