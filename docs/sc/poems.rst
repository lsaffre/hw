===================
Poems on Synodality
===================

One of my first reactions after reading the :term:`PD` and the :term:`Vademecum`
was a poetic attempt to express what synodality is all about. Since then I had
"attacks" several times, and my collection of "poems" has grown. They remain
unfinished work, childish attempts. May they inspire true professional artists.


.. contents::
   :depth: 1
   :local:



Ode to synodality
=================

| Synodality is when we understood that
| God wants us united in diversity;
| we are side by side on the same road;
| we don't kick anybody out just because we disagree.

| Synodality is
| when we listen with an open mind and an open heart,
| when we condemn our prejudice rather than other people,
| when we react to critics gratefully instead of angrily.
| when we speak with courage, freedom and love,
| when we honestly say what needs to be said,
| when we patiently explain our idea again and again,
| when we wisely discern between good and evil,
| when we courageously search peace with our enemy,
| when we fully forgive our nastiest offender,
| when we stay calm where panic reigns,
|
| Synodality is
| when no cry dies in your throat before it has been heard;
| when no foolish idea stays alive for long;
| when different opinions are enrichment, not threat;
| when other cultures are brothers, not enemies.

| Synodality is
| When truth grows while foolishness shrinks.
| When foolishness no longer overgrows truth.
|
| When arrogance becomes pride, and pride becomes gratefulness.
|
| When useless ideas don't hide the important ones.
| When long-term issues don't lock urgent decisions.
|
| When we show who we are, not who we want to be.
| When we write history as it was, not as we want it to be.
| When we love others as they are, not as we want them to be.
| When we forgive our mistakes instead of forgetting them.


| When we refuse getting influenced by prejudice, partiality and ignorance.


| When we stay on the way of truth
|
| because our knowledge is limited
| because our justice is influenced by prejudice
| because our dreams are just dreams
|
| When we find our unity without giving up our diversity.
| When we live sustainably without leaving burned earth to our children

You annoying fool
==================

| You annoying fool,
| you arrogant winner,
| you pitiable looser,
| you insane sick,
| you useless old,
| you criminal psychopath,
| you aggressive offender,
| I know that God loves you as you are,
| help me to love you as well.


Not afraid
==========

| When we are not afraid to look at ourselves.
| When we can admit that we are weak and sinful.
| and that our thoughts are influenced by ignorance.
|
| When we are not afraid to look at our past.
| When we can admit that we were weak and sinful
| and that our actions were influenced by partiality
|
| When we are not afraid to imagine a future.
| When we can admit that we need guidance
| and that our dreams are just dreams.


Synodality is what we all need to learn

Without the gospel
==================

| Without Jesus,
| our only hope is
| to live as long as we can,
| to trust in physicians
| to tell us how to stay healthy.
|
| Without Jesus,
| our only hope is
| to get as strong as we can,
| to trust in trainers
| who tell us how to win.
|
| Without Jesus,
| our only hope is
| to punish the wrong,
| to trust in judges
| who tell us who is right.
|
| Without Jesus,
| our only hope is
| to find the truth,
| to trust in scientists
| who tell us why we live.
|
| Without Jesus,
| our only hope is
| to be as smart as we can,
| to trust in teachers
| who
|
| Without Jesus,
| our only hope is to be without sin,
| to trust in priests
| to save our souls.
|
| Without Jesus,
| our only hope is to be independent,
| to trust in soldiers
| to fight for our liberty.

| Without Jesus,
| our only hope is to win,
| to trust in leaders
| to destroy our enemies.

We want a church
================

| We want a church
| who honestly searches for the good and the true
| and eagerly learns from reality

| who reliably preserves our common knowledge
| and clearly explains what it means today

| who makes and then learns from her own mistakes
| and humbly knows that she doesn't know all

| who loves her enemies and those who doubt
| and honours their different cultures and styles

| who teaches her knowledge to whoever wants
| and patiently answers even to fools

| who helps us to see the kingdom of god
| and to tell it to those who need it most

.. carefully listens


..
  |
  | God Holy Spirit, Father and Son,
  | who are at work in every place and time forever and ever,
  | come and fill our hearts with your love,
  | teach us the way to go.
  |
