======================
The official synthesis
======================

In :doc:`my comment </sc/comment>` I had still been saying "We did not publish
our final synthesis because we consider it an intermediate part of the Synod".
But in August 2022 I read that they changed their mind and published it:

- The full report is here: `Sinodi raport 2022
  <https://katoliku.ee/index.php/et/katoliku-kirik/sinod-2021-2023/819-sinodi-raport-2022>`__

- The blog post to introduce it is here:
  `Kokkuvõte sinodi piiskopkondlikust etapist Eesti Apostellikus Administratuuris
  <https://katoliku.ee/index.php/et/uudised/eesti/818-kokkuvote-sinodi-piiskopkondlikust-etapist-eesti-apostellikus-administratuuris>`__
