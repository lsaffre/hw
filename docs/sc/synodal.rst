==============================
A surprisingly synodal process
==============================

This report is the result of a surprisingly synodal process. Many people have
contributed to it directly or indirectly, with their prayer and their feedback,
which includes both approval and critics.

..
  It is based on a
  personal vision. I started it on my own, as a private project without approval
  of the Bishop and the "official" team and against their advice.
  From April to August I plan to continue on my draft together
  with the official team and the Bishop. (:doc:`more <synodal>`)

It started for me in September 2021: :doc:`/blog/2021/0914`.

In October 2021, the core team (:term:`sinoditiim`) was appointed during a
public mass celebration and the official consultation phase started. This team
developed the official website http://sinod.katoliku.ee and a questionnaire in
three languages. It was a lot of work and we are satisfied with the result.

While being thankful for the sinoditiim and their work, I was far from being
satisfied.  I had the feeling that we hadn't done enough. I wanted to also
consult people from other confessions and even people who have no contact with
the Roman Catholic church. It took me some time to realize that the official
sinoditiim was unable to help me with this idea. So I stopped bothering them. In
November we switched to "waiting mode" because there was nothing to do for us
before the end of the consultation. I can understand their reaction because just
two months earlier I had myself a similar experience in another team where an
overzealous team member was blocking the urgent teamwork with his idealistic
dreams.

At this point, I could have switched to a simple work-to-rule mode: let them do
as they want, I will put my signature under their document. And then I would
probably stop believing in the Church. So what. Nothing new. I am not the first
one to leave the Church like this.

But then I had :doc:`this vision </blog/2021/1119_1421/>`, which encouraged me
to continue. I started to run my private consultation project without the help
of the sinoditiim. I informed the Bishop about it, and he is a wise man: he did
not forbid me to act on my own, he even encouraged me. We both knew that he
wouldn't act like this, but why would he ask me to stop doing more than I need
to do? I continue to report him about what I do, he listens attentively and
finds encouraging words, but deliberately avoids to dive too deeply into my
private things.  I am still fully under his authority and he has the right to
unappoint me at any moment. That's how :term:`apostolic governance` works: you
delegate some task to another person and then give her much independence because
you trust her.

In the end of December 2021 I wrote a version 1 of a "fictive report" where I
"imagined what my synthesis would look like if I had to write it already now."

This fictive report was the synthesis of what I had heard from people of diverse
back­grounds, ranging from consecrated faith teachers to people who refuse the
idea of an institutionalized Church, ranging from people with less than standard
education to people who received high school education,... I sent the first
version to some friends and asked them to read it and give their feedback. I
"digested" their feedback and then published version 2, which started a next
iteration. With each iteration I asked more friends to contribute.

I use this iterative approach also in my daily work as a soft­ware developer. My
software projects start with an "interview" where the customer tries to describe
their requirements, my next step is to write a quick prototype for them ("rapid
prototyping") so that they can give feedback. Each feedback phase leads to a new
version, which the customer must evaluate and comment. The main differences are
that now the result is a readable document instead of a computer program and
that my "customer" is no less than the Pope.

In January 2022 I started to realize that it is difficult to comment on a
"draft" of a 10 pages report (:doc:`/blog/2022/0116_0348/`). I realized that I
have to find people who agree to participate in work sessions where we focus
together on the text during some hours. This is where the idea of the
:term:`sinoditalgutiim` was born. I started to call a handful of friends and
invited them to help me "more systematically" and with real-live meetings
(:doc:`/blog/2022/0118_2257`). Formulated in terms of :term:`apostolic
governance`, I formed a second, "inofficial" team.

I plan to work with the :term:`sinoditalgutiim` on my draft until March 2022.

On 2022-03-31 I will submit the report produced by our common work to the
:term:`sinoditiim`, i.e. we simply `participate in the official synodal process
<https://sinod.katoliku.ee/en/2/>`_. Our report will be one of many submissions.

Between April and August, the :term:`sinoditiim` will read all the reports and
then write the actual report to be sent to the Vatican.  I happen to be one
member of this team, but I have no authority there. We are 5 lay persons and one
priest. It is a :term:`synodal community`, we must ideally find a consensus. And
even when we found a consensus, it will finally be the Bishop who will sign our
report. If he disagrees, he will discuss with the us. If really we wouldn't find
a consensus, he might ultimately write the report himself.

I predict that there will be intensive discussions. And these discussions won't
be public.

Just my personal opinion?
=========================

One can say that my draft is "my" report, my personal opinion. My job was not to
say my personal opinion but to represent the opinions of all participants. Am I
"answering the wrong question"?

Some friends exclaimed “But this is your own opinion, not a summary!”.

Some friends exclaimed “But this is not teamwork!”. I reply that this project is
teamwork, but of a special style, to which not everybody is used. This project
is actually real example of synodal teamwork, which differs in some aspects from
“classical” teamwork. Synodal teamwork is characterized by an appointed leader
who has more autonomy than a board of managers (but on the other hand is under
authority of the person who appointed them).

The procedure I chose to apply for this project is efficient for teamwork in
this particular type of project and brings satisfying results. Producing a
common text like this document requires an editor. I offer my talents and
motivation to assume this role. Let me know it you prefer to assume this role in
my place; I would be glad to delegate it.

My job as the editor is to listen carefully to every contributor and to the Holy
Spirit. Each iteration will bring us closer to a consensus. There will probably
be no single contributor fully happy with every detail. A consensus means that
each contributor can say at least “I agree to make some compromises because I
want us to reach our common goal, which is to have a common statement at the
Synod”
