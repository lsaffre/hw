==============
The naked Pope
==============

In November 2021, after two months of listening for the Synod, I had the
following vision:

Pope Francis will do yet another important step during his lifetime. He will
delegate the leadership of the Roman Catholic church to another person and then
found the Synodal Church, a new institution that will embrace and unite all
Christian institutions under his leadership.

Most institutional property will remain with the Roman Catholic church, he will
take with him only what an institution needs to survive. I guess that this will
be the Secretariat of the Synod of Bishops, parts of the Holy See and a few
other administrations, maybe a few real estate objects. Like his patron St.
Francis of Assisi when he left his parents, Pope Francis will be "naked" during
a short moment.

This step is is not a schism. In the contrary, it is the only way to *avoid
another schism* and to reconcile the Church. It is a step towards greater unity.
No single faithful will get lost and no single faithful will be forced to change
more than they are able to change.

The other parts of the Roman Catholic church will follow little by little, each
at their pace and when their pastor decides so. They will do this more or less
carefully, more or less quickly, each of them as it is due. They will discover
their place under the new institution.

At the same time other religious institutions, protestant and orthodox, will do
the same and unite in joy with the Synodal Church. Also many of the faithful who
left the visible Church during the last years will return.  And even a series of
corporations that have not called themself "Christian" until now, will join this
choir and align into the Synodal Church.

The Roman Catholic church will simply no longer be the "top of the tree", it
will find itself at the same level together with protestant and orthodox
Christians.

There will be resistance because humans are naturally resistant to change. This
resistance itself is rather a confirmation than an obstacle.


.. rubric:: Comments

- This vision is based on a  :doc:`real-life story  </blog/2021/1119_1421>` and
  inspired by the hope to "plant dreams, draw forth prophecies and visions,
  allow hope to flourish, inspire trust, bind up wounds, weave together
  relationships, awaken a dawn of hope, learn from one another and create a
  bright resource­ful­ness that will enlighten minds, warm hearts, give strength
  to our hands." (:term:`PD` no 32).

- In daily life I work as a professional software developer. The issues I worked
  upon during the last 30 years with my customers are of the same nature as the
  issues I see in the Church. If the Church were a software application and I
  the developer and the Vatican my customer, then this is what I would suggest
  them to do.

- The time indication ("Pope Francis" and "during his lifetime") is not
  absolute. It rather says that this step is the only possible path. Any
  endeavour that is not into this direction is a waste of time.

- This is a vision, not a prophecy. It won't happen like this. Most Catholics
  would go with the Pope, so he wouldn't be "naked" at all.
