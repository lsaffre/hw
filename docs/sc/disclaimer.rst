==========
Disclaimer
==========

I also happen to be *diocesan contact person* of the Catholics in Estonia, but
please don't mix up two different things:

- As the *diocesan contact person* my job is to coordinate a team where we act
  under the authority of the Bishop to collect and report what the Catholics in
  Estonia have to say at the Synod. The work of this team is published on
  another website, https://sinod.katoliku.ee

- The Human World website (which you are reading right now) is where I try to
  formulate, as a private person, my own input to the Synod.

..
  , and because I happen to be the webmaster of this
  team, their website looks similar to the website you are reading right now.
