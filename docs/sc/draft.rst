====================================
Our input to the Synod on Synodality
====================================

This draft is no longer maintained since :doc:`2022-02-07 </blog/2022/0207_0523>`.
For the work of the :term:`sinoditalgutiim`, see :doc:`/sc/talgud/index`.

.. Keep in mind that this is just a draft.
  See :doc:`report` for how we are working on it.

.. contents::
  :local:

Introduction
============

(:count:`item`) This is what we have to say at the :term:`Synod on Synodality`.
"We" are the :term:`sinoditalgutiim` formed by Luc Saffre in January 2022. We
worked on this document between December 2021 and March 2022 in a synodal
process, described :doc:`separately <synodal>`.

(:count:`item`) This is the online version of our report, which is work in
progress and may change at any moment. You can also dive into the
:doc:`changelog <changes>` and consult old versions of this document.

(:count:`item`) There are about 4.500 baptized Roman Catholics living in
Estonia. Roman Catholics are a small group within the Christians in Estonia
(300.000), which is itself a small group within the population of Estonia
(1.330.000), which is itself a tiny group of the world population (0.0168%).

(:count:`item`) Rather than organizing polls and studies in order to provide a
representative description of religious life in Estonia, our work focuses on a
**prophetic answer** to the question "What steps does the Spirit invite us to
take in order to grow in our ‘journeying together’?"  Already the mere process
of writing this report contributed to "allow hope to flourish, inspire trust,
bind up wounds, weave together relationships, awaken a dawn of hope, learn from
one another and create a bright resourcefulness that will enlighten minds, warm
hearts, give strength to our hands." Our work is inspired by what the :term:`PD`
(no 32) says: let's "dream and draw forth prophecies and visions".


Let's speak about problems
==========================

..
  (:count:`item`) Estonians are masters in the art of keeping things secret.
  "Sellest me ei räägi" is a venerated slogan.  Estonians perceive it as impolite
  to mention problems.  And We know that

(:count:`item`) When evaluating how well a community does her job, it is usually
more *tempting*, more *easy* and more *profitable* to see the "speck in your
brother's eye" (`Luke 6:41-42
<https://www.bibleserver.com/ESV/Luke6%3A41-42>`__) and to blame others.

(:count:`item`) Also it is disturbing for the life of a community when some of
its members fail to do their part because they are obstinately focus on some
irrelevant issue that disturbs only them.

(:count:`item`) But there are situations where `Luke 19:39-40
<https://www.bibleserver.com/ESV/Luke19%3A39-40>`__ applies: "if these were
silent, the very stones would cry out."
If we dare to speak about problems, we don't do this in order to tease some particular church organization.
We do it because we feel co-responsible for a world-wide community of all Christians.
The authors of this document declare to be motivated by their
love towards the (ideal) catholic and apostolic Church.

(:count:`item`) We believe that the Church has some disturbing but existential
issues. We are relieved to see that the :term:`Synod on Synodality` is going to
address these issues. We are confident that the Church is led by many wise men.



..
  We sometimes feel like that child, and some of my brothers and sisters tell us
  "Shut up! Her coat is correct, it just *looks* inside-out!".

..
  (:count:`item`) The main question of the Synod and the Catholic church is how we can live as
  the Church according to our teachings, and how to influence other people and
  especially the members of our church in a reliable way. The teachings of the
  church about the Gospel are reliable, well elaborated and carefully researched.
  We must not doubt in these teachings. We encourage you to remain faithful to the
  truth of Jesus Christ as it has been taught by the Church from its beginnings.

  (:count:`item`) Regarding critics on administrative questions, we advise to learn from the
  other big organizations in the real world regarding transparent accounting,
  efficient human resource management, reliable auditing procedures, ...


Vision: A new institution
=========================

(:count:`item`) Our report is based on a genuine real-life vision described
:doc:`in Luc's blog </blog/2021/1119_1421/>`. Everything we say is inspired by
the hope expressed in this vision.  Without this vision we would have nothing
relevant to say at the Synod. Here is our summary of this vision:

(:count:`item`) Pope Francis will do yet another important step during his
lifetime. He will appoint another person as the apostolic head of the Roman
Catholic church. This person, under authority of the Pope, will have full
responsibility over the Roman Catholic church. The Pope himself will become the
head of a new institution, called "Synodal Church", which will embrace and unite
all Christian denominations.

Most institutional property will remain with the Roman Catholic church, the Pope
will take with him only what the Synodal Church needs to survive. I guess that
this will be the Secretariat of the Synod of Bishops, parts of the Holy See and
a few other administrations, maybe a few real estate objects. Like his patron
St. Francis of Assisi when he left his parents, Pope Francis will be "naked"
during a short moment.

This step is is not a schism. In the contrary, it is the only way to avoid
another schism and to reconcile the Church. It is a step towards greater unity.
No single faithful will get lost and no single community will be forced to
change more than they are able to change. Many faithful from all denominations
have been working to prepare this step for more than 70 years.

The Roman Catholic church will align her teachings little by little to the
directives of the Synodal Church, step by step, as their apostolic head decides.
At the same time other religious institutions, protestant and orthodox, will
unite in joy with the Synodal Church and do the same. Each confession will align
more or less carefully, more or less quickly, each of them as it is due.  Also
many of the faithful who left the visible Church during the last years will
return. And even a series of corporations that have not called themself
"Christian" until now, will join this choir, discover the :term:`Gospel` and
align into the Synodal Church.

The Roman Catholic church will simply no longer be the "top of the tree", it
will find itself at the same level together with protestant and orthodox
Christians. There will be resistance because humans are naturally resistant to
change. This resistance itself is rather a confirmation than an obstacle.

The opposite of a schism
========================

(:count:`item`) The foundation of the Synodal Church is the opposite of another
schism: it will unite all Christians back under one institution.

(:count:`item`) The Church has experienced two major schisms during her history.
More than twenty human generations have passed since Reformation, more than
forty generations since the Schism of 1054.

(:count:`item`) Schisms are a natural side effect of growing communities. Every
living being grows by cell division. Diversity of the members is not the problem
of the Church. The problem is the lack of an institution that unites them all.

(:count:`item`) Despite more than 70 years of effort, the `World Council of
Churches <https://en.wikipedia.org/wiki/World_Council_of_Churches>`__ and other
initiatives still seems to be stepping on the spot.

(:count:`item`) This is because we have an architecture problem: The Pope
exercises two different roles: research ("What does the Gospel mean today?",
"Where to go?") and deployment ("How to get there?", "How to explain this to the
faithful?"). While accumulation of offices can be okay in small communities, it
is unacceptable in bigger communities. To illustrate this with an image from
Estonian culture: The director of a big choir must resist the temptation of
standing in one of the voice groups and singing with them, she must leave the
flock and gain an elevated, distanced and impartial position.

.. A choir doesn't work if the director also sings. The director
  of an ensemble may also sing because it is a small community, but when the
  community grows, there comes a moment where the leader must stop singing.

(:count:`item`) The natural attitude of every Christian denominations when
talking to her siblings is to consider her own teachings as the true ones while
those of other Christian denominations are "heresy". We try to suppress or hide
this more or less successfully, but we can never fully avoid it. It is a natural
self-confidence, an attitude that is required by every community.

But the Gospel can never be in contradiction with :term:`pure science`. We have
scientific evidence that people in other confessions witness salvation through
the Gospel. We cannot consider them any longer as heresy.  The teachings about
the Gospel that have been developed by other denominations cannot be ignored. We
must learn from each other. Loving our neighbour means to give those teachings
the same careful consideration as our own teachings.

(:count:`item`) In case of conflict we cannot apply lynch justice. We need a
superior authority. This superior authority cannot be one of the siblings.
A referee cannot participate in the game.

(:count:`item`) The Synodal Church must be a new legal person, and its work must
be at a "higher" level, more spiritual, closer to the Christ, less bound and
burdened with earthen worries.

(:count:`item`) According to theory of papal succession, only the Pope can make
this step. True ecumene will happen only when we fix the issue of the Pope's
cumulated offices.

(:count:`item`) Our vision suggests a step that has been waiting at least since
the Second Vatican Council.  It describes a fundamental change in the *modus
vivendi et operandi* of the Church.  This step seems so simple and obvious that
the only surprising thing is that we did not see it earlier. We feel like the
two disciples in Emmaus we exclaim "Did not our hearts burn within us?" (`Luke
24:32 <https://www.bibleserver.com/ESV/Luke24%3A32>`__)


Comments about the vision
=========================

(:count:`item`) The Synodal Church will have two main goals:

(:count:`item`) Her institutional goal is to ensure the `apostolic succession
of the Pope <https://en.wikipedia.org/wiki/Apostolic_succession>`__. The related
administrations of the Roman Catholic church will move together with the Pope
and become part of the Synodal Church.

(:count:`item`) Her operational goal is to explain the Gospel to everybody.
She will develop and maintain *master teachings* that are used as *directives*
for the different concrete churches. These master teachings are dynamic because
our understanding of the real world is dynamic. These master teachings focus on
the question "What does the Gospel *ideally* mean in the real world today?" and
deliberately ignore the question "How to *practically* forward these teachings
to the faithful of a given community?", which is delegated to the concrete
churches. She will help the concrete churches to align their existing teachings
to those of the Synodal Church.

(:count:`item`) The central visible result of her activity will be a single
Internet website with (1) authoritative teachings maintained by the SC herself
and (2) a huge database of documents produced by other entities, together with
their approval state.

(:count:`item`) While the foundation of a new institution is basically "just an
administrative shift of power" in the executive floor of the Church, there are
more important consequences that will influence daily life of many people. These
will be the actual challenge.

(:count:`item`) The Synodal Church won't force any concrete church institution
to follow her teachings. She won't be "a kingdom of the visible world". She
won't have executive power. Her teachings will be :term:`directives <synodal
directive>` to the concrete church institutions and to every faithful, which
these are invited to "either follow or criticize" as long as there is no
consensus. Finding a consensus on every topic is important, but not urgent and
not always immediately possible.

(:count:`item`) The Synodal Church will also develop and publish rules, to which
she will adhere herself as a legal person.

..
  (:count:`item`) Summary of my vision: There will be a new legal person that will
  unite all Christian confessions in their full diversity.


Progressive versus conservative
===============================

(:count:`item`) Many aspects of the consultation project showed that the Church
in Estonia and in the world is experiencing a controversial battle between two
"camps".  There are two fundamentally opposing views about how the Church should
"live and operate". Certain parts of these views do exclude each other, there is
no way to embrace both. Jesus refers to such situations when he says "No one can
serve two masters" (Matthew 6:24, Luke 16:13), or "I came to cast fire on the
earth, and would that it were already kindled!" (Luke 12,49)

(:count:`item`) The Synod on Synodality gives us hope that we can change this
battle into a dialogue. It is going to make a few things clear.

(:count:`item`) We cannot solve a problem by refusing that it exists.  The first
step in every dialogue is to agree on what we are talking about.

(:count:`item`) Often the two camps are labelled "traditional" or "conservative"
versus "liberal" or "progressive". But trying to cut reality into two halves
(dichotomy of a :term:`controversial question`) would be a naive simplification.
Nobody is fully on one side. You can be labelled "liberal" without perceiving
faith as individual choice. You can be labelled "progressive" and still be very
conservative in many regards.

(:count:`item`) Because the problem is more complex than a simple
:term:`dichotomy`, some call it a `paradigm shift
<https://en.wikipedia.org/wiki/Paradigm_shift>`__.

(:count:`item`) We can also call it a `metamorphosis
<https://en.wikipedia.org/wiki/Metamorphosis>`__: Synodality as a butterfly that
emerges from its `pupa <https://en.wikipedia.org/wiki/Pupa>`__ state within a
few hours after long weeks of invisible activity.

(:count:`item`) The neologism "synodality" seems to have been a signal for this
metamorphosis of the Church. Synodality reminds the expression "Unity in
Diversity", a prominent principle of the Baháʼí Faith, which also has become the
Motto of the European Union. Synodality also reminds the code of conduct and the
Conflict of interest policies of the Wikimedia Foundation, or the Ubuntu
philosophy (“A collection of values and practices that people of Africa or of
African origin view as making people authentic human beings”).


One Gospel, many teachings
==========================

Content of this section is now in :doc:`/talks/cntp`.


Dialogue in the digital age
===========================

(:count:`item`) Humanity in the digital age is experiencing an unprecedented
challenge: we need to find ways to dialogue about questions that had never
required a world-wide consensus as long as real-life contact between cultures
was rather exceptional. Video conferences have become normal during the COVID
pandemic. This is just a recent and probably not the last revolutionary change
in our social infrastructure.

(:count:`item`) The art of dialogue is the art of finding a consensus in
conflicts. The result of dialogue is always a compromise, a *modus vivendi* to
which all partners can agree. If one of them does not fully consent, the result
won't be durable.

(:count:`item`) Some examples of these questions are
"How to protect our nation against attempts of another nation to gain governmental authority over us?"
"How to balance the profit gained by industry
and its risk of having harmful effects?"
"Who decides what is harmful?"
"Who decides which questions need an immediate decision and which decisions can wait?"
"Who decides which decision in some controversial question is good and true?"

(:count:`item`) Science cannot answer these questions because science is is just
a tool that helps us to observe reality and the effect of our decisions. But it
does not choose between good and evil. It is ethically neutral and as such
opportunistic: it serves those who use it.

(:count:`item`) Each step towards the global village shows us more clearly: it
was an illusion to believe that an "invisible hand" can lead us to peace. The
United Nations as the world government needs an independent and reliable
advisory body on ethical questions.

Presynodal convictions about the Church
=======================================

(:count:`item`) In this section we collect statements about the Church that we
perceive as "presynodal conceptions" about the Church. We classified them using
the 10 nuclei and  tried to give them a "synodal answer". We acknowledge that
many of them express *convictions*, i.e. are unlikely to change during the
lifetime of a person. See :ref:`draft.elastic`.

**1.\  Companions on the journey – In the Church and in society we are side
by side on the same road.**

"We must hold together to protect our community against the world." →
We must hold together to celebrate the Gospel and make it shine to the world.

"We must protect the Gospel against getting spoiled by satanic teachings."
→ The Gospel is bigger than any human teaching, nothing can ever make it fade.

"We must keep a distance from those who refuse the Gospel."
→ We must explain the Gospel to those who did not yet understand it.


**2.\  Listening – Listening is the first step, but it requires an open
mind and heart, without prejudice.**

"Don't listen to what makes us believe something else than the truth.
Listening to lies causes you to believe them in the end."
→ Listen to every voice, especially the unpleasant ones can help you to learn.

"Fighting for the truth means to not leave any lie unanswered."
→ Avoid listening only in order to find arguments against "their" opinion and to explain once more "our" opinion.

"Replying clearly to a wrong opinion can need patience and is an act of love.
Failing to reply encourages the other to remain in their mistake."
→ Loving your enemies means listening to their opinion with your heart wide open.


**3.\  Speaking out – All are invited to speak with courage and parrhesia, that is, in freedom, truth, and charity.**

"We must not speak out everything without worrying about how the enemy might use
our words against us. Diplomacy is the art of saying the truth without offending
the enemy." → We will learn the art of speaking boldly where needed. When our
brother sins, we will explain him what disturbs us even if he might feel
offended. Hurting can be the opposite of harming. Of course every unpleasant
message needs special prudence.

"Do not give dogs what is holy, and do not throw your pearls before pigs, lest
they trample them underfoot and turn to attack you." (Mt 7:6) → Respect that not
every faithful is ready to understand every teaching.

.. rubric:: 4. Celebration – “Walking together” is only possible if it is based on communal listening to the Word and the celebration of the Eucharist.

"The Holy Mass fosters our identity and grows our faith. We must not let
modernistic tendencies tell us how to celebrate." → Many rites developed by many
cultures are a way of celebrating the Gospel. We will embrace them and learn
from each other.

.. rubric:: 5. Sharing responsibility for our common mission – Synodality is at
  the service of the mission of the Church, in which all members are called to
  participate.

"A lay person must not instruct other people regarding faith questions;
teaching should be done by a priest."
→
The Gospel propagates mostly through our lives, through what we say and do to
other people.

.. rubric:: 6. Dialogue in Church and society – Dialogue requires perseverance and patience, but it also enables mutual understanding.

"We must not waste our energy trying to understand people who refuse to accept
the truth." → Before we can explain the Gospel to somebody, we need to
understand their worries and fears and convictions.

"Beware of the 'stench effect': constantly living in a faithless environment
makes you indifferent to the stench. You can loose your faith when you are
constantly surrounded by people who don't believe."
→
Nothing can ever come between us and the love of God revealed to us in Christ
Jesus.


.. rubric:: 7. Ecumenism – The dialogue between Christians of different confessions, united by one baptism, has a special place in the synodal journey.

"The purpose of ecumenism is to open a door to other confessions so that lost
faithful can turn back to the Roman Catholic church." → We will learn from each
other and share what we have learned during our different history.

"During Holy Mass we welcome non-Catholic visitors and expect them to follow the
rules." → Every rite is open for visitors and we help them to understand what is
going on.

.. rubric:: 8.\ Authority and participation – A synodal church is a
  participatory and co-responsible Church.

"The Church needs a clear hierarchy. Whenever humans do something together, it
must be clear who is the boss. The boss is responsible in the end." → An
apostolic leader is like a choir director: he does not produce a single sound,
but he is the most important actor of the choir.

"Co-responsibility is counter-productive and creates chaos. If you are not the
boss, then you must obey, not criticize. That’s called discipline." → Your boss
is your brother. If you feel that he is wrong, you must speak to him:  first
confidentially, then with a witness and if needed before the community.

"Participation means communism. History shows that communism has failed. It is
not a sustainable way for living together."
→ ...


.. rubric:: 9.\  Discerning and deciding – In a synodal style we make decisions
  through discernment of what the Holy Spirit is saying through our whole
  community.

"Common decisions must be the result of a well-organized democratic process. We
cannot allow corrupt leaders who do what they want, even if they call it
discernment." → ...

.. rubric:: 10. Forming ourselves in synodality – Synodality entails
  receptivity to change, formation, and on-going learning.

"The Church must align to Jesus Christ alone, not to a multicultural mix of
ideologies." → ...

"The mission of the Church is to teach others, not to learn from
others." → "Teaching requires learning. While the Church teaches what she has
learned so far, we must never forget that her learning continues. The Church
must not claim to teach the only valid teaching about God."

More presynodal convictions about the Church
============================================

(:count:`item`) A series of traditional expressions used for explaining the
Gospel need clarification because they tend to get misunderstood, which leads to
disorder.

(:count:`item`) "The teachings of the Church are eternal and immutable."
→ "The teachings of the Church need constant revision and update"

(:count:`item`) "The Church teaches the truth"
→ "The Church teaches what we assume to be true"

(:count:`item`) "No salvation *outside the Church*" → "No salvation *without the
Gospel*"

(:count:`item`) "'Christ Saviour' means that you are lost if you do not
repent from your sins and decide to follow Jesus."
→
"Christ Saviour" means that Jesus saved humanity from the idea that God
accounts our sins. See Sins_

(:count:`item`)
To follow Jesus means to follow the Gospel.
There are people who follow the Gospel without using the Bible as a :term:`Holy Scripture`.

What Synodality means
=====================

(:count:`item`) It is good to have a name for an important topic. But what does
"synodality" mean in practice? The big task of the Synod is to develop clear
answers to this question.

(:count:`item`) Here is our list of suggestions:

- Synodality means to be *together* (we can't refuse anybody who wants to join
  or to stay) and *on our way* (i.e. we know that we don't know the truth).

- Synodality means to cultivate true and fully open-minded **dialogue**.

- Synodality means to be **as transparent as possible**. The :term:`Church` must
  announce and explain the Gospel to everybody and in public space. The Church
  must publicly document her own functioning in a transparent way. The children
  of the light don't act in the dark.

- Synodality means to listen to **all members of the Church**. This means to
  listen to every :term:`faithful`, including people from other confessions,
  even queer people.

- Synodality means to practice **apostolic governance and teamwork** in an
  *open*, *systematic* and *documented* way. Church fails where it is done in a
  *hidden*, *unsystematic* or *undocumented* way.

The church teachings need a review
==================================

(:count:`item`) Encouraged by our vision, which fixes the *institutional* issue of
the Pope's cumulated offices, we dare to speak about an *operational* issue of the
Church, which is especially visible in Estonia but becomes increasingly visible
in other countries:

This section was moved to :doc:`/talks/cntp`

How to teach the Gospel
=======================

This section was moved to :doc:`/talks/cntp`


Tell me the gospel in 60 seconds
================================

(:count:`item`) The Synodal Church will publish an authoritative "executive
summary" of the Gospel that can be used by a :term:`faithful` to check their own
understanding and for explaining it in a reasonable to an interested human of
the 21st century.

About rules
===========

(:count:`item`) Rules are a special form of teachings. Rules are different from
teachings about the Gospel in that they speak about the :term:`visible world`.
Unlike teachings, rules bind us to follow them in a legally measurable way.
Declaring that you follow a rule makes you legally responsible in the visible
world. Rules can be used in documents that legally bind two business partners.

(:count:`item`) Besides teachings about the Gospel, the Synodal Church will also
develop and publish rules, to which she will adhere herself as a legal person.

(:count:`item`) The Roman Catholic church has her rules. Some of them need a
major update in order to formulate them using modern civil law language and to
avoid misunderstandings that can lead to :term:`sin`.

Apostolic governance
---------------------

(:count:`item`) The Church has always been apostolic.

(:count:`item`) The basic idea behind :term:`apostolic governance` is that an
individual person gets *appointed* to do a given job. This is inspired by Jesus
who appoints very diverse people (fishermen, tax collectors, ...) or the prophet
Samuel who appoints Saul and later David as the kings of Israel (`1 Samuel 9
<https://www.bibleserver.com/ESV/1%20Samuel9>`__ and `1 Samuel 16
<https://www.bibleserver.com/ESV/1%20Samuel16>`__). Any appointed person can
herself appoint other persons to help her with her job. The Pope is the
top-level appointer and gets appointed by the procedure of papal succession
(which, by the way, is similar to that of the Dalai Lama).

(:count:`item`) The rules of the Synodal Church will define the concept of
:term:`apostolic governance`, which embraces democracy and monarchy and as such
can be an answer to issues of these government forms. This has the potential to
eventually lead to a new social and economic system called synodalism, as an
alternative to capitalism, communism and socialism.

(:count:`item`) These rules will cover transparency and privacy. They will
define the roles of workers and contributors in the Church (priests, teachers,
inspectors, healers, prophets, ...). They will explain the value of celibate
persons for the community.

(:count:`item`) These rules will enforce privacy of relations with natural
persons, including e.g. the duty to confessional secret. On the other hand these
rules will refuse any right on privacy to organizations. All agreements of a
synodal organization with other organizations will be accessible to the public.
See also Public money, public agreements.

Consensus
---------

(:count:`item`) One of the differences between apostolic governance and
(classical) democratic government is that when a minority of the community says
"something is wrong", the community needs to listen. Synodality seeks consensus,
not majority.

(:count:`item`) There are topics that need time. As long as there is no
consensus about some controversial question, synodality means that the
institution remains impartial and suggests possible compromises. Silencing down
minority opinions in controversial discussions is not the synodal way of finding
sustainable peace.

(:count:`item`) There are things we cannot change, and we need to accept these
things. It would be a waste of energy to continuously quarrel and complain about
them. And of course it is not always easy to discern the things we can change
from those we can’t. If you are really the only one to see a problem, you need
to humbly ask yourself whether your inner voice is right, whether that problem
is really important enough and whether you really have the duty to disturb the
community process with your concerns right now.  One of the important functions
of celebrating is to offer a spiritual place where “the rule” says to everybody:
“shut up now!” If we fail to obey this rule, we spoil the celebration. One
benefit of art of celebrating is to cultivate our detachment skills. On the
other hand, if you comply with the majority against your inner voice and just
because you are too lazy or shy or arrogant to talk about the problem, then you
sweep the dirt under the carpet. The problem will remain and eventually grow.
Synodality means to cultivate a humble and patient form of perseverance.

(:count:`item`) Apostolic governance is more than pure democracy, which can lead
to lazy compromises, resignation, despair, loss of motivation and attitudes
called “work-to-rule”. It is also more than monarchy, which can lead to
idolatry, elitism and nationalism.

Sexual assault
--------------

(:count:`item`) (not sure whether we have something to say here...)

(:count:`item`) The work of a pastor comes together with the risk of accidents
caused by human sexuality and the intimate nature of the job as a spiritual
advisor. When a professional football player is diagnosed with osteoporosis, he
cannot work on the field any more, but he can continue to work in the same
organization, e.g. as an administrator.

.. _draft.sacraments:

Sacraments
----------

This section needs revision. See :doc:`/blog/2022/0128_1709`.

(:count:`item`) The Synodal Church won't define sacraments. Her directives will
use modern civil legal language to define concepts like "membership" (entering
and leaving the organization), and generic "job descriptions" (priest, pastor,
teacher, inspector, etc).

(:count:`item`) You become member of the Synodal Church by declaring that you
believe in the Gospel as taught by the Synodal Church. The SC encourages its
members to also be member of some concrete church (Roman Catholic, Protestant,
Orthodox, ...) where they attend community life and receive sacraments.

(:count:`item`) The Roman Catholic church does not currently provide procedures
to revoke life-long sacraments like baptism, marriage, priesthood. This is in
conflict with modern legislation, which requires each contract a way to end it.
Therefore it will be in conflict with the rules of the SC. But that's not a
problem: the RC will simply know that she is invited to align her rules with the
new directives "as soon as possible", step by step. The head of the catholic
church has full authority to decide how much time they need. If it takes
generations, let it take.

(:count:`item`) The Synodal Church will also define concepts like
:term:`marriage` (a contract between two humans) and :term:`oikos`. The concrete
church institutions may potentially add their own conditions. For example the
Roman Catholic catholic church might add that a marriage must be heterosexual.

..
  (:count:`item`) Sacraments are visible symbols, a gift of the Church to the
  faithful, a confirmation given by the Church about something holy. But they
  aren't contracts. They are unconditional gifts. They apply only as long as the
  receiver wants them.

  (:count:`item`) When somebody gives you a book, but you fail to read it, then
  the book unfortunately didn't work for you. You can fail to read a book for many
  reasons. For example because it was stolen before you had the time to read it.
  Or because you didn't care, which just means that you didn't understand that
  this book would be important.

  (:count:`item`) Consequently, the Synodal Church will describe a way to revoke
  any life-long sacrament (baptism, marriage, priesthood). And this revoking is
  done by the receiver(s) of the sacrament, the Church has no veto, her job is to
  register the decision of having revoked. We cannot say that your failure of
  receiving this sacrament is a sin. We can be sorry for you, but we cannot blame
  you or even exclude you from our communion.

  (:count:`item`) Note that while baptism and priesthood requires a single human
  to revoke it, revoking a marriage needs both partners to agree on the divorce.

  (:count:`item`) Note that there are "baptized" people who don't understand the
  Gospel. And there are people who follow the Gospel but refuse to get baptized
  because they have seen too much of the harm caused by the Church. We can tell
  the Spirit from its fruits


The Church is a content publisher
=================================

(:count:`item`) Juridically spoken, the teachings of the Synodal Church are
:term:`published content` and a fruit of our human work. Its authors have a
right and duty to protect their work from getting used in a wrong way.

(:count:`item`) On the other hand, our teachings are a gift of the Holy Spirit.
They are intended to be freely available to everybody.  Therefore we cannot
refuse to anybody to right to use them. Using them includes modifying them.

(:count:`item`) There will be a **Synodal Licence** that formulates these
principles. This licence will be developed by the Creative Commons and adds
another option ("Synodal"), which adds a condition: you must clearly report how
your derived work is related to its sources and whether or not you have their
approval. You are not allowed to use the reputation of a name without explicit
approval.

(:count:`item`) The Synodal Church will learn from existing organizations like
the Free Software Foundation, Creative Commons or the Wikimedia Foundation, who
have done important work about how to manage knowledge as a public resource in a
way that aligns with the Gospel. These organizations fundamentally differ from
organizations who consider published content as a private property.

(:count:`item`) The Synodal Church will use concepts and technologies developed
by these movements for publishing and maintaining complex documentation systems.
An example of such a technology is the one used by Wikipedia. Note that
Wikipedia has different requirements because it is an encyclopedia, not an
authoritative teaching.

.. _sc.draft.examples:

Examples of some teachings
==========================

(:count:`item`) Inspired by the vision, we went yet another step further and
imagined how the teachings of the Synodal Church might look like if a group of
:term:`faithful` would sit down and rewrite them all "from scratch".

..
  (:count:`item`) Keep in mind that moral teachings are not the topic of the
  Synod, they are here in order to observe our feelings while considering them. We
  will reflect on our feelings in the last section of our report.

Sins
----

(:count:`item`) For example, let's imagine a clear and short authoritative
definition of what the word :term:`sin` means for a Christian:

  A :term:`sin` is a choice that causes harm to somebody.

(:count:`item`) Christians dare to speak about sins because we believe that God
forgives them even before we realize or repent them. Your sins may cause you
trouble in the :term:`visible world`, but they cannot separate your from God’s
love nor from communion in the Church. This is one of the central messages of
the :term:`Gospel`. It is especially visible in  in Luke 15:11–32 (the `Parable
of the Prodigal Son
<https://en.wikipedia.org/wiki/Parable_of_the_Prodigal_Son>`__) where the father
had been waiting for the return of his son already *before* the son had shown
repentance.

(:count:`item`) This way of speaking about sins is relatively new and not yet
fully integrated into the hearts of the members of the church. It became
possible after the Second Vatican Council. It enables the Church to confess her
own collective sins.
(`List of apologies made by Pope John Paul II
<https://en.wikipedia.org/wiki/List_of_apologies_made_by_Pope_John_Paul_II>`__,
`Vatikan: „Gebührende Aufmerksamkeit für Münchner Gutachten“
<https://www.vaticannews.va/de/vatikan/news/2022-01/vaticano-stellungnahme-gutachten-muenchen-missbrauch-bruni.html>`__)

(:count:`item`) This applies to our own sins as well as to those of other
people. Our own sins are the only ones we can --sometimes-- change directly, but
they are difficult to see. The individual sins of other people are basically not
our business, but speaking about them --in an appropriate way-- is an act of
love because our mirror is often the only way to help other people realize their
own sin.

(:count:`item`) The :term:`collective sins <collective sin>` of our communities
and of humanity as a whole are our business as much as we are co-responsible.
Unlike individual sins, collective sins cannot be avoided by single person.

Abortion
--------

(:count:`item`) The Synodal Church will have a shift of focus in her work
against child abandonment and abortion. Traditional approaches for avoiding
unwanted pregnancies are based on some form of culpability. But history shows
that no law and no faith culture, however strict, can avoid unwanted pregnancies
completely. Parents happen to get pregnant by accident.

(:count:`item`) The Synodal Church will move the action responsibility from the
mother to the community. Instead of saying „pro life“ or „pro choice“, it is
going to say „pro care“. Questions like “Who is guilty?”, “Who pays the bill?”
or “How to avoid unwanted pregnancies?” are to be regulated by each nation,
while the Church asks “How to save both the child and its parents?” It will
teach that every pregnant woman must have the option to renounce from her
maternal responsibility without feeling guilty for her choice. The community
must give her moral, social and economical protection during the remaining phase
of her pregnancy. After having given birth to the child, this woman is free from
any maternal duty towards her child.

(:count:`item`) The Synodal Church will say clearly that the question of when
exactly an embryo can be considered a human is controversial. Rather than giving
a doctrinal answer, it will help each faithful to find peace of heart by giving
advice on reliability and trustworthiness of the different sources of
information.

Queer people
------------

(:count:`item`) The controversial dialogue about the rights of homosexual and
queer people are especially interesting for synodality because the topic itself
is rather simple (compared e.g. to the Covid discussions) and touches a rather
small minority.  And at the same time it is a very hot topic in many countries.
Compare the mainstream opinion about homosexuality in Denmark with that in
Nigeria.  Why does this dialogue so easily turn into a battle? Where is the sin?

(:count:`item`) After many centuries of research we can now assume with
scientific evidence that homosexuality does not harm anybody. Therefore we
cannot any longer consider it a sin. It is just an extraordinary sexual
orientation. It has been considered a sin for historic reasons. The fact that it
was considered a sin by biblical authors says as much about God’s plan as the
fact that slavery wasn’t.

(:count:`item`) If homosexuality is not the sin, where is the sin? Can it be
that speaking about it is a sin? Indeed sexuality is a topic that should always
be handled with prudence. On the other hand, initiating a controversial dialogue
in a decent way is a normal step of our learning process. Raising the topic
cannot be a sin. Neither can our difficulties to find consensus be considered a
sin because our personal convictions about sexuality are deep in our hearts, in
the lower layers of our consciousness. A human's opinion about sexuality are
unlikely to change during his or her lifetime. So it is normal that this
discussion needs more than a few generations

Two new sins
------------

(:count:`item`) If speaking --even emotionnally-- about topics like abortion or
homosexuality is not the sin, where is the sin? Fratelli Tutti started to point
out that the sin has to do with money and industry. Many news industries make
money from people who get excited and speak hatefully. Some medical industries
make money from harmful wishes caused by new types of desires, e.g. sexual or
cosmetic fantasies. Many entertainment, technology and food industries make
money by cultivating desires that are harmful to the consumer or to the Earth.

(:count:`item`) But we cannot say that industry or money as such are sins: they
are a neutral infrastructure, which can be used to do good or to do evil. We
start to understand that the sins are certain law systems that allow
corporations to make money this way.

(:count:`item`) The Synodal Church will be the first church institution to
declare two established law systems as :term:`collective sins <collective sin>`.

(:count:`item`) We all are guilty because our civilization created these laws.
The Synodal Church will condemn these law systems as the most important
collective sins, from which we must repent urgently because they cause so harm
that the Earth as our common home is in danger. Repenting from these sins will
be difficult because they have deep roots and because much wealth depends on
them.

(:count:`item`) The first of these collective sins is **granting unlimited
profit** while demanding only limited responsibility for the risks. This idea is
rooted in limited liability law, which was first enacted by the state of New
York in 1811. It causes the strong to become even stronger, and the weak to
become even weaker. It leads to screaming injustices and causes harm to many
creatures. The modern world is built on two centuries of industrialisation, much
of which was built by equity finance, which relies on the idea limited
liability. Limited liability corporations are the key to industrial capitalism
(economist.com, Wikipedia).

(:count:`item`) The second of these collective sins is **preventing others from
using published knowledge**. By published knowledge we refer to publications
formulated as text, picture, sound, movie, software source code or any other
media. It covers publications of any investment size, ranging from spontaneous
postings in an Internet forum to books, songs, movies, scientific reports or
patents. The established copyright system mixes up the (legitimate) right to get
identified and honoured for your work of creating these public resources and the
(illegitimate) right to control their usage. It  creates a social and economic
system that benefits some strong actors while causing harm to most people by
limiting their liberty. It causes social disorder. It leads owners of
proprietary knowledge to use malicious strategies in order to increase their
power.

(:count:`item`) The idea that published knowledge should be considered a public
resource are not new. Already in 1985 Richard M. Stallman designated
intellectual property as a seductive mirage. Another software developer, Eric S.
Raymond did similar work. Both remained focused on software, and both also
caused controversial battles on unrelated topics. In 2010 Lawrence Lessig
extended the idea to all forms of published content. Both failed to consider the
Church as their ally, and the Church failed to realize their work as important.

(:count:`item`) The Synodal Church will collaborate with `Creative Commons
<https://creativecommons.org>`__,  to define a new `license option
<https://creativecommons.org/about/cclicenses/>`__ that obliges the publisher of
a derived work to specify the institution that "approves" their work. She will
develop her own *approval system*, an `imprimatur
<https://en.wikipedia.org/wiki/Imprimatur>`_ suitable for the digital era. She
will advise legislations to regulate the usage of filtering systems and develop
the human right to be warned about or even protected against unwanted
information.

(:count:`item`) For example the `Catholic Answers
<https://www.catholic.com/about>`__ website publishes well-written information
that leaves the impression of representing the teachings of the Roman Catholic
church. But it doesn't. Is is run by a group of private persons and its spirit
and message differs fundamentally from those of the Vatican. This is an example
of reputation theft. Hence this website promotes disorder because there are
people in Estonia who believe that "Catholic Answers" represents the true church
more accurately than websites run by the Vatican under authority of the Pope.

(:count:`item`) Another example is the Taizé community who has the problem of
artists who inadvertently publish derived work that harms the spirit of the
original work. The *approval system* opens a possibility for publishing
unapproved derivates without harming the reputation of the original because
consumers are warned that the new content is not approved by the Taizé
community.

How to recognize a faithful?
============================

You recognize a tree by its fruit.
(`Luke 6:43-46 <https://www.bibleserver.com/ESVLukas6%2C43-46>`__)

The :term:`Gospel` calls us to **love reality, and love each of our fellow humans.**
(`Mark 12:28-34
<https://www.bibleserver.com/ESV/Mark12%3A28-34>`__).
See also :doc:`/sc/special`.

"By this all people will know that you are my disciples, if you have love for
one another." (`John 13:35 <https://www.bibleserver.com/ESV/John13%3A35>`__)

"Not everyone who says to me, ‘Lord, Lord,’ will enter the kingdom of heaven,
but the one who does the will of my Father who is in heaven". (`Matthew 7:21
<https://www.bibleserver.com/ESV/Matthew7%3A21>`__)


Celebrating together
====================

(:count:`item`) Liturgy and rites play an important part when teaching the
Gospel because the Gospel is a divine message, far more than any teaching can
express. On the other hand, teaching the Gospel is more important than insisting
on a given rite or liturgical form.  How can we celebrate together, united in
diversity?

(:count:`item`) The Synodal Church will foster a diversity of rites that
respects and unites the existing traditions. She will provide a system of
approvals that relates different rites to each other and helps the faithful to
choose what suits them best.

(:count:`item`) Changes in rites and liturgy are especially difficult and
dangerous because a small detail can seem "wrong" and trigger allergic
reactions. Once you are used to some detail, you are reluctant to change.
Compare for example a Catholic Mass with a service in a Baptist community. Look
at the discussion about the Old Mass. Consider the emotions of
Lutherans, Baptists or Orthodox people about Catholic rites and teachings.
Consider the emotions of people who grew up without any religion when they got
in contact with Christian rites.

(:count:`item`) Rites and liturgy are a very "traditionalistic" domain because
identifying such details is difficult. "Experimenting" with liturgy is a
temptation for creative natures, but can cause harm, and recovering from such
harm is even more difficult that changing something.

(:count:`item`) The Synodal Church will teach clearly that the Bible does not
give clear instructions regarding whether female priests are possible or not.
But it will also point out that a concrete church can have her rules for
historical reasons.

Non-religious faithful
======================

(:count:`item`) There are individual persons and organizations (governments and
political parties, non-profit groups) who engage for peace, healthcare,
fraternity, charity, human rights, climate justice or free knowledge. They don't
actively use the Bible as a tool of inspiration and hence their language is not
biblical, but their mission is in harmony with the Gospel (compare `Luke 9:49-50
<https://www.bibleserver.com/ESV/Luke9%3A49-50>`__).  They work for the Kingdom
of God and are a visible fruit of the Holy Spirit.  An illustration and an idea
for a next step is in :doc:`/blog/2022/0119_1010`.

(:count:`item`) The Synodal Church emphasises the importance of religious rites
and practices, but will not require them. This will open the door to such
organizations who can adhere to our rules and receive our approval even though
they do not read the Bible directly as their source of inspiration. We consider
them as "indirectly inspired".


.. _draft.elastic:

Human hearts and the Church
===========================

(:count:`item`) A considerable number of convinced Christians in Estonia seem to
feel irritated or even threatened by the ideas about Synodality presented in
this report.

(:count:`item`) When we see a way of celebrating or understanding that is not
"as we were taught", then we feel irritated and are tempted to say "this is
wrong".

.. I have observed my own
  emotions as a Catholic during twenty years of contact with people of other
  faith cultures.

(:count:`item`) Such feelings are caused by our convictions. Convictions are
part of our hearts. Human hearts are not elastic. Our convictions are based on
our personal history. We cannot rewrite our personal history. More than this,
our convictions are designed by nature to not subdue to our reason.  Every
conviction deserves respect. This is why we must handle every emotion with
respect and prudence.

(:count:`item`) The teachings of the Church are the synthesis of the convictions
of many generations of :term:`faithful`.

..
  They have evolved as our response to natural human fears.
  We all suffer from miscellaneous fears that are a result
  of our personal history. We all are only partially conscious about our fears.
  Our fears, especially when many members of a community feel them, naturally have
  influence on the teachings of the Church.

(:count:`item`) Every change in a teaching causes fear.
It is the job of the Church to discern which fears are "good" and which are bad (or useless).
The Gospel calls us to overcome bad and useless fears.

(:count:`item`) Overcoming a fear does not mean that you personally must stop
feeling or experiencing it. Fears are based on our convictions, which we cannot
change by our will. Even when we sometimes wish we could. God never calls us to
do something that we are not able to do.

(:count:`item`) We cannot change our convictions, but the Church can change her
teachings by making conscious synodal decisions. The Church is an organization,
not a human being. Organizations have no emotions. Overcoming bad and useless
fears means to adapt our master teachings as needed. Teachings are more flexible
than human hearts. This is why teachings must change first while human hearts
will follow later. Refusing to update a teaching that needs to be updated would
increase disorder.

(:count:`item`) No human will ever be free from all fears. We have inherited
these fears since the day we left Paradise. But by cultivating the Gospel, the
risen Christ can little by little make himself at home in our heart and help us
to evolve out of Hell into Heaven.

(:count:`item`) Believing in the Synodal Church then means to decide at least in
the conscious part of your mind: "Even if I won't get rid of these particular
fears during my lifetime, I will decide to not teach them to our children." Mark
9:14-29 calls us to cry out with the father of the muted child: "I want to
believe! Help my unbelief!"


Glossary
========

.. glossary::

  apostle

    An appointed individual who received authorization and responsibility to do
    a particular job.

  apostolic governance

    A style of governing based on :term:`apostles <apostle>` who are seen rather
    as servants than as masters.

  discernment

    A synodal method used to summarize the opinion of a community.

    It relies on our belief that the :term:`Holy Spirit` helps us to "sort the
    wheat from the chaff" and "intuitively" select what is important.

  faithful

    Anybody who cares about their :term:`faith` and believes in the
    :term:`Gospel`. See also :doc:`/talks/faithful`.

  faithless

    Anybody who does not care about their :term:`faith`.

  Gospel

    The message brought to humanity through :term:`Jesus Christ`.

Unclassified chunks
===================

(:count:`item`) The SC will develop, publish and maintain advice on political
questions.
