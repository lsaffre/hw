=============================================
What queer people would say to the pope
=============================================

Here is what I imagine that queer people and their friends have to say to the
Roman-Catholic church. It is work in progress. Please contact me if you want to
contribute. Maybe we could translate this and publish it on
https://petitsioon.ee in order to collect signatures. Before doing so, I need
help for the final formulation.

Dear Holy Father,

we are often disappointed when we hear and see what many Christians say and
think about homosexual people.  For example the `EKN position paper about
homosexuality <https://ekn.ee/eesti-kirikute-noukogu-seisukoht-homoseksuaalsuse-kusimuses/>`__ does not make us say
"See how they love one another" (`Tertullian
<https://onecanhappen.com/2020/04/16/tertullian-see-how-they-love-one-another-they-say-about-us-how-they-are-ready-even-to-die-for-one-another-church-in-197-ad/>`__)
or that they "have love for one another" (`John 13:35
<https://www.bibleserver.com/ESV/John13%3A35>`__).

Many of us are really angry with the Church.
This is more than unfortunate, it is a serious issue because `extra ecclesiam
nulla salus <https://en.wikipedia.org/wiki/Extra_Ecclesiam_nulla_salus>`_. We
want to help the Church on her synodal journey of seeking what is good and
true\ [#synod]_.


- It is true that we happen to collaborate with an entertainment industry that
  uses the situation for making money.

- It is true that we happen to be impatient or provocative.

- It is true that we happen to not be prudent. We agree that sexuality is a
  topic that deserves delicateness. Especially children must be protected from
  getting answers to questions that they didn't ask.

- It is true that we sometimes fail to understand that a life-long binding
  makes sense only when there is a life-long purpose like a consecrated life
  or a biological family.

We acknowledge that the topic of :doc:`/lgbt` is a :term:`controversial
question`, being discussed fervently among Christians in the whole world. We
acknowledge that individual humans have strong opinions and beliefs about this
topic. We do not blame anybody for having a strong opinion. We acknowledge the
importance of sexual polarization for the human race.

We ask you to please explain to the Church:

- Homosexuality is an extraordinary sexual orientation, not a :term:`disorder`.
  A disorder is an attitude that hinders you from loving your brothers and
  sisters and disturbs social behaviour. Homophobia is a disorder, queerness
  isn't.

- Queer people are very welcome in the Church. Homosexuals are a gift to
  humanity that the church has unfortunately refused to accept for a long
  time.

- Some queer people are called to live a life in celibacy, some as eremites,
  some as consecrated people. Others are called to found a biological family
  with a partner who values them so they can find workarounds for their
  particular couple.

- Same-sex couples make sense under civil law and are no obstacle to being a
  fully recognized member of the Church.


Note: Above text has never been approved nor even been discussed by anybody in
Estonia. I lacked energy and time to organize any discussions, especially
because I was the only member of the :term:`Sinoditiim` to consider talking with
LGBT people.


..
  rubric:: Avalik pöördumine Eesti Kirikute Nõukogule

  We, the signers of this letter,

  We feel obliged to say that the following two documents need a fundamental revision:

  `EESTI KIRIKUTE NÕUKOGU SEISUKOHT HOMOSEKSUAALSUSE KÜSIMUSES
  <https://ekn.ee/eesti-kirikute-noukogu-seisukoht-homoseksuaalsuse-kusimuses/>`__ (16.10.2008)

  `SEISUKOHAVÕTT HOMOSEKSUAALSUSE KÜSIMUSES
  <https://ekn.ee/seisukohavott-homoseksuaalsuse-kusimuses/>`__ (18.11.2010)

  These position papers reflect an opinion that is strongly biased towards one
  camp of the controversial question.  While a clear position might please those
  faithful who share this position, it excludes those of the other camp.

  The Board of Estonian Churches is an institution, not an individual human.
  Furthermore its goal is to represent all faithful living in Estonia.

  We ask the Board of Estonian Churches to act as a :term:`synodal community`.
  Silencing down minority opinions in controversial discussions is not the synodal
  way of finding peace because it leads to lazy compromises, loss of motivation
  and increased work-to-rule.\ [#consensus]_

  We ask the Board of Estonian Churches to fundamentally review the two
  aforementioned documents in order to acknowledge both camps of opinions found
  among the faithful in Estonia.

  Suggestion for a new position paper:
  https://belglane.saffre-rumma.net/arvamus/queer/




.. rubric:: Footnotes

.. [#synod] See https://sinod.katoliku.ee/en/2/#who-can-participate

.. .. [#consensus] See also :doc:`consensus`.
