===============================
Teaching the Gospel
===============================

The main activity of the Synodal Church will be to publish and maintain reliable
:term:`teachings <church teachings>` about the Gospel.

.. glossary::

  church teachings




.. contents::
   :depth: 1
   :local:

How to teach the Gospel?
========================

The mission of the :term:`Church` is to "announce" the Gospel to all peoples.

You cannot announce something without having reliable knowledge about it. But
what is knowledge?

.. glossary::

  individual knowledge

    The sum of beliefs you rely on, the result of what you have learned during
    your personal history. Also called :term:`faith`.

Community knowledge is analogue, but with a fundamental difference:  it isn't
stored in your heart. It is stored somewhere else. It needs a medium. This
medium is the collection of texts we call teachings. Every community is defined
by its teachings. Teachings are the "heart" of every community.

.. glossary::

  teachings

    The memory of a community, the collection of texts that defines it.

The :term:`Church` has developed a rich treasure of :term:`teachings` about the
:term:`Gospel` during her long history.

Meanwhile humanity has entered the digital era. If already the advent of the
printing press 500 years ago caused revolutionary changes to the way of teaching
the Gospel, how could we assume that the digital era will be less revolutionary
for our work?

Every teaching *includes answers to moral questions*. No teaching can be
ethically neutral. Even a teaching about how to cook an egg assumes certain
choices regarding moral questions (e.g. "Is it :term:`good` to eat eggs?" or "If
it's okay to eat them, isn't it better to eat them uncooked?"). The Gospel does
not say "everything is okay". There are things in this world that are *not*
good. The Gospel *is* a moral message and gives answers to ethical questions. It
tells us what is good and what isn't.

Every teaching is *meant to be reliable*. That's why teachings exist. A teaching
makes no sense when nobody relies on it.  But what can we teach reliably about
the Gospel, which we consider a divine message, which by definition is beyond
human knowledge and human language? Or more shortly: How to explain the
unexplainable?

The teachings of the Bible
==========================

The Bible is a first answer to this question. It is acknowledged by all scholars
of all religions as a historic text, and as such a milestone in human history.

The :term:`Church` is the community of those who use the :term:`Bible` as their
:term:`Holy Scripture`, as the immutable base of their :term:`teachings`.

But the Bible is a very fundamental document. It can give contradicting answers
to certain concrete questions. It can get interpreted in different ways, leading
to different sets of teachings.  Each :term:`church institution` has its own set
of teachings.   While big parts of these different teachings are in harmony with
each other, some of them differ considerably among different church
institutions. Which shows that there must be some problem somewhere.

Teachings can change
====================

The Synodal Church will make clear that teachings are neither eternal nor
immutable.  The Gospel is eternal, the Bible is immutable, but teachings aren't.
Teachings are dynamic and need constant maintenance.

This is because teachings are "only" human-made responses to the Gospel. Ideally
they are in harmony with :term:`God's plan`. But it would be an illusion to
claim that they are perfect.

Publishing a teaching and then discovering that it need to be reviewed is an
integral part of our learning process.  We are on a journey, we do not stand
still.

Human hearts are not very flexible.
Teachings are more flexible than human hearts.
Teachings must change first, human hearts will follow.


Teachings aren't perfect
========================

The ultimate goal of every teaching is to be **true**, i.e. that it reflects
:term:`reality` without distorting it.

We may trust that our teachings are the most true teachings in the visible
world, but must keep in mind, at least in some corner of our mind, that no
teaching about God can be perfect.

The traditional attitude of the :term:`Roman Catholic` church is that "our"
teachings are the true ones. History shows that this approach doesn't work. See
:ref:`sc.other.christians`.

The teachings that have been developed by other church institutions cannot be
ignored. We must learn from them. Loving our neighbour means to give their
teachings the same careful consideration as our own teachings. In case of
conflict we cannot apply lynch justice. We need a superior authority.

Rule of thumb: As long as one heart of a faithful disagrees with a teaching,
this teaching can't be the full truth. (Okay, if there is really only one single
human in the world who disagrees with a teaching, we can assume that this human
is not a faithful. This rule is just a rule *of thumb*.)

Teachings can't contradict science
==================================

"the tree is known by its fruit" (`Mt 12:33 <https://www.bibleserver.com/ESV/Matthew12%3A33>`__)

When some new discovery brings :term:`scientific evidence` that a given teaching
is suboptimal or even wrong, the the teaching needs to get updated.


Teachings should be clear and accessible to everybody
=====================================================

An important feature of every teaching is to be **clear**. I dare to say that
the current teachings of the Roman Catholic church are *not* very clear. You
need years of education before you can claim to understood them more or less. As
a software developer and businessman I have certain visions about what means
"clear" in the digital era.

The teachings of the Synodal Church must be accessible. Every human must be able
to access them without paying a license fee and without being distracted by
commercial advertisements.


Teachings are published content
===============================

The Synodal Church will learn from the Free Software and Free Knowledge
movements (Wikimedia Foundation, Free Software Foundation, ...). These movements
don't call themselves "religious", but they work for a better world. Their
language is different from the Church, but their mission is fundamentally the
same. They are a visible fruit of the :term:`Holy Spirit` and they operate using
synodal principles. The Synodal Church will embrace these movements use and even
optimize the technologies developed by them. See :ref:`sc.other.nonreligious`.

The :term:`Synodal Church` will use concepts and technologies developed by the
:term:`Free Software` community for publishing and maintaining documentation
about complex systems. An example of such a technology is the one used by
Wikipedia (but note that Wikipedia is an Encyclopedia, not a teaching). Another
example is my *Human World* website, which uses free software products like
Python, docutils, Sphinx, Git and others.


Rules are a type of teachings
=============================

.. glossary::

  knowledge element

    A :term:`definition` or a :term:`thesis statement`.

  definition

    The convention that a given *name* has a given *meaning* in a given
    *context*.

  thesis statement

    A short statement that is considered important to discuss about.


The Synodal Church won't be a kingdom of this world, it won't have its own
`executive <https://en.wikipedia.org/wiki/Executive_(government)>`__.  The rules
it formulates aren't laws. You are free to accept them or not.

The Synodal Church will develop :term:`directives <synodal directive>`, which
are rule propositions.  Directives are used by the concrete synodal institutions
when formulating their rules.

The Synodal Church itself, as an institution, will of course define  rules for
regulating her own activities. And these rules will be a part of her teachings.

But the activities of the Synodal Church a very limited compared to those of the
concrete institutions. The Synodal Church will not regulate the activities of
the concrete institutions, it deliberately leaves this job to them.

Rules are different from teachings in that they are more concrete.
Rules are often defined in documents that legally bind two business partners.
Unlike rules, teachings never force us to follow them. They invite us.
