=============================
The workers in God's vineyard
=============================

I imagine that the Synodal Church will differentiate between pastors, teachers
and priests.

.. glossary::

  pastor

    Cares for the souls of a community. Practices the art of listening and
    spiritual mentoring.

  priest

    Celebrates :term:`sacraments <sacrament>` of Eucharist, Baptism, Marriage
    and Consecration. Practices the art of liturgy.

  teacher

    Explains the Gospel to other people.



Inspired by
2021-11-18 `Cardinal Sarah Calls Catholic Priests to Spiritual Renewal in New Book
<https://www.ncregister.com/cna/cardinal-sarah-calls-catholic-priests-to-spiritual-renewal-in-new-book>`__
