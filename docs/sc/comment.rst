=============================
My comment on the synthesis
=============================

On 2022-07-22 I sent the official Estonian synthesis together with the Bishop's
accompanying letter to the Secretariat of the Synod of Bishops. At that moment
the synthesis was not published. But here is my comment on the synthesis, which
the Bishop quotes in his accompanying letter.

..
  This page is still under development. I will send my comment to the Bishop on
  June 20th. I am still collecting feedback, and I might change this page
  accordingly.


.. The synthesis we decided to submit to the Synod on Synodality shows my failure.

Catholics in Estonia are a small community. We humbly did our
best to fulfil the instructions received from the Secretariat of the Synod. Our
synodal team consisted of five lay persons and one priest, all lay persons were
active members of the Tallinn Peter and Paul congregation. Receiving inspiration
from the Holy Mass, we managed to focus on administrative topics during our
meetings.  We invited other confessions and non-religious people to participate
in our synodal meetings, but as expected they showed little interest. During the
discernment phase we proceeded systematically: each member received two of the
ten topics to focus on, then read the 17 reports received during the
consultation phase and sent their observations to the main editor. The main
editor synthesized these observations into a final draft, which she presented to
the team on 2022-06-04. The team suggested two corrections. Our draft was given
to the Bishop on 2022-06-08. We carefully avoided causing controversial noise in
public. We did not publish our final synthesis because we consider it an
intermediate part of the Synod. (P.S. see :doc:`/blog/2022/0817_0344`)

My thanks to the members of the synodal team, who faithfully did their job
despite my disturbing presence. My special thanks to the main editor who wrote
the final synthesis despite her high work load. My special admiration to the
Bishop, who remained a wise and constructive advisor even in the most critical
moments.

I was the only disturbing element in the team, the only to remain deeply
**dissatisfied** with our synthesis because I feel that it leaves out an
important part of the church's reality.

In a personal endeavour I had formed a second team during the consultation
phase, this time taking care for diversity. This "Team B" was inter-confessional
and consisted of mostly Protestant, several Catholic, one Anglican and one
Orthodox Christians and even of non-religious people. It published 17
intermediate versions of a draft report, collected feedback from several dozens
of contributors. The team met 4 times 4 hours in a prayerful atmosphere, where
we discussed about our draft. Their final draft was produced by an editor team
of 4 authors. Besides the four authors, ten more team members agreed to have
their name published in our report, which we submitted on 2022-03-31 at the end
of the consultation phase. The work of this team is abundantly documented on my
personal website: https://hw.saffre-rumma.net/sc

And the work of this inter-confessional team is not mirrored, not even
*mentioned*, in our synthesis. The synodal team did not consider it noteworthy.
My plan had been to have the two teams sit together with the job of writing a
single text to which both teams would agree.  When I realized that this was not
going to happen in due time, my hopes collapsed.  I even considered leaving the
church. I had failed across the board:  My job as the Synodal Contact Person was
to form and lead a team that synthesizes what people have to say at the Synod.
But already when forming the synodal team I failed to care for diversity.
Furthermore I am impatient, do things on my own and too quickly. I lack
experience with managing bigger projects. I don't speak clearly when needed. My
provocative style causes counter-productive emotions. But there is no way out of
the church once you "have believed, and have come to know, that [Jesus] is the
Holy One of God" (`John 6:66-69
<https://www.bibleserver.com/ESV/John6%3A66-69>`_). And indeed there is no
reason to despair. As the Bishop wrote, "if the Holy Spirit didn't blow
sufficiently in Estonia during this phase of the Synod, let's hope that it blew
elsewhere." And I would add that it actually *does* blow quite much also here in
Estonia, we are just very good in silencing things down.

.. And yet I hope that I have learned
  some important lessons and that I will learn more.

The fact of having **failed to reach a consensus** between the
two teams might be our most valuable contribution to the Synod. And I remain
confident that God can turn even our unfinished reconciliation into a useful
contribution to the Synod.

I hope that the Synod on Synodality will encourage all church leaders to take
the fundamental steps required for reconciling and uniting in full diversity all
those who follow Jesus.



| Luc Saffre
| Synodal Contact Person for the Apostolic Administration of Estonia
| June 2022
