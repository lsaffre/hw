====================
Synodal Church
====================

This section of my website contains material about the :term:`Synod on
Synodality`, on which I worked as the *Synodal Contact Person* for Estonia
between September 2021 and June 2022. I prematurely retired from that job
:doc:`in October 2022 </blog/2022/1004_2200>`.

.. toctree::
  :maxdepth: 1

  what
  history
  camps
  vision
  report
  justme
  comment
  synthesis

.. rubric:: Unfinished work

.. toctree::
  :maxdepth: 1


  conclusion
  poems
  test

.. toctree::
  :hidden:

  main
  appendix
  parrhesia
  wip/index
  changes
