==========
Appendix
==========

Some thoughts about the :term:`Synodal Church` that didn't make it into my
report.

.. toctree::
  :maxdepth: 1

  prayer
  workers
  glossary
  fool
  consensus
  transparent
  not_a_kingdom
  all_peoples
