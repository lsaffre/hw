=============
About divorce
=============

An attempt to reconcile divorced and remarried people with the Church.

We acknowledge that :term:`marriage` is a life-long :term:`sacrament`. It is a
holy sign and promise given by God to a man and a woman who plan to give birth
to children and to raise them in a family.  This sacrament is holy, that is, it
exists beyond any human reason.  A marriage that is in a crisis, even in the
deepest imaginable crisis, even when every therapist advises to divorce, is
still blessed, and there remains hope that it will come out of the crisis and
grow stronger than anybody would imagine. The Church has no way to divorce a
marriage because that would mean a failure of our unconditional submission into
God's will, a failure to fully hope for God's mercy, despite all human reason.

But above truth is valid at the institutional level. When speaking about
individual humans, the Gospel introduces another rule, which says that humans
are more important than rules.   This sheds a new light on reality. Furthermore,
our civilization evolves. As social network systems of modern civilizations
evolve, it becomes possible to legally handle complex patchwork family
situations. A divorce no longer means a serious economical or social threat to
the partners or the children. Being open-minded means to be able to change our
teachings without bitterness or shame.

The Synodal Church will acknowledge that marriages can break because individual
humans can fail to see the unconditional hope they have received from God
through a sacrament. We see and confirm that even in such cases, God's mercy
never ends. God can give even to divorced individual humans another chance, a
new sacrament of marriage. A synodal Church can confirm remarriage even when one
of the partners is divorced.  Under certain conditions. For example the divorce
of the previous marriage must have been legally confirmed and executed. This
does in now way reduce the holiness of the sacrament of marriage.
