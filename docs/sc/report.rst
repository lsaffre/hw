=============================
The inter-confessional report
=============================

In December 2021, disappointed by the :term:`sinoditiim`,  I formed an second,
"alternative" team because I still felt responsible for doing what the *Pope*
asked me, which was :ref:`more than what the Bishop asked me <i_want_more>`.
During the next months I lead this team to write a 10 pages report where we
synthesize "what all Estonians have to say about Synodality". In this team we
used the iterative method of starting with a draft and developing this document
during working sessions.

Here is the final result of our work:

`v18.pdf <https://hw.saffre-rumma.net/dl/2022/talgud/v18.pdf>`__

.. I did this as a private initiative and against the advice of the *sinoditiim*.

I submitted this document as our synodal report to the *sinoditiim* on
2022-03-31, the end of the consultation phase.

The document has four authors (three theologians and me).  We met four times
during 4 hours in a row (:doc:`sinoditalgud </sc/talgud/index>`) where we
discussed about the content of our report.   If it's true that three Estonians
who discuss about something have at least four different opinions, we can be
proud of this result.

Our report is probably the most representative document about Synodality in
Estonia that has been written during the consultation phase. It has not only
four authors, it also has dozens of participants who contributed to the content,
ten of which publicly confirmed our summary, among them two atheists.

Of course our synthesis is unfinished. We had limited time and resources.
Some groups of people haven't been invited at all.
Some people were invited but didn't want to contribute.
Some people left during the process because they felt offended.
Some contributors did not fully agree with the final result.

..
  A few people expressed refusal and seem to say that we should say something
  completely different. Unfortunately they
..
  I submitted this document to the :term:`sinoditiim` on 2022-03-31, and they
  received a handful of other reports as well.  The **consultation phase** of the
  Synod on Synodality is now over, and the :term:`sinoditiim` started the
  **discernment phase**, which lasts until August 2022 and where they will
  synthesize all reports.

..  This phase
  is internal, but we can hope to see at least the result of their work, which
  will theoretically reflect what Estonians *really* have to say about Synodality.

.. rubric:: Background

Between December 2021 and March 2022, I asked a lot of people in Estonia: what
do *you* have to say about synodality? I wanted to reach as diverse people as
possible. I even wrote a :doc:`special invitation <special>` for those who don't
care about the church.

I consider it an example of "synodal teamwork" despite the fact that it was my
personal endeavour, I did it without having been asked to do it.

The story continues as explained in my message :doc:`/blog/2022/0331_2021`.


.. either the latest published version (`12.pdf
  <https://hw.saffre-rumma.net/dl/2022/talgud/12.pdf>`__) or
  the version I am working on (`v13.pdf
  <https://hw.saffre-rumma.net/dl/2022/talgud/v13.pdf>`__).


.. toctree::
  :hidden:

  special
  special2
  draft
  synodal
  contribute
  talgud/index




..
  (:count:`item`) Now please read our draft: `ReportEstonia.pdf
  <https://hw.saffre-rumma.net/dl/2022/ReportEstonia.pdf>`__. See you back here
  when you have finished reading the document. Take your time.

  written in a truly synodal community experience: I hope

  Before participating, please read four pages about this project and about how to
  contribute: `ReportEstonia-Contribute.pdf
  <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-Contribute.pdf>`__


..
  Everybody is invited to respond, whatever your opinion about church and
  religious activities. :doc:`special`. You are invited to share this document on
  your turn with everybody. It is public and my goal is to collect responses from
  as many faithful as possible.

.. The following pages on my website refer to this page: .. refstothis::



..
  Welcome to my online game "Let's write a letter to the Pope". Playing this game
  is easy and free. You just read, in sequence, the following instructions and
  follow them.

  (:count:`item`)  You should ideally read everything in sequence.  You may
  interrupt at any moment and continue at a later time. If you feel impatient and
  prefer to skip a series of instructions, then try to return back to where you
  left at a later moment.

  (:count:`item`) This website is owned and controlled by me and it uses no
  cookies. It doesn't track your activity in any way. I will never know that you
  have been trying to read this unless you tell me. When you interrupt, you must
  yourself keep track of the paragraph number you reached so far.

  (:count:`item`) At any point you may give me your feedback. Either face to face,
  by phone, by instant message or via email. I will treat every feed­back
  confidentially. You can call me any time --day and night-- because I am used to
  mute my phone when I don't want to be disturbed.

  (:count:`item`) Please forgive my style of speaking to you with instructions.
  Don't take it personally. This style is called a `tutorial
  <https://en.wikipedia.org/wiki/Tutorial>`__ and is often used for learning. This
  is a game. A social experiment.

  (:count:`item`) You participate by your free will. Feel free to leave at any
  moment if you don't like this game any more. Before leaving I would ask you one
  favour: **tell me where you stopped.** And if possible **why** you stopped. Even
  if you stop right now, informing me that you decided to leave at this point is
  an interesting piece of information for me because it tells me "Something went
  wrong there".

..
  In September 2021 I have accepted to do a big job: I promised to write a report
  to the Vatican. And I invite you to help me with this.
