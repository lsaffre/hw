==========================================
Special note to those who wonder why I ask
==========================================

You might wonder "Why should I care about the church?" or "Why would Luc be
interested in what I have to say to the Pope?". Thank you for wondering.

I ask because I hope that you and me believe in the following *basic code of
conduct* for all people who want to live in peace on this planet: **Love the
world as it is, and love each of your fellow humans as they are.** (:doc:`more
</talks/bcc>`).

If this leaves you indifferent (you "don't care"), then I don't need your help.
Thank you for having tried to help me. You may stop reading now.

Otherwise you may now enter the next level: :doc:`special2`
