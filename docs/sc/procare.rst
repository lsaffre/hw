:date: 2021-10-31 03:29

====================================
More about abortion
====================================

Comments about what is written in :doc:`/sc/report` about :term:`abortion`
(80-82).

See also :doc:`/abortion`.
