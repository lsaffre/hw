===============
Sinoditalgud
===============

This is the dashboard page for the :term:`sinoditalgutiim`.

Our meetings are not public, they are only for those who received my personal
invitation. If you want an invitation, let me know.

.. We meet at **Vana Kalamaja 31** (our host for this meeting is `EELK Saksa Lunastaja Kogudus <https://kirche.ee/>`__).



.. At the moment 5 participants have registered.

.. toctree::
  :maxdepth: 1

  1
  2
  3
  4
  about
  rules
