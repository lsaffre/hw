==========
Team rules
==========

To the members of the :term:`sinoditalgutiim`.

Membership
==========

(:count:`team`) I called you to join this team because I have the feeling that
we might need just you. You confirmed that you agree to join this team. You are
free to revoke your decision and leave us any time (simply let me know me your
decision).

(:count:`team`) Our goal is to work as a :term:`synodal community` to
participate in the Synod on Synodality.

..
  on my
  original draft in order to "develop" it together into "our" report to the Synod.

(:count:`team`) Being a member of this team means --at least-- that you are
praying for us and that you are interested in our work. It also means that I am
interested to hear your feedback now and then. And of course you are invited to
contribute as much as the Spirit asks you to contribute.

(:count:`team`) At the moment our team has 18 members. I am currently the only
one who knows you all and I promise to keep your names confidential.  You will
eventually get to know each other during our meetings.

(:count:`team`) I use a mailing list for sending messages to the whole team at
once.  You recognize my mails to the team by their subject starting with
"[synod]" and because they go to the address sinod@lino-framework.org.

(:count:`team`) This mailing list runs on one of my own servers and only I can
see the list of members. Your privacy is guaranteed **as long as you do not send
any email** to the address sinod@lino-framework.org.

(:count:`team`) We are not working in the dark. The archive of messages to our
mailing list is publicly visible at the following address:
https://lists.lino-framework.org/pipermail/sinod/

Our job
========

(:count:`team`) On March 31th I will submit our report to the
:term:`sinoditiim`, i.e. we simply `participate in the official consultation
process <https://sinod.katoliku.ee/en/2/>`__. Our report will be only one among
many submissions.

(:count:`team`) Independently of whether our report will find its way to Rome,
already the process of writing it will be for us all an *experience of
synodality*.

(:count:`team`) In March I will ask each member whether you publicly consent to
the result of our work. It is not needed that all members do so. Those who don't
consent publicly (1) may consent privately (2), remain undecided (3) or
explicitly disagree with our report (4), and I plan to publish the number of
each of these opinions as part of our report.

Meetings
========

(:count:`team`) I guess that we will meet about 4 to 10 times. It seems that
most meetings will be on Wednesdays from 12:30 to 17:00 (but that's not a rule).

We start each :term:`talgu` with a common dinner (which is integral part of the
meeting).
