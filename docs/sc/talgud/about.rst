===========================
About the *sinoditalgutiim*
===========================

Don't mix up the :term:`sinoditalgutiim` with the :term:`sinoditiim`.

.. glossary::

  sinoditalgutiim

    A team of diverse people I asked to help me to write an *ecumenical* report
    to be submitted to the :term:`sinoditiim` in March 2022.

  talgu

    A meeting of a group of volunteers who work together for a `communal project
    <https://en.wikipedia.org/wiki/Communal_work>`__ during a limited time.


I formed\ [#form_a_team]_ the :term:`sinoditiim` in September 2021 after having
myself been appointed as the *Synodal Contact Person* for Estonia by the Bishop.
The Bishop then confirmed us as a team during the diocesan opening mass of the
Synod on October 17th.

The :term:`sinoditalgutiim` was also formed by me, but as a private initiative.
I informed the Bishop about my plans, but acted independently without asking his
opinion or approval. I am currently the only one who knows all members of this
team. They will eventually get to know each other during our meetings. I
maintain a separate page with  :doc:`rules <rules>` for this team.

.. rubric:: Footnotes

.. [#form_a_team] To *form a team*, in :term:`apostolic governance`, means that
  the team leader chooses and appoints members personally one by one. See also
  :doc:`/talks/teamwork`.
