=========================================
The church is not a Kingdom of this world
=========================================

If we want the Church to be synodal, we must hinder it from becoming a kingdom
of this world.

Every church institution is also a social network in the real world. Every
successful social network has the natural tendency of becoming wealthy and
powerful. People donate money or material ownings to the Church, e.g. when they
die as a heritage, or when some miracle happens in their life and they want to
express their gratefulness. This is normal because the Church is the
representative of God in the visible world. The Church, as a real-world
corporation, constantly grows in power and material wealth.
We cannot avoid this because it is a natural law.
It would be foolish to try to avoid it.
The important question is how to manage this wealth.
