====================
Introduction
====================

This document is how I introduced the project until March 2021. I didn't adapt
it since then because it can help to understand what happened afterwards.

.. contents::
  :local:


The Synod on Synodality
=======================

In October 2023 there will be the 16th "General Assembly of the Synod of
Bishops" in Rome, a world-level meeting of bishops that will potentially lead to
a series of really important decisions regarding the *modus vivendi et operandi*
of the Roman Catholic church. It's *not* about whether priests or queer people
may marry, or what to when some important decision turns out to have been a
mistake. This Synod is not about *some hot topic*, it's about *how the church
handles* hot topics in general. I would say that synodality is a topic where the
Church and the "political" world can learn a few fundamentally important things
from each other.

And in order to prepare this synod, Pope Francis asked us to run a consultation
where we ask every human on the globe for their input.  More precisely only
those who "want to help the :term:`Church` on her synodal journey of seeking
what is good and true".  You don't need to be Catholic, the only condition is
your good will (source: :term:`Vademecum`, section 2.1 and :term:`PD`, 18).

Estimations about how many voices this actually means range between 1 and 8
billion. Pope Francis is known to have crazy ideas.

Here is the main question of this gigantic poll:

| A synodal Church, in announcing the Gospel, ‘journeys together’.
| How is this ‘journeying together’ happening today in your local Church?
| What steps does the Spirit invite us to take in order to grow in our ‘journeying together?’

We probably agree that this question cannot have a *simple* answer. That's why
Christians around the world are currently organizing lots of meetings where they
exchange about this question.

What I have to do with it
=========================

I am the official main responsible for organizing this consultation in Estonia
(see :doc:`/blog/2021/0914`). I see it as a honour and a challenge.  At least
one friend :doc:`sees my appointment as a wise decision </blog/2022/0112_2151>`.

My job is to write a text of maximum of 10 pages, which answers the question
"What do the *faithful* in Estonia have to say at this Synod?" And for me,
"faithful" means everybody who cares :doc:`about their faith </faith>`.

I will write this text together with the :term:`sinoditiim`, a group of 5 lay
persons and one priest. We published a website for the consultation phase:
https://sinod.katoliku.ee

Our big job from April to August 2022 will be to write a summary of all the
responses received during the consultation phase. More precisely our report is
not a "summary" but a "synthesis". Some people have a loud voice and speak much.
Some others have a faint voice and speak rarely. We are not asked to write a
statistically representative summary but to apply the method of
:term:`discernment`.

Our summary will be merged with the summaries of all other dioceses in Europe.
And the continental summaries will be summarized again into a summary of all
summaries, and this document (called `instrumentum laboris
<https://en.wikipedia.org/wiki/Instrumentum_laboris>`__) will serve as the
official input for the Synod.

The "fathers of the Synod" (about 300 bishops from around the globe) will read
this document and then meet in Rome in October 2023 in order to talk about it.
They will themselves produce a document with recommendations to the Pope. And
then the Pope alone will decide what to do. That's how the Roman Catholic church
works. I call this :term:`apostolic governance`.

..
  And the big question of all this will be:
  What does :term:`synodality` mean more exactly?
  How does the Church function?
  What steps does the Spirit call us to do?

.. _i_want_more:

I want more than the Bishop
===========================

While working for the :term:`Synod on Synodality` I had the feeling to be the
only Roman Catholic in Estonia to believe that also non-Catholics have something
important to say at this Synod. My definition of :term:`faithful` is somewhat
wider than the traditional definition of the Roman Catholic church.

Many of my friends believe in the :term:`Gospel`. Among them are church-goers
and refusers of institutionalized religion, EKRE voters and SDE voters, priests
from Poland and Gay Christians, antivaxxers and anti-antivaxxers. I wanted to
listen to these people as well. We can imagine that their responses to the
Pope's question vary considerably.

In December I wrote a first draft of "what all these people have to say" at the
Synod. I started to ask my friends about their feedback.  I also informed the
Bishop and the members of the official :term:`sinoditiim` about my work, and as
expected they did not show much interest for it.

In January I went more systematic. I formed another team, the
:term:`sinoditalgutiim`. With this team we will work on my draft until end of
March 2022.

In February I abandoned my approach of suggesting "my" draft as a base for our
work (see :doc:`2022-02-07 </blog/2022/0207_0523>` for details). The project
became more simple: Let's meet in diversity, think, discuss and pray together,
and then let's write our report.

On March 31st I hope to submit "our report" to `the official synodal process in
Estonia <https://sinod.katoliku.ee/en/2/>`__.  Between April and August, the
:term:`sinoditiim` will read our submission, together with all other submissions
coming from Estonia, and write the official summary to be sent to the Vatican.

The report of the :term:`sinoditalgutiim` is likely to cause some discussion
within the :term:`sinoditiim`. And I am optimistic that we will find a consensus
even there.

..
  One can say that my draft is "my" report, my personal opinion. My job was not to
  say my personal opinion but to represent the opinions of all participants. Am I
  answering the wrong question?
