==========================
Can a fool teach wise men?
==========================

I am obviously a fool. A naive amateur who invests considerable time and energy
in order to explain something to wise men.

I made this website by my own private initiative, against the advice of my
Bishop, who is obviously wiser than me.  I know that the Vatican engages hordes
of experts who are even wiser than my Bishop and who have been working on these
topics for many years.

How can I dare to speak about :term:`Synodality` to hordes of wise men? How can
I dare to hope that my Bishop would change his mind and that this website will
receive his benediction until August 2022?

One answer are Bible words like `Luke 10:21
<https://www.bibleserver.com/ESV/Luke10%3A21>`__ and `Matthew 11:25
<https://www.bibleserver.com/ESV/Matthew11%3A25>`__, or `Luke 18:16
<https://www.bibleserver.com/ESV/Luke18%3A16>`__. Another answer is that "all
are invited to speak with courage and parrhesia" (:term:`PD`). There are also
answers from outside of the Church. For example `The Emperor's New Clothes
<https://en.wikipedia.org/wiki/The_Emperor%27s_New_Clothes>`__.

In many modern cultures, fools aren't being listened to. At the best we ignore
them politely and make sure they cause no harm. Sometimes they are actively
silenced down. At the worst they are repressed or even killed.

I believe that listening also to fools is one of the important principles of
:term:`Synodality`.
