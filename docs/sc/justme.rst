================
Just my opinion?
================

The first feedback to my first draft came from my wife, and it was something
like "You are kind of missing the point! Your job is not to write down your own
opinion but to collect and synthesize the opinion of other people!" I often
heard this reaction afterwards, even from the Bishop during our final
discussions.

Short answer: the inter-confessional report is *much less* the result of a
single person's opinion than the official synthesis. The draft of the sinoditiim
was written by a single person and there was quasi no discussion about its
content. None of the contributors had confirmed the document before it was
submitted to the Secretariat.

Some numbers:  :doc:`/blog/2022/0728_0619`
