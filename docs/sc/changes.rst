==============
Change history
==============

Here is a history of the changes in :doc:`my report <report>`.

2021-12-28
==========

Published version 1. Created a mailinglist `sinod@lino-framework.org
<https://lists.lino-framework.org/mailman/listinfo/sinod>`__. TODO: get the
archive to be visible.

A first reaction was that the fictive character of the document needs to be
expressed more clearly in the title and the introduction.

Published `version 2 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v2.pdf>`__.


2021-12-29
==========

See :doc:`/blog/2021/1229_0637`.

Published `version 3 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v3.pdf>`__.

2022-01-03
==========

See :doc:`/blog/2022/0103_0204`.

Published `version 4 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v4.pdf>`__.


2022-01-06
==========

Some grammar and style optimizations.

Published
`version 5 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v5.pdf>`__.



2022-01-08
==========

Changes after feedback regarding abortion. :doc:`/blog/2022/0108_0443`.

Published
`version 6 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v6.pdf>`__.

2022-01-09
==========

I replaced "apostolic management" by :term:`apostolic governance`.

2022-01-10
==========

I reviewed item (18) as a reaction to :doc:`/blog/2022/0108_1508`.

I extended (37) to add some clarifications (see ):doc:`/blog/2022/0109_1442`).

Published
`version 7 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v7.pdf>`__.



2022-01-13
==========

I published `version 8 <https://hw.saffre-rumma.net/dl/2022/ReportEstonia-v8.pdf>`__,
which comes together with four pages of "instructions for
contributors" in a separate document. See :doc:`/sc/report`. There were lots of
input that lead to these changes, but I did not yet manage to them all.


2022-01-31
==========

I published version 9:
`odt <https://hw.saffre-rumma.net/dl/2022/talgud/v9.odt>`__
|
`pdf <https://hw.saffre-rumma.net/dl/2022/talgud/v9.pdf>`__
(modified on 2022-02-17 to fix a copyright issue).

2022-02-16
==========

I published version 10: `odt
<https://hw.saffre-rumma.net/dl/2022/talgud/20220216.odt>`__ | `pdf
<https://hw.saffre-rumma.net/dl/2022/talgud/20220216.pdf>`__. This version was
used as the *instrumentum laboris* for our :doc:`first meeting </sc/talgud/1>`.
