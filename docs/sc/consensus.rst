=========================
Consensus versus majority
=========================

One of the differences between :term:`synodal government` and (classical)
democratic government is that when a minority of the community says "something
is wrong", the community needs to listen. Synodality seeks *consensus*, not
*majority*.

There are topics that need time and patience. As long as there is no consensus
about some :term:`controversial question`, synodality means that the government
must remain undecided.

Of course there are things we cannot change, and we need to accept these things.
It would be a waste of energy to continuously quarrel and complain about them.
And of course it is not always easy to discern the things we can change from
those we can't. If you are really the only one to see a problem, you need to
humbly ask yourself whether your inner voice is right, whether that problem is
really important enough and whether you really have the duty to disturb the
community process with your concerns.

But if you comply with the majority against your inner voice and just because
you are *too* humble (or maybe too lazy, too shy or too arrogant) to talk about
the problem, then you just sweep the dirt under the carpet. The problem will
remain and eventually grow.

Silencing down minority opinions in controversial discussions is not the synodal
way of finding peace because it leads to lazy compromises, loss of motivation
and increased work-to-rule.
