==============================
Announce it to all the peoples
==============================

The main task of the Church is to announce the Gospel to
*all the peoples*.

The priest and lyric Andreas Knapp describes some real-life examples
`Sucht neue Worte, das Wort zu verkünden
<https://www.euangel.de/ausgabe-1-2017/sprache/sucht-neue-worte-das-wort-zu-verkuenden/>`__

Listen to the song `O Come, All Ye Faithful
<https://en.wikipedia.org/wiki/O_Come,_All_Ye_Faithful>`__

If the mountain won't come to the prophet, the prophet will go to the mountain.

Must we insist on the language of the Bible? 
