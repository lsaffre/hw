=================
How to contribute
=================

(:count:`item`) The only official condition for participating in the Synod is
that you want to "help the Church on her syno­dal jour­ney of seeking what is
good and true". If you reached this page (after having read :doc:`what` and
:doc:`draft`), we can assume that you qualify for participating.

(:count:`item`) Think for yourself (or ideally even with your friends or family
members) about the "fundamental question of the consultation phase". Also read
the additional questions (10 topics).

(:count:`item`) If you dislike some part of this document, I invite you to explain me what
is wrong, with boldness and patience. The more you dislike some­thing, the more
your feedback is important. Thank you in advance for participating.

(:count:`item`) I will treat every feed­back confidentially. I will do my best
to publish it on my private blog (after carefully removing confi­dential
infor­mation to protect your privacy) so that other readers can follow our
dialogue afterwards. You can see on my blog how I reproduce your feedback, and
you should notify me if I did not reproduce your contribution correctly, or if
you realize that you want to optimize or even revoke something what you said
earlier (yes, it is allowed to change your mind).
