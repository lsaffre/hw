==========================================
Self-test: What kind of Christian are you?
==========================================

Inspired by (56) of :doc:`report`.

The statements in the following table are seemingly opposing, and yet each of
them is can be found in the Scriptures.

..
  The Scriptures don't express "clear
  positions" but need to be explained anew in every culture and generation.

Exercise: discuss with others about these statements.
How much do you agree with each of them?
Which of them do like most?

.. list-table::

  *
    - God is just and punishes a sinful life.
    - God is merciful and forgives our mistakes.

  *
    - We should be well-organized and obedient.

    - We should be creative and co-responsible. "We must obey God rather than men." (Acts 5:29)

  *
    - We should speak carefully and with reverence.

    - We should speak openly and without fear.

  *
    - We should make no compromises.

    - We should embrace differences and are inclusive.

  *
    - We are strong because we hope in God.

    - We are weak, but we put our trust in God.

  *
    - Our teachings are solid and the truth.

    - Our teachings are inspiring and open-minded.

  *
    - The Bible is self-explaining, our job is to pro­claim it.

    - The Bible needs explanation, our job is to ex­plain it.

  *
    - God protects us from evil.

    - God protects the poor and weak from the rich and strong.

  *
    - You cannot be saved without baptise. No salvation outside of the Church.

    - There are people who follow the Gospel without being baptized (Matthew 7:21)

  *
    - The Gospel calls us to follow the teachings of the Church.

    - The Gospel calls us to love our neigh­bour.

  *
    - Speak out against immorality. Tell your brother when he is wrong (Mt 18:17).

    - Do not look for evil in others, but good.

  *

    - We shouldn't speak loud in public. Live hidden to live happily. "He will not cry aloud
      or lift up his voice, or make it heard in the street" (Isaiah 42:2)

    - We should say our opinion in public. „You are the light of the world“
      (Mat­thew 5:14). “What I tell you in the dark, say in the light, and what
      you hear whispered, proclaim on the housetops.” (Matthew 10:27)
