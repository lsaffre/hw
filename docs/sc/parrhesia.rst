=============================
Message to those who disagree
=============================

If you feel offended by some idea presented on this website --or by the way it
is presented--, I don't ask you to *stop* feeling offended.  You are welcome to
feel offended.  I'd rather ask you to forgive me my :term:`boldness` and to
formulate what's wrong with my thoughts. Obviously my thoughts are not free of
mistakes.

It is important to find a consensus on these questions because when two groups
of people have controversial opinions about an :term:`existential question`,
then one of them is in :term:`Hell` and and the other in :term:`Heaven`. If you
feel that I am in Hell and you in Heaven, then it is maybe you who can help me
to get out of it. We are together on our way of searching what is good and true.

My vision pushes me indeed into the role of a :term:`bull in a china shop`.

When you realize that you are a bull in a china shop, the best reaction is to
get out of the shop and to limit the damage as much as possible. But in the
present case there is no way out. We *are* together in a same boat, the
:term:`Church`, whether we want it or not. We have no choice but to find a
:term:`modus vivendi`.

When you have a bull in your china shop and can't get it out of it, then you
need to find a way to gain control over the bull. A traditional agricultural
method to calm down bulls is a `nose ring
<https://www.4wileyfarm.com/blog---down-on-the-farm/bull-bling-why-we-put-nose-rings-in-our-bulls>`__.
But I guess that *you* would *not* like *me* to use your nose ring. So please
stop considering using mine. Remember the `Golden Rule
<https://en.wikipedia.org/wiki/Golden_Rule>`__.

.. feeling offended is a form of :term:`fear`.

Feeling offended is an :term:`emotion` and as such does not need to be
justified.

The teachings of the Church that need to be marked as obsolete have evolved
because *they respond to natural human fears*. We all suffer from these fears
more or less, depending on our personal history.

The fears triggered by the Synod on Synodality are fears that the Gospel calls
us to *overcome*.  Overcoming these fears does not mean that you personally must
stop feeling or experiencing them. These fears are based on convictions, which
themselves are based on our personal history. We cannot undo our personal
history just by making a conscious decision. God never calls us to do something
that we are not able to do.

But conscious decisions *are* our choice. There are :term:`good` fears and "bad"
fears. The Gospel calls us to *cultivate* the good fears and to *overcome* the
bad fears. Overcoming these fears means to discern what is good and what is bad.
The Church is an institution, not a human being.  Institutions have no emotions.

Trusting in the Synodal Church then means to deliberately decide, at least in
the conscious part of your mind: "I do not want to teach these particular fears
to our children. At least in theory I *do* wish I could get rid of these fears!
Even if I cannot get rid of them during my lifetime, I want at least our
children to not suffer from these fears."  `Mark 9:14-29
<https://www.bibleserver.com/ESV/Mark9%3A14-29>`__ calls us to cry out with the
father of the muted child: "I want to believe! Help my unbelief!"

No human will ever be free from all fears. We have inherited these fears since
the day we left Paradise. But by cultivating and training the Gospel, the risen
Christ can little by little make himself at home in our heart and help us to get
out of :term:`Hell` and enter into :term:`Heaven`.

Let us be aware of our :doc:`weaknesses and mistakes </mistakes>`:

- Sometimes we fail to speak boldly because we are shy, we fear our enemies. We
  fear that some knowledge must be kept secret in order to protect it against
  the enemy.

- Sometimes we refuse to speak because of :term:`arrogance`. We believe that our
  explanation was clear. We think that the other is not worth that I patiently
  explain my opinion again.


..
  Note that the ten topics identified for the preparatory document are only a
  draft.  Maybe some of these topics aren't existential and do not need a
  consensus.  The Synod of Bishops will have to decide about this, too.


..
  Let us consider what Jesus told to the Pharisees and scribes: "repent and
  believe in the Gospel!" (`Mark 1,15
  <https://www.bibleserver.com/ESV/Mark1%3A15>`_).
