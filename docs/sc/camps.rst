==========================
The two camps
==========================

On this page I try to summarize what caused stress and communication problems
between me and the :term:`sinoditiim` during the diocesan phase in Estonia.


.. contents::
   :depth: 1
   :local:

Differing choices
=================

The :term:`sinoditiim` and I had intuitively different choices regarding

- which approach to use for the project as a whole. I am a convinced adept of
  the "Agile" approach, while the :term:`sinoditiim` consisted of experienced
  experts of the "Waterfall" approach (see :doc:`/talks/agile`).

- how to invite non-Catholic people. I was convinced that we must invite other
  people more insistingly than through our website if we want them to
  participate.



Differing principles
=====================

I would formulate the differing principles that led to these different choices
as follows (in parentheses a label for each opinion).


.. list-table:: Major points of discord

  *

    - We should not cultivate distrust or doubt in the church. ("obedient")

    - Expressing criticism is an important part of the learning process.
      ("co-responsible")

  *

    - We gave other confessions and non-religious people the possibility to
      participate, but they showed little interest. ("self-assured")

    - We actively invited other confessions and even non-religious people to
      participate. ("open-minded")

  *

    - The general public is not interested in our internal discussions and
      struggles. External persons have no right to see our final synthesis.
      Transparency causes administrative costs and anyway is never fully
      possible. ("solid")

    - Every detail of our work, and especially our synthesis, except for
      privacy-sensitive information, should be publicly visible. ("transparent")

  *

    - We must summarize only what has been said by others. ("safe")

    - We must look out for new ideas that have not been written
      before. ("inspired")

  *

    - The church should bring Christ to the world and not give humanistic
      answers ("mystical")

    - The church should give clear directions in all questions of daily life,
      including political, economic and cultural opinions ("political")


Differing methods
=================


Reactions to the questionnaire
==============================

Reactions to the `questionnaire of the Synod
<https://sinod.katoliku.ee/en/nuclei/>`__ . On the left side I collected
attitudes I heard from others, on the right side I formulate my reaction.

**1. Companions on the journey** -- *In the Church and in society we
are side by side on the same road.*

.. list-table::


  *

    - We must hold together because we are a small community in a dangerous
      world. Those who do not live according to the traditional Church teachings
      are not on our side, we need to protect ourselves against them.

    - We must clarify our definition of :term:`faithful` and "baptized". There
      are "baptized" people who don't understand the Gospel and who follow
      something else. And there are people who follow the Gospel but refuse to
      get "baptized" because they have seen too much of the harm caused by the
      Church.

  *

    - We must keep a clear distance from those who want to make us believe
      something other than the truth.

    - We will be especially attentive for those who did not yet decide to follow
      Jesus.


.. I see at least two consequences:

  - All :term:`faithful` are part of the Church.  The Church must clearly signal
    that every :term:`faithful` is welcome, including those who come to different
    conclusions regarding certain "details".
    The rite itself obviously gives no guarantee.


**2. Listening** -- *Listening is the first step, but it requires an open mind and heart, without prejudice.*

.. list-table::

  *

    - We must be careful because listening to lies causes you to believe them in the end.

    - We will be careful because we tend to listen only in order to find arguments
      against "their" opinion and to explain once more "our" opinion.

  *

    - Listening to a wrong opinion without clearly replying can encourage the
      other to remain in their mistake.

    - We will learn to not fear other humans as our "enemies".


.. rubric:: 3. Speaking out -- *All are invited to speak with courage and
  parrhesia, that is, in freedom, truth, and charity.*

.. list-table::

  *

    - We must not speak out everything without worrying about
      how the enemy might use our words against us.
      Diplomacy is the art of saying the truth without offending the enemy.

    - We will learn the art of speaking boldly where needed. When our brother
      sins, we will explain him what disturbs us even if he might feel offended.
      Hurting can be the opposite of harming. Of course every unpleasant message
      needs careful consideration.

  *

    - "Do not give
      dogs what is holy, and do not throw your pearls before pigs, lest they trample
      them underfoot and turn to attack you." (`Mt 7:6
      <https://www.bibleserver.com/ESV/Matthew7%3A6>`__)

    - We will respect that not every faithful is ready to understand every
      teaching.


.. rubric:: 4. Celebration -- *“Walking together” is only possible if it is
  based on communal listening to the Word and the celebration of the Eucharist.*

.. list-table::

  *

    - The Holy Mass fosters our identity and grows our faith.
      We must not let modernistic tendencies or other people tell us how to celebrate.

    - The traditional Roman Catholic rites are venerable and valid ways for
      celebrating the Gospel, but they are not the only way. We will embrace and
      document a wide set of rites.


.. This is a dilemma only as long as you see it as a dichotomy.  The logical
  solution for this to extend our teachings about rites.  The Synodal Church will
  embrace and document a wide set of rites. Not only the Old Mass and the Holy
  Mass of the Catholic Church, but also the big treasure of rites that have
  evolved in other church institutions. The SC won't *control* these rites (they
  remain under the authority of their respective church institution) but evaluate
  them and explain them in the synodal light.

Inspired (among others) by
`Vatican's top liturgy official confirms restrictions on Latin Mass
<https://www.ncronline.org/news/vatican/vaticans-top-liturgy-official-confirms-restrictions-latin-mass>`__


Inspired (among others) by
`The Discalced Carmelite Sisters of Fairfield and Vatican document Cor Orans
<https://www.catholicworldreport.com/2021/11/20/the-discalced-carmelite-sisters-of-fairfield-and-vatican-document-cor-orans/>`__,



.. rubric:: 5. Sharing responsibility for our common mission
  -- *Synodality is at the service of the mission of the Church, in which all
  members are called to participate.*

.. list-table::

  *

    - A lay person must not instruct other people
      regarding faith questions; teaching should be done by a priest.

    - The Gospel propagates mostly through our lives, through what we say and do
      to other people.

  *

    - Beware of the "stench effect": constantly living in a "stinking
      environment" makes you indifferent to the stench. You can loose your faith
      when you are constantly surrounded by people who don't believe.

    - Nothing can ever come between us and the love of God revealed to us in
      Christ Jesus.


.. rubric:: 6. Dialogue in Church and society
  -- *Dialogue requires perseverance and patience, but it also enables mutual
  understanding.*

.. list-table::

  *

    - We must not waste our energy trying to understand people who refuse to
      accept the truth.

    - We will learn that we cannot announce the Gospel to somebody if we fail
      --or refuse-- to understand their problem.


.. rubric:: 7. Ecumenism
  -- *The dialogue between Christians of different confessions, united by one baptism,
  has a special place in the synodal journey.*

.. list-table::

  *

    - The purpose of *ecumenism* is to open a door to other confessions so that
      lost faithful can turn back to the Roman Catholic church.

    - We will learn from each other and share what we have learned during our
      different history.

  *

    - During Holy Mass we welcome non-Catholic visitors as long as they behave
      adequately and follow the rules.

    - Every rite is open for visitors and we help them to understand what is
      going on.


.. rubric:: 8. Authority and participation
  -- *A synodal church is a participatory and co-responsible Church.*

.. list-table::

  *

    - The Church needs a clear hierarchy. Whenever humans do something together, it
      must be clear who is the boss. The boss is responsible in the end. If you
      are not the boss, then you must rather obey than feel co-responsible.
      That's called discipline.  We don't believe in co-responsibility.

    - While a well-regulated democratic government is probably the best
      management system in a world where faithful co-operate with faithless
      people, :term:`synodal government` has several strategic advantages over a
      pure democracy.

  *
    - Participation means communism. History shows that communism has failed. It
      is not a sustainable way for living together.

    - ...

Inspired (among others) by `Debatte um Synodalen Weg: Was eine Kirchenrechtlerin
antwortet
<https://www.vaticannews.va/de/kirche/news/2021-11/debatte-um-synodalen-weg-was-eine-kirchenrechtlerin-antwortet.html>`__


.. rubric:: 9. Discerning and deciding
  -- *In a synodal style we make decisions through discernment of what the Holy
  Spirit is saying through our whole community.*

.. list-table::

  *

    - Common decisions must be the result of a well-organized democratic
      process. We cannot allow corrupt leaders who do what they want, even if
      they call it discernment.

    - ...


.. rubric:: 10. Forming ourselves in synodality
  -- *Synodality entails receptivity to change, formation, and on-going learning.*

.. list-table::

  *

    - The Church must align to Jesus Christ alone, not to a multicultural mix of
      ideologies. The mission of the Church is to *teach* others, not to *learn*
      from others.

    - Teaching requires learning. While the Church --of course-- teaches what
      she has learned so far, we must never forget that her learning continues.
      The Church must not claim to teach the *only* valid teaching about God.


Bible-based versus Gospel-based faith
=====================================

I also observed the following more general points of discord between other
people and me.



.. list-table:: Bible-based versus Gospel-based faith

  *
    - The Bible is our infallible and authoritative guide in all questions
      about God.

    - The Bible requires interpretation, our job is to ex­plain it anew in each
      situation.

  *
    - The Bible *is* the :term:`Word of God`.

    - The Bible *speaks about* the :term:`Word of God`.

  *
    - Women can't get accepted as priests because Christ consciously selected
      only men as apostles. (`vaticannews.va
      <https://www.vaticannews.va/de/kirche/news/2022-07/schweiz-bonnemain-bischof-chur-priester-frauen-weihe-debatte.html>`__)

    - The laws of the church are human-made and need constant maintenance. Every
      rule should be based on evidence; no rule should be based solely on some
      interpretation of the Bible.



Formulating controversial convictions
=====================================

Formulating controversial convictions is itself controversial because the
attitude behind a statement always depends on the context. And of course I have
no right to judge. One friend reacted "Your table reads as if the good people
who didn't understand anything were on the left side, and the enlightened genius
who understands everything was on the right side."

Such dichotomies remain human-made "labels" that connect to diverse meanings and
can trigger diverse emotions. Formulating controversial opinions is difficult
because nobody likes being pinned down under some label.

But such comparisons are indicators that where we need to advance on our
journey. They can give a grasp on certain aspects of reality. Refusing to
differentiate, for example, between a "safe church" and an "inspired church" is
like refusing to differentiate between "cold" and "hot".
