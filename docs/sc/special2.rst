==================
For those who care
==================

.. contents::
  :local:

A stormy world
==============

The world seems to be quite stormy these times: pandemic, climate crisis, fake
news, information pollution, ransomware, terrorism, ...

Humanity in the digital age is experiencing an unprecedented challenge: we need
to find ways to dialogue about questions that had never required a world-wide
consensus as long as real-life contact between cultures was rather exceptional.
Video conferences have become normal during the COVID pandemic. This is just a
recent and probably not the last revolutionary change in our social
infrastructure.


Religion is everywhere
======================

We all rely on some "religion" if we define "religion" as a set of beliefs we
rely on. Religion is unavoidable because we cannot fact-check every statement,
we always need to rely on certain basic assumptions.

I didn't say that we need a religion for *every* decision. When the terminal in
the supermarket tells me that there isn't enough money on your account, then you
don't need to "believe" it because you can check the facts.

But already a question like "Should Estonia build a nuclear plant?" (or if you
prefer: "Should we raise the wages of teachers?) cannot be fully fact-checked.
There are honest people who are convinced that the answer is "yes", and there
are other honest people who answer "no". *That's* a fact: we don't have a
consensus on these questions.

Any organization that propagates information we cannot fact-check is a religion.
Every nation of the world is a religion. Every political party is a religion.
Every :term:`private corporation` is a religion. Every football club can become
a religion.

Being realistic means to acknowledge the fact that there are many questions to
which we don't know the answer. And some of these questions are
:term:`existential <existential question>`.

The *basic code of conduct* on :doc:`the previous page <special>` is such an
answer to an existential question. Nobody can prove it, we can only believe in
it (or not). It is a question of :term:`religion`, not a question of
:term:`science`. Science cannot answer it, doesn't even *try* to answer it.

The Roman Catholic church has useful know-how
=============================================

Also the Catholic Church is experiencing storms. Loud debates between
conservative and progressive schools, or scandals about sexual or financial
topics, etc.

Such storms are nothing new in the long history of the :term:`Roman Catholic`
church. She experiences such storms occur every now and then, and until now has
survived them all. She is the oldest and biggest organization in the world. The
Church not only survives all storms and crises, she actually grows and evolves
*through* them.

A :term:`faithless` historian once worked for several months in the Vatican's
archives on some research project.  After this he became :term:`faithful` and
asked for being baptized. His friends asked "why?". He answered: "In the
archives of the Vatican I saw how many mistakes have been done by the Church
during its history. If a corporation would make this kind of mistakes, it would
soon go bust (bankrupt). Why is the Church still there? There must be some
divine will behind it." (I didn't fact-check that story, but *se non è vero, è
ben trovato*.)

Note the difference between "the Roman Catholic church" (a real and legally
defined organization in the :term:`visible world`) and "the :term:`Church`" as
the ideal community (in the :term:`invisible world`)) that unites all those who
believe in the :term:`Gospel`.

I can understand if you don't care about the former, but I hope that you care
about the latter.

The :term:`Roman Catholic` church has of course her teachings and rules, but
these teachings have grown during history and "obviously" need a major update.
I write "obviously" between quotes because this is a taboo topic!
A considerable number of Christians seem convinced that "the
teachings of the Church" are eternal and that the mere idea of updating them is
heresy!

But *how exactly* does this organization work? What is the secret of her
success, the reason for her survival despite her mistakes? It has always worked
"somehow automatically" or "because God wants it" (as we use to say).

But a comprehensive answer to this question is being demanded with more and more
urgency. Not only because internal debates demand clarifications. Not only
because public order demands explanations. There is another, even more important
reason: humankind as a whole struggles with exactly the same kind of challenges
as the Roman Catholic church.


The world needs a Church
========================

The *basic code of conduct* on :doc:`the previous page <special>`
is inspired from the Bible (`Mark 12:28-34
<https://www.bibleserver.com/ESV/Mark12%3A28-34>`__).  Or maybe it's the
opposite: the Bible is inspired by this *basic code of conduct*, which seems to
be universally accepted by many humans. Most of us *try* to love the world as it
is, and *try* to love each of their fellow humans.  Most of us :term:`believe
<to believe>` that this is a "good" principle to adopt.

Of course it is just a principle. It leaves much room for interpretation in
most real-life situations.

The :term:`Church`

and if the church teaches a wrong religion,
we need to tell her.

The :term:`Synod on Synodality` is about how to *cultivate* this basic code of
conduct in the real world. It is about how all humans can live together in peace
on this planet.

We all are "faithful" in the meaning of "full of faith". Some of us
:term:`believe <to believe>` that :term:`prayer` and going to church help them to grow,
others believe the opposite. Read my definition of :term:`faith` and let me know
if you feel that I am missing something.

So whatever you think regarding religious teachings, rites and prayers, you too
might have something important to say at this Synod.

The challenges of the :term:`Church` are of same nature as those we experience
world-wide in politics, social media and public life.

I am aware that not all "Christians" agree with all above. I beg your pardon for
them. They will understand sooner or later.





The world needs synodality
==========================

The *modus vivendi et operando* of the :term:`Roman Catholic` church is special,
it is obviously neither democratic nor monarchistic. It is a know-how about
living together sustainably and in diversity, and this know-how might be useful
for public administrations and governments as well.

The Church herself started only quite recently to discover that her business
model and code of conduct has something special and that it deserves a name.
The word "synodality" (actually a neologism), emerged as a name for this model.

Synodality is our way of living together, sustainably and united in diversity.

The Church has done already some work for explaining this synodality. A number
of documents have been published by the Vatican. But even more work is ahead.

..  "The harvest is plentiful, but the labourers are
  few. Therefore pray earnestly to the Lord of the harvest to send out labourers
  into his harvest." (`Lk 10:2 <https://www.bibleserver.com/ESV/Luke10%3A2>`__)

In order to prepare this Synod, Pope Francis started a huge and unprecedented
project: he wants to hear the voice of everybody.  Not only Catholics, but all
the Baptized and even non-Christians are invited to participate.

Believe it or not: on this journey we *are* together with the *whole* mankind.
:term:`Synodality` is unavoidable.

The :term:`Synod on Synodality` will be about all this.
The Pope has important influence on global politics.

The Pope invites you to participate.
That's why I invite you to help me with my report.

Maybe *your* voice is the straw to break the camel's back that makes things
change (or prevents things from changing).


..
  Why should you bother to read 12 pages of dense text and then explain me your
  opinion?

  I believe that the Gospel is much more than what the church institutions in
  Estonia have been teaching until now. Partly they are even teaching something
  that is not the Gospel.



.. One valid reason to *not* participate is to say that you can't believe that my
  project would bring a change.
