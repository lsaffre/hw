=============================================
The Church must be as transparent as possible
=============================================

Every union of humans is faced with the fact that humans happen to make
mistakes, to "grow evil" and that `shit happens
<https://en.wikipedia.org/wiki/Shit_happens>`__.

But the Gospel calls us to cultivate our trust in the fact that *Nothing can
come between us and the love of God.* (Romans 8:31-39)

It also calls us to love those who persecute us.  "Indeed, all who desire to
live godly lives in Christ Jesus will be persecuted, while evil men and
imposters go from bad to worse, deceiving and being deceived. But as for you,
continue in the things you have learned and firmly believed, since you know from
whom you have learned them." (2 Tim 3:12-14)

Therefore, if we want the Church to by synodal, we we cannot allow it to operate
in the dark or to practice secrecy. Corporations have no right to secrecy. The
Church as a corporation has no right to privacy.
