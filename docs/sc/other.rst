=================================================
What non-catholic people have to say at the Synod
=================================================

The :term:`Synod on Synodality` might look like an internal project of the
:term:`Roman Catholic` church, but I believe that it is much more.  I see two
big groups of people who have to say important things at this synod:

- `Other Christian denominations`_ and `Other religions`_
- People who work for a human world in `Non-religious movements`_

.. _sc.other.christians:

Other Christian denominations
=============================

The :term:`Church` has experienced two major schisms during her history. More
than twenty human generations have passed since `Reformation
<https://en.wikipedia.org/wiki/Reformation>`__, more than forty generations
since the `Schism of 1054
<https://en.wikipedia.org/wiki/East%E2%80%93West_Schism>`__.

The traditional approach of the :term:`Roman Catholic` church is to assume that
"our" teachings are the true ones while other Christian :term:`denominations
<denomination>` have broken apart. History shows that this approach doesn't
work. It is time to change this approach.

The vision of the :term:`Synodal Church` is the opposite of another schism, it
will unite all Christians back under one institution. Many :term:`faithful` from
all :term:`denominations <denomination>` have been doing important work to
prepare this step.

It seems obvious to me that this institution will be a new :term:`legal person`
because its teachings will introduce some backwards-incompatible changes. It
will take generations before "everything" has become "clear" because convictions
usually don't change during the lifetime of an individual human.

Other religions
===============

Just to name a few: Judaism, Islam, Buddhism, the Baha'i faith, ...

My report doesn't cover them because I lack personal contacts to these
religions.


.. _sc.other.nonreligious:

Non-religious movements
=======================

The :term:`Synodal Church` will publish and maintain the "synodal guide",
which can be used independently of religious rites and practices. This will open
the door to :term:`faithful` people who work for a human world without
identifying themselves as "religious". This includes Wikipedians, Free Software
activists, climate activists, human rights activists, political parties and
independent artists.
