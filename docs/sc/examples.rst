==================================
More examples of synodal teachings
==================================

Encouraged and inspired by my vision, in this section I imagine how the
teachings of the :term:`Synodal Church` might look like.

.. toctree::
  :maxdepth: 1

  sins
  divorce
  queer
  /family
