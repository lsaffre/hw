===========================
The Church in Estonia
===========================

There are about 4500 Catholics living in Estonia. We are a relatively small
group within the group of Christians in Estonia (300.000), which is itself a
very small group within the population of Estonia (1.330.000), which is itself a
tiny group of the world population (0.0168%).  Recommended readings: `Religion
in Estonia <https://en.wikipedia.org/wiki/Religion_in_Estonia>`__

Despite this, or maybe because of this, Estonia might have to say interesting things regarding the future of the Church.

.. https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population

Message from Catholics in Estonia
=================================

The majority of active members of the Roman Catholic church in Estonia have a
simple and clear message to the fathers of the Synod:

The main question of the Synod and the Catholic church is how we can live as the
Church according to our teachings, influence other people and especially the
members of our church in a reliable way. The teachings of the church are well
elaborated and carefully researched. We must not doubt in these teachings. We
encourage you to remain faithful to the truth of Jesus Christ as it has been
taught by the Church from its beginnings.

Regarding critics heard from within the Church and from the world, we advise to
adapt the church administrations to how other organizations do it.
