=====================================
Theses about the Synodal Church
=====================================

On this page I try to collect controversial :term:`thesis statements <thesis
statement>` (i.e. some participants feel that they are important and true while
others feel that they are either pointless or wrong).

TODO: remove statements that *nobody* would refuse/accept


.. glossary::

  TS01

    Any :term:`conviction` can give the wrong answer to a :term:`controversial
    question`.

  TS02

    A :term:`controversial dialogue` turns into a :term:`battle <controversial
    battle>` if and only if the participants fail to be aware of :term:`TS01`.

  TS03

    We are called to differentiate between the (ideal) :term:`Church` and "our"
    (visible) church. No visible church has a priority over her sister churches.

  TS04

    "The :term:`church teachings` are [the authoritative|a satisfying]
    manifestation of [the truth|our knowledge] about God"

  TS05

    We are called to witness the [truth|knowledge] we have been given through
    the :term:`church teachings`.

  TS06

    Our church is not *searching* for truth, it *has found* the truth in
    :term:`Jesus Christ`.

    In contradiction with :term:`TS19`?

  TS07

    Every teacher is always also a learner: these two go together. [`Jürgenstein
    <https://kjt.ee/2022/01/kirjutamisest-ja-opetamisest-intervjuu-toomas-jurgensteiniga/>`_]
    Teaching always includes learning.

  TS08

    Any :term:`knowledge element` that is refused by at least one group of
    :term:`Baptized` cannot be part of the :term:`church teachings`.

  TS09

    The major outcome of the :term:`Synod on Synodality` will be to discern
    which :term:`knowledge elements <knowledge element>` of the :term:`church
    teachings` need an update.

  TS10

    We are called to protect ourselves from temptations that make us doubt in the
    teachings of our church.

  TS11

    The teachings of the Church must never be in conflict with :term:`scientific
    evidence`.

  TS12

    Announcing the Gospel to all peoples means to constantly find new words for
    it.

  TS13

    The Church lives and grows like a vineyard or a cedar of Lebanon. It is not
    designed to "journey" around.

  TS14

    We are called to faithfully trust in the truth about the Gospel as it has
    been taught by our church from its beginnings.

  TS15

    The truth of the Gospel has been taught reliably and without mistakes by our
    church from its beginnings.

  TS16

    The teachings of our church are reliable, doubting in them is heresy.

  TS17

    The teachings of the Roman Catholic church are more valuable than those of other
    church denominations.

  TS18

    The main question of the Synod and the Catholic church is how we can live as
    the Church according to our teachings, and how to influence other people and
    especially the members of our church in a reliable way.

  TS19

    Anyone who seeks truth seeks God, whether or not he realizes it.
    -- `Edith Stein (1891-1942) <https://en.wikipedia.org/wiki/Edith_Stein>`__

  TS20

    The Church protects our community against our enemies.

  TS21

    Those who do not follow the teachings of the Church are not on our side, we
    need to protect ourselves against them.

  TS22

    The teachings of our church are reliable, well elaborated and carefully
    researched.  We must not doubt in these teachings.

  TS23

    The Bible [teaches us clearly | tells us important stories about humanity's
    searching for] what is good and true.

.. glossary::

  Q1

    In the Church and in society we are side by side on the same road.


All :term:`Baptized` are called to hold together because we need to learn from
each other.

The gospel is a radical liberation from death, which enables us to love without fear.
[Henry Nouwen, *Jesus Sinn meines Lebens*, p.43]

There are :term:`Baptized` who misunderstand the Gospel in a given situation and
therefore follow something else. And there are people who follow the Gospel in this
situation but refuse to identify as :term:`Baptized`.

There are people who refuse to get :term:`baptized` because they have seen more
harm than good caused by some church.
