=========================
My synthesis (unfinished)
=========================


.. contents::
  :local:

As I was not satisfied with our synthesis, I tried to formulate what the people
of good will Estonia *actually* have to say at the :term:`Synod on Synodality`.
I didn't finish it in time.

About this document
===================

This document is based on the reports I received as a member of the
:term:`sinoditiim` in Estonia during the consultation phase. It is also based on
the synthesis developed by that team. It is furthermore based on diverse
contributions I heard after the synodal team had closed their synthesis. And it
is based on my personal observations during more than 30 years as a Catholic who
grew up in Belgium and emigrated to Estonia.

Of course I interpret all these contributions and synthesize them according to
my personal beliefs; the result is my *personal* synthesis, *my* voice and
style, nobody will fully agree with every formulation in this document.

But isn't this was I was asked to do as the Synodal Contact Person for Estonia?
I tried to listen to all those who care for the church "as she is, despite all
justified criticism", who want to help her "on her synodal journey of seeking
what is good and true", including "those who have abandoned the practice of the
faith, people from other faith traditions, people who have no religious beliefs
at all." (compare :term:`Vademecum` 2.1).  I listened especially carefully to
those who *refused* to speak to me. Why did they refuse dialogue? What mistakes
did I make when communicating with them?

And after listening to all those people, after listening to the Holy Spirit
during many prayer meetings, I tried to write a text that would filter out the
useless noise and emphasize what is important; a text that is worth your time.

This document can become more than my personal opinion only if a relevant number
of individual humans says "that's what I would say as well". So at some moment I
will have to publish a printable version and collect signatures. There is no
other way to test whether this document expresses more than my personal opinion.
So when I write "we" in this document, I mean the future signers.

This document remains the work of an amateur who is doing this in his spare
time.


We believe in the ideal church
==============================

Humanity is facing unprecedented challenges: we need to dis­cuss about questions
that had never required world-wide consensus before. Lack of synodality all over
the world, not only among religious people, causes humans to despair, couples to
divorce, governments to fail, wars to break out, and world-wide challenges –like
a pandemic or climate changes– to escalate into crises. Humanity is about to
destroy itself if we don’t learn synodality.

Also the :term:`RCC` is facing challenges. Hordes of members leaving the church,
consecrated people getting accused of severe crimes, ... How is the biggest
religious institution in the world going to address these issues?

Lack of synodality in a church community causes people who understand the Gospel
to *leave* that community, and people who don't understand the Gospel to *join*
her. This doesn't happen only in the :term:`RCC`, it also happens in other
communities. It is an inter-confessional phenomenon. Of course we need to
explain what we mean when we say `Understanding the Gospel`_.

We believe that the world needs a recognized :term:`legal person` that
represents all those who have the right to call themselves "Christians", and who
protects the label "Christian" against those who claim to be "Christian" but
aren't.

The ideal Church might have the most important mission in the world, which is to
announce the Gospel to everybody. We believe and profess that there is a Gospel,
and that understanding this Gospel is our only path towards salvation and peace
in our hearts and on earth. We need to announce and explain this Gospel to
everybody if we want to live in sustainable peace on Earth. No :term:`private
corporation` can do this job.


The Roman Catholic church is not synodal
========================================

The :term:`RCC` has the potential of becoming this legal person because it has a
series of noteworthy assets. But it will require a series of changes that seem
utopian ("church fiction") right now. For example, a really synodal church
institution can't insist on human-made rules like "abortion, euthanasia and sex
without being married are always against the will of God" or "priests must be
celibate men".



The assets of the Roman Catholic church
========================================

The :term:`RCC` has a few assets that no other church institution has.

1. **Consecrated people** as individual humans who give their whole lives by
renouncing from a family and private property.

  The church can be gratefully proud for consecrated people who choose a
  celibate life and give their whole life for the Kingdom of God, renouncing
  from all temporal treasures. Consecrated people are a living sign of God's
  presence in the visible world. The sacrament of consecration is a unique and
  sustainable economical model where the church commits towards every
  consecrated person as their life-long employer.


2. The **Holy Mass** as a genial *smallest common denominator* for celebrating
communion with all members of the institution.

3. A standardized **reading plan** of the scriptures that has been tested during
centuries. It includes a minimum programme for those who practice only once a
week, and a programme with daily readings for those who want more. It is of
course cyclic because the Gospel is eternal. It repeats every 3 years, which
seems an optimal duration for the cycle.  This reading plan might be the best in
the world, but of course it remains human work and needs continuous testing and
careful maintenance.

4. An **apostolic government model** with a pope as top-most apostle. This
government model has fundamental advantages over any other known government
model.

Understanding the Gospel
========================

You cannot understand the Gospel. At least not fully. "If you understand it,
then it's not God" wrote Saint Augustine.  When somebody asks us :doc:`"Tell me
the Gospel in 60 seconds" </talks/gospel60>`, we are unable to give a clear and
concise answer.

We can say that the Gospel is *our axiom*. We *assume* that it is a divine
message brought to every single human through the life and resurrection of Jesus
Christ. It is an eternal message that has always been true and will always
remain true. It is beyond any reason. We cannot formulate it in human language.
That's about all we know that the Gospel.

The Scriptures are the best written formulation of the Gospel we have. They are
the fruit of the work of the church. The church is the author of the Scriptures.
But even our Scriptures are just childish babble compared to the Gospel. You
won't understand the Gospel by just reading the Scriptures.

And yet it is easy to recognize when somebody understood "a sufficient part" of
the Gospel. Everyone can see it the fruits: Does he love God? Does he love his
neighbor? For example you can love the Bible more than God, or your convictions
more than your neighbor.

And even this simple rule of thumb is not water-proof, it is juridically
speaking nonsense because "to love" can be interpreted in very different ways.
No lawyer and no judge on earth can tell you what "to love" means.

.. Don't prepare your words, don't worry about what to say.

The church can help us to avoid confusion by providing clear answers to what I
call :doc:`indicator questions </talks/indicator_questions>`.

A clear answer to an indicaator question means to say "no" to one option and
"yes" to the other.

That said, we need to respect that the convictions of an individual person don't
change overnight, maybe not even during her lifetime. The church must provide
for the living of every consecrated person. She cannot undo a consecration . But
she must prevent misleading teachings from causing further harm. She must
acknowledge that she has published misleading teachings and will probably
continue to do so forever. Publishing a misleading teachings is just a mistake,
and it becomes a :term:`sin` only by refusing to confess the mistake.


Announcing the Gospel
=====================

As soon as you understood "a sufficient part" of the Gospel,
you want it to grow it within you,
you want to share it with your beloved,
with your children, and with all humans.
You want to cultivate the Gospel.



The church deserves criticism
=============================

I imagine what I would do if the RCC were a software program and if I were the
developer responsible for maintaining it.

And I asked many people: where is the bug?

There must be some bug in the church, something she has been doing wrong during
centuries. Because otherwise every human would exclaim “look how they love each
other!” and would want to learn how we do this.

Muriel found the following short formulation for this bug:

  **The scriptures, traditions and rites of the church are good.
  But the church must stop saying:
  only this is the right way,
  you can't get saved otherwise,
  it must remain like this forever.**


Two reports instead of one
==========================

The fact of having failed to reconcile the :term:`sinoditiim` and the
:term:`sinoditalgutiim` might be our most valuable contribution to the Synod.

In a certain way we stand here with two reports instead of one.  This is
surprising because both teams were very similar:  They both had the same
responsible leader. I can witness that each individual member of both teams are
valuable and honest people. Both teams were inspired by a burning love for the
church. Both teams received the same task.

And yet they come to such contrasting results! Our two documents seem to
*exclude* each other.  All test readers had a clear *preference* for one of them
and a clear *refusal* against the other.

This phenomenon is neither specific to the church nor to Estonia. It happens
whenever humans must journey together but lack synodality.

.. We are probably not the only diocese to witness this issue.


When synodality fails
=====================

Synodality fails when we aren't *together* or when we refuse to *journey*:

- when we exclude others from our community;

- when we believe that we have arrived

- when we stay in some safe or comfortable harbour instead of moving on

- when we believe to know the truth.

Lack of synodality causes individuals to despair, couples to divorce,
governments to break, it causes wars between nations, it causes world-wide
*challenges* --like a pandemic or climate changes-- to escalate into *crises*.
Humanity is about to destroy itself if we don't learn synodality.

Synodality is difficult because Humans have different gifts and preferences.
Some love to race over a mountain pass road, others fear it. Some love to
provocatively speak in public, others love to keep secrets in their hearts. Some
love to boycott human-made laws, others love to faithfully apply them; some love
to act spontaneously, others love to meticulously plan and report their
activities.

When we experience conflicting preferences, one question is how we behave as
humans, the other question is how God wants the church to behave. Every
*individual human*, regardless of their gifts and weaknesses, should feel
welcome in the church.  But the church as an *institution* is more than her
members. She can decide which institutional behaviours to foster, and which
behaviours to get rid of.

Synodality is more than unity in diversity. It cannot be measured nor be
enforced by laws and rules because it has a divine dimension. The world needs
more than synodal governments, it needs a synodal church.


..
  Every human follows some set of ideologies.  The church must be governed in a
  way that unites all ideologies.

..
  Fixing this bug becomes urgent because the world needs a visible and audible
  institutions.

  - speak clearly in the name of the One and Almighty;
  - do not depend on money, power and other temporal treasures;
  - act as an independent and trustworthy advisor in every situation;
  - provide reliable orientation to those in power;
  - patiently explain the Gospel anew to every generation;





Steps to take
=============

What steps does the Spirit invite us to take in order to grow in our ‘journeying
together?’


Stop trying to "sell" the Gospel
--------------------------------

The gospel is not for sale. Our mission is not to sell it.


Stop saying that the Bible was written by God
---------------------------------------------

The Bible was written by the church. It does not have a single author, nor a
clearly defined group of authors. It is a collection of inspired texts that help
us with announcing the Gospel. This collection has been tested and maintained
for 400 years until it was canonized.

The church is the community of humans who heard the Gospel and decided to follow
Jesus.

We need more courage to repent from biblicism and to say clearly that the Gospel
itself remains an axiom, which we cannot and do not want to prove. The Church is
responsible for protecting the Bible, not for protecting the Gospel. The Bible
is just one one way to teach the Gospel. The Gospel needs no guardian.

The message of Dei Verbum, which explains that the Gospel is more than the
Bible, is a major asset of the RCC where no compromise should be made.

Many people, both followers and opponents of the Church, see the Bible as a
“magic” book that gives “clear” instructions for “getting into Heaven”. The
Gospel has nothing ma­gical. Christian faith is about right relation­ship to
reality, not an idolatry of something “su­pernatural”.

No teaching can be the full truth.

Get rid of clericalism
----------------------

The Church in Estonia has a rather clerical image. Christians say “Speaking
about faith should be done by  priests and professional teachers” or “I am not
wiser than books”.

Reassign your workers when needed
---------------------------------

Every institution is responsible for assigning jobs to their workers and for
reassigning a job to another person when needed.
We can't endlessly be careful
to not offend anybody.

Love endures and forgives everything, but love is the duty of humans, not of
institutions.

Everybody agrees that a worker with pedophilic disorder must not get in contact
with children.

But I suggest a similar rule regarding government and leadership: a worker with
what I would call a X disorder must not exercise a leading position.

Such disorders, pedophilic and X, happen to get diagnosed only after an
assignment, after having caused harm.

Reassigning a leading position to another person because the current assignee
has been diagnosed with X can be a very delicate topic. Every concerned person
is to be treated with respect for their privacy, honour and reputation. But the
church as an institution has no right to privacy, honour or reputation. Never
should the institution be permitted to protect herself. The life-long commitment
of the church towards her consecrated people does not mean that a person cannot
get reassigned.

The church remains responsible employer even for workers who get diagnosed with
human weaknesses. There is no reason to hide such cases. The church should
assume her responsibility to repair any damage done and to protect us from
further damage.

The church should be able to provide legally valid reporting about all her
activities.



Rites need continuous integration
---------------------------------

Just an example. In Estonia the following invitation to the confession of sins
(*mea culpa*) is still used invariantly during each mass:

  (...) tunnistagem oma patud, et meid arvataks väärilisteks seda püha ohvrit
  tooma.

Compare this to common invitations in other
congregations:

- Im Vertrauen auf Gottes Erbarmen wagen wir, unsere Sünden zu bekennen. (Eupen,
  2022-06-19)

There is a fundamental difference between these two formulations. The first one
is from before :term:`Second Vatican Council`. It has been translated 30 years
ago by Father Rein Õunapuu and has never been adapted since then.

These words a central part of the Holy Mass. Faithful Catholics know it by heart
because they practice it as a routine every Sunday.


Use modern communication technology
-----------------------------------

Assume your role as a compass for our governments
-------------------------------------------------

The compass knows where the north pole is. It does not know about cliffs. It has
no power over the ship. The captain knows about cliffs and knows the destination
of the freight. Sometimes the straight route is suboptimal or simply impossible.
The Church with power in the visible world would be like a compass that directly
controls the steering wheel. A captain refusing to look at the compass would be
a blind man trying to lead a blind man (Luke 6:39). An unreliable compass is
worse than a weak motor.


Further reading
===============

See also some separate articles I considered including here:

- :doc:`/christian_values`


Lessons to learn for me
=======================

. Learn to speak clearly. Don't try to say everything at once.

- `Stop Trying to Sound Smart When You're Writing
  <https://hbr.org/2016/10/stop-trying-to-sound-smart-when-youre-writing>`__
  by Liane Davey.

- Treat your audience "like they're idiots. Not because they are, but because
  what is obvious to you may not be obvious to them. As Sir Richard Branson
  said: Complexity is your enemy. Any fool can make something complicated. It is
  hard to keep things simple." Think of every joke in your speech as a barrier
  for a part of your audience. (Inspired from `The KISS Method For Acquiring
  Clients <https://thefullybookedcoach.com/kiss-method/>`__ by Tim Brownson).


Glossary
========

.. glossary::

  RCC

    Roman Catholic church
