==========================
About rites and sacraments
==========================

I imagine that the :term:`Synodal Church` will make a few things clear regarding
the communal listening to the :term:`Holy Scripture`, the celebration of the
Eucharist, rites, liturgy, sacraments, and rituals.

.. contents::
   :depth: 1
   :local:


Human hearts are not flexible
=============================

Human **hearts are not very flexible**.  Once they are used to a given way of
celebration, they usually have difficult to change it. We all have a temptation
to say "this is a wrong way of celebrating", when it is not "as I was taught".

Compare a Holy Mass, a mass in a charismatic Emmanuel community, a service in a
Baptist community, and Orthodox or Lutheran Sunday celebration.

Announcing the Gospel is more important than insisting on a given rite.

The :term:`Synodal Church` will foster a **diversity of rites** where  a
faithful who grew up in one synodal community can find a place in any other
synodal community.





.. _sc.sacraments:

Sacraments are revocable
=========================

Sacraments are visible symbols, not contracts. They are a life-long holy
warranty given by the Church, but only as long as the receiver wants them. They
are a gift of the Church to the faithful.

When somebody gives you a book, but you fail to read it, then the book won't
work for you. You can fail to read a book for many reasons. For example because
it was stolen before you had the time to read it. Or because you didn't care.
Which just means that you didn't understand that this book would be important.

For all life-long sacraments (baptism, marriage, priesthood) there will be a way
to revoke them. And this revoking is done by the receiver(s) of the sacrament,
the church has no veto, her job is to register the decision of having revoked.
We cannot say that your failure of receiving this sacrament is a :term:`sin`. We
can be sorry for you, but we cannot blame you.

Note that while baptism and priesthood requires a *single* human to revoke it,
marriage needs *both* partners to agree on the divorce.

Inspired (among others) by
`In Catholic Italy, 'de-baptism' is gaining popularity
<https://religionnews.com/2021/11/16/in-catholic-italy-de-baptism-is-gaining-popularity/>`__,
:doc:`/blog/2021/1215_1944`


More about rites
================

https://www.psypost.org/2022/01/new-psychology-research-sheds-light-on-how-social-hierarchies-influence-extreme-collective-rituals-62327 
