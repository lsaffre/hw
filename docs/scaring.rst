==============================
The scaring parts of the Bible
==============================

The :term:`Bible` contains scaring passages that describe a God who punishes
those who do evil and therefore deserve punishment. "But the Lord is with me as
a dread warrior; therefore my persecutors will stumble; they will not overcome
me. O Lord (...), let me see your vengeance upon them, for to you have I
committed my cause." (`Jeremiah 20:11-13
<https://www.bibleserver.com/ESV.EU.BDS/Jeremiah20%3A11-13>`__)

Indeed Christian faith evolved from the Jewish culture, which had the idea that
God punishes sins even through generations (`2Mo 20:5
<https://www.bibleserver.com/ESV/Exodus20%3A5>`__: "for I the Lord your God am a
jealous God, visiting the iniquity of the fathers on the children to the third
and the fourth generation of those who hate me"; 34,6-7; 4Mo 14,18; 5Mo 5,9).
Such words express the observation that a culture is more than a single human's
life, and that we are also responsible for the world where our children and
grandchildren will have to live after us.

Punishment is an integral part of divine justice in pre-Christian faith.

On the other hand, already five hundred years before Jesus, when the biggest
part of the Jewish population suffered as slaves under Babylonian oppression,
the prophet `Ezekiel 18:20–21 <https://www.bibleserver.com/ESV/Ezekiel18>`__
wrote: "The son shall not suffer for the iniquity of the father, nor the father
suffer for the iniquity of the son. The righteousness of the righteous shall be
upon himself, and the wickedness of the wicked shall be upon himself. "But if a
wicked person turns away from all his sins that he has committed and keeps all
my statutes and does what is just and right, he shall surely live; he shall not
die."

The Gospel does not say that everything is hunky dory and that the world is
peace, joy and egg cake. Believing in a :term:`Kingdom of God` includes the
possibility that I --or my friends-- might "miss the connection" and get
excluded from this Kingdom.

Jesus tells quite some stories about people who "missed the connection". The
most impressives of them include `Luke 16:19–31
<https://www.bibleserver.com/ESV/Luke16%3A19-31>`__ (The rich man and Lazarus)
or `Matthew 25:1–13 <https://www.bibleserver.com/ESV/Matthew25%3A1-13>`__
(Parable of the Ten Virgins),  `Matthew 22:2–14
<https://www.bibleserver.com/ESV/Matthew22%3A2-14>`__ (Parable of the Wedding
Banquet). Or Paul later (`Romans 6:23
<https://www.bibleserver.com/ESV/Romans6%3A23>`__) wrote: For the wages of sin
is death, but the free gift of God is eternal life in Christ Jesus our Lord.

These worries arise from the idea that there must be some kind of divine
justice.

The Bible warns us about these dangers, but also tells us what we can do about
them. It calls us to "repent", that is, to *turn away* from something, to
*review* and fundamentally *change* something.

But turn away from what?  From our sins?  That seems to be a wide-spread answer
among many people who classify themselves as Christian. But that interpretation
makes no sense. The message "Stop doing sins" was known already before Jesus.
That's why it cannot be the message for which Jesus had to die on the cross.

Furthermore there is a fundamental problem with that pre-Christian advice of
avoiding sins: it is impossible for humans to know whether something is a sin or
not. We can only make *assumptions* about this.

No, the :term:`Good News` is something else. It calls us to repent *from the
image of a God who punishes us for our sins*.  We realize and acknowledge that
nobody can ever avoid sin completely. That's the Gospel: your sins are not a
reason for God to punish you.

This does not mean that we can lead a sinful life. It is important to avoid
being sinful *because sins hurt yourself or other people*.  This (not a fear of
getting punished by God) is our motivation to constantly learn how to identify,
admit and avoid sin.

TODO: The following partly covers with :doc:`/talks/cross`.

Christianity answers this question by giving Jesus the title of "Lamb of God" or
"Redeemer" because he died on the cross as an atonement for all our sins,
including the original sin. This image of the Lamb of God is an important aspect
of Christian faith and a base of inspiration for many liturgical formulations.

Of course, like every image of God, this image can get misunderstood. Statements
like "the holy God cannot let sin go unpunished" (`found here
<https://www.gotquestions.org/why-Jesus-die.html>`_) can lead to
:term:`comminorism`.
