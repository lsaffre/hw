==================================
A beginner's guide to Christianity
==================================


.. glossary::

  Christianity

    Everything that's based on the faith in :term:`Jesus Christ`, i.e. on the
    assumption that the historic *Jesus of Nazareth* is the *Christ*.

  Jesus Christ

    Title given to the historic *Jesus of Nazareth* when seeing him as the
    *Messiah* (*χριστός*, *chrīstós*, the anointed one) announced by Jewish
    religion.

Every other :term:`religion` was founded by somebody, by a prophet or a sage or
a figure, who came and said "Here is the way to find God". Only Christianity was
founded by a man who said "I am God coming to find you". -- Tim Keller (in
:term:`The Gospel-Shaped Life`)

..
  One thing is clear: don't expect me to fix a problem on which millions of humans
  have been working much harder than I will ever do.  I am just bringing in my
  two cents, my naive little attempt.

Christianity is a big country. Without a map and a compass you can easily get
lost.


.. glossary::

  Church

    The ideal community of all humans who believe in :term:`Jesus Christ` and
    decided to follow him.

  denomination

    A part of the :term:`Church` represented by an official organization that
    works according to human-made laws and rules, in order to cultivate and
    teach a given interpretation of the :term:`Bible` and the :term:`Good News`.

Most modern denominations try to reconcile with other denominations and
acknowledge them as being part of the :term:`Church`. On the other hand they all
also have a tendency to claim representing the whole Church.

Christian denominations give different answers to many concrete
:term:`controversial questions <controversial question>`.  Here are some
concepts used for classifying them along different scales of dichotomies.

In Christianity, as in every group, it is easy to observe different groups of
people holding up the :term:`Bible` and citing it as a proof for why the other
group is wrong. See also :doc:`/talks/controversial`.



Types of Christian faith cultures
=================================


.. list-table:: Controversial questions for Christians

  *
    - Justice. Responsibility. Equality. Same rules for everybody.
    - Mercy. Liberty. "Love and do as you will".

  *
    - militant, proselytizing, intrusive
    - reconciling, embracing, inclusive

  *
    - well-founded, true, clear, authorised, strong, loud
    - searching, shining, inspired, empathic, weak, silent

  *
    - To love means to care and protect.
    - To love means to encourage and tolerate.

  *
    - Only those who formally believe in Jesus can be saved.
    - Salvation is possible without formal belief in Jesus.


Misleading comparisons
======================

The opposite of "progressive" is not "traditional" but "conservative".

You cannot say that progressive people don't care about traditions. Somebody
with :term:`progressive faith` may want to change some tradition just *because*
they care about it.

Some authors differentiate "traditional" from "progressive" (or "modern", or
"post-modern").

Some authors designate :term:`progressive faith` as "liberal".  That's an
offensive misnomer. Progressive faith does not mean that everybody should be
allowed to do what they want. Every religious faith says that you must do what
*God* wants you to do and that this might differ from your own ideas.
Progressive Christians don't doubt about this. You cannot say or insinuate that
:term:`progressive faith` is less religious than :term:`conservative faith`.

See also
========

- https://www.patheos.com/blogs/faithonthefringe/2020/11/an-explanation-of-modern-christianity/
