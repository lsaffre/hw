====================
About fundamentalism
====================

Here is my compilation of `Wikipedia
<https://en.wikipedia.org/wiki/Fundamentalism>`__, `thefreedictionary.com
<https://www.thefreedictionary.com/fundamentalism>`__ and other sources.

.. glossary::

  fundamentalism

    Unwavering attachment to a set of irreducible beliefs.
    Strict adherence to certain scriptures, dogmas, or ideologies.

  fundamental

    Related to the base, the principal :term:`mission statement` of a
    corporation.

Fundamentalism is a tendency among certain groups (e.g. Christianism, Islamism,
Capitalism).

Fundamentalism is characterized by an emphasis on purity and the desire to
return to "the fundamental principles" or the established ("proven", "accepted")
interpretation of the group's :term:`Holy Scripture`.  A good fundamentalist
refuses diversity of opinion regarding these principles and their interpretation
within the group. A good fundamentalist finds it important to differentiate
between members and non-members of the group, which can lead to intolerance of
other views.

.. Fundamentalism is *not* evil.  When you have experienced, in your life, the
   beauty of the Gospel, then it is normal that you want to  eagerness
