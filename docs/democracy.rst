===============
About democracy
===============

.. contents::
  :local:


Theoretically the best method
=============================

Our western civilization is based on the assumption that governments should be
democratic.  History has told us that monarchies, aristocracies and dictatorship
fail to bring peace and welfare in the long run.

Democratic organizations are not free of problems.
The current implementations of democracy have their weaknesses.

Using nowadays technologies, even a sandwich bar is difficult to govern in a
democratic way.  Not to speak about a village, a country or a planet.  From a
sociological point of view we are still in the stone age of democracy.

"Democracy is a poor system of government at best; the only thing that
can honestly be said in its favor is that it is about eight times as
good as any other method the human race has ever tried."  [#heinlein]_

We are eager to see lots of bugs and issues in your local town administration,
our national government, the European Union and finally the United Nations.  But
note that in most countries we are allowed to write about these problems and
publish our writings.  Isn't this alone a reason to hope?  Let's be grateful for
having democracy. Let's believe in democracy.


Not everybody agrees
====================

Not everybody agrees that democracy is better than dictatorship.  Every year
brings at least one sad example of a dictatorship that tries to gain power over
their neighbours. My only explanation why people sometimes tend to reach back to
obsolete government methods is that they have not been informed correctly about
why they are obsolete.


Who holds the power?
====================

We must worry whether our democratic structures actually do govern the world.
Recent years reveal a *shift of power from national states to transnational
corporations* [#vatican]_.  This is a serious danger.  See also
:doc:`big_egoists`.


Who holds the power? Who is representative for the "people"?

"[O]ne of the many major problems with governing people is that of
whom you get to do it; or rather of who manages to get people to let them do it
to them.  To summarize: It is a well known fact that those people who most want
to rule people are, ipso facto, those least suited to do it. To summarize the
summary: anyone who is capable of getting themselves made President should on no
account be allowed to do the job."  -- Douglas Adams, The Restaurant at the End
of the Universe

The classical democracy model separates the power into three branches:
legislative, executive and judiciary.

Democracy is not good for everything
====================================

There are human activities that should be governed by democratic power and other
activities that shouldn't.  Art, Science, Culture, Education, Medicine should
remain independent of national power.



.. rubric:: Footnotes


.. [#heinlein] `Robert A. Heinlein
               <https://en.wikiquote.org/wiki/Robert_A._Heinlein>`_

.. [#vatican] `Ethics in Internet
              <http://www.vatican.va/roman_curia/pontifical_councils/pccs/documents/rc_pc_pccs_doc_20020228_ethics-internet_en.html>`_
              (Pontifical Council for Social Communications, Vatican
              City, February 22, 2002.
