======================
What "religion" means
======================

What do we mean when we say "religion"?
Just to mention that the topic is complex:
Wikipedia doesn't only have an article
about Religion, it has a separate article entitled  `Definition of religion
<https://en.wikipedia.org/wiki/Definition_of_religion>`__.

But let's keep it simple. Here are some definitions that work for me.

.. glossary::

  religion

    A collection of teachings expressing a comprehensive :term:`world view`
    specified by a :term:`Holy Scripture`.

    A :term:`faith culture` that identifies common patterns of
    :term:`faith` and formulates them in order to cultivate them.

    A social-cultural system of designated behaviours and practices,
    morals, world views, texts, sanctified places, prophecies, ethics, or
    organizations, that relates humanity to supernatural, transcendental, or
    spiritual elements. --`Wikipedia <https://en.wikipedia.org/wiki/Religion>`__

  culture

    A set of traditions, conventions and habits shared by a group of humans.
    Any given culture implies and cultivates a given set of values and
    convictions.

    A set of ideals that makes a group of humans cooperate efficiently by
    creating "artificial instincts", which accustome individuals "to think in
    certain ways, to behave in accordance with certain standards, to want
    certain things, and to observe certain rules".  --Yumal Harari, Sapiens,
    p.163 (adapted)

  world view

    The way an individual or group thinks about and interprets the world around
    them. --`Open Education Sociology Dictionary
    <https://sociologydictionary.org/worldview/>`__

    The fundamental cognitive orientation of an individual or society
    encompassing the whole of the individual's or society's knowledge and point
    of view. --`Wikipedia <https://en.wikipedia.org/wiki/Worldview>`__

    A comprehensive conception or apprehension of the world especially from a
    specific standpoint. Called also *weltanschauung*. --`Merriam-Webster
    <https://www.merriam-webster.com/dictionary/worldview>`__

  Holy Scripture

    A document that is used by a :term:`religion` as the base for their
    teachings.


Religion as a social phenomenon is the fruit of  our longing for an ultimate
truth.

A :term:`religion` gives me a language that I can use to
communicate more efficiently about :term:`faith questions <faith question>`.

Buddhism is a religion because it is a comprehensive :term:`world view` and
has holy scriptures.

:term:`Marketism <marketism>` is a comprehensive :term:`world view` with
spiritual components, but has no :term:`Holy Scripture`.

Football is a :term:`culture`, but not a :term:`religion` because it does not
embrace all aspects of life and has no :term:`Holy Scripture`.


"Unlike other great religions, Christianity has never proposed a revealed law to
the State and to society, that is to say a juridical order derived from
revelation. In­stead, it has pointed to nature and reason as the true sources of
law (...)." -- Pope Benedict XVI speaking to the German Bundestag, Berlin, 22
September 2011

..
  "Im Gegensatz zu anderen großen Religionen hat das Christen­tum dem Staat und
  der Gesellschaft nie ein Offenbarungsrecht, nie eine Rechtsordnung aus
  Offenbarung vorgegeben. Es hat stattdes­sen auf Natur und Vernunft als die
  wahren Rechtsquellen verwiesen (...)"
