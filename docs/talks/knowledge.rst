=============================
Storing knowledge sustainably
=============================

"The mission of Software Heritage is to `Collect, Preserve and Share
<https://www.softwareheritage.org/mission/>`__
:term:`source code` from as many
origins as possible. This is a humbling undertaking, as there is a huge number
of different code hosting and distribution platforms out there, using different
underlying technologies." (from `Introducing the Add Forge Now feature
<https://www.softwareheritage.org/2022/07/05/add-forge-now-new-feature-announce/>`__)

At the `Digital Assembly 2022
<https://www.youtube.com/watch?v=vuAchhnJ6PU&start=4320>`__, held in June, we
had an opportunity to discuss the role of Software Heritage as a key digital
commons with its ambitious mission to collect, preserve and make readily
accessible the source code of all publicly available software ever written.

(from `Planning for the future
the short, the medium and the long term
<https://mailchi.mp/softwareheritage/a-peek-at-the-plans?e=634c57cf81>`__)
