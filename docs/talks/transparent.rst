========================================
Must the Church become more transparent?
========================================

Transparency is an important thing nowadays, if you want others to trust you.
The Church has grown almost 2000 years in a world where people did not require
transparency. They just trusted the Church.

But nobody is fully transparent. And transparency is not cheap, it causes
considerable administrative effort. There is always a balance between what is
useful and what is realistic. If you try to explain to everybody why you act as
you act, you will never get your job done.
