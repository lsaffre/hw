================
About being bold
================

One principle of :term:`synodality` is that "all are invited to speak with
courage and parrhesia". **Parrhesia** (from Greek παρρησία (parrhēsía), πᾶν
"all" and ῥῆσις "utterance, speech") means literally "to speak everything" and
by extension "to speak freely" or "to speak boldly" (via `Wikipedia
<https://en.wikipedia.org/wiki/Parrhesia>`__).

We can translate *parrhesia* as :term:`boldness`.

.. glossary::

  boldness

    Boldness is the opposite of fearfulness. To be bold implies a willingness to
    get things done despite risks. Boldness may be a property that only certain
    individuals are able to display.  (via `Wikipedia
    <https://en.wikipedia.org/wiki/Boldness>`__)


Of course there are unpleasant things that we need to accept. Because we cannot
change them. It would be a waste of energy to continuously complain about things
we cannot change. And of course it is not always easy to discern the things we
can change from those we can't. Of course, if you are really the only one to see
a problem, you need to humbly ask yourself whether your inner voice is right,
whether it is really important and whether you really have the duty to disturb
the process with your concerns. Sometimes we lack humbleness or patience. But if
you comply with the majority against your clear voice and just because you are
too lazy or shy or arrogant to talk about the problem, then you just sweep the
dirt under the carpet. The problem will remain and eventually grow. Silencing
down minority opinions in controversial discussions is not the synodal way of
finding peace because it leads to lazy compromises, loss of motivation and
increased work-to-rule.

See also :doc:`/talks/teamwork`, :doc:`/sc/parrhesia`.
Inspired by :doc:`/blog/2021/1215_2207`

"In a situation of injustice and oppression
there can be no neutrality.
You have to take side.
You have to say
am i on the side of justice
or am i on the side of injustice."
-- Desmond Tutu
(via `etv.err.ee <https://etv.err.ee/1608431381/eesmark-leida-roomu-raskel-ajal>`__)
