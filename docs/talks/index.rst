========
Talks
========

Here is a collection of "talks" I have held or love to hold whenever there is
somebody who listens.

.. contents::
  :local:



Introductions
=============

.. toctree::
    :maxdepth: 1

    atheism
    my_sins
    heaven
    clear
    good_news
    controversial
    cross
    convictions
    personifying_reality
    depression
    eternal
    bcc
    the_only_thing

The digital era
===============

.. toctree::
    :maxdepth: 1

    /fs/index
    /fb_wm
    guide_to_internet
    hot

About faith
===========

.. toctree::
    :maxdepth: 1

    /faith
    /glasses
    /existential_questions
    /meaning
    faithful
    /faith_camps
    /religion
    /revolution
    /prayer
    /sin
    /miracles
    /confession
    name_of_god
    calling
    /idolatry
    /letters
    slicing


Life itself
===========

.. toctree::
    :maxdepth: 1

    /love
    /sex
    /disorder
    /lgbt
    /homosexuality
    /abortion


My favourite planet
===================

.. toctree::
    :maxdepth: 1

    /greta


.. _talks.politics:

About politics
================

"I am puzzled about which Bible people are reading when they suggest that
religion and politics don't mix." -- Archbishop Desmond Tutu (b. 1931)

.. toctree::
    :maxdepth: 1

    /plan
    /democracy
    money
    /law
    /un
    /territories
    /cvc
    /liberal
    /challenges
    /respublica
    /greedy_giants
    /public_corporations
    /get_control
    /whistleblowing
    /corporations
    /property
    /privacy
    leaders
    disarmament
    nato_ukraine
    war


About life
================

.. toctree::
    :maxdepth: 1

    /names
    /giving
    /mistakes
    /useless
    /advice1
    /trust
    /fundamentalism
    /liar_king
    /traditions
    /prudence
    competition


About the Church
================

.. toctree::
    :maxdepth: 1

    /sc/index
    /credo
    /christianity
    /catholic
    /christian_values
    /bible
    /gospel
    /kingdom
    /scaring
    /atonement
    /saviour
    /spreading
    /deity
    /future
    /hypertrophies
    cntp
    confess
    eucharist

.. toctree::
    :maxdepth: 1

    /state_religion
    /individualism
    /nature
    transparent
    humans_are_like_computers
    vaccinate

Lingustics
==========

.. toctree::
    :maxdepth: 1

    gnl
    whichvsthat


Unclassified
============


.. toctree::
    :maxdepth: 1

    /science
    /style_guide
    /stop_believing
    listening
    parrhesia
    holy
    friends
    /game
    repent
    moral
    dangerous_rights
    identity
    hornbill
    gospel60
    lucky
    tertium
    praise
    good_enough
    teamwork
    narcissism
    beatitudes
    friend_jesus
    bad_christians
    agile
    knowledge
    giving_up_a_discussion
    indicator_questions
    dust_off_your_feet
    itrustinreality
    /soc
