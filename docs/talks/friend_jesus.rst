============================
Imagine Jesus watching you
============================

Here is a simple spiritual game that I suggest you to learn: imagine Jesus as
your friend who is always next to you. Whatever you do or decide: imagine that
he is watching you.

This is not like "Big Brother is watching you" because you imagine Jesus as your
true and best friend. He is not like a big brother or your colleagues your
father or your mother, who want to control you.  He forgives you everything
before you even realize that there is something to regret about. He will never
hinder you from doing something. He always understands you really. He comments
only when you are ready to listen.

Inspired by John 20:19-31 and by father Igor's homily 2022-04-24.
