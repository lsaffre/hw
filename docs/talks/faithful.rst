==================
Who is "faithful"?
==================

The :term:`Roman Catholic` church uses the term :term:`faithful` to designate
its members, i.e. those who have been baptized using the procedure recognized
by :term:`Roman Catholic` church. ([CIC1983]_, `204
<https://www.vatican.va/archive/cod-iuris-canonici/eng/documents/cic_lib2-cann204-207_en.html>`__)

I try to avoid this meaning of the word because it is offending other Christian
:term:`denominations <denomination>` or people who declare to believe in the
:term:`Gospel` but refuse to join some concrete church institution.

The official French translation of :term:`faithful` is *fidèle* (loyal). That's
more understandable. The German translation *der Gerechte* (German) sounds weird.


Christians can indeed use the word :term:`faith` to designate the faith in the
:term:`Gospel`.  For Christians, a :term:`faithful` is a human who "understood"
the :term:`Good News` and "decided" :term:`to believe` in it.\ [#Merton]_ Also
other religious institutions use the word "faithful" in that specific meaning of
belief in their specific teachings. But this usage becomes questionable in
multicultural contexts because it assumes that there is one "right" religion.



..
  No human institution can decide whether you are :term:`faithful` or not.
  Only God will judge you in the end of times.
  All :term:`people of good will` are part of the :term:`Church`.



..
  Some of my friends say that they are not :term:`faithful`.
  They mean that they don't care about the :term:`Roman Catholic` church?
  follow any :term:`religion` or :term:`denomination`. I prefer to say
  that they are :term:`non-religious`. Which does not necessarily mean that they
  are less "faithful" than people who follow some religion.

..
  .. glossary::
  people of good will
    A synonym for :term:`faithful`.


.. rubric:: Footnotes


.. [#Merton] "Faith is a decision, a judgment that is fully and deliberately
   taken in the light of a truth that cannot be proven--it is not merely the
   acceptance of a decision that has been made by somebody else."   `Thomas Merton
   (1915-1968) <https://en.wikipedia.org/wiki/Thomas_Merton>`__
