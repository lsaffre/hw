==============================
Shaking the dust off your feet
==============================

There are situations where you have no other choice than to leave somebody on
their own and to stop wasting your energy trying to help them. This can be
frustrating and cause anger on both sides. But it's not good to remain angry or
feel offended for too long. Reality is more complex than a single human can
grasp. The best action in such situations is to keep a distance while remaining
open to the truth, which obviously is somewhere beyond your differing opinions
and may remain hidden for the rest of your lifetime.

"And if any place will not receive you and they will not listen to you, when you
leave, shake off the dust that is on your feet as a testimony against them."
(`Mk 6:11 <https://www.bibleserver.com/ESV/Mark6%3A11>`__)

"And if anyone will not receive you or listen to your words, shake off the dust
from your feet when you leave that house or town." (`Mt 10:14
<https://www.bibleserver.com/ESV/Matthew10%3A14>`__)

"And wherever they do not receive you, when you leave that town shake off the
dust from your feet as a testimony against them." (`Luke 9:5
<https://www.bibleserver.com/ESV/Luke9%3A5>`__)

"But they shook off the dust from their feet against them and went to Iconium."
(`Acts 13:51 <https://www.bibleserver.com/ESV/Acts13%3A51>`__)


Inspired by:

- https://www.gotquestions.org/shake-dust-off-feet.html
- https://idioms.thefreedictionary.com/shake+the+dust+from+feet
- https://whatjesussaidtodo.com/2022/01/14/shake-off-the-dust/
