=========
About war
=========

Currently just a collection of ideas.

.. glossary::

  arms trade

    The business of selling arms to those who have the money for buying them.

- `Arms industry <https://en.wikipedia.org/wiki/Arms_industry>`__
- `Swords to ploughshares <https://en.wikipedia.org/wiki/Swords_to_ploughshares>`__
- :doc:`nato_ukraine`
