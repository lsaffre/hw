===================================
The noisy hornbill and the ladybird
===================================

The noisy Hornbill kept quacking through the forest. A Ladybird was worried that
the noise of the Hornbill might bring trouble to the forest. "But I don't dare
to talk to him. I am so small. Maybe he will eat me." So the Ladybird asked her
friends: the Liana, the Banana tree, the Mushrooms, the Snail. But they all said
"Hornbill's problems are Hornbill's problems. It's none of our business."

One day, a hunter whose traps had not caught any game heard the Hornbill and
followed the noise. He shot it off the tree. The hornbill fell on an ant hill
where there were mushrooms. And the hunter saw the mushrooms and took them to
eat them with the Hornbill. He found a Snail next to the mushrooms and took it
as well to make a good soup.  Since he had no bag to carry all what he had
gathered, he cut a banana tree and took its leaves to package his hunt, and a
liana to tie them with.

Inspired by:

- https://www.youtube.com/watch?v=K6At7zDR2TY&t=65s
  Brother Githinji about Luke 10:25-37

- https://kevinwamaya.wordpress.com/2015/09/18/before-you-mind-your-own-business/

- https://nationofafrica.blogspot.com/2011/06/allegory-of-hornbill.html
