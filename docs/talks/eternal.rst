==================
About eternal life
==================

Christians believe that when we die, we actually get born into another world,
where our soul will live until the end of times.

- "I believe in (...) the life everlasting." (`Apostles' Creed <https://en.wikipedia.org/wiki/Apostles%27_Creed>`__)

- "We believe in (...) the life of the world to come." (`Nicene Creed
  <https://en.wikipedia.org/wiki/Nicene_Creed>`__)

Like a foetus in its mother's womb must develop most parts of its body *before*
birth, and failing to do so cannot be repaired afterwards, our soul must develop
certain things during its life on earth when living in its human body. And
failing to do so cannot be repaired afterwards.

This belief in *resurrection* is more dramatic than a belief in *rebirth* or a
belief in *nothing*. Rebirth means that you get a new chance after death.
Believing in *nothing* means that the game is simply over when you die. It's
easy to live when you believe in *rebirth* or in *nothing*. Even in the worst
scenario and if your whole life turns out to be a complete failure, you can ask
"so what?"

But once you have realized that

| *maybe* my soul
| will remain for eternity
| as it will have been
| in my hour of death,

then you won't get rid of it any more.
You can hide it under your carpet, but that's not a sustainable solution.

Believing in resurrection puts a huge challenge to your life. And note that I
say to your *life*: Christian faith is not about death, it's about life.

If these thought scared you, then make sure to never read `Luke 16:19-31
<https://www.bibleserver.com/ESV/Luke16%3A19-31>`__ because that's yet another
alarming story.

Yes, sorry: the :term:`Good News` is no opium for the people.
