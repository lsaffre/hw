=======================
When God is calling you
=======================

.. glossary::

  divine call

    When God personally calls a particular human to do something particular in
    the :term:`visible world`.

How do you recognize that God calls you? One symptom is the feeling that this is
rather a call from "outside" than "my own choice or decision".

How to be sure that it is God who called, and not some evil spirit, or my own
vanity? Answer: You can't be 100% sure. In simple situations you can discern
afterwards whether this was God. In other cases it can take generations before
the people after you can tell this. So you should always remain vigilant and
humble, listen to the reactions of other people. On the other hand you need to
be :term:`resistant to advice`.
