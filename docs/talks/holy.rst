==============
About holiness
==============

There are people we can describe as "Nothing is holy to him".


.. glossary::

  holy

    Unreachable to our reason.
