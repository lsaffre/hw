===============
The name of God
===============

Christians say that *God's name* should be hallowed.  God is so unreachably
beyond any cognitive human knowledge that even his *name* is holy to us.

The name of a being or an object is something that a group of humans decides to
use for referring to it.  Giving a name implies that we know what we are talking
about.

But do we know what we are talking about when we say :term:`God` or :term:`Will
of God`?  We can :term:`believe <to believe>` something about God or his plans,
but we cannot :term:`know <to know>` them. The name of God is not definable by
any human :term:`science`.

This is what the first commandment tells us when it states "Thou shalt
not make unto thee any graven image" or when Jesus instructs us to pray
"Hallowed be your name".

Which in turn simply means that **we must not hurt each other in the name of
God**.

Humans can have very opposing answers to :term:`controversial questions
<controversial question>` and still respect each other.  Because the name of God
is holy.

Idolatry, in that context, is not the creative act of trying to paint or grave a
picture of God, but the spiritual attitude of believing that you know God.

As Christians we believe that we have been given a name for God: :term:`Jesus
Christ`. But this does not mean that we *know* God, it means that we can talk to
him, that we can enter into relation with him. Standley Hauerwas describes this
beautifully in his article `Naming God: The Burning Bush, the Cross and the
Hiddenness of the Revealed God
<https://stanleyhauerwas.org/naming-god-the-burning-bush-the-cross-and-the-hiddenness-of-the-revealed-god/>`__
where he concludes:

  Yet we believe that the God we worship has made his name known. We believe we
  have been given the happy task of making his name known. We believe we can make
  his name known because the God we worship is nearer to us than we are to
  ourselves – a frightening reality that gives us life. (...) By being consumed by
  the Divine Life we are made God's witnesses so that the world may know the fire,
  the name, Jesus Christ.

The Jewish culture usually used "JHWH" as the name for God. Which is translated
to "Lord" in modern Bibles.
