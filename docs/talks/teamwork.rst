======================
About synodal teamwork
======================

.. contents::
  :local:

Democratic teamwork is not synodal
==================================

Synodality is not a synonym for "efficient teamwork". I'd even say that a
wide-spread or classical understanding of "teamwork" is rather the opposite of
synodal conciliation.


.. list-table:: Differences between democratic teamwork and synodality

  *
    - democratic teamwork
    - synodal teamwork

  *

    - During our meetings we don't waste time with emotional discussions when
      there is no solution in sight. We cannot let others hinder us from doing
      our work peacefully if they are just emotional and do not even understand
      the topic.

    - When one member of the community insists that "something" is wrong, the
      community needs to listen. Synodality requires consensus, not
      majority. [SR1]_

  *
    - We know what we want. We have a clear plan.

    - We can have a mission, principles, strategies and plans, but we actually
      never know everything in advance and each of these documents may change at
      any time.


Consensus rather than majority
==============================

One of the differences between (classical) democracy and synodality is the
following rule:

.. [SR1] as long as one member of the community insists that "something" is
  wrong, the community needs to listen. Synodality requires consensus, not
  majority.

When a community has a problem that a majority fails to see, saying "let's be
polite and not speak about, let's avoid discord and battle" is not a sustainable
solution.

What makes a synodal team successful?
=====================================

My cheat sheet for the leader of every :term:`synodal community`. Inspired by
:term:`HaasMortensen201606` and other sources (e.g. Natürliche
Gemeindeentwicklung).

- The personalities, attitudes, or behavioural styles of the team members don't
  matter. What matters are certain `enabling conditions`_.

- Add members only when necessary. Larger teams are more vulnerable to poor
  communication, fragmentation and "free riding" (due to a lack of
  accountability). Not more members than the minimum. (:term:`More than good
  enough is too much`).

- Teams are vulnerable to two "corrosive problems": "us versus them" thinking
  and incomplete information

Enabling conditions
===================

- Compelling direction  -- we have a goal that we want to reach

- Strong structure -- well-designed rules, norms, tasks and processes.
  Discourage destructive behaviour.

- Diversity in knowledge, views and perspectives.
  The right mix of "cosmopolitan" and "local" members.

- Supportive context:

  - a good reward system (reinforces good performance)
  - a good information system (provides access to the data)
  - a good educational system (training)
  - material resources (funding, technological assistance)

- Shared mindset (not each member needs technical or social skills, but the team
  overall needs a healthy dose of both)


Destructive behaviour
=====================

- withhold information
- pressure people to conform
- avoid responsibility
- cast blame upon individual members


Other texts
===========

See also :doc:`/blog/2022/0912_1130`
