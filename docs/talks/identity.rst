=====================
Identity as a value
=====================

(not finished)

Let's talk about **identity**.
The topic deserves particular attention when discussing about :doc:`/lutsu/index`.

.. glossary::

  identity

    The set of beliefs, personal traits and qualities (skills, look, voice,
    style, behaviour, ...) that make up a person or a group of persons.

Every human being has an identity.
Some call it the **soul**.

Also every group of people (:term:`corporation`) has its identity.

"Instead of defining ourselves in opposition to others, can we develop an
identity and a sense of belonging that does not exclude openness to others?". --
Br. Alois, `Becoming Creators of Unity
<https://www.taize.fr/en_article32847.html>`__, December 2021

Identities usually have a :term:`name`. Sometimes a same identity has different
names ("A good child has many names" -- Estonian proverb). But the name is just
how we call them.

.. glossary::

  name

    A sequence of sounds that we use to refer to a person, an animal, an object
    or a :term:`shared idea`.

  reputation

    The public opinion about a person, animal, object or :term:`shared idea`.

    But what means *public* opinion? The average of all opinions? And how could
    we measure it? Other authors seem to say that reputation is the opinion of
    a *single person* (about another person, an animal, an object or a
    :term:`shared idea`). To be meditated.

    https://en.wikipedia.org/wiki/Reputation
    https://blog.reputationx.com/whats-reputation

Every identity has a given :term:`reputation`. The reputation of a person or
:term:`corporation` is the result of their activity. Therefore it makes sense to
say that they *own* it. Reputation is something you can own, it is a potential
object of :term:`private property`.

Every :term:`reputation` represents a commercial value. You can use a reputation
to make money. In general it is not fair to make money from someone else's
reputation.

Many :term:`shared ideas <shared idea>` also have a :term:`logo`.

.. glossary::

  logo

    An image or symbol that is used to refer to a :term:`shared idea`.

Not only *human* beings have an identity.
A dog or a cat can have an identity (`list of famous cats <https://en.wikipedia.org/wiki/List_of_cats>`__).
Some plants have an identity.
For example the `Kernu juniper <https://et.wikipedia.org/wiki/Kernu_kadakas>`__, which died in 2013 at the venerable age of about 300 years.
I wouldn't say that a fly has an identity.
But I'd say that these identities of non-human beings are an edge case.
Even `Hamish McHamish <https://en.wikipedia.org/wiki/Hamish_McHamish>`__
didn't really care about his :term:`reputation`.


.. rubric:: Illustrations

Let's read about
`Clever Hans <https://en.wikipedia.org/wiki/Clever_Hans>`_
an its trainer and owner, `Wilhelm von Osten <https://de.wikipedia.org/wiki/Wilhelm_von_Osten>`__.
We can say that "Clever Hans" is the name of an identity.
The horse was famous and was the carrier of its reputation.
But horses do not own things. so von Osten was the owner of this reputation.

The :term:`logo` of  `Mercedes-Benz
<https://en.wikipedia.org/wiki/Mercedes-Benz#Logo_history>`__ was registered as
a trademark in 1909.

Imagine some rich guy calling the CEO of Mercedes-Benz and asking "I want to buy
your logo. How much do you ask? Your price will be mine." I imagine that the CEO
won't even think about a price. A logo is an unalienable part of your identity.

There are constantly new identities emerging. `What is an indie developer
<https://grumpygamer.com/what_is_an_indie>`__?



.. todo .. glossary::
  soul
  fame
  shame
  defamation
