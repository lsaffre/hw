====================
Personifying reality
====================

The Jewish people invented the approach of personifying reality.
They were probably the first in history to cultivate a monotheistic religion.

The story where Moses asks God "when they ask me, ‘What is his name?’ what shall
I say to them?" and God answers "I am who I am" (`Exodus 3:14
<https://www.bibleserver.com/ESV/Exodus3%3A14>`__) was genial and revolutionary.

Monotheistic religions basically personify :term:`reality`. They call it
:term:`God` and then start talking to it. And they imagine that this God
communicates with them. If you define God like this, there is obviously *one and
only one* God. Such a God must be the same for everybody.


.. glossary::

  God

    The hypothetical unique being assumed by monotheistic religions as the
    creator of the world.

"God is not a person;
God is a mythic personification of reality. If we miss this we miss everything."
-- `Rudolf Bultmann
<https://en.wikipedia.org/wiki/Rudolf_Bultmann>`__, "one of the major figures of
early-20th-century biblical studies, a prominent critic of liberal theology
(...) and a proponent of dialectical theology"

When you personify something, you must give it a name.
See :doc:`/talks/name_of_god`.
