========================
Dare to change your mind
========================

Being **open-minded** means that you are able to listen to opposing opinions.

Being open-minded is **not** being fickle or shilly-shally but rather the
opposite. The stronger and the more convinced your opinion is, the more are you
able to listen to opinions that differ from yours.

Being open-minded includes readiness to change your mind (:term:`to repent`)
when you realize that you were wrong.



..
   and , to change "your mind" i.e. your
  :term:`conviction`, about some concrete question, if needed, ,
  This happens when you realize that God calls you to do so.

Changing your mind about which pullover to wear at an event can be difficult
when your wife has a different opinion than you, but we probably agree that it
is not an existential change.

The bible tells a story about how a fervent enemy of Jesus became his follower
after having experienced God's call. This was a more existential change of mind.
This guy was struck with blindness and did neither eat nor drink during 3 days.

Another story in the Bible tells how Joseph (husband of Mary) had to
fundamentally change his mind about marrying pregnant women.

Changing your mind is problematic and frowned upon in classical teamwork. When
some common decision has been made, to which you agreed, then it is considered
bad practice to change your mind. For example let us imagine that you are in the
shop with your 3 years old daughter and ask her: ice cream or chocolate bar? She
answers "chocolate bar", you put the chocolate bar into your shopping cart and
continue shopping. Two minutes later (you are still in the shop but in the next
department) she changes her mind and decides that she prefers ice cream. You
will probably shout (or think) something like "OMG! Can't she make up her mind
in due time?"

Our mind is like a radar monitor. We constantly send out "listener signals" and
then record the echo we receive\ [#radar]_. Our mind is always "directed",
turned into a given direction. We see only the things we are looking at\
[#radar2]_. We need to unlock our radar antenna and have it constantly turn in
order to listen to every direction.


.. rubric:: Footnotes

.. [#radar] Reality is of course more complex because we
  also record incoming signals that are caused by something else than our
  listener signal. For example when the sun shines, the optical signals we
  receive are not an echo to our listener signal.

.. [#radar2]  Sometimes we perceive a movement outside of the focussed area, and our
  eyes then "automatically" redirect, turn into the new direction.
