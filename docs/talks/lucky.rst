==============
Are you happy?
==============

I used to ask this question to my children every now and then.

After hearing :doc:`/blog/2022/0403_1942`, I started asking it to other people
as well, collecting (anonymous) answers to this question.

- "Kas sa oled õnnelik?" -- "Mis mõttes?" -- "Noh üldselt" -- (mõtleb veidi)
  "Jah".

- "Kas sa oled õnnelik?" -- "Eesti keeles on 'õnnelik' pigem hetkeline eufooria
  kui üldine seisund" -- "See on nii ka inglise ja prantsuse keeles. Sa pöördud
  teemast eemale." -- "Ma ei vasta sellele küsimusele. Ma lähen kaitseasendisse,
  kui sa niimodi küsid."

- "Bist du glücklich?" -- "Kann ich nicht sagen. Eher nicht."

NB  There are two quite different things called either "happiness" or "joy": (1)
the (temporary) **emotion** and (2) the (lasting) **attitude** or
**self-image**. It seems that these words get mixed up. I tend to call the
temporary emotion "joy" and the long-term attitude or self-image "happiness",
but some authors seem to do the opposite. My question is of course about the
long-term attitude or self-image.

Can we measure happiness?
=========================

Currently just a collection of links.

`What the Longest Study on Human Happiness Found Is the Key to a Good Life
<https://www.theatlantic.com/ideas/archive/2023/01/harvard-happiness-study-relationships/672753>`__
The Harvard Study of Adult Development has established a strong correlation between deep relationships and well-being. The question is, how does a person nurture those deep relationships?
By Robert Waldinger and Marc Schulz.
The Atlantic. January 2023.

`What Makes Us Happy?
<https://www.theatlantic.com/magazine/archive/2009/06/what-makes-us-happy/307439/>`__
Is there a formula—some mix of love, work, and psychological adaptation—for a good life?
By Joshua Wolf Shenk.
The Atlantic. June 2009.

The World Happiness Report (WHR):
https://worldhappiness.report/

March 2019 :
`Finland is the world’s happiest country – again
<https://www.weforum.org/agenda/2019/03/finland-is-the-world-s-happiest-country-again>`__

:doc:`/blog/2022/0403_1942`

January 2023 : `Finland tops World Happiness Report for fifth time in a row, highlights trust, wellbeing and freedom – other Nordics in top 8
<https://finland.fi/life-society/happinessreport2022/>`__

January 2023 : Frank Martela (`cnbc.com,
<https://www.cnbc.com/2023/01/05/what-people-in-finland-happiest-country-in-world-never-do-according-to-psychologist.html>`__)
names three important things Finns do not do:
We don't compare ourselves to our neighbors.
We don't overlook the benefits of nature.
We don't break the community circle of trust.


Quotes
======

"Die glücklichsten Menschen sind die, die mit dem wenigsten zufrieden sind."
"The less you care the happier you will be." (Mahatma Gandhi?). "The less you
want, the more you have.".

"If we don't have what we would like to have, we must like what we have" (from
"Wenn mer nit dat hät wat mer jän hätt, muss mer dat jän han, wat mer hätt!" via
https://verliebtinkoeln.com)

"The man is richest whose pleasures are cheapest." --- Henry David Thoreau
