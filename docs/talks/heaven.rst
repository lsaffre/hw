===========================
Heaven is a wonderful place
===========================

"Our father in Heaven", that's probably one of the best-known Christian title
when talking about --or with-- God.

What is Heaven? It is probably not that "big bluish-black thing" that covers the
world (as :term:`Timon explains to Pumba <PumbaTimon>`). But note that only a
few generations ago, in 1961, long after the Middle Age and Enlightenment,
Soviet leadership thought it important to propagate that Yuri Gagarin, the first
human who left our planet for a while, would have announced "I went up to space,
but I didn't encounter God." (It seems that Gagarin actually never said these
words (`source
<https://www.beliefnet.com/columnists/on_the_front_lines_of_the_culture_wars/2011/04/yuri-gagarin-first-human-in-space-was-a-devout-christian-says-his-close-friend.html>`__),
but that's another story.)

I am surprised each time when I notice that some humans still imagine
:term:`Heaven` as a *place*. Heaven is not a place, it is an attitude of our
:term:`heart`, a state of mind, a way of perceiving this world. And it is not
something we can reach only after our death.  It is already here among us,
within us.

.. glossary::

  Hell

    A state of mind you don't want to be in.

  Heaven

    A state of mind you want to be in.


Arthur C. Brooks (via `theatlantic.com 20220208
<https://www.theatlantic.com/magazine/archive/2022/03/why-we-are-never-satisfied-happiness/621304/>`__)
explains why Jimi Hendrix's song "I can't get no satisfaction" is so deep.
Lasting satisfaction is another word for :term:`Heaven`, our failure to keep it
is just another word for "Mother Nature's paradox" that causes us to be excluded
from Paradise. He explains how already Thomas Aquinus explained "the
satisfaction problem as one of misbegotten goals: idols that distract us from
God, the true source of our bliss." And he says that "Even if you are not a
religious believer, Thomas's list of the goals that beguile but never satisfy
rings true. They include money, power, pleasure, and honor." He concludes "Each
of us can ride the waves of attachments and urges, hoping futilely that someday,
somehow, we will get and keep that satisfaction we crave. Or we can take a shot
at free will and self-mastery. It's a lifelong battle against our inner caveman.
Often, he wins. But with determination and practice, we can find respite from
that chronic dissatisfaction and experience the joy that is true human freedom."

For a Christian, Heaven means lasting satisfaction. A satisfaction that
remains forever when we enter eternity, but which we can smell and feel already
during our life in the :term:`visible world`: just by :term:`believing <to
believe>` in the :term:`Gospel`.

..
  (not as a punishment but as an unavoidable side
  effect of being able to differentiate between "good" and "evil").

The Bible is full of images that illustrate our search for lasting satisfaction.
The "children of light" are those having their heart in Heaven, the "children of
this world" are those who are still in Hell. (`Luke 16:8
<https://www.bibleserver.com/ESV/Luke16%3A8>`__)

Keith Giles provides a beautiful explanation in his book "Jesus Undefeated:
Condemning the False Doctrine of Eternal Torment" (via `Why Bad People Will Be
In Heaven
<https://www.patheos.com/blogs/keithgiles/2020/09/why-bad-people-will-be-in-heaven>`_),
which I summarize --or expand-- as follows:

  When I will die, my soul will get born into another world, which we can call
  "Heaven". But only the "good" parts of my personality will be part of my soul.
  These "good" parts of my personality are the :term:`good` skills and virtues I
  developed during my life on Earth. The :term:`evil` parts will be removed,
  "burned" in a purifying "fire".  All humans will be there in Heaven, but they
  will be purified. Even Adolf Hitler is in Heaven because he also had skills
  and virtues that were in harmony with God's plan, regardless of the mistakes
  he made and the evil things he caused.

We all are somewhere between these two extremes. No one is fully in the light or
fully in the world. We all have our personal history, our character, our
individual skill set. During our lifetime we constantly move back and forth,
following "temptations" and "calls". Where temptations are calls from an unholy
spirit. Calls are calls from a holy spirit.

The important thing in life is to somehow manage to be on the right side of the
scale at the moment of our death. Because this is where we get born into
eternity. After this moment we will no longer have a chance to change our mind,
we will remain there forever.

A person can live for decades on the left side without ever seeing the right
side, and still she can get saved during the last hours of her life. And the
opposite is true as well. We don't know the hour of our death. So be prepared.

"Ihr werdet schon in diesem Leben beginnen, den Vorgeschmack des ewigen Lebens
zu kosten; denn die Glückseligkeit der Seele im Himmel wird vor allem darin
bestehen, für immer im Willen des Vaters gefestigt zu sein. So kostet sie die
göttliche Süße, aber sie wird sie nie im Himmel verkosten, wenn sie sich nicht
auf Erden, wo wir Pilger und Wanderer sind, bereits damit bekleidet hat." -- Hl.
Katharina von Siena (1347-1380), Brief 102 an Dom Christophe, Nr. 56 (Lettres,
Téqui 1976, tome 1, p. 673–674, rev.; ins Dt. trad. © Evangelizo)


See also :doc:`beatitudes`.
