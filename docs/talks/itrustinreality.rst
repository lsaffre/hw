==================
I trust in reality
==================

I trust in :term:`reality` rather than in :term:`science`.

Science is just our image of reality, what we assume to know about it.
