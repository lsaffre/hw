=======================
Prayer for disarmament
=======================

Written in April 2022.

Oh my God,

regarding the conflict in Ukraine I pray more than ever for a disarmament that
also covers conventional weapons.

Russia's invasion to Ukraine on February 24 was an obvious political mistake and
causes innocent people to suffer.  But the increasing armament and expansion of
the NATO during the last decades is another obvious mistake.
Something must have led Russia into a mood
where mistakes are more likely to happen.
Limiting freedom of speech is a mistake.


I hope that Russia will admit their own part of responsibility in this political
accident and will make reasonable suggestions for repairing the damage as far as
possible.

I don't agree with those who say that weapons are needed when settling conflicts
between nations. I believe the opposite: all kind of weapons should be banned
forever for our political conflicts. When settling conflicts between
peers (be it between two humans or between two nations) weapons cannot do any
good. Our laws should say that it is a crime to make and to sell them.

I am not fully against weapons: they can be useful and needed by police to
control an individual who lost control of themselves and causes harm to other
people. This is not a conflict between peers. It is rather like a father who
uses force and maybe even hurts his 5 year old son in order to prevent him from
running into a street with dense car traffic.

I am worried to see that mainstream media in the NATO countries is focused on
Russia's mistake while hiding our own mistakes. Help us to become more aware of
our own guilt. Help us to renounce from hate speech and to enter into
co-responsible dialogue.

I am worried whether sanctions against Russia will help Putin and his advisors
to understand their mistake. Let us not give up dialogue. Help us to be bold
without making mistakes on our side. Help us to find solutions to which Putin
and his advisors can agree without loosing all of their honour.

Maybe the conflict in Ukraine is even a part of your big plan towards world-wide
disarmament? I can imagine that the NATO would have the courage of proceeding to
unilateral disarmament and ban all conventional weapons from their own
territory. Unilateral disarmament would be something like holding the other
cheek. (:doc:`nato_ukraine`)


And as usual: let *your* will be done.
You don't need my advice, but I need yours.
Help me to understand how I can help.
Praying with you triggers my imagination.
I try to understand your divine thoughts.
I feel them as hopes, fears and ideas.
I try to formulate then using my human vocabulary.
If the words on my website are wrong or misleading,
let me know so that I can update them.

Amen.



This document is inspired by

- https://www.un.org/en/global-issues/disarmament

- `Eugen Drewermann antwortet seinen Kritikern <https://youtu.be/WMHqdemalY0>`__

- `Eine erste Reaktion zu den aktuellen Ereignissen von Reiner Braun und Willi
  van Ooyen vom 24. Februar 2022
  <https://nie-wieder-krieg.org/2022/02/24/eine-erste-reaktion-zu-den-aktuellen-ereignissen-von-reiner-braun-und-willi-van-ooyen-vom-24-februar-2022/>`__

- https://www.worldfuturecouncil.org/peace-restoration-ukraine-crisis/

TODO:

- This is the whole world's inexcusable mistake. We could have avoided so many
  deaths if a Basic Income had been introduced to the world. UBI will reduce
  humanity’s aggression and destroy the economic, political, and psychological
  causes of war. -- Irina Soloveva, Jr. in `Ukraine's Basic Income: an antidote
  to war
  <https://basicincome.org/news/2022/04/ukraines-basic-income-an-antidote-to-war/>`__

- `Queen of Spain wishes Ukrainian soldiers victory.
  <https://www.democraticunderground.com/100216641071>`__
