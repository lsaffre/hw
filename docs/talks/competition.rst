=================
About competition
=================

One of the basic axioms of :term:`capitalism` is that humans want to be better
than their neighbour. Without :term:`competition` they tend to get lazy.

.. glossary::

  competition

    The activity or condition of striving to gain or win something by defeating
    or establishing superiority over others.

But then it seems that I am not a good capitalist. I always wondered how winning
--or loosing-- can trigger such hefty emotions in other people. I always
wondered what's wrong with being lazy.

So I rejoiced when I read this:

  "I'm not interested in competing with anyone. I hope we all make it."

  (seems to be attributed to an author named Erica Cook)

TODO:

- `Zero-sum game <https://en.wikipedia.org/wiki/Zero-sum_game>`__
