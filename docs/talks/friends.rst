=============
About friends
=============

When I moved from Belgium to Estonia at the age of 30, one of my biggest worries
was: will I lose my friends?

- 2021-11-22 `Stress is a health hazard. But a supportive circle of friends
  can help undo the damaging effects on your DNA
  <https://theconversation.com/stress-is-a-health-hazard-but-a-supportive-circle-of-friends-can-help-undo-the-damaging-effects-on-your-dna-171607>`__.

Here is how I currently define "friend":

.. glossary::

  friend

    Somebody to whom you do not refuse your help
    even when it requires a reasonable effort

Of course "reasonable" still remains subjective.

This definition is special in that it turns the classical definition
upside-down: the condition lies within me, not within the other person.

I don't need to ask "Are you my friend?", I can say " I consider you as my
friend and I hope that you also consider me as your friend."
