===========================
Slicing reality into chunks
===========================

We need to slice reality into chunks because that's the only way for storing
knowledge. Our brain is basically nothing but a bunch of neurons that react to
an incoming signal by either "firing" or not. Saying that we "realize" something
means ultimately that some neuron has fired.

Me kasutame erinevad mudelid. Astroloogia jagab inimesi 12 rühmadeks, enneagramm
9, temperamendid 4 ja seksualistid 2 rühmadeks. Aga Jumal ei tee vahet.

Jagunemine aitab meil mõelda. Igal süsteemil on plussid ja miinuseid.
Loomulikult on seksuaalsus bioloogiliselt selge polariseerumine. Ja loomulik, et
seksuaalsus mõjutab aju ja süda. Aga väide, et mehed peaksid käituma või mõtlema
nii ja naised naa... see on nii nagu sa ütleksid, et kaksikud peaksid olema nii
ja sönnikud naa.
