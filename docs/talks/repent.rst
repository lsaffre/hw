================================
Repent and believe in the gospel
================================

"The time is fulfilled, and the kingdom of God is at hand; repent and believe in
the gospel." This is how `Mark 1:14-15
<https://www.bibleserver.com/ESV/Mark1%3A14-15>`__ summarizes in a single
sentence what :term:`Jesus Christ` calls us to do.

.. glossary::

  to repent

    To :doc:`change your mind <openminded>` about some concrete topic because
    you realized that you were wrong. If your mistake has caused any measurable
    damage, repentance includes that you do your best to repair this damage.

    To review one's actions and feeling contrition or regret for past wrongs,
    which is accompanied by commitment to and actual actions that show and prove
    a change for the better. (`Wikipedia
    <https://en.wikipedia.org/wiki/Repentance>`__)

Mark does not explain what this :term:`Gospel` actually *is* (that's
:doc:`another topic </talks/good_news>`), but one thing seems clear here: some
people :term:`believe <to believe>` in it and others don't. And these others are
called to change their mind about it.

When you call somebody :term:`to repent`, you must always make clear about what
particular question you ask them to repent. It means "I believe that you are
wrong in this particular question and that you would better change your mind
about it."

Believing in the Gospel seems to be an :term:`existential question`, an
important question, a question of life and death. Which leads us to a next
question: :doc:`What is so good about the Good News? <good_news>`
