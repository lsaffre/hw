=================
Tertium non datur
=================

In a controversial discussion it can be helpful to remind all participants of
two fundamentally different ways of saying "No":

- You refuse **the position of the other side** because you are against it. For
  example see the other camp as an enemy, perceive the solution they defend as a
  :term:`sin`, ...

- You refuse that **the question as such** (this way of dividing reality into
  two :term:`dichotomies <dichotomy>`) is invalid or meaningless.

Definitions and thesis statements:

.. glossary::

  vicious circle

    A situation where a :term:`learning process` is stuck by a
    :term:`controversial battle`.

  Paulus trauma

    A change of mind in a fundamental :term:`controversial question` because you
    realize that your :term:`conviction` needs an update.

  tertium non datur

    The `law of excluded middle
    <https://en.wikipedia.org/wiki/Law_of_excluded_middle>`__, which states that
    for every proposition, either this proposition or its negation is true.

  prejudice

    A conviction

It is important that both sides are aware of this differentiation. A participant
who fails to be aware of :term:`TS01` is perceived as a browler, squabbler,
disturber or peace breaker.

The reconciling "third," not logically foreseeable, is characteristic of a
resolution in a conflict situation -- Frith Luton in `Tertium non datur – the
‘Third’ that reconciles Opposites
<https://frithluton.com/articles/tertium-non-datur/>`

Paul Watzlawick published work on this in [Watzlawick1980]_.
