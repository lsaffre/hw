=============================
God alone deserves veneration
=============================

Every :term:`faith culture` has some "symbol" that it venerates as its "God".

Examples seen in Estonia:  "Au tööle!", "Anname au!", "Ma austan kirikut", "Ma
austan traditsiooni"

For example, in most countries you risk serious juridical problems if you put
fire to a flag of your country.

To honour something means to show your appreciation.

"to venerate" means fundamentally the same as "to honour", but is used
your praise
