================
About depression
================

Here is what I have to say to you if you are diagnosed with depression.

.. I won't try to convince you to stop being depressed.

The first step out of depression is to open your heart and your mind and
decide that you choose life and not death. Nobody can do this step for you.

"The core symptom of depression is said to be anhedonia, which refers to loss of
interest or a loss of feeling of pleasure in certain activities that usually
bring joy to people." -- (Gilbert P (2007). Psychotherapy and counselling for
depression (3rd ed.). Los Angeles: Sage. ISBN 978-1849203494. OCLC 436076587,
via `Wikipedia <https://en.wikipedia.org/wiki/Depression_(mood)>`_)

Medical treatment can bring relieve --and might help you survive-- but is not
the solution.

After watching :term:`ShortFilmUnspoken` I have two reactions:

First observation, the protagonist lives in an insane environment where adult people are
being asked "Are you okay?" when they make children laugh. I believe that
depression is actually a sign of emotional intelligence, a sane reaction to an
insane environment.
See your depression as your unique way of reacting to this world.
Embrace it. Accept it.
Read this: `Is Depression Actually a Unique State of Consciousness?
<https://www.psychologytoday.com/us/blog/maybe-its-just-me/202112/is-depression-actually-unique-state-consciousness>`__


My second reaction: Yes, feel free to **imagine your suicide**.
Imagine it as intensively as needed, but be careful to **not actually do**
anything that might harm you physically.

Also realize that *if* the belief in :doc:`eternal life </talks/eternal>`
happens to be true, suicide would cause you to be depressive for eternity.
You *don't* want that.
Don't tell me that you do.
Living in depression is hard,
fighting against it is harder,
but suffering *a whole life* is still much better than suffering *eternally*.
You don't want to die in depression.
Suicide is not a realistic solution. It won't fix your problem.

Learn the following :doc:`prayer </prayer>` by heart and repeat it whenever you
find yourself thinking about your problem:

| God, if you exist, then help me.
| Because I have no hope,
| no clue how to get out of this.
| No human can help me.
| You alone can help me.

You can join the Pope's prayer for people with depression:

- `November 2021: Papst Franziskus ruft zum Gebet für Menschen mit Depression
  <https://www.vaticannews.va/de/papst/news/2021-11/franzikus-gebet-november-video-vom-papst-depression.html>`__

Here is another recommended read: "Making negative interpretations is not
dysfunctional; what is dysfunctional is a lack of flexibility in information
processing." -- `To Overcome Depression, Think Flexibly, Not Positively
<https://www.psychologytoday.com/us/blog/finding-new-home/202111/overcome-depression-think-flexibly-not-positively>`__.
Yes, "interpretation flexibility" is an important emotional skill. Try to work
on this.

Did you know?

- `Pyotr Ilyich Tchaikovsky
  <https://en.wikipedia.org/wiki/Pyotr_Ilyich_Tchaikovsky>`__
  was punctuated by personal crises and depression.
