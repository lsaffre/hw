======================
Agile versus Waterfall
======================

On this page I try to explain why I believe that the Agile approach is more
synodal than the Waterfall approach for most projects of the :term:`Church`.

"Un objectif sans plan s'appelle un vœu."
("An objective without a plan is called a vow.")
-- Antoine de Saint-Exupéry

TODO: `Agile versus Waterfall
<https://www.productplan.com/learn/agile-vs-waterfall/>`__
