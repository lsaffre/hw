==============================
Dangerous rights in wrong hand
==============================

Most law systems are giving to corporations two rights that should be reserved
to humans: the right to be greedy (:doc:`to own property </property>`) and the
right to have secrets (:doc:`privacy </privacy>`).

These two rights are indeed *human rights*, but they shouldn't be a right for
:term:`corporations <corporation>`. Why?

One reason is that corporations **don't need** these rights.  The humans who
interact with a corporation (employees, customers, providers, ...) do need these
rights, but not the corporation itself. **A corporation does not need to get
rewarded** in order to remain motivated.  The "motivation" of a corporation
results from its mission statement, which bundles the collective motivation of
the individual humans who support or own the corporation. **A corporation does
not need to have secrets**. There are many examples of corporations (basically
all non-profit organizations) that work well without having any secrets. An edge
case are security issues, we treat these in a separate article. See
:doc:`/whistleblowing`.

Another reason is that corporations **can potentially misuse** these rights
against humans. They are immortal and ruthless. Letting them function on their
own must inevitably lead to unwanted results. Corporations must remain under
human control. We must not give them any chance to get out of our control.

Let us :doc:`Subdue the greedy giants! </stgg>`

.. rubric:: Related quotes

"We have constructed a system we can't control. It imposes itself on us, and we
become its slaves and victims. We have created a society in which the rich
become richer and the poor become poorer, and in which we are so caught up in
our own immediate problems that we cannot afford to be aware of what is going on
with the rest of the human family or our planet Earth." -- `Thích Nhất Hạnh
<https://en.wikipedia.org/wiki/Th%C3%ADch_Nh%E1%BA%A5t_H%E1%BA%A1nh>`__ via
`Realize You are the Earth
<https://creativesystemsthinking.wordpress.com/2015/10/24/realize-you-are-the-earth-thich-nhat-hanh/>`__:

"The tech industry's largest companies (...) stifle the flow of new ideas. [We
need] a broader re-evaluation about the tech industry and government regulation.
(...) [We] see markets --search, social networks, online advertising,
e-commerce-- not behaving according to free-market theory. Monopoly or oligopoly
seems to be the order of the day." --
`Paul Romer <https://en.wikipedia.org/wiki/Paul_Romer>`__
via `Paul Romer: Once tech’s favorite economist, now a thorn in its side
<https://www.moneycontrol.com/news/trends/features-2/paul-romer-once-techs-favorite-economist-now-a-thorn-in-its-side-6924421.html/amp>`__:

"We all have a responsibility to educate our human brothers and sisters. Inner
values are the ultimate source of happiness, not money and weapons, whether
you’re talking about individuals or the whole of humanity." -- Dalai Lama, via
`Twitter <https://twitter.com/DalaiLama/status/1434811848436580354?s=03>`__

Google's Chromebook laptop computers have a built-in "death date" that
deliberately makes them useless after a few years. `mercurynews.com 2023-07-24
<https://www.mercurynews.com/2023/07/24/built-in-software-death-dates-are-sending-thousands-of-schools-chromebooks-to-the-recycling-bin/>`__
