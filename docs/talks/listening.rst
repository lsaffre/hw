========================
An exercise in listening
========================

There is a simple exercise that I do when I am in the mood and an occasion
presents: I ask people a question and then listen to what they answer.

For example I happen to ask "If you had one wish to ask from the President in
order to make your country a bit better, what would you ask for?" Here are some
answers to this question:

- That there would be more health workers at school so that you don't need to
  wait when you need help (10y, female, 2021-11-07)

- That pupils were allowed to choose their teacher (10y, female, 2021-11-07)

- That our country would become more environment-friendly (15y, male, 2021-11-07)

- That he would take the reins and finally starts to govern this country. Not as
  what is currently happening at Parliament (male, 55y, 2021-11-13)

- That he would give me one million euros without causing inflation (male, 60y, 2021-11-14)

- That he would enforce the Covid restrictions (male, 10y, 2021-11-16)

The key point to keep in mind is to *not even try* to find a *common* answer.
It is not always easy to resist the temptation of entering into a discussion.

Besides training my listening skills, this exercise also teaches me interesting
things about humans.

Some rules I learned:

- Ask open questions (because registering the answer to a multiple choice
  selection won't develop you listening skills)

- Don't ask manipulating questions (e.g.)

- Ask questions that actually interesting (e.g. "What is your preferred colour?"
  is not a very exciting question)

Some detailed reports:

- :doc:`/blog/2021/1101_0730`.
- :doc:`/blog/2021/1103_0730`.
- :doc:`/blog/2021/1116_0730`.
