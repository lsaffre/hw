==============================
Don't let money rule the world
==============================

Money is a good tool for working together, but it's not something we should let
rule the world.

The :term:`Old Testament` describes the ideological battle between the followers
of `Baal <https://en.wikipedia.org/wiki/Baal>`__ and those of `Yahweh
<https://en.wikipedia.org/wiki/Yahweh>`__.
