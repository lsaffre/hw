=================
Me and the Church
=================

I grew up in a Catholic environment. As a child I was an accolyte.

.. image:: 1978-03-messdiener.jpg
  :width: 80%

Picture: Me as an accolyte at the age of 9. With Rudolf Hennes on the left and
Pastor Xhonneux in the middle. Picture taken by Manfred Laschet who was sexton
at this time.

My :term:`faith` started to become mature (i.e. I consciously decided that I
want to believe in the Church) at the age of 18, one year after a traumatic
event, the death of a classmate who was my best friend. During my year of
mourning I found considerable consolation through a priest, François Palm, who
was our faith teacher in school.

I married with a Lutheran. The church wedding was an ecumenical celebration in
Vigala church, without a Catholic priest but with a "dispense" issued by the
Bishop). When we were fully living in :term:`Vigala` (from 2006 to 2018) I was
active in this Lutheran congregation. Our children grew up as Lutherans and I
was active as a Sunday school teacher. Until now I am still a member of that
congregation. I am a Catholic who ties deep relations with other Christians.

My faith is deeply influenced by the spiritual and musical work of the Taizé
community.  I discovered Taizé relatively late, at the age of 25. When still
living in Belgium, I went there every year for one week. It was in Taizé where I
met my wife for the first time. In Estonia, between 2005 and 2010 I coordinated
the translation of the Taizé songs and helped considerably with producing the
Estonian Taizé songbook. In 2020 I founded the "Association of Estonian Taizé
friends" and developed a website about Taizé in Estonian (`laudate.ee
<https://www.laudate.ee>`__)

Here in Estonia I also worked during five years (2017--2020) with the Avatud
Piibli Ühing (APÜ), the Estonian part of the Scripture Union International.
However this collaboration is currently idle, partly because my vision of Church
is quite different from theirs.

My belief in the :term:`Roman Catholic` Church became more active again under
Pope Francis. I am fascinated by this man and his message. I participated
actively during his visit in Estonia in 2018, my job was to help receiving the
guests of the Apostolic Administration. In September 2021 the Bishop appointed
me as the contact person for the :term:`Synod on Synodality`, which motivated me
to start the :doc:`/sc/index` section on my website.
