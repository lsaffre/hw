=============
About leaders
=============

A **leader** is a person who leads a given **project**. Anybody can create a
project and becomes the **self-assigned** leader of this project. A project
needs a leader to become **alive**. A project becomes **orphaned** when its
leader dies without having designated a successor.

The leader of a project can work alone on his project, or decide to **form a
team**, i.e. to delegate parts of his work to other persons, called **workers**.
He does this by defining a number of **positions** and by engaging a worker for
each of these positions.

For the outside world, the leader remains the main and only responsible. When
something goes wrong, the leader is to blame. The leader decides how he
distributes critics and praise, shame and fame, profit and loss among his
workers.

The act of forming a team is not reserved to the leader. Every worker of a team
can himself form a team for doing his work.  A position can be considered a
sub-project or a "child" of its "parent" project. The fundamental difference
between a top-level project and a sub-project is that its leader is not
self-assigned and can be replaced by somebody else by the leader of the parent
project.

A big question is how to organize the **succession** of the leader of a given
top-level project. Ideally the leader designates his successor while he is
alive. But this is not always feasible. For example if the leader dies
unexpectedly without leaving any inheritor.

Some projects need by definition multiple leaders.

For example, a natural family is a project with *two* de-facto leaders, the
mother and the father. "Patriarchal" law systems consider the father as the one
and only leader.  But even in "modern western" law systems, where both parents
are considered as peers with equal rights and responsibilities, administration
can require a single contact person called the head of household.

A modern :term:`nation state` should implement `separation of powers
<https://en.wikipedia.org/wiki/Separation_of_powers>`__, that is, have three
branches: a legislature, an executive, and a judiciary. Each of these branches
should have its own leader. On the other hand, most modern nation states try to
find a single leader, called the president. Which leads to division or even
dictatorship.
