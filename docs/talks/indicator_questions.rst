===================
Indicator questions
===================

One way to get acquaintained another person is to ask indicator
questions.

An example of such an indicator question is whether homosexuality is a
"disorder", i.e. something that disturbs the regular or normal functions of
life.

a) If it's a disorder, then we should help homosexual people to get healed from
their disorder or, for incurable cases, to live with it in a way so that it
doesn't disturb other people's lives. And of course we wouldn't allow same-sex
couples to raise children.

b) But if homosexuality is not a disorder, we would have to say that
:term:`homophobia` is the disorder, that same-sex couples may raise children.

It seems to me that this is a clearly binary question. Its either (a) or (b),
never both. Philosophers would call it a case of *tertium non datur*,
programmers would call it an *exclusive OR*.

A binary question doesn't mean that you may not change your mind or remain
undecided. Answering "I don't know" to such a question can be better than
picking the wrong option.

Oops, did I say the "wrong" option? Is there an absolute truth?

A binary question becomes a potential source of conflict when people with
opposing opinions "clash" together.

Independently of whether you say (a) or (b), you need to explain your choice

I cannot find any scientific study
saying that homosexuality causes any measurable harm.

A person
who says that homosexuality is a disorder

- loves the Bible more than God because she refuses scientific evidence,

- causes harm to homosexual people by giving them wrong advice, e.g. trying to
  "help" them to become "normal", and by fostering :term:`homophobia`
