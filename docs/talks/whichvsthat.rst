=====================
"which" versus "that"
=====================

I suggest to stop saying "which" when we want to say "that". The difference
isn't that difficult to understand. For example you say

(1) "The bike **that** has a broken chain comes first"

when there are multiple bikes to repair, and you tell your colleague which one
comes first. But you say

(2) "The bike, **which** has a broken chain, comes first."

when there are multiple objects to store in the lorry, and you want the bike to
come first because it has a broken chain.

Yet many people still seem to prefer using "which" also in the first case. They
say:

(3) "The bike **which** has a broken chain comes first."



A common mistake
================

We can't really call it a mistake because the English language has a long
history. It seems that in British English it is explicitly allowed to use
"which" instead of "that". 

Examples of "wrong" usage:

- "This jar is not empty but contains a small amount of white sand which shifts on
  the bottom." (Erin Morgenstern, Night Circus, p. 299)

- "There are, it is true, works of recent Japanese literature which are
  relatively untouched by Western influence." (Donald Keene, 1958, in
  *Translator's introduction* to *No longer human* by Osamu Dazai)

- "Django 5.0 brings a deluge of exciting new features which you can read about
  in the in-development 5.0 release notes." (Natalia Bidart, September 2023,
  `djangoproject.com
  <https://www.djangoproject.com/weblog/2023/sep/18/django-50-alpha-1-released/>`__)




Sources and further reading
===========================

- https://www.grammarly.com/blog/which-vs-that/
- https://www.merriam-webster.com/grammar/when-to-use-that-and-which
- https://preply.com/en/blog/when-to-use-which-or-that-the-most-common-cases/
