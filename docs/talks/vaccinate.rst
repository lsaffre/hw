=================================
To vaccinate or not to vaccinate?
=================================

It's amazing to see how quickly and efficiently humanity was able to react and
adapt to the Covid pandemic.  This gives hope that we might now dare to face
similar revolutions in order to make an end also with famines, wars and
over-exploitation of natural resources.

But meanwhile I'm worried and remain :ref:`unvaccinated <unvaccinated>`. I don't
join the choir of those who repeat "I believe in science". Because
:term:`science` is not something you "believe" in or not. Science is a tool, and
a very valuable one. But also an expensive one. Science is opportunistic: it
serves those who pay for it. And those who make profit from the COVID pandemy
(:term:`private corporations <private corporation>` without exception) are more
powerful than the :term:`public corporations <public corporation>` who are
supposed to protect the weak and the poor. A new form of slavery is evolving
where :term:`private corporations <private corporation>` are the masters and all
humans (including top-level managers) are slaves.

So what about those who refuse to get vaccinated?
Are they really just stubborn fools?
Are they really such a big threat to the others?
Will our problems be solved when everybody has been vaccinated?

Why doesn't the Church protest against the idea that national governments might
enforce vaccination? Austrian Cardinal Christoph Schönborn answers "Because it
is not a question of faith" [#Schonborn]_.  I'd say that he actually means it is
not a question of the :term:`Gospel`. Indeed, the Gospel doesn't answer neither
no nor yes to this question. A question of *faith*, according to my
understanding, is something else, every :term:`controversial question` is by
definition a question of :term:`faith`. 

Schönborn brings at least one good argument into the discussion: vaccination is
very comparable to the obligation of using the safety belt or the ban of smoking
in public buildings.

But Schönborn also fails to mention at least one argument: there are people who
"have grave concerns about the damaging physical and mental health impacts of
the prevailing COVID-19 policies, and recommend an approach we call Focused
Protection." (`Great Barrington Declaration <https://gbdeclaration.org/>`__,
authored in October 2020)

.. rubric:: Footnotes

.. [#Schonborn] `Österreich: „Impfen ist keine Glaubenssache“
  <https://www.vaticannews.va/de/kirche/news/2021-12/schoenborn-kardinal-oesterreich-wien-impfen-corona-politik.html>`__

.. .. [#Schonborn2] `Impfpflicht: Schönborn wirbt für Dialog und versöhnliche Gesten
  <https://www.vaticannews.va/de/kirche/news/2021-12/impfpflicht-schonborn-wirbt-fuer-dialog-und-versohnliche-gesten.html>`__
