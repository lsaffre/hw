=============================
Reality and our image of it
=============================

I can have communication issues with people who fail to understand the
difference between :term:`reality` and their :term:`convictions <conviction>`.

.. glossary::

  belief

    An opinion you :term:`believe <to believe>` to be true.

  conviction

    A :term:`belief` you are reluctant to discuss about.


"Reality is that which doesn't go away when you stop believing in it." -- Philip
K. Dick in `I Hope I Shall Arrive Soon
<https://en.wikipedia.org/wiki/I_Hope_I_Shall_Arrive_Soon>`__

.. glossary::

  reality

    The sum or aggregate of all that is real or existent, as opposed to that
    which is only imaginary.
    -- inspired from `Wikipedia <https://en.wikipedia.org/wiki/Reality>`__

  truth

    A subset of :term:`reality` that is formulated in human language and assumed
    as true within a given context.


It seems that Zen philosophy uses the word "truth" for what I call
"reality", and "reality" for what I call :term:`world view`. At least Brian
Thompson says (in `this blog entry
<http://www.zenthinking.net/blog/are-there-multiple-realities-or-is-there-only-one>`__):
"What we refer to as reality is actually an entirely subjective experience,
one that's different for each of us--as in, what's real for me isn't real to
you. It's a matter of personal perception. The world I live within differs
vastly from yours." I do not foster this usage of the word "reality".


In other words, *to believe* means :term:`to know` that you don't *know* the
answer to some question and --despite of this-- to *choose* one of them. In that
case you can say that you are *convinced* of this answer.

When you tell me that you :term:`know <to know>` the answer, then we cannot
enter into dialogue because you :term:`believe <to believe>` that there is no
question and that my doubt is invalid. We can enrich each other and grow towards
a consensus only if you "repent", i.e. you open your heart to the possibility of
doubt.

We won't have peace on earth as long as our leaders and institutions don't learn
to see the difference. This is why we need to cultivate our faith if we want to
live in peace on this planet.

Human laws should grant `freedom of religion
<https://en.wikipedia.org/wiki/Freedom_of_religion>`__ because :term:`faith`
cannot be regulated by human laws.

On the other hand, when a nation refuses to consider religious activities as
part of their culture (and to finance them accordingly from their cultural
budget), their population cannot learn to publicly talk about :term:`faith
questions <faith question>`. Refusing to cultivate faith leads to general lack
of spiritual competence.

Linguistic observations
=======================

I saw that the two Wikipedia articles `Belief
<https://en.wikipedia.org/wiki/Belief>`__ and `Faith
<https://en.wikipedia.org/wiki/Faith>`__ seem to live in separate worlds.  I
added Faith to the "Series on Certainty". Also I saw that Wikipedia has no
article about the word :term:`conviction` in the meaning of the  German word
`Überzeugung <https://de.wikipedia.org/wiki/%C3%9Cberzeugung>`__.

Another fun side note is that Germans have only word "Glaube" for the words
"belief" and "faith".  Wikipedia "fixes" this by having two articles "Glauben"
and "Glaube" (which linguistically is nonsense).  For me :term:`faith` is the
individual set of beliefs of a human.

========== =========== ========= ============
en         de          et        fr
========== =========== ========= ============
faith      Glaube      usk       foi
belief     Glaube      uskumus   croyance
conviction Überzeugung veendumus conviction
========== =========== ========= ============

Don't trust your convictions
============================

If there is one thing I am convinced of, then this: Don't trust your
convictions.  You can consider this as a corollary of Joachim Ringelnatz's
statement "Nothing is sure, and even *this* isn't".\ [#Ringelnatz]_


Don't say "I know" when you mean "I believe"
============================================

Don't mix up "to know" and "to believe".

.. glossary::

  to believe

    To assume correctness of a statement without claiming to have a proof.

  to know

    To assume that a statement is correct and proven and needs no further
    investigation.

.. Both words mean that you *rely* on what you know or believe.

..
  "I believe in this" means "I know that I don't know whether it's true, but I do
  as if it was."

When you say to your wife
"I *know* that this car is too expensive for us",
it actually just means that you don't want her to investigate further.

I don't like when somebody tells me "I *know* that we must raise the wages in
educational sector" because it indicates that he isn't open to the idea that
there might be more important sectors where we must raise the wages.

When somebody tells me "I *know* that God exists", then he doesn't mean they
have a proof for it; it just means he decided to not doubt about it.

We can say "We *know* that Earth is a planet in a solar system" because during
the last centuries humanity has reached a kind of :term:`scientific consensus`
about this question. The question is being answered by science, at its current
state of art. But it might turn out, in some far future, that human science of
the 21st century completely ignored certain aspects of reality regarding planets
and solar systems.

Refusing to differentiate between knowing and believing can lead to
:term:`idolatry`, which is "always an attempt to establish certainty where it is
both unwarranted and unearned"\ [#VanceMorgan]_.

To *believe* is more than to *assume*. Assuming something is just an
intellectual choice that doesn't cause any serious change in your :term:`world view`.
Believing something has an influence on your world view, which influences your real life decisions and choices.
To believe in God is more than to assume he exists.
Believing in God is a conscious decision to educate and to train myself so that I
trust in God.  To believe is a lifelong learning process.



.. rubric:: Footnotes

.. [#VanceMorgan] `Idolatry and Abortion
   <https://www.patheos.com/blogs/freelancechristianity/idolatry-and-abortion>`__
   by Vance Morgan on Patheos, 2020-02-02).

.. [#Ringelnatz] German author and cabaret artist Joachim Ringelnatz wrote
    "Sicher ist, dass nichts sicher ist. Selbst das nicht!" (`source
    <https://www.zitate-online.de/literaturzitate/allgemein/1547/sicher-ist-dass-nichts-sicher-ist-selbst-das-nicht.html>`__)
