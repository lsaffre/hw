=======================================
The Bible is not clear for most things
=======================================

The :term:`Bible` is clear about one thing: that nothing is clear. It encourages
us to *ultimately* trust in only one thing: reality itself, which Christians
personify as :term:`God`.

Note the important word *ultimately*. Trusting in God does not mean to distrust
humans.

And reality (God) is unreachable to human mind. No human can validly say "I know
God". A person who says "I know God" is a liar. A teaching that claims "We know
God" is :term:`sectarian <sectarian faith>` because it excludes those who don't
agree to the teaching. The Bible expresses this by saying that God's name is
:term:`holy` (see :doc:`name_of_god`).

So don't mix up "God" with "our image of God" or "our teaching about God".

The Bible contains many stories about people who trusted in a right image of
reality (God) despite the fact that it differed from the established image, and
who then turned out to be right, sometimes only after their death.

And the Bible also contains many stories about people who trusted in a wrong
image of God (reality) and then turned out to be wrong, sometimes only after
their death.

See also :doc:`/defs/biblicism`.

Paul's letter to the Romans
===========================

Here is an example.

Keith Giles and Steve Scott explain (in `Romans 101: Why so many Christians miss
the point
<https://www.patheos.com/blogs/keithgiles/2021/06/romans-101-why-so-many-christians-miss-the-point>`__) that Romans 1 and 2 need to be read together: Rm 1 develops an empathic
description of those hated by the readers, Paul "stokes the fire of
condemnation" in the reader's emotions. And then, in the second chapter, he
drops the spiritual bomb on them: "If you condemn your fellow humans like this,
how can you hope that God would forgive *your* sins?".

And something similar can happen for Romans 12 and 13: these two chapters
explain that when somebody does evil against you, and when you live in a
civilized state, you won't punish them yourself but rather count on police and
judge to do their work. But if you start reading at chapter 13, you can get
the impression that Paul tells us to create an authoritarian government and
demands that we obey all laws. The actual message is that if *we* commit evil,
*we* should submit to the punishment given to us.

I believe that this is a great explanation. I re-read chapters 1 & 2 under this
light in three different languages (German, French and Estonian) and feel that
they are right.

Note that even the greatest explanation doesn't help those who are in
:term:`Hell`: I forwarded the thought to a woman who was angry about a bishop
who had dared to co-assist the ordination of a gay pastor in a LGBT-friendly
church. She answered "Reading the 2nd chapter doesn't revoke the truths of
chapter 1. What a pity that such academic complacency has blinded the eyes of
many to the truth of God."
