======================================
A basic code of conduct for all humans
======================================

I believe that a *basic code of conduct* for all people who want to live in
peace on this planet is this: **Love the world as it is, and love each of your
fellow humans as they are.**

Or a bit more detailed:

- We all sometimes spend some time worrying about what's wrong with this world
  and how to save it. To worry means to love.  And we try to understand this
  world using common sense and :term:`science`. That's love. And sometimes we
  even have crazy ideas and get excited about them (:doc:`/game`). That's love.

- Maybe you don't spontaneously call "love" your relation towards each of your
  fellow humans. Sometimes we rather seem to hate them. But there is a
  difference between hate and angriness. Most of what is called "hate speech" is
  actually just angry speech. Anger is just a deceived form of love.
