=================================================
Let's remove grammatical gender from our language
=================================================

OMG! Gender-neutral language is as ugly in French as in German! What a stupid
solution!

Examples: `[Nouveau livre] Un monde en lutte
<https://france.attac.org/nos-publications/livres/article/nouveau-livre-un-monde-en-lutte>`__

It's actually doing the opposite of what its users want. When we say "him or
her" instead of just "him", we want to make clear that we don't care about sex.
In most cases we don't care about the sex. Not caring about the sex should be
the *default*.

If I was to decide, I would adapt the grammars of all living languages to what
fenno-ugrian languages have been doing from the beginning: I would simply remove
the female form from our grammars.
