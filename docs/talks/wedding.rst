===========
Our wedding
===========

Actually we married twice. The first time in Belgium, on October 7th, 2000,
after the legal wedding act. The second time in Vigala, on June 23rd, 2001,
after the wedding ceremony in Church.

Our wedding in Belgium is certainly one of the most beautiful feasts in my life
so far. I am not a very motivated organizer of feasts, and furthermore we had
only a few weeks to prepare it. But three of my friends, when they heard that
the administrative paper battle was won (that battle deserves another story of
its own; Estonia was not yet part of the EU by then), told me: "If you want to
celebrate this, you can count on us for helping." Without this promise we
wouldn't have celebrated a lot. This feast then was really my style. I had two
major conditions: (1) *everybody* who wants to join our feast is invited (I am a
bit allergic against having to ask "who of my friends deserves to get invited?")
and (2) no playback music, only live music. The feast was outside (we had no
idea how many people would actually come). Despite the late time of the year,
weather was beautiful. The rain started only the next day, just as if it had
been holding back for this occasion. Afterwards one of my friends said "This was
the most beautiful wedding I have ever been to".

Our wedding in Vigala was quite exhausting for both me and my wife. It lasted
three days. When they were over, I said to my wife: "One thing is clear: I will
*never ever* marry again!" This was one of my ironic jokes. Note that it does
*not* say that I regret it. It is rather a renewal of the promise I had done in
Church: I will be your husband until death separates us.
