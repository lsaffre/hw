=====================
Guide to the Internet
=====================

"Instead of looking at what's true and important, we often notice only the
flashiest, most shocking news items." -- `Eric Sangerma
<https://ericsangerma.com/>`__

"You can’t expect somebody to become a biologist by giving them access to the
Harvard University biology library and saying, “Just look through it.” That will
give them nothing. The internet is the same, except magnified enormously."
-- `Noam Chomsky <https://en.wikipedia.org/wiki/Noam_Chomsky>`__
