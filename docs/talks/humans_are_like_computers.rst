==========================
Seeing humans as computers
==========================

It sometimes help me to understand myself and my fellow humans by comparing them
with a computer.

Our senses are the input devices: our eyes are like a camera, our ears like a microphone.
We also have output devices: our mouth is like the speaker, our face and body is a bit like the monitor.
Some I/O devices fundamentally differ between humans and computers:
for example we don't have a keyboard, and most computers don't have a nose.

Our brain contains a central processing unit (CPU)
and different types of storage devices:
hard disk drives for long-term memory,
RAM for short-term memory,
ROM for inherited memory.

Our body is full of output devices, e.g. our legs and arms. Every single muscle
is actually an output device.

Our brain is a multi-core processor with integrated storage devices. It
"controls most of the activities of the body, processing, integrating, and
coordinating the information it receives from the sense organs, and making
decisions as to the instructions sent to the rest of the body." (via `Wikipedia
<https://en.wikipedia.org/wiki/Human_brain>`__)

Our brain also contains something like device drivers, i.e. programmable parts
that are responsible for pre-processing the information streams.

We also have more input devices in our body, e.g. pain detectors.

The different parts of our body use two types of signals to communicate with
each other: electrical (nervous system) and chemical (hormonal system).

At any moment of our life, we receive a stream of information events, process
them and potentially decide to trigger a reaction.

A reaction can be
to modify something in the memory storage ("learn"),
or to consult the memory ("remember"),
or to send some nervous signal (e.g. move some muscle),
or some hormonal signal (e.g. increase adrenaline production),
or send a voice message ("say something"), ...


TODO:

- `The Human Brain Versus The Computer <https://sites.psu.edu/psych256sp14/2014/01/29/the-human-brain-versus-the-computer/>`__
