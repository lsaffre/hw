==================================
The Church is not a truth provider
==================================

For many centuries the Church has been seen as a "truth provider", an
institution that "knows" the truth and shares this truth with its members.

This is a presynodal conception. Let us get ret rid of it.


.. contents::
  :local:



See also :doc:`/talks/gospel60`.

As long as one member of the Church disagrees with a teaching, this teaching
can't be fully true.

Estonians don't only have the reputation of being the least
religious country in the world, they are even *proud* of it. This deserves some
thoughts. Many people of good will in Estonia refuse to even ask about the
Gospel. Our government refuses to cultivate anything that mentions the Gospel.
As a result, many people in Estonia have no chance to hear about the Gospel.

As a Christian I believe that there is no salvation without the Gospel. That's
why I am sometimes a bit worried about the salvation of our nation.

The main job of the Church, which is to announce the Gospel, is
not well done in Estonia and in the world.

It is true that "among the most important causes of the crises of the modern
world are a desensitized human conscience, a distancing from religious values
and a prevailing individualism accompanied by materialistic philosophies that
deify the human person and introduce worldly and material values in place of
supreme and transcendental principles" (`20190204
<https://www.vatican.va/content/francesco/en/travels/2019/outside/documents/papa-francesco_20190204_documento-fratellanza-umana.html>`__).
The situation has complex causes. We can blame our history, or those who "threw
the child out with the bathwater".

But it might also be that we as the Church fail to explain the Gospel in an
understandable language.  Who is responsible when people fail to understand the
Gospel: the teacher or the pupil?  We have a collective guilt of not doing our
job very well.

(:count:`item`) When you are a child and go shopping with your mother, and when
you see that your mother has put her coat inside-out, will you let her get
exposed to laughter of other people? No! Of course you will help her to turn her
coat right. (This image is from `Marie-Noëlle Thabut, une vie avec la Bible
<https://www.la-croix.com/Religion/Catholicisme/France/Marie-Noelle-Thabut-vie-Bible-2018-12-23-1200991349>`__

(:count:`item`) Estonians are a sceptical people. When you tell them something
that is in contradiction with plain common sense, they simply turn away and are
unforgiving: they will distrust you for the rest of their life. Each time the
Church is seen "wearing her coat inside-out", people of good will turn away from
her and from Jesus. The Church in Estonia happens to say things that are in
contradiction with common sense because she has issues in her understanding of
the Gospel. She lacks directives from a superior instance. The vision of the
Synodal Church inspires our hope that these issues may get fixed.

(:count:`item`) Here are some real-life examples where people who believe in the
Gospel came to the conclusion that a church teaching is in contradiction with
common sense.

(:count:`item`) We perceive it as a shame that people get excluded from our
communion when they dare to "try another time" after their marriage failed, or
when they dare to live as a same-sex couple.

(:count:`item`) It is a shame that we remain silent when Non-Christian
organizations do our work. We are called to actively express our thankfulness
and distribute "Thank you" badges (pronounce a :term:`benediction` or
"certificate of approval") to organizations who do our work. See also
:doc:`/blog/2022/0119_1010/`.

(:count:`item`) We often focus on internal questions and fail to realize that
our primary job is to bring the Gospel to those who didn't yet hear it.

(:count:`item`) We still cultivate the belief that our faith is "private
sphere". This belief is a natural consequence of our history, but we are called
to overcome it.

(:count:`item`) We still follow obsolete faith teachings that ignore at least 50
years of human scientific evolution.


One Gospel, many teachings
==========================

(:count:`item`)
There are many teachings about the Gospel, many theological schools,
many ways of understanding and summarizing the Bible.
They constantly grow, evolve, influence each other.
They sometimes give opposing answers to concrete questions.
No single :term:`faithful` can claim to understand them all.

(:count:`item`)
We see that our interpretations of the Scriptures can conflict with each
other. As long as one member of the Church disagrees with a teaching, this
teaching can't be fully true.

(:count:`item`)
Each teaching bears temptations to :term:`sin` (to harm others).
A teaching, however well intended, can turn out to be wrong or misleading.
A major job of the Synodal Church will be to constantly evaluate and judge every
teaching document by issuing her approval or withdrawing it.

Humans have right to mercy, institutions don't.
("Temptations to sin are sure to come, but woe to the one
through whom they come! It would be better for him if a millstone were hung
around his neck and he were cast into the sea than that he should cause one of
these little ones to sin." `Luke 17:2
<https://www.bibleserver.com/ESV/Luke17%3A1-2>`__)


(:count:`item`) A teaching is just a collection of words in some human language.
There are many good teachings about the Gospel, but the Gospel is more than any
teaching, it is a divine message. It is revealed to us in much more than
teachings: in nature, in the thankful smile of a poor, in a personal experience
of success or failure. But also in rites, sacraments, and other non-verbal signs
that deserve regulation and formal approval.

How to teach the Gospel
=======================

(:count:`item`) The mission of the Church is to "announce" the :term:`Gospel` to
"all peoples", to every group and class of humans. That's why the main
operational activity of the Synodal Church will be to publish and maintain
reliable teachings about the Gospel.

(:count:`item`) You cannot announce something without formulating your knowledge
about it. But what is knowledge?

(:count:`item`) Individual knowledge, also called faith, is the sum of beliefs
you rely on, the result of what you have learned during your personal history.
It is stored in your :term:`heart`. Your :term:`faith` tells you in every
concrete situation, spontaneously and without further reflection, whether a
given choice is "good" or "bad". It says this with a varying degree of
conviction, ranging from "vague feeling" to "doubtless belief". This choice
happens unconsciously and independently of how skilful you are for explaining it
to others using words.

(:count:`item`) Community knowledge is similar, but with a fundamental
difference: it isn't stored in your heart. It is stored somewhere else. It needs
a medium. Every collection of teachings maintained by a community is such a
medium. Every community is defined by its teachings. Teachings are the heart of
every community, they are also the heart of the Church.

(:count:`item`) The Church has developed a rich treasure of teachings about the
Gospel during her long history. The teachings of the Church are probably the
biggest and most complex documentation library in the world, collected during
more than 3000 years, with document types ranging from blog entries, news,
homilies, prayers, songs, books, films, to dogmatic constitutions, research
reports or law collections, with audiences ranging from 2 year old children to
experts of every branch of science, with the biggest community of contributors.
Maintaining this documentation library is a huge and never-ending task.

(:count:`item`) Meanwhile humanity has entered the digital era. If already the
discovery of book printing 500 years ago caused revolutionary changes to the way
of teaching the Gospel, how could we assume that the digital era would be less
revolutionary for our work? How is it possible that a few private corporations
manage to provide a billion of humans individually, day by day, with teachings
that these humans crave to read, although much of this information is useless or
even harmful to them or others (see 86-90)? And at the same time the teachings
of the Gospel produced by the Church are followed –boldly spoken– by some
insiders?

(:count:`item`) Every teaching includes answers to moral questions. No teaching
can be ethically neutral. Even a teaching about how to cook an egg assumes
certain choices regarding moral questions (e.g. “Is it good to eat eggs?” or “If
it’s okay to eat them, isn’t it better to eat them uncooked?”). The Gospel does
not say “everything is okay”. There are things in this world that are not good.
The Gospel is a moral message and gives answers to ethical questions. It tells
us what is good and what isn’t.

(:count:`item`) Every teaching is meant to be reliable. That’s why teachings
exist. A teaching makes no sense when nobody relies on it. But what can we teach
reliably about the Gospel if we assume that God is beyond human knowledge? Or
more shortly: how to explain the unexplainable?

(:count:`item`) The Bible is a first answer to this question. It is recognized
as a historic text by all scholars of all religions, and as such a milestone in
human history. The Church is the community of those who use the Bible as their
Holy Scripture, as the immutable base of their teachings.

(:count:`item`) But the Bible is a very fundamental document. It can give
contradicting answers to certain concrete questions of the visible world, which
evolves constantly. It can get interpreted in different ways, leading to
different sets of teachings. Each church institution has its own set of
teachings. While parts of these teachings are in harmony with each other, some
of them differ considerably among the church institutions. Which confirms that
God is beyond human knowledge.

(:count:`item`) Teachings evolve constantly. Teachings are neither eternal nor
immutable. The Gospel is eternal, the Bible is immutable, but teachings aren't.
The teachings of the Church need constant maintenance because they are our
interpretation of the Gospel for now and here.

(:count:`item`) The ultimate goal of every teaching is to be true, i.e. that it
reflects reality without distorting it. We trust that our teachings are the
truest teachings in the visible world, but we must keep in mind that no teaching
about God can be perfect or definitive. Ideally our teachings are in harmony
with God’s plan, but it would be an illusion to claim that they are perfect.

(:count:`item`) A rule of thumb: As long as one member of the Church disagrees
with a teaching, this teaching can’t be fully true.

(:count:`item`) A corollary: Publishing a teaching and then discovering that it
needs to be reviewed is an integral part of our learning process. We are on a
journey, we do not stand still.

(:count:`item`) Teachings cannot contradict science. When some new discovery
brings scientific evidence that a given teaching is suboptimal or even wrong,
the the teaching needs to get updated. “The tree is known by its fruit” (Mt
12:33)

(:count:`item`) An important feature of every teaching is to be clear. The
current teachings of the Roman Catholic church are impressive but not very
clear. You need years of education before you can claim to understand them more
or less.

(:count:`item`) Another important feature of every teaching is to be accessible.
Every human must be able to access them without paying a license fee and without
being distracted by commercial advertisements.
