===============
How to be happy
===============

- Don't stick on material wealth.
  Blessed are you who are poor, for yours is the :term:`kingdom of God`.
  The happiest people are those who need the least.
  There are people who all their :term:`private property` away
  and then followed Jesus.

- Blessed are the pure in :term:`heart`, for they will see God.
  A pure heart is transparent like a crystal:
  you can see what happens inside.
  There is nothing you hide from yourself.
  Our hearts receive wounds every day.
  When a wound does not heal properly, it leaves a scar.
  Every scar makes your heart less pure.
  When a wound heals properly,
  then it becomes --after some time-- a blessing:
  it makes our heart grow, become bigger and wider.


Inspired by Matthew 5:3-12, Luke 6:20–22

Quotes:

- The real source of peace of mind is love and compassion; not the love
  we feel for those who are close and already affectionate towards us, but an
  unlimited sense of altruism, a love that can be extended to all beings,
  including your enemy, which only human beings are capable of. -- Dalai Lama
  via `Twitter
  <https://twitter.com/DalaiLama/status/1514898866508013569?t=sTAJ5KbUsJo2zsf_C1TiyA&s=09>`__

- An amazing thing happens when you stop seeking approval and validation from
  others; you find it. -- Mandy Hale
