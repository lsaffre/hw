=============================
About controversial questions
=============================

A :term:`controversial question` (also called a "hot topic") is a question that
causes a :term:`dichotomy` or *polarization* of a group of humans based on their
answer to the question, dividing this group into two "camps" who "fight" each
other. Sometimes there are more than two camps.

.. contents::
   :depth: 1
   :local:

Introduction
============

It's fascinating to see how humans can stand up and fight for what they believe
as "the only right" thing. A tiny concrete issue of daily life can make a whole
group of humans furiously yell against another group of humans who see the same
facts but come to an opposite conclusion. Wars have been fought about such
questions, wars between nations and wars between family members.

Controversial questions can cause division within existing groups of any size,
starting in :term:`couples <couple>` and ending in the big economic, political
and :term:`faith cultures <faith culture>` on our planet.

:term:`Controversial battles <controversial battle>` aren't limited to religious
groups.  We see them in politics as well.  For example the words "equality" and
"liberty" are considered basic democratic values, but they actually exclude each
other. Liberty means --also-- that the political power should not hinder the
stronger and successful humans from exploiting the weaker ones. This is quite
opposite to saying that the political power should foster equality. "Democratic
values support the belief that an orderly society can exist in which freedom is
preserved. But order and freedom must be balanced." (`ushistory.org
<https://www.ushistory.org/gov/1d.asp>`__) The main purpose of political parties
in democratic countries is to develop and promote certain sets of answers to
certain real-life questions.

Controversial battles aren't limited to "big" religious or political questions.
It can happen when the members of a local scout group, or the board members of
an international corporation, discuss about what they should learn from some
recent event, e.g. from a sad accident or from a successful project. It happens
every day in couples, families and communities who live together under a same
roof.

Controversial questions aren't limited to "serious" questions.  A funny example
is a debate about `a photo that shows a sneaker
<https://www.today.com/popculture/gray-teal-or-white-pink-internet-still-can-t-decide-t153572>`__.
The sneaker on the photo is coloured grey and green, without any doubt, at least
for me and some people.  But some other people perceive it *without any doubt*
as white and pink.

The digital era enables us more clearly than ever to observe eager and seemingly
endless :term:`controversial battles <controversial battle>`.

The observation that some questions are controversial isn't new.  Ancient
Chinese Philosophers described it as the `yin and yang duality
<https://en.wikipedia.org/wiki/Yin_and_yang>`__.

Content on the web is often presented in opposition to something else. For
example, when I was first starting to learn what web framework to learn, I saw
plenty of content talking about Django vs Flask, or Flask vs FastAPI (yes, I
contributed to that).

Controversial battles actually make sense.
We need to see things in relation to other things
in order to understand them better.
The trick is to see them as *complementary subjects*
instead of things that exclude each other.
(Inspired by `Mario Munoz <https://pythonbynight.com/blog/or-into-and>`__)


Naming the camps
================

Giving definitive and clear names to the two camps is often difficult because
reality is never simple, because the situations are complex and because human
spirit is limited.

It can happen that somebody belonging to a given camp fights for some concrete
cause into the opposite direction of what his camp fellows consider the "right"
direction, and that the fighter gets shamed as a traitor or whistleblower.

Jesus refers to this kind of "battles" when he said:

  "I have come to bring fire on the earth, and how I wish it were already
  kindled! (...) Do you think I came to bring peace on earth? No, I tell you,
  but division." (Luke 12:49.51)

  "Brother will deliver brother over to death, and the father his child, and
  children will rise against parents and have them put to death" (Mt 10:21)

Examples of "camp" names:

- :term:`traditionalist` against :term:`progressivist`
- "pro life" against "pro choice"
- "vaxxers" against "anti-vaxxers"
- mercy against justice
- believers against evildoers
- capitalism against communism
- liberal politics against social politics
- God against the Mammon
- the children of light against the children of dark
- ...


Controversial questions and science
===================================

No scientist will invest time in research about whether the Earth is flat
because no human doubts about this question.

The :term:`scientific method` is based on formulating a theory and then
challenging it. Science starts when there is a :term:`controversial question`.
Science becomes useless when consensus has been reached.

Saying "There is no :term:`scientific evidence` for claim X" doesn't mean that X
is false.

Science is not an institution. Science does not ask whether something is good or
evil. Science is a tool for looking at reality *without* asking whether it is
good or not.

Producing :term:`scientific evidence` on a controversial topic requires money.
Science works for their employers, who work for their investors.  Hence science
will answer only to questions that are asked by investors.

Those who ask "How can we sell more mobile phones?" have more money than those
who ask "Is it possible that we should completely stop using mobile phones?"

Somebody who claims to tell you "the truth" about a controversial topic is
obviously an imposter.


How to handle controversial discussion
======================================

Here is how I imagine what I would do if I had to organize a debate. Not yet
much tested.

- Identify the camps:
  Invite each camp to formulate their manifesto.
  If possible, cut the problem into smaller units:
  formulate your manifest as a series of numbered statements.
  Write a clear definition for every word that might be unclear.
  Sort your statements by importance.
  Check within your camp whether you agree among yourself about your manifesto.

Dialogue.

Each round of dialogue consists of the following:

- A representative of each camp meet and exchange about every single statement
  of their respective manifestos. For each statement, the representative is to
  formulate what the enemy says about it: "Agrees", "Refuses to answer",
  "Disagrees because..."

- As long as a group doesn't reach a consensus on a given controversial question,
  the only solution is to agree on a :term:`modus vivendi`, i.e. to limit the
  damage.

.. glossary::

  modus vivendi

    A set of compromise answers to a :term:`controversial question` to which all
    participants of a team can adhere.  A temporary agreement to be followed as
    long as there is no better solution.



Controversial questions in daily life
=====================================

I have been programming accounting applications throughout my career.
The direction of a financial movement is always **either Debit or Credit**.
It cannot be both, or none of them. It's a dichotomy.
But it's not easy to decide how to label it and how to store it in a database.
Some entries of my developer blog illustrate this:
`2020-10-15 <https://luc.lino-framework.org/blog/2020/1015.html#debit-and-credit>`__
`2013-10-03 <https://luc.lino-framework.org/blog/2013/1003.html#payment-orders-and-debit-credit>`__

Another example is that most of us assume that `1+1` is 2. But in mathematics
this is an axiom, a non-provable assumption.
See for example here
`stackexchange.com
<https://math.stackexchange.com/questions/243049/how-do-i-convince-someone-that-11-2-may-not-necessarily-be-true>`__



Examples
========

July 2021. A friend of a friend in a private chat:

  Ma arvan, et keegi ei peagi vaidlema teemal, kas maa on lame või mitte. Seal
  haritud inimesel "veidikest tõde" lamemaa poolt leida pole võimalik ju 🙂.
  Homoabielude suhtes saab kristlane alati võtta Piibli ning lugeda. Pole ju
  samuti vaidlusteks kohta.  Ma ütleksin, et kindlasti on kerge mugavalt
  armastada, aga päris armastus on kõike muud, kui mugav. Me ei lase ju lapsel
  tikkudega mängida, mis sest, et ta sedasi ju õnnelik on ... enne tulekahjut.

I answered:

  Ma alati imestan, kuidas eesti kristlased usuvad ikka veel, et "Piibel ütleb
  selgesti" seda, mida nemad ise sel teemal arvavad. Näiteks homoabielude kohta
  lugesin hiljuti saksa keeles hoopis midagi muud
  http://blog.thomashieke.de/blog/bibel-und-homosexualitat/aber-in-der-bibel-steht-doch/




Glossary
========

Some definitions:

.. glossary::

  controversial question

    A question about a concrete issue for which there are two mutually exclusive
    answers.

  pair of opposites

    The two opposite answers in a :term:`controversial question`.

  dichotomy

    A partition of a whole (or a set) into two parts (subsets) where this couple
    of parts is (1) jointly exhaustive: everything must belong to one part or
    the other, (2) and mutually exclusive: nothing can belong simultaneously to
    both parts. (Adapted from `Wikipedia
    <https://en.wikipedia.org/wiki/Dichotomy>`__)

  controversial dialogue

    A dialogue or :term:`discussion` about a :term:`controversial question`.

  discussion

    A *dialogue* whose participants perceive a certain urgency for a decision.

  controversial battle

    A :term:`controversial dialogue` that is perceived by at least one camp as a
    "war" rather than a part of a :term:`learning process`.
