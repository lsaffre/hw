========================
Oops a Christian?!
========================

Yes, I am a Christian, and even a native :term:`Roman Catholic`, and I believe
in the :term:`Gospel`. But please note that :doc:`there are two gospels
circulating </christian_values>` and you might have heard the wrong one.

My faith has been influenced by the people in my home town and by the
spirituality of the `Taizé <https://taize.fr/>`_ community.  After moving to
Estonia I started to engage in several Lutheran church communities and learned
that the Church is far bigger than what I had known before.


.. contents::
  :local:

Being a Christian
=================

Being a Christian means to me that I chose the :term:`Bible` as a tool for
cultivating my :term:`faith`, for working on it.  I want to learn from the
experiences of many generations of humans that are stored in the Bible because I
trust that they are helpful also today for finding my way.

Being a Christian means to me that I invest some of my time into activities like
:term:`praying <prayer>` and reading the :term:`Bible`.  I love to meet with
other people who share that faith and practice these activities. I consider
these activities as "faith training". Being a Christian is my favourite sport.

Being a Christian means to me that I imagine God a bit more precisely than e.g.
an atheist or a follower of any religion. I imagine God as a "father" who
created everything and loves his creation, especially us humans; as a "son" who
lived as a real human and became a divine friend of every human, especially the
poor; as a "holy spirit" who lives in our hearts and speaks to us through our
emotions. Christianity gives -theoretically- an answer to a big challenge of
every religion, which I call the "temptation of the Pharisee".

Being a Christian means to me that I do `my duty to God
<https://en.scoutwiki.org/Duty_to_God_%28Scout_Oath%29>`__ as a scout.

Being a Christian means to me that I trust in the :term:`Good News` and want to
rely on it and cultivate my :term:`faith`.

Being a Christian means to me that I believe  in the :term:`Church` as a family
of all those who believe in the  :term:`Good News` and whose mission is to
announce this message to every human. I believe that one cannot be Christian and
at the same time refuse the idea of the :term:`Church`.
I even believe that the :term:`Roman Catholic` has the
potential of reconciling all Christians back under one institution.
Despite all deserved criticism.
Though we have to change a few things before this can happen.
I don't expect these changes to happen during my lifetime.


Me and the church in Estonia
==============================

I am in open disagreement with the Council of Estonian Churches (EKN), which
claims to represent all Christian confessions in Estonia. I claim that this
organization currently represents only a pre-Christian, :term:`biblicist
<biblicism>` and :term:`clerical <clericalism>` vision of reality.

In June 2023 the EKN issued an unfortunate document. This document triggered
within me an avalanche of frustration that had accumulated over the past twenty
years: The church in Estonia does not only fail to do her job! It is worse than
a failure, she works against our mission!

That's why in July 2023 I started a "communion strike". I refuse to share the
eucharistic sacrament with those who agree with that document. I do this in the
hope of making the EKN repent. I don't expect this hope to become true during my
lifetime. More about this on my `Estonian website
<https://belglane.saffre-rumma.net/arvamus/streik>`__.

I practice `spiritual communion
<https://en.wikipedia.org/wiki/Spiritual_communion>`__ to remain within the
Church.  This and my participating in regular common prayers help me stay in
connection with the :term:`Church`.




My summary of Christianity
==========================

Here is how I summarize the Christian religion.

- Let us imagine God as a father in heaven who loves all humans.  Every single
  human can pray to God like a child.

- Let us imagine that every human has a "soul", something eternal, something
  that will get born into a new world when we die.  Our life in the visible
  world is a preparation for our life in that eternal world.

- God wants the whole visible world to exist and to lead it to a good end. He
  has a :term:`plan <God's plan>` with this world.  History is not chaotic.

- God wants humanity to act as the manager of planet Earth. Each of us has their
  particular individual role in :term:`God's plan`. My life is not meaningless,
  even when I fail to see or understand its meaning.

- We don't always act in harmony with God's plan.
  We don't do our job very well all the time.
  We experience mistakes, weaknesses, errors and failures.
  We experience them in ourselves and in others,
  at individual, local and global level.
  We are not perfect.

- Now here is the good news:
  God does not "make you pay your debts", he does not "punish your sins".
  He forgives your mistakes, weaknesses, errors and failures.
  He loves you unconditionally and will never let you down.
  See :doc:`/talks/good_news`.

- This Good News calls me to "love God, and to love my neighbour as
  myself" (`Mt 22:37-39 <https://www.bibleserver.com/ESV/Matthew22%3A37-39>`__).
  See :doc:`/love`.

- This Good News influences our values, priorities and choices. See
  :doc:`/christian_values`.


Me and other Christians
=======================

Not everybody who calls themselves a Christian agrees with `My summary of
Christianity`_. Here are some critical questions asked by (mostly
:term:`biblicist <biblicism>`) friends about my summary:

- You don't explain why Jesus had to die on the cross. See :doc:`/talks/cross`.

- Is Jesus the only Saviour? What about Buddha, Mohammed and Baha'ullah? ("I am
  the way, and the truth, and the life. No one comes to the Father except
  through me. If you had known me, you would have known my Father also. From now
  on you do know him and have seen him." `John 14:6-7
  <https://www.bibleserver.com/ESV/John14%3A6-7>`__)

  Short answer: Note that Jesus says "*I* am the way..." and not "*the
  Scripture* is the way...". The :term:`Good News` is far more complex than what
  a document in human language can formulate (See :doc:`/defs/word_of_god`). At
  another occasion Jesus says about another religious leader who doesn't join
  his disciples that "the one who is not against you is for you" (`Luke 9:49-50
  <https://www.bibleserver.com/ESV/Luke9%3A49-50>`__).

- If God wants this world to end well, it is going to end well anyway, so why
  should we care? (`2 Peter 3:10-13
  <https://www.bibleserver.com/ESV/2%20Peter3%3A10-13>`__)

  Short answer: If we don't care for every part of the :term:`visible world`, we
  are at risk of excluding ourselves from the :term:`Kingdom of God` and not
  becoming a part of it.

- If God forgives our sins anyway, why should we bother to live in abstinence?
  ("For if you live according to the flesh you will die, but if by the Spirit
  you put to death the deeds of the body, you will live." (`Romans 8:11-13
  <https://www.bibleserver.com/ESV/Romans8%3A11-13>`__)

  Short answer: God's unconditional forgiveness does not mean that we may live
  in sin (i.e. do nasty things to ourselves or to other humans).  When you live
  in sin, it decreases your quality of life and/or that of other humans. The
  Gospel makes clear that *this* (and not a need to fear God's revenge) is the
  reason to avoid sins.

But even those controversial discussions are normal. No human can fully
understand God or give a perfect summary of the :term:`Good News`. On the other
hand, every human is called to find and realize the part of it that applies to
them. More about this in :doc:`/christianity`.


Me and other faith cultures
===========================

Christianity is one of the bigger models of reality that have evolved on earth.
Christian teachings are a collection of vocabulary and definitions. We use them
because they help us to cultivate a model of the world (:term:`world view`), and
I perceive that model as realistic. Yes, there are oddities in our vocabulary.
Every language has its oddities. See also :doc:`/names`. There are other models
that use different vocabulary and definitions. But life is too short to learn
them all. Christian faith is a great model, a great game. Feel free to stay out
of our game, but I love it and it helps me, and I won't get tired inviting
others to join us.

As a Christian I see no problem when somebody uses other names than
:term:`Father`, :term:`Son` and :term:`Holy Spirit` when they pray. To hallow
God's name, in such a situations, means to resist our temptation of believing
that we know God's name better than them.

I have more problems when somebody *ignores* why :term:`faith culture` is
important. Then I feel a "missionary" desire of explaining them
:doc:`/talks/good_news`.

I can get sad, angry or frustrated when I see somebody who has a hateful image
of us and seems to *misunderstand* :term:`Christianity`. But in most of these
cases I blame ourselves, because, it is *our* job to explain the Gospel.  If
somebody misunderstands it, then *we* are to blame, not *they*. I believe that
:term:`Bible idolatry` and :term:`Church idolatry` are the main reasons why some
people have a hateful image of the :term:`Church` and her work.



.. I maintain my personal modernized summary of the traditional Christian creeds.
   Currently only in German:

   Ich glaube an den einen Gott, den Vater, den Allmächtigen, der alles
   geschaffen hat, die sichtbare und die unsichtbare Welt.

   Ich glaube an Jesus Christus, den wir anbeten dürfen als Gottes Sohn, als
   Mensch geboren nach göttlichem Plan, der in die sichtbare Welt gekommen ist,
   um uns zu erlösen von aller Angst vor Gottes Strafe für unsere Sünden.

   Ich glaube an den Heiligen Geist, der uns befähigt und ermutigt, Gottes
   Botschaft jenseits menschlicher Sprache zu erkennen.

   Ich glaube an die eine und vielfältige Kirche, die die Frohe Botschaft
   verkündet und unseren gemeinsamen Glauben kultiviert und mir hilft, in meinem
   Glauben zu wachsen.

   Ich glaube an die Vergebung der Sünden, das Letzte Gericht, die Auferstehung
   der Toten und das Ewige Leben.
