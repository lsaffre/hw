============================
Did Jesus die for *my* sins?
============================

Some Christians love to say that Jesus died on the cross "for *my* sins" or even
"*because* of my sins".

We should be careful with this formulation because people can get it wrong. I
have heard children understand it as "When I lie to my parents or steal
something, then Jesus is going to die on the cross". Ouch, that's obviously not
what the Gospel tells us!

Yes, of course *also my* sins have been atoned. It would be ridiculous to
believe that I would have committed some especially detestable sin that would
exclude me from God's forgiveness.

Christian authors use such formulations in order to express that God's
forgiveness is not limited to an abstract theological idea or the collective
sins of a whole people. The Gospel is indeed something personal, something that
happens between God and me.

Brother Pierre-Yves from Taizé sheds a reconciling light unto this expression:
`Death: What enables us to say that Jesus died "for us"?
<https://www.taize.fr/en_article5236.html>`__
