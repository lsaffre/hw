=======================
About home office teams
=======================

A **Home Office Team** is a team of people working at home for a common goal.

Working in a home office has advantages and disadvantages, joys and dangers. I
have been working at home for 20 years. Here is a collection of my advices and
suggestions I happen to give to my team members.  It's not finished and never
will be.

Leave traces of your thoughts
=============================

Keep in mind that you might die at any moment.  You are a *human*. Today might
be the last day of your life.

Don't rush on for days into some direction without leaving any written trace of
what you are doing.  And "doing" includes: failing to do, surfing around,
stumbling, asking questions, realizing that you were wrong, explain why you did
something the way you did it,...


You are not alone
=================

You are part of a *team*. Our team is unique. It consists of a limited number of
other humans.  The others need you and you need them.  You are not alone.

.. You work for an *open software* project. The result of your work is being
  published under a :term:`protective Free Software license`.  It is not your
  private property.  Neither is it the private property of anybody else.  It
  belongs to all humans. You agree that the team acts as the legal copyright
  holder of your work.

Doing your best
===============

Life is more important than work.  The team cannot expect you to deliver
satisfying work if you have serious troubles or are not satisfied with your
life.  Your availability for the team varies over time.

On the other hand the team has a mission and duties towards their customers. You
have a responsibility towards the team's mission.

Working as a software developer is often fascinating and can monopolize your
attention.

Love your work
==============

We want you to enjoy and love what you do.
Never do something you hate to do.

We want you to develop your full potential.

There is a difference between

- a job that nobody wants to do, but everybody agrees that somebody must do it,
  and you happen to be the most suitable or the least unsuitable team member.

- a job that doesn't suit you, and another team member should actually do it,
  but neither you nor the other team members know it.


About sleep
===========

Switch of your phone when you go to sleep. Ideally already half an hour before
you go to bed.

We have no fixed office hours. Other team members can't know whether you are
sleeping right now. A little text message or a missed voice call notification
can cause you to loose valuable hours of sleep.  One hour of lost sleep can cost
you a whole day of inefficient work. On the other hand, text messages and voice
calls are there to be used. Maybe it's 3am in the morning, but you happen to be
fully awake, and the other team member really needs your feedback to get
unlocked.

Sleeping is an important part of your life.
It's a duty you have towards your body and your health.
Sleep is not like money, you cannot make profit by giving credit to others and hoping to get it back later.

If you don't sleep enough, your work performance and efficiency decrease.
Learn to listen to your body.
When your body needs to sleep, then *doing your best* means that you go to sleep.

About burnout
=============

We should always *worry* about burnout, but we shouldn't *fear* it. It is a real
danger for everybody, especially for members of a home office team. But when you
know the warning signs, then you can safely wander quite far into areas that are
dangerous to those who ignore the warning signs. That's why it's useful to speak
about it, and even to collect advice for future team members.

My personal "now it's time to stop" warning sign is when I notice that my body
needs sleep but my spirit refuses to stop working.  This happens from time to
time, when there is a particularly interesting problem to fix. I then decide to
take one or several days off and to "play"  as if I was really sick.

Here is a poem written by French  actor and poet `Robert Lamoureux
<https://fr.wikipedia.org/wiki/Robert_Lamoureux>`__ (translation by Simon
Couderc on `lyricstranslate.com
<https://lyricstranslate.com/en/%C3%A9loge-de-la-fatigue-praise-fatigue.html>`__):

| You tell me, sir, that I look bad,
| That with this life that I lead, I ruin myself,
| That one does not gain anything to do too much,
| You finally tell me that I am tired.
| (...)
| And you advise me to go to rest!
| But if I accepted there, what you propose,
| If I surrendered to your sweet intrigue ...
| But I would die, sir, sadly ... of fatigue.
