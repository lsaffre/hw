=====================
Everybody is faithful
=====================

"[T]he intellectual and emotional energy it takes to figure out how God fits
into everything is far greater than dealing with :term:`reality` as it presents
itself to us. (...) [T]he existence of :term:`God` seems like an extra layer of
complexity that isn't necessary. The world makes more sense to me as it is,
without postulating a divine being who is somehow in charge of things."

This is how `Ryan Bell <https://en.wikipedia.org/wiki/Ryan_J._Bell>`__, a former
Seventh-day Adventist pastor who became an atheist after spending a "year
without God" as an experiment, explains it in an interview (`Huffington Post,
January 2015 <https://www.huffpost.com/entry/ryan-bell-atheist_n_6397336>`__).

Yes, all theistic :term:`religions <religion>` put an "extra layer" around
reality by "inventing" a personified "God". You may perceive this extra layer as
useless or even falsifying. Atheism wants to look at reality directly. An
understandable desire.

Ryan Bell explains in the same interview: "Anytime you can step outside your
comfort zone, you will learn important things about yourself and the world. I've
learned that atheists are not the miserable nihilists that many Christians think
they are.  I've also had a few remarkable moments of irony. Once I was in a
gathering of atheists and the speaker referred to "seeing the light" and
"finding freedom at last." It struck me then that most people really are
searching for the same thing."

Nobody --including you and me-- knows whether a religious "extra layer" is
useful for yourself or at cultural or sociological level. Your individual
preference depends on your personal history. It is a matter of taste.

Yes, the :term:`Bible` is a dangerous book.
It has caused much harm to many people.
But I claim that it has done even more good to even more people.
Like every :term:`Holy Scripture` it is a noteworthy phenomenon.

Dare to :doc:`vary the colour of your glasses </glasses>` when looking at reality.

..
  Modern Christians agree to say that the :term:`Good News` is much more than the
  texts themselves (see :doc:`/defs/word_of_god`). Yes, some Christians --they call
  themselves "traditional", I call them :term:`biblicists <biblicism>`-- do not
  agree with that statement.

Yes, there are still Christians who cause serious harm. I can only beg your
pardon for them. And let me remind you that there are also non-Christians who
cause serious harm.

If you have a problem with the words "God" or "the Lord" in the Bible, I
recommend pronouncing them "Reality". "The Lord" is how Christians translated
the word "JHWH" of the Hebrew `Tanakh
<https://en.wikipedia.org/wiki/Hebrew_Bible>`__, which was pronounced either
Jahweh or Jehovah. We definitively have no definitive name for this ultimate
reality we are talking about. The name of God is holy.

I disagree with people who say that religions are obsolete. Every world view
--including an atheistic one-- is a "religion" in the meaning that it relies on
assumptions. Atheistic cultures are as much :term:`faith cultures <faith
culture>` as any religious culture. Atheists and agnostics :term:`believe <to
believe>` in the teachings of their culture as much as Christians believe in the
:term:`Good News`.

You may be convinced that it is evil --or just useless-- to imagine
:term:`reality` using religious vocabulary, but this is just your
:term:`conviction`. And permit me to have mine.

Of course there no :term:`scientific evidence <science>` that my faith in the
:term:`Good News` is realistic. But there isn't any scientific evidence
*against* it, either. That's why every human should have the right to freely
decide whether to believe in the :term:`Good News` or not and how to cultivate
their faith.

Let's learn to :doc:`speak to each other </defs/dialogue>` lovingly and without
hate. My challenge to both Christians and Atheists: please stop assuming or
insinuating that a member of the other side is "naive", "wasting their energy"
or "needs to be saved by our Lord Jesus Christ" (see :term:`saviorism`).

Speaking requires a vocabulary, a set of concepts and definitions. Let's learn
to use a common vocabulary when talking about the spiritual part of reality.

This is especially important in a context where different :term:`cultures
<culture>` have to coexist next to each other on a same territory.  It we want
peace, we need to learn this.

I suggest to consider faith as a decision to live "as if" God exists. "I desired
the peace manifested by my Christian friends. Therefore, I decided to live my
life as if God existed. (...) I felt much more comfortable with an approach that
allowed me to view some of life's difficult moments in my work as a physician as
being meaningful, rather than as meaningless tragedies. As I gained more life
experience, I recognized that other "as if" approaches can be very helpful and
even healing for people."\ [#Anbar]_

.. rubric:: Footnotes

.. [#Anbar] Ran D. Anbar M.D.:
    Tentative Belief Can Lead to Real Change.
    What religion, hypnosis, placebo, and synchronicity may have in common.
    2023-02-25,
    `psychologytoday.com
    <https://www.psychologytoday.com/intl/blog/understanding-hypnosis/202301/tentative-belief-can-lead-to-real-change>`__



..
  :term:`fundamentalist atheists <fundamentalist
  atheist>` as well.

  .. glossary::

    fundamentalist atheist

      An atheist with a rigid, intolerant, and dogmatic adherence to atheism or an
      atheistic ideology.
      `source <https://www.learnreligions.com/definition-of-fundamentalist-atheist-247860>`_
