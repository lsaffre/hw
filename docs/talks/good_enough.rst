================
About being good
================

.. glossary::

  Good enough is not enough

    https://theconversation.com/when-good-enough-is-not-good-enough-34575

  More than good enough is too much

    This is one of my favourite self-made slogans. It is the opposite of
    :term:`Good enough is not enough`.
