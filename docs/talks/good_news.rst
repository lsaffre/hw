====================================
What's so good about the Good News?
====================================

.. :term:`Christianity` is a religion based on what we call the :term:`Good News`
  or the :term:`Gospel`.

The purpose of the :term:`Church` is to announce the :term:`Good News` or
:term:`Gospel` to all peoples. But then the only available summary of this
Gospel is the :term:`Bible`, which is too long for a typical announcement. How
are we supposed to announce something to everybody without even being able to
formulate it in a reasonably understandable text?

.. contents::
  :local:

Explaining the Gospel to newcomers
==================================

In :doc:`gospel60` I conclude that there is no `executive summary
<https://en.wikipedia.org/wiki/Executive_summary>`__ of the Gospel because it is
a *divine* message. Its translation to human language depends on the recipient
and the situation. We need to constantly invent new ways for announcing the
Gospel. `Psalm 98 <https://www.bibleserver.com/ESV/Psalm98>`__ invites us to
"sing a new song".

Jaan Lahe (a Lutheran pastor), has written a book of 383 pages about the Gospel
("Sõnum teisest maailmast", "Message from another world"), which ends with a
three pages summary "What is the basic message of Christianity?", which contains
22 theses and specifies that it is valid only for those who are "familiar with
his book as a whole".

On the other hand there are summaries of the Gospel to which I react by saying
"If *this* is the Gospel, then I am not a Christian!"  See for example
:doc:`/blog/2020/0311`.

I have lots of questions.
Is there a difference between saying
"You will be saved" and "You will go to :term:`Heaven`"?
Will you be saved unconditionally? If yes, why should I bother you
with this mysterious Gospel, which I claim to believe in?
Will you be saved *if and only if* you believe in my gospel?
From what will you be saved?
And why would my message use the future tense if a gospel is an announcement of
a victory that has happened?

..
  Our summaries tend to use biblical language and therefore exclude those who
  suffer from an allergy to biblical language. When somebody suffers from an
  allergy, we need to account for this. If we just say "That's your problem", then
  we didn't do our job.


More about the Gospel
=====================

One thing seems clear: the Gospel is neither an advice nor a commandment, but a
*news*, an *announcement*. It doesn't say "You must do this in order to go to
Heaven", it says that :term:`Heaven` *is* already now and here, that the door to
Heaven is open, because *something has been done* for you" \ [#keller]_.  The
*something* that has been done for us is the life of :term:`Jesus Christ` on
Earth, which brought a new element into our way of understanding
:term:`reality`.

The Gospel has fundamental consequences at theological,
political, economical, social and psychological level.
It causes a fundamental change in our thinking and our way of understanding the world.
It sheds a new light on the way we live together.

The Gospel is universal: it is not only for a particular nation or class of
humans, it is for every nation and for every single human. Jesus told his
disciples to explain the Gospel to *every* human, to *every* people, to *every*
culture, not only to their own people.

The Gospel says that Jesus Christ has redeemed us forever from the idea that
humans will have to pay for their debts or get punished for their :term:`sins
<sin>` after their death (see :doc:`/atonement`). It calls us to understand that
:doc:`mistakes are good </mistakes>`.

The Gospel says that the :term:`Kingdom of God` is not after our death but
already now and here. It calls us to understand that :doc:`Life is a gift
</giving>`.

The Gospel encourages us to `civil disobedience
<https://en.wikipedia.org/wiki/Civil_disobedience>`__ where needed. It calls us
to refuse human laws and teachings that promote the opposite of what God wants.

Henry Nouwen writes "The gospel is a radical liberation from death, which
enables us to love without fear." (in *Jesus Sinn meines Lebens*, p.43)

Creeds
======

Christians developed *creeds*, short texts that can be learned by heart.
They are a testimony useful to identify as a member of the Church.

They are a kind of "entry exam":
if you understand them and agree to say them aloud in front of witnesses,
then we assume that you understood the Bible as the Gospel,
that you are ready to get "baptized",
in other words to become a "certified Christian".

Creeds are some kind of summary of the Gospel, but they make sense only for
somebody who has received basic teaching about the Gospel.
They are not useful for introducing the Gospel to a newcomer.


What the Gospel is not
======================

The Gospel does not give instructions about how to organize the :term:`visible world`.
Jesus has no problem with big contrast between rich and poor (Luke 17:5-10),
neither with slavery,
nor with the dictator-like Roman emperor (Mt 22:15-22)
nor with the Roman occupation of the Jewish territory
or that the Romans forced Jews to join their army (Mt 5:41).

The Gospel does not ask us to live in traditional families or to protect this
way of living within our societies.

The Gospel does not ask us to protect unborn children from :doc:`abortion
</abortion>`, it asks us to not kill other humans. The question when an embryo
starts to be a human remains :term:`controversial <controversial question>` even
among Christians.

The Gospel does not say "If you don't want to burn in hell forever, repeat this
prayer and believe that Jesus died on the cross for sins, rose from the dead and
is coming back to destroy sinners and rescue the Christians."\ [#giles]_ See
:term:`saviorism`.

The Gospel does not say "Here is how to get to Heaven", but "The Kingdom of God
is already here, it has come to you via Jesus Christ" \ [#keller]_


The Church and the Gospel
=========================

Excerpt of a speech held by pope Francis in October 2015 in his `conclusion to
the synod on the family
<http://w2.vatican.va/content/francesco/en/speeches/2015/october/documents/papa-francesco_20151024_sinodo-conclusione-lavori.html>`__:

  The Synod experience also made us better realize that the true defenders of
  doctrine are not those who uphold its letter, but its spirit; not ideas but
  people; not formulae but the gratuitousness of God’s love and forgiveness. This
  is in no way to detract from the importance of formulae – they are necessary –
  or from the importance of laws and divine commandments, but rather to exalt the
  greatness of the true God, who does not treat us according to our merits or even
  according to our works but solely according to the boundless generosity of his
  Mercy (cf. Rom 3:21-30; Ps 129; Lk 11:47-54). It does have to do with overcoming
  the recurring temptations of the elder brother (cf. Lk 15:25-32) and the jealous
  labourers (cf. Mt 20:1-16). Indeed, it means upholding all the more the laws and
  commandments which were made for man and not vice versa (cf. Mk 2:27).

My summary of this paragraph:

- Humans are more important than ideas
- The spirit of the Gospel is more important than the letter
- Explaining God's love and forgiveness is more important than maintaining formulae
- Christians must overcome the recurring temptations of the elder brother (cf. Lk 15:25-32) and the jealous
  labourers (cf. Mt 20:1-16).
- Realizing that the laws and
  commandments were made for man and not vice versa (cf. Mk 2:27)
  encourages us all the more to uphold them.

`Klemens Stock SJ <https://de.wikipedia.org/wiki/Klemens_Stock>`__ writes \
[#stock]_: "Es ist zentraler Inhalt der Verkündigung Jesu, dass es nicht nur das
irdische Leben gibt, das wir alle aus unserer unmittelbaren Erfahrung kennen,
sondern dass es darüber hinaus das ewige Leben gibt. Der Tod ist (...) nicht das
absolute Ende, das Versinken im Nichts, sondern ist der Durchgang zum ewigen
Leben. Dieses besteht darin, dass wir am ewigen, unvergänglichen Leben Gottes
teilnehmen dürfen. Von diesem Leben haben wir keine Kenntnis aus unserer
menschlichen Erfahrung, sondern wir hören von ihm durch das Wort Jesu."



.. rubric:: Footnotes

.. [#giles]  Keith Giles in
             `Exposing The False Gospel Of Evangelicalism
             <https://www.patheos.com/blogs/keithgiles/2020/03/exposing-the-false-gospel-of-evangelicalism/>`__

.. [#keller]  Tim Keller in :term:`The Gospel-Shaped Life`
.. [#stock]  Betendes Gottesvolk 2024/4 (Nr. 300)
