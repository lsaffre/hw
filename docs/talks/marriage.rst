=================
About my marriage
=================

If I had known how much time and energy it takes to be married, then I wouldn't
have done it.  What a chance that I didn't know ;-)

  | S'aimer, ce n'est pas se regarder l'un l'autre,
  | c'est regarder ensemble dans la même direction
  | -- Antoine de St-Exupéry.


Don't think that our marriage is easy and romantic. My wife and I have a lot of
differences in communication style, :term:`faith` and temperament. We dispute
fervently. Sometimes I feel sorry for those who happen to see or hear one of our
battles.  Especially our children. But until now we always, always have managed
to reconcile.

When we see that so many marriages in this world fail, we are tempted to give
up, divorce, to be "realistic". But as a Catholic I say to myself in hard
moments "I took responsibility for my wife and our children, I have no right to
give up. It is not my job to decide whether it is realistic. If it was a wrong
decision, then let it be wrong until the end." In my hardest moments I say
"Okay, I am gonna do a pilgrimage to Santiago de Compostella, and *afterwards* I
will decide."  I can *imagine* that I would indeed do this, but until now I
always changed my mind before even starting to prepare...
