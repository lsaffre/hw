=====================
The good and the evil
=====================

I'll start with three definitions:

.. glossary::

  morality

    The science of differentiating between :term:`good` and :term:`evil`.
    Replace "good" with "right" and "evil" with "wrong" if you prefer.

  good

    What is in harmony with :term:`reality`.

  evil

    What is in disharmony with :term:`reality`.

Yes, I am simplifying.  Refer for example to the `Stanford Encyclopedia of
Philosophy <https://plato.stanford.edu/entries/morality-definition/>`__ for a
more complex discussion.

I love to use simple definitions. These definitions are easy in theory, but it's
of course not easy to tell which of them to assign to some concrete action or
message or thought.

Morality is similar to computer science in that it is based on series of binary
values. You look at the world in terms of using only two values, good and evil,
1 and 0, with nothing between them.

The root problem of our time, the father of all our problems, is a lack of faith
throughout society. The world is ruled by money, by :term:`greedy giants <greedy
giant>`.  And an increasing number of people ignore the :term:`Gospel` because
there are not enough workers to announce it. And the church often wastes her
time with spreading useless --if not counterproductive-- messages.
