===================
About the Eucharist
===================

The sacrament of Eucharist is a genial rite for confirming communion with the
church: receiving the consecrated host from the priest confirms that the church
accepts you as a member. Taking it and swallowing it confirms that you want to
be a member. It is like a weekly renewal of a membership contract. And it is  a
public profession because everybody can see it.

When the priest, after the consecration, exclaims "(This is) the mystery of
faith!", I perceive this it as follows: "That's the key to our faith! And we
don't understand it! So don't ask me to explain it." And the answer of the
assembly expresses our helplessness given these circumstances: "We announce your
death, O Lord, and we proclaim your resurrection, until you come in glory."
That's all we can do. Announce and proclaim, nothing more.  No forcing, pushing
or hindering anybody. No executive power over the world.



`ECCLESIA DE EUCHARISTIA
<https://www.vatican.va/content/john-paul-ii/en/encyclicals/documents/hf_jp-ii_enc_20030417_eccl-de-euch.html>`__

5. “Mysterium fidei! - The Mystery of Faith!”. When the priest recites or chants
these words, all present acclaim: “We announce your death, O Lord, and we
proclaim your resurrection, until you come in glory”.
