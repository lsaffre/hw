================================
Tell me the Gospel in 60 seconds
================================

I happen to challenge my fellow Christians, asking them "How would you summarize
the Gospel to a complete newbie?" Or more precisely: Imagine that a colleague
(or a classmate or another guest on a wedding) asks you "Oh, you are Christian.
I always wanted to ask a real Christian: What is this Gospel, this message that
you teach in your church?" You have 60 seconds to answer. What do you say?

I observed that Roman Catholics are more reluctant to answer my question than
Protestant Christians.

Answers from friends
====================

Here are some results I received from friends.

(:count:`summary`)
One friend answered with the following compilation of two
Bible verses [#fun]_ (based on `N.T. Wright
<https://en.wikipedia.org/wiki/N._T._Wright>`__ in his book "How God became
King"):

  Born is the Saviour King. He redeems/frees/saves/heals you, because he loves
  you. :term:`Repent <to repent>` and :term:`believe <to believe>` in this good
  news (i.e. witness God as King)!

  Sündinud on Päästja kuningas. Ta lunastab/vabastab, päästab, tervendab sinu,
  sest ta armastab sind. Sellele järgneb ülsekutse: paranda meelt ja usu
  evangeeliumisse (st tunnista Jumal kuningaks)

(:count:`summary`) Another friend answered quite spontaneously:

  See on rõõmusõnum. Headus võidab, kurjus kaotab.
  Jumal armastab inimkonda.
  Ja "armastab" tähendab, et ta soovib head.
  Jeesuse kaudu ta näitas, et heateod on õiged.
  Ja vägivald ja vihkamine ei ole õige.
  Kui sa tõesti seda usud, siis oledki juba taevas (jumalriigis).
  Muidugi me näeme et kahjuks meil ei õnnestu olla head kogu aeg.
  Kristlased julgevad eksimustest rääkida, sest me usume et need
  on meile andeks antud (Jumala poolt, mitte tigimata enda või teiste inimeste poolt).

(:count:`summary`) Yet another summary:

  Evangeelium õpetab, et on olemas kõrgem jõud, kes on loonud kogu Universumi
  ja keda kristlased nimetavad Jumalaks. Ning see Jumal hoolib inimestest nii
  nagu vanemad hoolivad oma lastest.

(:count:`summary`) Here is a summary from a Catholic friend:

  Love others like you love yourself. Go to church and learn the catechism. Keep
  practising and you are on a good path!

(:count:`summary`) And another one:

  You are children of God.
  There is much space in my father's house.
  Of course you need to "tune" your life towards God.
  Otherwise you don't notice.

.. Jumaldamine on takistus kirikuinimestele. "Mina vaene ussikene"

(:count:`summary`) And another one:

  The best moment of your life is here and now. God is Love and He loves you
  here and now with all the attention of a God to his eternal child. He only
  needs your Big unconditional YES. Fruit: Because Love loves me I become myself
  Love.

(:count:`summary`) And another one, from a German pastor who was preaching about
`Romans 1:14-17 <https://www.bibleserver.com/ESV/Romans1%3A14-17>`_:

  Gott hätte sagen können "Was soll ich mit diesen Menschen? Die interessieren
  mich nicht. Ich sitze in meiner Ewigkeit, habe meinen Spaß an Wurmlöchern,
  Spektralnebeln, Galaxien, den großartigen Regeln der Schwerkraft, die sich
  ganz emotionsfrei und ewig vollziehen -- was soll ich mit dem Menschen, dieser
  verzwirbelten Kreatur, die für alles offen ist nur nicht für mich?"

  Aber stattdessen besteht die Gerechtigkeit Gottes darin, dass er sie teilt mit
  uns. Dass er uns Jesus schickt und uns durch Jesus, in Jesus, für gerecht
  erklärt. Dass er uns ansieht, als hätten wir alles richtig gemacht. Uns
  ansieht als seine Kinder. Auch wenn wir das nicht durch unsere Werke zu
  erwarten hätten, aber uns so betrachtet, dass wir seine Kinder sind, der uns
  liebt genau so, wie gute Eltern ihre Kinder lieben. So handelt Gott an uns.
  Gott möchte uns retten. Und alles, was wir dazu tun können, selber, ist, diese
  Rettung anzunehmen. Diesem Wort zu glauben.

..
  Das Evangelium hat neben der subjektiven Seite, die jede\*r von uns damit
  verbindet, auch eine ganz objektive Seite, die von Gott ausgeht, wo Gott etwas
  tut, etwas beschließt und diesen Beschluss auch umsetzt. Und die fasst Paulus
  zusammen in dem Wort von der Gerechtigkeit Gottes.


(:count:`summary`) Here is a summary written by :doc:`Olga Krasnikova
</blog/2022/0225_0920>`:

  It is very difficult for us to keep at least for some time the memory of God's
  presence in our lives.  It demands internal work, effort, spiritual search,
  personal growth -- showing the task to be really complicated. We are trying to
  follow the path that seems easy for us. We are dreaming to find a person who
  will make us happy and fill our internal emptiness. But this is the wrong way.
  The real core of our inner life is the connection to God.

  It may be revealed in a feeling that God is loving me, accepting me, that I
  owe my life to Him, that I am responsible for the way of my living, as life is
  God-given, but also is subject to His judgement.  Even if we can hardly trace
  our feeling of communion with God, this is the very vector that we are moving
  along. It is important to see the perspective and start moving in the right
  direction.

  The main thing is to keep faith that when I am so scared, bad and lonely, when
  life seems to be a burden – at this moment God is holding me in his arms.

.. My own answers
  Here is the earliest attempt I remember, I don't know when I wrote it, and it's
  here just to show you how easily you can miss the point:

    :term:`Jesus Christ` has brought a message that can save the world.
    You get into :term:`Heaven` when you manage :term:`to believe` this message.
    Heaven is in the :term:`Kingdom of God`, which is not only after your death
    but already now and here.

  Had I given this answer to our wedding guest, it would have been a plain
  disaster for the Church.  He would have diagnosed it as (poor) propaganda
  rhetoric. After the first sentence he would think "Wow, save the world... who
  wouldn't want this message? Come on, let it out, your message!" After the second
  sentence: "Aha, you promise some sugar (getting into heaven) to those who
  believe your message. Okay, we will see whether I believe it. Just tell me this
  message." After the last sentence: "This is your Gospel?! We don't need any
  church to believe this."


.. (:count:`summary`) Here is another attempt, which I wrote in December 2021:
  Every human can reach, during their lifetime, a state of ultimate peace of
  heart.

.. (:count:`summary`) Or maybe this one (which I wrote in Summer 2021) is better?
  God does not punish your sins, he does not make you pay your debts.
  He forgives your mistakes, weaknesses and failures.
  He loves you unconditionally and will never let you down.

(:count:`summary`) Here is my own answer
(the current one... it may change at any moment):

  Whether you are an anarchist or a member of parliament, a churchgoer or an
  atheist, whether you are 12 years old or 96, we all experience situations
  where you realize that you you did (or still do) something wrong, that you
  failed (or still fail) to be :term:`good`.

  I don't mean trespasses against some law or some cultural convention. I mean
  the *individual feeling* in your guts (in your :term:`heart`), something you
  really *regret*.

  In such situations there are exactly two possible ways to react, which exclude
  each other:

  (1) you need to do something in order to get "cleaned"

  (2) you are valuable as you are, despite your failure

  Answer (2) is the Gospel. Answer (1) is the opposite of the Gospel.

  And then the Gospel is addressed to *every* human, not only to you and your
  friends.

  That's all. Just believe it.
  If you manage :term:`to believe` in the Gospel,
  you will be *sustainably happy*
  every day of your life
  and even beyond your own death.

  But it's not easy to believe. It needs constant training. Some might be more
  talented than others, but nobody is perfect. Our faith is a life-long learning
  process. We need to cultivate our :term:`faith` so that the Gospel is the
  first answer of our heart when something unpleasant happens.

  Religious rites are a kind of deliberate brainwashing:
  you "clean" your heart from any "dirty" beliefs
  that try to make you believe something else than the Gospel.


..
  We humans are almost constantly busy with asking ourselves:
  Am I good enough? Am I loved? Does my life make sense?
  Will I survive this humiliation?

  The Gospel says:
  yes, you are good enough, you are loved, your life makes sense,
  you will survive this humiliation.
  God made the world such that
  the good always wins over the evil in the end.
  God loves every single human *unconditionally*,
  like parents love their children
  regardless of whether they deserve this love.
  Your mistakes and weaknesses are no obstacle to his love.
  (Though of course they can harm yourself or others.)
  Nothing is impossible for him,
  he is always listening to you,
  he is always helping you,
  though of course you do not always grasp every detail of his plan.




Conclusions
===========

So what?  My summary so far: It seems that there is no official summary of the
Gospel because the Gospel is a *divine* message. Its translation to human
language depends on the recipient and the situation.
It takes more than 60 seconds to explain the Gospel.
There is more to say about it. See :doc:`good_news`.

Footnotes
=========


.. [#fun]  There are two places in the Bible that seem to formulate the
  :term:`Gospel` directly:

  - Luke 2:11:
    - For unto you is born this day in the city of David a Saviour, which is Christ the Lord. (ESV, KJV)
    - Today in the town of David a Saviour has been born to you; he is the Messiah, the Lord. (NIV)
    - Un Sauveur vous est né aujourd’hui dans la ville de David; c’est lui le Messie, le Seigneur. (BDS)
    - c'est qu'aujourd'hui, dans la ville de David, il vous est né un Sauveur, qui est le Christ, le Seigneur. (LSG)
    - aujourd'hui, dans la ville de David, il vous est né un Sauveur qui est le Messie[1], le Seigneur. (S21)

  - Mark 1:14-15:
    - and saying, “The time is fulfilled, and the kingdom of God is at hand; repent and believe in the gospel. (ESV)
    - And saying, The time is fulfilled, and the kingdom of God is at hand: repent ye, and believe the gospel. (KJV)
    - ‘The time has come,’ he said. ‘The kingdom of God has come near. Repent and believe the good news!’ (NIV)
    - Il disait: Le temps est accompli. Le royaume de Dieu est proche. Changez et croyez à l’Evangile., (BDS)
    - Il disait: Le temps est accompli, et le royaume de Dieu est proche. Repentez-vous, et croyez à la bonne nouvelle. (BDS)
    - et disait: «Le moment est arrivé et le royaume de Dieu est proche. Changez d'attitude et croyez à la bonne nouvelle!» (S21)

    Note that the reader's version of NIV replaces "Repent" by "Turn away from your sins"

  The dogmatic constitution "on divine revelation" :term:`Dei Verbum`
  (Latin for :term:`Word of God`) does not mention neither Luke 2:11 nor Mark
  1:14-15.
