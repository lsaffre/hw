=================================================
Does God forgive our sins before we confess them?
=================================================

I believe --and have been told by the Church-- that God forgives our :term:`sins
<sin>` unconditionally, even before we tell anybody about them. God loves you
*as you are*, including of your sins. *This* is the :term:`Gospel`. *This* is
why Christians *dare* to speak about sins. We speak about sins because we *dare*
it.

For God there is only one *unforgivable sin*, very different in nature from
everything we use to call a :term:`sin`: refusing to be aware of your sin. When
you are an alcoholic and can't get rid of your dependency: no problem for God as
long as you acknowledge that you have a problem with alcohol. Similar words for
sins that really harm other people and can't be undone. Because if you
acknowledge them, you know your responsibility and act accordingly.

Refusing to realize your sin makes you sweep them under the carpet. You don't
get rid of them, they will accumulate and grow. This *unforgivable sin*, the
"sin against the Holy Spirit" because it separates
you from God. You exclude yourself from :term:`Heaven`.

Clarification: Of course a sin causes *harm* to you or others (or both).  But that's
not a divine punishment, it is a natural part of life. God does not punish. The
father of the prodigal son (Luke 15:11–32) had been waiting for the return of
his son already *before* the son had confessed his mischiefs to anybody. If the
prodigal son hadn't decided to return to his father, he would have died
miserably without receiving God's forgiveness.

See also `Die Grenze seiner Gnade
<https://www.erf.de/lesen/glaubens-faq/die-grenze-seiner-gnade/33618-30>`__

Jeremy Myers (`redeeminggod.com
<https://redeeminggod.com/confess-1-john-1-9/>`__) for example agrees with me.
But Suzie Hartwright (`covenantkeepers.com
<https://covenantkeepers.com/the-everlasting-covenant/does-god-forgive-our-sins-before-we-commit-them/>`__)
clearly doesn't. She writes "Does God forgive our sins before we believe in
Jesus Christ as our Lord and Savior? Does God forgive our sins before we confess
and repent of them? The answer to all three questions is no, we have no evidence
or examples in either the Old Testament or the New Testament that would support
that." Note that she is a :term:`biblicist <biblicism>` (her statement is based
on the Bible and she does not ask for evidence).

To **confess your sin means** to agree with God that it is a sin.  It is more
than just to admit, proclaim, or declare it. The opposite of *confession* is
**denial**.
