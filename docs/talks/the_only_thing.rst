==================================
The only thing we are on Earth for
==================================

How would you finish the sentence "The only thing we are on Earth for is..."?
What is the top-level meaning of life for a human?
Is there an infinity of answers?


- Some answer "... to reproduce". (e.g. `Elon Musk's 76-year-old dad says he's
  had another child with his 35-year-old stepdaughter: 'The only thing we are on
  Earth for is to reproduce'.
  <https://www.businessinsider.com/elon-musks-dad-errol-has-second-child-with-stepdaughter-report-2022-7>`__)

- My personal answer is "... to praise God"
