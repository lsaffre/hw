================
About the cross
================

The **cross** is the logo of Christianity. It is probably one of the best-known
logos in the world.

Why did Jesus have to die on the cross?  Was it necessary to use such a cruel
picture?  Isn't it harmful for your children to have a picture of a dying man
being tortured in your living room?  Isn't this picture in contradiction with a
God who loves us unconditionally?

Regarding our children, I think they are used to much crueller pictures than the
one of Jesus' crucification.

The cross originally signifies a complete failure. Getting convicted to death on
a cross meant that you had done something utterly evil and criminal. No
:term:`religion` can reasonably be based on such a guy.  Nobody can be proud
about a hero just because that hero managed to get convicted to death in a
miserable way. When the hero of a story is such a miserable guy, then the
message of the story must be very good.

Christians worship the cross because it reminds us that Jesus had to *go through
death* in order to confirm the :term:`Good News`. To go *through* death means
that Jesus did not only die, but that he also resurrected from death. The symbol
of the cross would make no sense without Jesus' resurrection (`1 Corinthians
15:17 <https://www.bibleserver.com/ESV/1%20Corinthians15%3A17>`__).

Worshipping the cross also means to refuse to claim that everything is okay.  I
wouldn't trust any culture that sweeps under the carpet unpleasant topics like
death, hate and war.  The Gospel doesn't fix these issues for us, it helps us to
embrace them.

Another aspect of the cross is theological: :doc:`/atonement`.

We can develop this into an answer to our introducing question: Yes, Jesus *had*
to die. The cruel end of his human life is part of :term:`God's plan`. It was
necessary because a theological problem needed to get fixed.  By his death,
Jesus "pays back our debt" so that God can be merciful without being unjust.

Brother Alois from Taizé gives it a reconciling aspect in his daily prayer on 15
April 2021:

  Eternal God, open our hearts to your love. Jesus the Christ has given his life
  so that nothing can separate us from you any more, neither troubles nor anguish,
  neither what is present nor what may lie in the future. Praise be to you for
  calling us to bear witness to this truth by our whole life.

  -- `Daily Prayers by Brother Alois <https://www.taize.fr/en_article21542.html>`__
