================
About narcissism
================

One of my close friends is a narcissist. All I read about the topic tells me
that he is. He is unable to say "Oops, I admit that I made a mistake, please
forgive me". Even in minor situations he would never say "Oops, I forgot the
key" but rather "Oops, the key stayed at home". And when a situation gives
undeniable evidence that he is to blame, he falls into a deep hole where he sits
for hours or even days, unable to do anything useful or to think about anything
else. He is convinced that others in general don't love him as he deserves. He
hates to get labeled. He is allergic to critics. He uses other people to meet
his needs. His feelings are the only ones that matter. He believes in a double
standard regarding how he is treated. He doesn't care about how other people
feel. It is very difficult to collaborate with him.

That's called narcissistic personality disorder. Therapists give clear advice:
Run away! Save yourself! Seems like a tough diagnose.

- https://www.psychologytoday.com/us/basics/narcissism
- https://www.youtube.com/watch?v=yMbSK_zlv7w
- `You May Be Narcissistic if These 4 Traits Don’t Bother You.
  <https://www.psychologytoday.com/us/blog/stress-fracture/202203/you-may-be-narcissistic-if-these-4-traits-don-t-bother-you>`__

But I am not a psychotherapist, so who am I that I dare to judge? And even if it
is true: shouldn't I rather forgive him for having this disorder? Does it help
*him* to get healed from this disorder?
