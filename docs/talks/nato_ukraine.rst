:date: 2022-04-04 13:45

=============================
The end of the war in Ukraine
=============================

I am collecting feedback about the following vision.

As a lesson from the war in Ukraine, the NATO will soon start to withdraw all
weapons from the territories around those of the Russian Federation. Instead of
using weapons, they will reorient their armed activities towards `non-violent
resistance <https://en.wikipedia.org/wiki/Nonviolent_resistance>`__ (NVR) by
using modern and traditional technology to develop security infrastructure with
three main priorities:

- evacuate the population in case of
  danger and ensure their temporary living in a safe area

- make the territory unusable for the invader
  and easy to restore when the conflict has found a resolution

- record what's happening on the territory so that later
  court decisions can be based on evidence

At the same time, the Roman Catholic church will appoint an international group
of consecrated people that gives independent expert advice on territory claims.
The members of this group have renounced from private property and have no
heirs. Their decisions will be based on historic, ethnic, sociological and
geographical evidence. This group assigns every geographical region of the world
to a :term:`nation state` that is given sovereign responsibility for regulating
its usage. It will explain, also in Russian, the reasons for their decisions.

The `International Court of Justice <https://www.icj-cij.org/en>`__ will
register all material and human damage caused by armed forces during an
incident, and all costs caused by the incident will be billed to the
:term:`nation state` that started to use weapons. Also the invaded nation will
receive a refund for every day of occupation of their territory.

Invading a territory by armed forces will no longer be an option because  the
conquered areas are unusable and represent a considerable cost of ownership.

The UN won't even need to "ban" usage of weapons because nobody will buy them
any more.  Trying to settle territorial conflicts using armed force will simply
become financially unbearable. The NATO will start selling their weapons or
convert them into protective material. NVR will become a new market.

The Russian Federation will, after a period of temptation to invade even more
territories, quickly embrace the new rules of the game, prove their
trustworthiness and become a successful exporter of NVR technology.


.. rubric:: Comments

The word "weapons" needs clarification.

Even after :doc:`/blog/2022/0405_2025` and :doc:`/blog/2022/0407_2231`
I still don't understand why the NATO
can't develop a professional, systematic and efficient form of NVR that can be
considered "military means". We can read `on their website
<https://www.nato.int/cps/en/natohq/topics_68144.htm>`__ that "NATO's essential
and enduring purpose is to safeguard the freedom and security of all its members
by political and military means."  Who says that "military means" requires usage
of weapons?  `Merrian-Webster
<https://www.merriam-webster.com/dictionary/military>`__ explains "military" as
being related to "armed forces", and "armed" indeed can mean "furnished with
weapons", but it can also mean "furnished with something that provides security,
strength, or efficacy". The `Treaty itself
<https://www.nato.int/cps/en/natohq/official_texts_17120.htm>`__  says that they
assist each other "as it deems necessary, including the use of armed force, to
restore and maintain the security of the North Atlantic area." It says
"including", not "only" or "mainly". There is nothing in the Treaty that says
that the NATO must use weapons.

In case you feel that the topic is too serious, watch Monty Python's version of
history in `The killer joke <https://m.youtube.com/watch?v=YeMnPyusuBE>`__.


TODO:

- 2022-08-08 `The Hill: Pink Floyd's Waters backs Russia, calls Biden a 'war
  criminal' over Ukraine.
  <https://thehill.com/blogs/in-the-know/3592647-pink-floyds-waters-backs-russia-calls-biden-a-war-criminal-over-ukraine/>`__


- `Article 12 of the Constitution of Costa Rica
  <https://en.wikipedia.org/wiki/Article_12_of_the_Constitution_of_Costa_Rica>`__
  "abolishes Costa Rica's army as a permanent institution, making Costa Rica one
  of the first countries in the world to do so".


.. rubric:: Change history

- :doc:`/blog/2022/0404_0518`
- :doc:`/blog/2022/0407_2231`
