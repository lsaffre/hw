======================
Second letter from God
======================

My dear children,

please remember to leave to me what is mine.

I gave you this planet as a place to live on. It is my gift to you. You get it
for *free*. But it's meant to *all* living beings. Please share it equally. I
don't like to see that some of you appropriate some part of it for themselves.

The natural resources of your planet are meant to be shared with all. I don't
like when some of you believe to own some part of it.  For example when some
petrol source happens to become available on a given point of the planet's
surface. Some of you work hard to discover such points. Others work hard to get
the petrol out of the ground. Some of you live next to this place and may need
to change their habits or even move away. Even more people work hard to
distribute that petrol to different places of the planet.  Of course all these
people deserve a remuneration for the work they do.  Of course those who use
this petrol to make their lives more comfortable must pay for this service.  Of
course it makes sense to assign a monetary price to each barrel of petrol
because you need to somehow measure all this.  But please stop believing that
you own any drop of petrol.

Then your intellectual resources.  The names you give to everything. The great
ideas you have for using and optimizing everything. And your fantastic ideas in
art, entertainment and culture that contribute to increase joy of living in this
world.  All your so-called knowledge that you can now digitize and store a as
text, pictures and sound. Unlike natural resources, the intellectual resources
are unlimited. Sharing your idea with somebody else does not deprive you of your
idea. Having an idea does not mean that you own it. Your brain might be the
place where an idea occurred for the first time, but your brain is mine, and
that idea needed other people's ideas to grow. Here again: of course many people
work hard for having ideas, rendering and developing them and making them
available to others.  Of course these people deserve a remuneration for their
work.  And of course you must imagine methods for collecting the money needed to
pay them. But please stop believing that any of you can own any of these ideas.
There is no such thing as intellectual property because knowledge is not not
bound to any physical object. Review your copyright laws. You are entering the
digital era. The era of printed knowledge is becoming history. Your next step is
to realize that all human knowledge is to be considered as common good. Because
all human knowledge is mine.

Your Father in Heaven
