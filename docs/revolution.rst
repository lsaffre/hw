====================================
Jesus started a religious revolution
====================================

Jesus grew up as a `Jew <https://en.wikipedia.org/wiki/Jews>`__, but developed a
series of changes to his religion that turned out to be fundamental.  One of
these fundamental changes is the role of our :term:`Holy Scripture`.

Any belief system needs a definition. For most religions this definition is some
:term:`Holy Scripture`.  But not for Christianity.  Christianity is not defined
by a Holy Scripture. It is defined by a person, a person who didn't write any
book.

Jesus didn't say that the Jewish Holy Scriptures (most of which is known to
Christians as the Old Testament) were "wrong", but he look at them in a new
way.

  “Do not think that I have come to abolish the Law or the Prophets; I have not
  come to abolish them but to fulfill them. For truly, I say to you, until
  heaven and earth pass away, not an iota, not a dot, will pass from the Law
  until all is accomplished. Therefore whoever relaxes one of the least of these
  commandments and teaches others to do the same will be called least in the
  kingdom of heaven, but whoever does them and teaches them will be called great
  in the kingdom of heaven. For I tell you, unless your righteousness exceeds
  that of the scribes and Pharisees, you will never enter the kingdom of heaven.
  -- `Matthew 5:17-20 <https://www.bibleserver.com/ESV/Matthew5%3A17-20>`_

Jesus claims to "fulfill" the Jewish Holy Scriptures.
He says that only he (not these scriptures) is "the way, and the truth, and the life".

  "Let not your hearts be troubled. Believe in God; believe also in me.
  In my Father’s house are many rooms. If it were not so, would I have told you
  that I go to prepare a place for you? And if I go and prepare a place for
  you, I will come again and will take you to myself, that where I am you may be
  also. And you know the way to where I am going." Thomas said to him,
  "Lord, we do not know where you are going. How can we know the way?" Jesus
  said to him, "I am the way, and the truth, and the life. No one comes to the
  Father except through me. If you had known me, you would have known my
  Father also. From now on you do know him and have seen him."
  -- `John 14:1-7
  <https://www.bibleserver.com/ESV/John14%3A1-7>`__

This destroyed the authority of these scriptures. Jews called the "Word of God".
No wonder that they accused Jesus of blasphemy.

Seeing God as a father who "forgives everything" differs fundamentally from the
Jewish image of a God who will "make them pay for their sins". Jesus redeemed
humanity of this yoke, this wrong image of God.  That's why we sometimes refer
to him as the Saviour.

On the other hand, refusing a Holy Scripture for your :term:`Church` is no
solution.  Note that religions were the first cultures that defined themselves
using written descriptions. Kingdoms were defined by the person of their king,
empires by their emperor, peoples by their territory.

Every modern culture needs a written description that lasts longer and is more
reliable than human brains. For :term:`corporations <corporation>` this written
description is called a :term:`mission statement`. For religious groups it is
called the :term:`Holy Scripture`.

That's why even Christians had to formulate their Holy Scripture.  It took them
about four centuries.  But since then they all agree: the :term:`Bible` is their
:term:`Holy Scripture`.

But there is another problem.  Every Holy Scripture has been formulated at a
given time for a given audience.  As every :term:`culture` changes over time. So
there is no way around the fact that every religion needs, in addition to a Holy
Scripture, a :term:`legal person` who is responsible for interpreting their
mission statement.

We might say here that Jesus was naive when he did not consider theses issues.
But keep in mind that he lived 2000 years ago when writing technologies were
only at their beginnings, it was more than 1500 years before humankind began to
worry about copyright laws. And then don't forget that Jesus was killed at the
age of 30, before he had time to think about these issues.

And we cannot say that he didn't think at all about them.  For example he said
to one of his disciples (Simon Peter)

  Now when Jesus came into the district of Caesarea Philippi, he asked his
  disciples, "Who do people say that the Son of Man is?" And they said, "Some
  say John the Baptist, others say Elijah, and others Jeremiah or one of the
  prophets." He said to them, "But who do you say that I am?"  Simon Peter
  replied, "You are the Christ, the Son of the living God." And Jesus answered
  him, “Blessed are you, Simon Bar-Jonah! For flesh and blood has not revealed
  this to you, but my Father who is in heaven. And I tell you, you are Peter,
  and on this rock I will build my church, and the gates of hell shall not
  prevail against it. I will give you the keys of the kingdom of heaven, and
  whatever you bind on earth shall be bound in heaven, and whatever you loose on
  earth shall be loosed in heaven.” -- `Matthew 16:13-19
  <https://www.bibleserver.com/ESV/Matthew16%3A13-19>`__
