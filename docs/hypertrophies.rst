=============================
About religious hypertrophies
=============================

My cartooned definitions for some Christian schools that I consider **religious
hypertrophies**. A hypertrophy is when some part of a living organism becomes
overly big. A hypertrophy does not mean that it is "bad". For example many
cultivated fruit are hypertrophied forms of the original wild fruit. A religious
hypertrophy over-emphasizes certain aspects of a religious teaching. They are a
side effect of the fact that individual humans are fascinated by God.

- :doc:`/defs/evangelicalism`
- :doc:`/defs/creationism`
- :doc:`/defs/marianism`
- :doc:`/defs/superstition`

- :term:`elitism`, :term:`clericalism`

- :term:`comminorism`, :term:`saviorism`, :term:`biblicism`.
