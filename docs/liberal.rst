==============================
The many meanings of "liberal"
==============================

The word "liberal" has so many meanings that it has become misleading.

In `Liberal Christianity
<https://en.wikipedia.org/wiki/Talk:Liberal_Christianity>`__ (a term that is
obviously pirated by biblicists) it is being misused to mean "individualistic",
"anarchist" or the opposite of "traditional".  From the etymological standpoint
the best meaning would be a faith that has been liberated from doctrine.

Economic liberalism, the ideological belief in organizing the economy on
individualist lines, such that the greatest possible number of economic
decisions are made by private individuals and not by collective institutions.
The State keeps out of ethical decisions and sees its main job in creating the
legal infrastructure for a :term:`free market`. Which means in the end that
those who have money decide what's good and what's evil.

These side effects of the word "liberal" is what makes some people believe in
strong God-like leaders.  When "liberal" means that some :doc:`greedy_giants`
rule the world, people prefer a human leader who protects the poor against the
rich, the weak against the strong.  Even if that human leader is a dictator.
Better being exploited by a human dictator than by an anonymous inhuman system.
See :doc:`idolatry`.
