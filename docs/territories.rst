=======================
Territories and nations
=======================

The Earth is divided into :term:`territories <territory>`.  A territory is a
geographical area on the surface of Earth *governed* by a given group of humans
during a given period.

The government of a territory is responsible for making this territory useful.
This includes maintaining peace among his citizens within the borders of its
territory and with its neighbors.

The governing group of a territory can change in time.

A **territorial conflict** is when two or more groups claim responsibility for a
given territory.


Trying to write some definitions...

.. glossary::

  territory

    A named geographical area on the surface of the planet.
    Usually assigned to a given :term:`sovereign`.

  national territory

    The :term:`territory` assigned to a :term:`nation state`.
    All national territories together cover the surface of the planet.

  nation state

    The :term:`legal person` that represents the group of humans who are
    registered as citizens of a :term:`national territory`.  The nation state
    acts as :term:`sovereign` over the territory, its culture and its citizens.

  sovereign

    The natural or legal person acting as representative and highest responsible
    for a :term:`territory`, a :term:`real estate` or a :term:`shared idea`.

  owner

    The natural or legal person acting as responsible of a :term:`real object`.
    Decides who may use the object and how to use it.

  real object

    A tangible object, e.g. a car, a painting, a knife, a ship, a shell on a
    beach, ...

  real estate

    A building, e.g. a monument, house, street, dam, solar plant, ...

  government

    A :term:`corporation` responsible for making binding decisions
    within a given geopolitical system (such as a state).
