==============
Life is a gift
==============

A key question in life is whether you want to perceive it as a gift or
not.

**Perceiving life as a gift** means that you feel like a child who gets from their
parents everything they need for daily life. Young children usually take this
gift as something natural, without gratefulness. When they get older, they
gradually realize how lucky they are or have been. They become grateful and
motivated to give something back in turn.  Of course this mechanism is not
fool-proof and can easily get disturbed.  But it is an essential component of
human life on earth.

The benefits and challenges of perceiving life as a gift can be studied
:term:`scientifically <science>` for real children and their parents.

Scientific observations in the real world invite us to imagine that something
similar might be valid at a higher **metaphysical level** as well: humanity as a
"child" of a "father" in "heaven". This idea is a basic teaching of Christian
faith. Seeing Life on Earth as a gift is one of the essential messages of the
:term:`Good News`.

Your interactions with your fellow humans fundamentally depend on
whether you believe that life on Earth is a gift for which you want to
give something in return.

*Fundamentally* implies that the result is not always very visible. The key
question is whether you *want* to perceive it or not.  Your motivations, goals
and intentions are more important than how well you actually achieve them.  Success is
important as well, but at a secondary level.

External links
==============

- `gratuitousness <https://en.wiktionary.org/wiki/gratuitousness>`__

- "The earthly city is promoted not merely by relationships of rights and duties,
  but to an even greater and more fundamental extent by relationships of
  `gratuitousness <https://en.wiktionary.org/wiki/gratuitousness>`__ , mercy and
  communion." -- :term:`Caritas in Veritate`

- "The great challenge (...) is to demonstrate (...) also that in commercial
  relationships the principle of gratuitousness and the logic of gift as an
  expression of fraternity can and must find their place within normal economic
  activity." -- :term:`Caritas in Veritate`

- `La gratuité
  <https://www.lesedc.org/publication/cahier-edc-gratuite/>`__ in a collection "Les
  cahiers des EDC" (Entrepreneurs et dirigeants Chrétiens)
  provides inspiration for implementing a culture of gratuitousness in a profit-oriented
  enterprise.

- "Par tradition, la «culture de la gratuité» est associée à l’envers du marché,
  à un mode alternatif de penser les échanges, à des démarches d’émancipation
  sociale, au don. Mais elle subit aujourd’hui de puissants effets de
  brouillage. Le développement d’Internet entremêle inextricablement vraies et
  fausses gratuités." -- Jean Louis Sagot-Duvauroux in `De la gratuité
  <http://www.lyber-eclat.net/lyber/sagot1/gratuite.html>`__
