=================
How to support me
=================

Besides sending an occasional :doc:`feedback <feedback>`, you might want to help
me more systematically.

First of all I ask you to :term:`pray <to pray>` for me and for this project.

As a second step, you might formulate a :doc:`benediction <blessme>`.

As a third step, you may contribute **more systematically** and suggest concrete
changes. Changes include modifications in existing pages, writing new pages, and
reviewing the general structure and presentation of the website. The biggest
challenge will probably be that you need to be patient with me because I am
:term:`resistant to advice`. It is not enough to tell me "I don't understand
anything of what you write", you need to tell me where you stopped to understand
some thought flow. It is not enough to tell me "I don't agree with this text",
you also need to explain me *why* you disagree, and suggest an alternative
formulation, which  might be acceptable for both of us.

..
  As long as the project has no income, you need to work for free, as I do. On the
  other hand, the assistants will probably be the first salaried workers in case .

If you have no time, but do have money, then you may contribute financially to
this project :-) I am currently doing this as a volunteer job in my spare time,
but I am also a husband, family father, entrepreneur and have miscellaneous
social engagements. If you feel that you might help here, then let us talk about
ways to contribute financially.
