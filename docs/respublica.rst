================================
Res publica -- the common wealth
================================


.. glossary::

  res publica

    The "public thing", i.e. everything that needs to be managed, maintained and
    regulated by "everybody".

  common property

    Everything that is owned by "everybody". The material part of the :term:`res
    publica`.

  public transport

    The infrastructure that gives individual humans the freedom of moving
    physically around.

    See :doc:`/ptsf`.
