======================
Fourth letter from God
======================

My dear children,

yes, you love the idea that humanity will rule the world some day. Some of you
still believe in this illusion. Let me remind you that it's *me* who rules the
world because *I* am the owner. *You* don't own anything of it, your job is to
manage some part of it, the part that I assigned to you. A good manager does not
try to replace the owner.

Some of you still believe that science and technology will make them rule the
world. Though that era is mostly over. Most scientists know that science doesn't
explain me and that it does not replace religion. Some of you still believe that
getting famous, wealthy or powerful will make you happy. Though that era is
mostly over. Most humans know that famousness, wealth and power don't add any
meaning to their life.

Your big challenge for the next generations is to find a universal consensus
regarding your faith culture.  Some of you still believe that Holy Scriptures
are a tool for making their religion rule the world. The most powerful religions
are the most dangerous ones in this point. Here are my recommendations for those
of you who love their Holy Scripture.

Your Holy Scriptures are beautiful and valuable images of me. Please use them.
But please stop using them in the wrong way. Please put them back to where they
belong. Please repeat after me:

  | Don't say that your Holy Scripture is the only valid image of me.
  | There are so many other useful and beautiful images of me.
  | Don't say that your Holy Scripture is my word.
  | I have much more to tell you.
  | Don't say that your Holy Scripture is a cookbook.
  | Holy Scriptures rather remind you that I am the cook.
  | Don't say that your Holy Scripture gives clear answers.
  | It rather gives you useful questions.
  | Don't say that your Holy Scripture is fool-proof.
  | History gives enough examples of fools who used their Holy Scripture in a wrong way.
  | Don't say that you are my soldiers.
  | I don't need no army because I have no enemies.

You cannot go on as you did during the recent millenia. We cannot continue and
let your religions trigger wars when they meet each other. In the digital era
you cannot live in peace without recognizing cultures that grew up in another
region of the planet. I cannot let the most aggressive culture kill everything
else.  A world-wide faith culture cannot be based on a single religion.

My dear children, I know that you will manage with that challenge. You will find
a universal consensus regarding your faith culture. I am looking forward to the
see how much time it will take you!

Your father in Heaven.


(August 2020.)
