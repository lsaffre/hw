==================
About corporations
==================

Corporations are one of the prerequisites for human civilization.
They are the medium for ideas

.. glossary::

  corporation

    An incorporated entity --usually an organization, a group of people or a
    company-- authorized by a nation state to act as a single entity (a legal
    entity recognized by private and public law "born out of statute"; a legal
    person in legal context) and recognized as such in law for certain purposes.
    (via `Wikipedia <https://en.wikipedia.org/wiki/Corporation>`__)


Private versus public corporations
==================================

There are two big types of corporations.

.. glossary::

  private corporation

    A :term:`corporation` whose mission is to protect and grow the capital of
    its owners and to increase their revenue.

  public corporation

    A :term:`corporation` whose mission is described and recognized as being of
    public interest.

    See `Public corporations`_ below.

.. What about private LP whose owners are a mix of private and national persons?

The difference between private and public corporations is their reason of being.
The ultimate mission of a private corporation is to make financial profit to be
shared among their owners, the shareholders. The ultimate mission of a
:term:`public corporation` is described and recognized as being of public
interest, and any profit that may result from their activity is being reinvested
into the corporation's mission.

As an example, let's look at the mission statements of three big corporations:

- "Our mission is to organize the world’s information and make it
  universally accessible and useful." `Google
  <https://www.google.com/about/>`__

- "Microsoft is a technology company whose mission is to empower every
  person and every organization on the planet to achieve more. We strive
  to create local opportunity, growth, and impact in every country
  around the world." (via `geekwire.com
  <https://www.geekwire.com/2017/microsofts-new-corporate-vision-artificial-intelligence-mobile/>`__)

- "The mission of the Wikimedia Foundation is to empower and engage
  people around the world to collect and develop educational content
  under a free license or in the public domain, and to disseminate it
  effectively and globally." (`wikimediafoundation.org
  <https://wikimediafoundation.org/about/mission/>`__)

These three mission statements sound similar in nature.  They all declare to
fight for a noble cause.  What they don't say --because it is obvious-- is their
*ultimate* mission, their fundamental motivation and reason of being. Google and
Microsoft are :term:`private corporations <private corporation>`, which means
that their ultimate mission is to make financial profit to be shared among their
owners, the shareholders. The Wikimedia Foundation, by contrast, is a
:term:`public corporation`. Any financial profit that may result from their
activity is being reinvested into the corporation's mission.


Corporations aren't humans
==========================

All corporations are of same nature:  they are intangible, immaterial.  They
exist only in our minds, they are legal fictions, ideas, created by a group of
humans by giving them a :doc:`name <names>` and allowing them to exist as
:term:`legal persons <legal person>`.

.. glossary::

  legal person

    An officially recognized :term:`corporation` that has certain privileges and
    obligations similar to those of natural persons. For example they can enter
    into contracts, they can sue others, or can be sued by others.

  juridical person

    Synonym for :term:`legal person`. Wikipedia articles are still a bit messy
    and redundant: `Legal person <https://en.wikipedia.org/wiki/Legal_person>`__
    `Juridical person <https://en.wikipedia.org/wiki/Juridical_person>`__
    `Natural person <https://en.wikipedia.org/wiki/Natural_person>`__
    `Corporation <https://en.wikipedia.org/wiki/Corporation>`__


The word :term:`legal person` reminds us that every corporation has goals,
intentions and motivations. As such they are similar to real humans (sometimes
called "natural person").

Very small corporations, e.g. a choir or a local scout group may not have been
registered as a :term:`legal person`, and their mission statement might be less
formal. They might eventually regulate their existence and become a legal
person.

See also :doc:`mixup`.


Public corporations
===================

A :term:`public corporation` is a :term:`corporation` whose mission is described
and recognized as being of public interest.

For example a :term:`national government`, a public school or hospital, a
local scout group, or a :term:`supernational body` or a :term:`foundation`.

The mission of a public corporation is not necessary "universal". Not everybody
needs to agree that their mission is "good".  It's enough that their mission is
fully known to the public and that it is *not* to increase the wealth of its
owners.  A public corporation does not have any owners.


.. glossary::

  national government

    A :term:`public corporation` responsible for the survival and welfare of a
    given nation.

  supernational body

    A :term:`public corporation` responsible for collaboration between a group
    of nations.

    .. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Supranational_European_Bodies-en.svg/640px-Supranational_European_Bodies-en.svg.png

    Euler diagram of supranational European Bodies. (`Wikipedia <https://en.wikipedia.org/wiki/File:Supranational_European_Bodies-en.svg>`__)

  foundation

    A :term:`public corporation` that was created using the capital of a
    founder, who formulated the mission statement and did the administrative
    work of getting it legally registered.
