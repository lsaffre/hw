=============
About science
=============

In our daily life we are faced with observations for which we have no valid
explanation. There are questions for which we don't :term:`know <to know>` any
valid answer.

For some of these questions we *know that we cannot know* the answer. "Does it
all make sense?" is one of them. See :doc:`meaning`.

Some of these questions have been unanswered at some time but got an answer as
:term:`science` evolved. For example, 500 years ago we had no valid answer to
the question whether the Earth is flat or spheric, but today we can validly say
that we :term:`know <to know>` the answer to this question.

And then a lot of questions are kind of work in progress: What are the causes of
cancer or asthma?  Is climate change so urgent that it requires actions that
might put at risk economic stability?  How can we satisfy the needs for water,
food, shelter, energy, health, education of a growing human population?  How can
we manage to live on this planet in sustainable peace?

These questions are the field of :term:`science`.

.. glossary::

  science

    The art of managing human knowledge.

  scientific method

    A set of methodologies used to answer questions formulated as theories.

  scientific evidence

    Evidence that confirms (or counters) a theory in accordance with
    :term:`scientific method`.

  scientific consensus

    The fact that no member of the :term:`scientific community` doubts about a
    theory.

  scientific community

    All those who are acknowledged as "serious scientists".

    But how to become a serious scientist? Reputation.

    ..
      A world-wide network of scientists
      interacting in diverse ways in accordance
      with :term:`scientific method`.

  pure science

    Science that is funded by a :term:`public corporation`, whose expectations
    are free from private interest of a small group.

  biased science

    Science that is funded by a :term:`private corporation`, whose expectations
    are to optimize profit of a small group.


Intermezzo: watch :term:`PumbaTimon`.

Science doesn't claim to be objective
=====================================

Evangelical author Gene Veith purports (in `Canceling Science
<https://www.patheos.com/blogs/geneveith/2020/07/canceling-science/>`__) that
science is "the realm that purports to be totally objective".  I don't agree
with this claim. Real scientists don't claim to be totally objective. Rather the
opposite.  A real scientist says that nothing is totally objective. This is also
what Jesus told the religious leaders of his lifetime: even the Holy Scriptures
aren't the whole truth, aren't totally objective.

Does science push back God to his ramparts?
===========================================

Some people say that :term:`science` "pushes back God to his ramparts",
constantly gaining ground as our technologies evolve.

This statement is based on a correct observation: certain questions have moved
during human history from the invisible to the visible world. Two thousand years
ago humanity did not know about bacteria, viruses, evolution, atoms and such
things. We did not know whether Earth is a sphere or not.  In two thousand years
humanity will probably know much more than today, and humans will be surprised
when learning from historic sources that we didn't know these things.

What I dislike with this statement is that it implies that the Universe would
consist of two "territories", a visible "human world" and an invisible
:term:`Kingdom of God`. Christians don't believe this. Christians believe that
the :term:`Kingdom of God` penetrates the :term:`visible world`.  They are not
separate territories.

.. _science_confirms_gospel:

Science confirms the Gospel
===========================

`Judgment and Storytelling: The Deadly Nature of Self-Esteem
<https://www.psychologytoday.com/us/blog/anxiety-another-name-pain/202112/judgment-and-storytelling-the-deadly-nature-self-esteem>`__
says: *Much of our "identity" or self-esteem are "stories" we create to make
sense of the world. Our stories involve judgment and labeling, which are
cognitive distortions. They aren't real, although they seem so. Once we become
aware of the nature of these stories, it's easier to let go and move on.* -- I
call these "stories" our :term:`faith`. And he describes exactly what the
:term:`Gospel` does: it gives us an external base ("source") for our self-esteem
so that we can let our "stories" go and move on with our lives.

.. refstothis:: science_confirms_gospel


Science is an expensive tool
============================

A "systemic" problem with science is that you need money to do it. Which means
that only the rich can order a study. Which means that the rich dictate the
questions to be asked by science.


Related blog posts
==================

:doc:`/blog/2021/0904/`

Conclusion
==========

Yes, humans want to know the whole truth. But let's acknowledge that we will
never reach that goal. Let's stop claiming that some Holy Scripture or some
scientific report is infallible or perfect. If we want to get useful results
despite our lack of absolute truth,  we need to combine :term:`science` and
:term:`religion`. The former focuses on the visible world, the latter on the
invisible world.  And we should consider every decision from both sides. Science
and religion complement each other.  When one of them refuses to acknowledge the
other, then something is wrong.


Read more
=========

- People have turned to religious practice to help them deal with issues of life
  and death, loss and meaning for thousands of years. In his new book `How God
  Works <https://davedesteno.com/books/how-god-works>`__, psychologist David
  DeSteno uses the latest scientific evidence to examine how rituals help shape
  behaviors such as compassion, trust and resilience and why many of them are so
  beneficial. -- `washingtonpost.com
  <https://www.washingtonpost.com/religion/2021/10/05/science-benefits-religion/>`__
  (2021-10-05)
