=================
About faith camps
=================

The :term:`faith` of a human is deeply rooted in their personality. This is why
faith differences can cause serious conflicts and hefty battles.

Because faith is not something you can clearly define in human language, the
opposing camps recognize themselves by their opposing answers to a given
:term:`controversial question`.

Such battles are not limited to religious groups. These opponents exist
throughout all faith :term:`faith cultures <faith culture>`, including
non-religious ones.

Here are some :term:`dichotomies <dichotomy>` that I have observed so far. See
also :doc:`/christianity` or :doc:`/blog/2020/0810`.

embracing -- excluding
======================

.. glossary::

  embracing faith

    A faith that tends to reach all humans.
    Focuses on the things that unite us rather than those that separate us.

    "Anyone who seeks truth seeks God, whether or not he realizes it." -- `Edith
    Stein (1891-1942) <https://en.wikipedia.org/wiki/Edith_Stein>`__

    Requirements for embracing faith are :term:`open-minded faith` and
    :term:`reconciling faith`.

  excluding faith

    A faith that tends to exclude others from my own group. Cares about group
    identity. Differentiates between "them" and "us", between those who share
    "our" faith and those who don't.

  sectarian faith

    A faith that cuts "us" from "them".

conservative -- progressive
===========================

.. glossary::

  conservative faith

    A faith that wants to keep things as they have always been.

    A conservative Christian says that the teachings of the :term:`Church` are
    eternal and immutable.

  progressive faith

    A faith that wants to progress rather than staying in the place we are now.

    "The :term:`Bible` is a historic source that describes the :term:`Good
    News`; its interpretation  and the teachings of the :term:`Church` must
    change and evolve."

authoritarian -- liberal
========================

.. glossary::

  authoritarian faith

    Faith based on some ultimate authority, e.g. a :term:`Holy Scripture` or an
    institution.

  liberal faith

    Faith based on the idea that God talks directly to every human and that
    :term:`religion` is just our answer to God's word.

    "We must obey God rather than men" (Acts 5:29)

    The temptations of a :term:`liberal faith` are :term:`individualism`
    and :term:`relativism`.


protective -- open-minded
=========================

.. glossary::

  protective faith

    A faith that makes you feel responsible for defending your group against its
    enemies.

    "Put on the whole armor of God, that you may be able to stand against the
    schemes of the devil." (Ephesians 6:11)

    "Let us neither be dogs that do not bark nor silent onlookers nor paid
    servants that run away before the wolf. Instead, let us be careful shepherds
    watching over Christ's flock." -- `Saint Boniface (675--754)
    <https://en.wikipedia.org/wiki/Saint_Boniface>`__

    "We are God's soldiers."

  open-minded faith

    A faith that makes you feel secure without need of protection.
    A requirement for :term:`embracing faith`.

    "We are God's witnesses."

I classify myself as :doc:`/talks/openminded`.


militant -- reconciling
=======================

.. glossary::

  militant faith

    A faith that loves to fight against its enemies.

  reconciling faith

    A faith that avoids fighting and tries to reconcile with others.
    A requirement for :term:`embracing faith`.

    "But I tell you, love your enemies and pray for those who persecute you,
    that you may be sons of your Father in heaven. He causes His sun to rise on
    the evil and the good, and sends rain on the righteous and the unrighteous."
    (Matthew 5:44-45)



scriptural -- non-scriptural
============================

.. glossary::

  scriptural faith

    Faith based on a :term:`Holy Scripture` as ultimate authority.

    "The :term:`Bible` is an inerrant and infallible authority."

  non-scriptural faith

    Faith based on the idea that God talks directly to every human and that
    :term:`religion` is just our answer to God's word.


reasonable -- charismatic
=========================

.. glossary::

  reasonable faith

    A faith that tends to trust in human reason. Refuses :term:`faith statements
    <faith statement>` that conflict with established scientific knowledge.

    "God does not act against natural laws. A miracle is something that
    surprises us because we don't know everything about nature."

    "Science is a valuable tool and God wants us to use it."

    "Refusing to accept :term:`scientific evidence` as a more trustworthy tool
    than a :term:`Holy Scripture` is a form of :term:`superstition` and leads to
    unrealistic results."

  charismatic faith

    A faith that tends to distrust human reason. Focuses on intuition.

    "Science is a dangerous temptation."


..

  I am still trying to find names for these two camps. At the moment my favourite
  names are "inclusive" versus "separatist".

  .. glossary::

  .. list-table:: The two Christian camps
    :header-rows: 1

    * - embracing
      - fighting

    * - reconciliation
      - salvation

    * - inclusive
      - separatist

    * - tolerant, progressive, ...
      - evangelical, traditional, ...

    * - God loves every human
      - Only those who believe in Jesus will be saved

    * - progressive liberal theology
      - traditional strict theology

    * - reasonable approach
      - charismatic approach


  .. Note that both inclusive and separatist Christians care about traditions.
     Members of both camps can be more or less traditional.  There are inclusive
     Christians who feel very bound to traditions, and there are separatist Christians
     who don't care very much
