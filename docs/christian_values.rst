======================
About Christian values
======================

It's not possible to say for sure what the :term:`Gospel` says and what not
(more about this in :doc:`/talks/gospel60`), but still it seems that some very
weird misunderstandings are circulating.

"The average man on the street believes that Christianity is a religion that
imposes a particular morality with specific ethical behavior. He has concluded
that "a Christian is one who lives by certain rules and regulations imposed upon
him by divine or ecclesiastically dictated 'thou shalts' and 'thou shalt nots,'
and that behavioral conformity to these moral codes of conduct is what the
Christian strives to perform in order to please and/or appease God." The tragic
part of this misconception is that Christian religion has "faked" the world into
believing that such is the essence of Christianity." --
James A. Fowler, 1998, `Christianity is NOT Morality
<http://www.christinyou.net/pages/Xnotmor.html>`__

The :term:`Gospel` does **not** say anything of the following:

- priests are more honorable than other people

- God wrote the Gospel into a book called the Bible

- we should trust our leaders rather than our hearts

- marriage is reserved to couples of opposite sex

Here is a list of things I do **not** consider "Christian values":

- :term:`Democracy <democracy>` is not a Christian value. The Gospel is not a
  political message. Jesus has no problem with big contrast between rich and
  poor (Luke 17:5-10), neither with the dictator-like Roman emperor (Mt
  22:15-22) nor with the fact that Romans occupied the Jewish territory and
  forced Jews to join their army (Mt 5:41).

- Some people believe that a **traditional family** with a father, a mother and
  children is a Christian value. The family is a basic element of most human
  civilizations and should be protected by our laws. But it should be protected
  *for sociological reasons*, not for religious reasons.  It is *not* a value we
  can call "Christian". Jesus himself was born into an untraditional family. See
  :doc:`more <family>`.

- Some people believe that **obedience** is a Christian value.  I don't think
  so.  Obedience is important when we want to collaborate and achieve great
  things together, and it is a very good thing if you have good laws and a good
  leader, but this is a general wisdom, it is not particularly propagated by the
  Gospel.  The Gospel rather encourages us to evaluate our laws and leaders with
  a critical eye and to *not* obey when a law is against :term:`God's plan`. See
  also :doc:`revolution`.

- Some people believe that **abortion** is *always* a sin because the Bible says
  `You shall not kill <https://en.wikipedia.org/wiki/Thou_shalt_not_kill>`__.
  But this rule has been written in a time when humans had very little medical
  knowledge about how a child develops in a mother's womb. Our knowledge has
  evolved since then. Reality is more complex. See :doc:`/abortion`.

Maybe we need to realize that Christian values cannot be formulated as a
definitive list like those mentioned above. The Gospel does not bring a new
value system, it brings a new way of seeing value systems, how they change over
time and how to handle conflicts between cultural groups.

If there is one unique :term:`law system` that can be called *the* Christian law
system, then this law system cannot include detailed rules about :doc:`abortion
</abortion>`, :term:`family` or :term:`marriage`; it must be at "another level",
in another "world", the :term:`invisible world`.

  My kingdom is not of this world. If my kingdom were of this world, my servants
  would have been fighting, that I might not be delivered over to the Jews. But
  my kingdom is not from the world. (`John 18:36
  <https://www.bibleserver.com/ESV/John18%3A36>`_)


Here is a list of things I would call "Christian values". But they are *my*
ideals, they are what *I* read from the Bible, and I cannot expect you to have
the same list.  Your list may differ because your personal experiences differ.
On the other hand it is always great to discuss with other people about such
lists of values. You are welcome to send me your feedback.

- Don't judge yourself and others based on earthen values. The value of a human
  is much more than money, success, wealth and health.

- Be merciful. Forgive your fellows when they did something wrong.
  This will help you to not fear when *you* did something wrong.
  See :doc:`mistakes`.

- Be modest. Don't say "I deserve this".  Because *of course* you deserve it. Of
  course you deserve your fellows' attention, respect, love.  Of course you
  deserve something to eat and drink, clothes and a place where you can rest.
  *Of course* you also deserve a beautiful car, house, bicycle, phone and
  whatever you may need for your life.  But don't *worry* about these. Worry
  rather about whether your fellows have them as much as you.

- Be simple. Don't ask for more than you need. Humanity won't survive if we
  cultivate ideas like "Good enough is not enough".  The most satisfied are
  those who need the least.

- Cultivate civil courage. Don't be afraid to question the human laws and
  cultural rules you adhere to. No human law should give you permission --or
  enforce you-- to act against God's plan.

- Seek diversity. Love your enemy. Love reality, even the one that exists
  although you don't believe it. Don't trust your convictions, don't rely solely
  on your judgement.

To do: read  *Meta-Values: Universal Principles for a Sane World* by C. Franklin
Truan (2004)
