==================
About queer people
==================



.. contents::
   :depth: 1
   :local:

A hot topic
===========

Whenever a situation requires people to consent about something related to
queerness, we can observe that this is a :term:`controversial topic
<controversial question>`.  In both camps there are people who refuse to imagine
that a :doc:`dialogue </defs/dialogue>` is possible.

I prefer the word ":term:`queer`" over "LGBT".  "Queer" is a beautiful and
sympathetic name, while "LGBT" is a boring acronym that doesn't even cover
things correctly. And then I still must think twice each time I pronounce or
spell "LGBT", even after years of talking about it.

..
  This page is my personal opinion, which is obviously not in direct harmony with
  the current official teachings of the :term:`Catholic Church`. But until now I
  have not been excommunicated, and maybe `help the Church
  on her synodal journey of seeking what is good and true
  <https://sinod.katoliku.ee/en/>`__.

Vocabulary
==========

.. glossary::

  queer

    An general word for humans with a sexual :term:`extraordinarity`.

    Originally meaning "strange" or "peculiar", queer came to be used
    pejoratively against those with same-sex desires or relationships in the
    late 19th century. Beginning in the late 1980s, queer activists began to
    reclaim the word as a deliberately provocative and politically radical
    alternative to the more assimilationist branches of the LGBT community.
    Read the `Wikipedia article <https://en.wikipedia.org/wiki/Queer>`__ about
    it.

  heterosexuality

    The (normal) :term:`sexual orientation` of a human feeling sexually
    attracted by other humans of the opposite sex.

  homosexuality

    The :term:`sexual orientation` of a human feeling sexually attracted by
    other humans of same sex.

  bisexuality

    The :term:`sexual orientation` of a human feeling sexually attracted by
    other humans of both sexes.

  transgender

    The fact of having a :term:`sexual identity` that differs from your
    :term:`birth sex`.

    In other words, either a man who would prefer to be a woman, or a woman who
    would prefer to be a man.

  sexual orientation

    An enduring pattern of emotional, romantic and sexual attractions developed
    as the result of a complex interplay of genetic, hormonal, and environmental
    influences.

  sexual identity

    How one thinks of oneself in terms of to whom one feels sexually attracted.

  cisgender

    The fact of having a :term:`sexual identity` that matches your :term:`birth sex`.

  birth sex

    The sex that has been assigned to you when you were born.

  homophobia

    A range of negative attitudes and feelings like contempt, aversion, hatred
    or antipathy toward humans who are identified or perceived as being
    :term:`homosexual <homosexuality>`.

    It is often expanded to otherwise :term:`queer` people (:term:`bisexual
    <bisexuality>` and :term:`transgender`).

    Homophobia can cause stigmatization and discrimination.
    Homophobia is based on ignorance, irrational fear and obsolete religious
    beliefs.

  sexual polarization

    The natural differences between men and women.



Sexuality means polarization
============================

Humans are part of the living beings that reproduce sexually.
You need a male and a female to come together in order to get a child.
Nature has given our species the gift of enjoying sexuality in both body and
spirit.

We cannot teach children that there is no difference between men and women.
:term:`sexual polarization` is a natural law. We cannot deny :term:`sexual
polarization` as a normal part of humanity and a natural requirement for our
reproduction.

This law won't change even when role distribution of sexes becomes more
diversified with increasing complexity of a society.

This law won't change even when medical research finds surprising ways to work
around it and to discover the joy of revolting against nature.


..
  Artificial reproduction

  There are technological variants of the natural reproduction process.  Most of
  these were developed to help couples who cannot have children the natural way.
  We can, at least theoretically, imagine to mass-produce human beings for
  industrial purposes.


Homosexuality
=============

I have a separate page on :doc:`homosexuality`.


Transgender people
==================

Transgender sexuality is fundamentally different from :term:`homosexuality`. The
extraordinary thing with transgender people is the sexual *identity*, not the
*orientation*.

There are two classes of transgender people: those who long for a permanent
transition seeking medical assistance, and those who don't.

Are there known cases of people who permanently seeks transitions to the other
gender using medical assistance including hormone replacement therapy and other
sex reassignment therapies, might later in their life suffer from side effects
that might be more problematic than the original problem. That makes sense
because our biological sex has deep influences on our life, our brain, our
mental development.  This development starts already in the womb. The fruits of
this development cannot be changed at will without causing serious other
problems.

TODO: Find examples of transgender people who got happy and satisfied only after
a permanent change of identity, and who remained happy and satisfied until the
end of their days.


Condemnation of homosexuality predates Abrahamic religions
==========================================================

"In cultures influenced by Abrahamic religions, the law and the church
established sodomy as a transgression against divine law or a crime against
nature. The condemnation of anal sex between males, however, predates Christian
belief. It was frequent in ancient Greece; "unnatural" can be traced back to
Plato." (`Wikipedia <https://en.wikipedia.org/wiki/Homosexuality>`__)

Not only Abrahamic religions, but also e.g. the Sowjet regime ignored human
rights of queer people. [`2021-09 Inimesed, keda polnud
<https://www.levila.ee/raadio/inimesed-keda-polnud>`__]

Many humans are not ready to recognize same-sex marriage. Obviously because they
fear that the partners might have sex together and because they find the mere
idea disgusting.  This feeling is not caused by religion. Also many
non-religious people find it disgusting to see two men kissing each other.

Can you "heal" queerness?
=========================

Is an extraordinary sexuality something *definitive*? Is it something that you
must "learn to accept" and "manage"?  Or can you "heal" it and become "normal"?

I believe that both possibilities exist.  Our health care system should provide
support in both directions: you may be extraordinary and want to become
heterosexual, or you may want to remain extraordinary and get respected as such
by other people. In both cases you may need professional help.

There *are* people who were homosexual and then managed to reorientate.  They
married a partner of opposite sex and lived happy ever after until their end.

Even when you fail or refuse to reorientate and remain without biological
children during your life on Earth: there is more to life than having children.


My message to Christians
========================

Let us differentiate between queerness and certain negative effects that happen
when people discuss about these topics: for example pornography, arrogance,
hateful speech or denial of :term:`sexual polarization`.

When something is disgusting, does that mean it is a :term:`sin`? Does that give
me the right to pronounce a verdict?  I am not God. I read in the Gospel that
Jesus calls us to love our fellow humans. And he does *not* add that we should
exclude those who do disgusting things.   We do not need to find ways to "save"
them from their "sin". Leave that job --if needed-- to God.

It is unfortunate that some religious institutions and leaders still stigmatize
homosexuality as something bad. I think that it is time to review our religious
teachings.

My message to queer people
==========================

The belief that homosexuality --or the fact of accepting it and living
accordingly-- is a :term:`sin` has grown during thousands of years, it has deep
roots. Changing this belief needs time.  Cultures don't change quickly. But we
are on our way. The :term:`Church` has learned already. Homosexuality as such is
no longer a :term:`sin` for the :term:`Roman Catholic` church. I believe that it
won't take very long --only a few generations-- until queer people can live a
honoured and peaceful social life and that they can :term:`marry <to marry>` in
order to form an :term:`oikos` with the blessing of the :term:`Church`.

Be careful when using the word :term:`homophobia`. As a queer person you face
miscellaneous problems in daily life because you are extraordinary.  These
problems are similar to the problems of left-handed people.  Don't expect other
people to become left-handed just because it helps you to feel better.
Accept your extraordinary sexuality and be grateful for it.




TODO
====

-  Brian Holdsworth: `Sexuality is a Choice <https://youtube.com/watch?v=wR4V11MPhlQ>`__
-  `Christen und Juden in Ungarn: Ehe ist Verbindung von Mann und Frau <https://www.vaticannews.va/de/welt/news/2021-12/christen-juden-ungarn-ehe-mann-und-frau.html>`__
- `Australian MP in emotional plea over religion bill <https://www.bbc.com/news/world-australia-60152011>`__
- `"Pealtnägija": mittebinaarsete inimeste välimääraja
  <https://www.err.ee/1608502097/pealtnagija-mittebinaarsete-inimeste-valimaaraja>`__
- `Wie gut finden Sie Zwangssterilisation? <https://www.spiegel.de/netzwelt/geschlechterdebatte-fuer-manche-menschen-geht-es-um-leben-und-tod-a-4d0b4ab9-92a0-43e1-a753-da69e994b52b>`__
- "The estimated frequency of genital ambiguity is reported to be in the range of 1:2000-1:4500"
  (`PMID 29503125 <https://pubmed.ncbi.nlm.nih.gov/29503125/>`__ via `Wikipedia <https://en.wikipedia.org/wiki/Intersex>`__
