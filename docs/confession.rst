=================================
About confession
=================================

A **confession** (as part of the Sacrament of Penance) is a spiritual exercise
practiced in the roman-catholic church. It is both important and dangerous.

Something I want to say about confession : The priest's role is not to judge,
but to give absolution.  As the priest during confession you don't need to help
the confessor understand what's wrong in their life.  They *know* that because
God tells them.   If they are in doubt and ask for your advice, you can can
answer their questions and tell them your opinions. But if the confessor does
not regret some concrete situation, then it's not your job to make them change
their mind. You don't need to make them regret something they fail to regret.


Web links

- https://en.wikipedia.org/wiki/Sacrament_of_Penance
