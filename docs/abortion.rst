==============
About abortion
==============

.. contents::
   :depth: 1
   :local:


When does a cell become human?
==============================

Is a human egg cell that has just been invaded by a spermatozoid already a human
being?

There are people who feel that the correct answer is "yes".  If you answer yes,
then abortion is --of course-- a form of murder because the right to life of the
unborn child is more important than their parent's right to avoid problems or
troubles. Abortion is usually done because the child disturbs the mother's life.
A **murder** is when you kill a human being because it disturbs your own life.

And if you feel that the correct answer is "no", then --of course-- you wonder
how  some stubborn moralizers attack your liberty of doing what you want with
your body.

What makes you assume that a fertilized egg cell *actually is* already a human
being? Why not wait until week 6 when its heart starts to beat? Or  until week
21 when there is no known case of a premature born baby that actually survived?
Or until the first breath after birth, as some :term:`biblicist <biblicism>`
religions say?
And why should we stop at birth?
But what makes you say that a one-year-old baby *is* already a human being?
After all it can't even survive on its own.
And why not wait until they are 18 years old and legally adult?
And do you then agree that people with some disorder are human beings at all?

When does a human become human? There is no scientifically clear answer to the
question because "humanity" is not a measurable value.

I'd even bet that scientists will *never* be able to answer this question
because it is not their business, it is out of the scope of scientific research.

A controversial topic
=====================

So one thing seems clear: abortion will always remain a :doc:`controversial
topic </talks/controversial>`.

And repeating ever more forcefully that "children in the womb are human beings
and members of the human family entitled to the right to life by virtue of their
inherent dignity and worth"\ [#cfam]_, doesn't really bring new insight for any
decision. Neither does it help to repeat that you believe "in the sanctity and
protection of all human life" or to call others to "boldly stand up for the
protection of life, against the liberal and destructive spirit of the age,
against the attacks on life".\ [#citizengo]_

Vance Morgan\ [#VanceMorgan]_ describes the abortion topic as an example of
:term:`idolatry` that caused Donald Trump to become president of the United
States: "I am willing to turn a blind eye to and ignore any moral failings, even
the most obvious and blatant ones, as long as the person in question has the
right moral position on this one issue," the evangelical Trump supporter says.
(...) [I]n his evangelical supporter’s estimation, even murder doesn't occupy
the same moral universe as being right about abortion. One issue --being
pro-life on the abortion issue-- has been raised in status to being the only
thing that matters. The abortion issue has become the only moral issue. And
that, my friends, is idolatry."

Civil laws
==========

The fundamental question is unanswered, but reality needs decisions. We need
civil laws about questions related to abortion because things happen.

Because the fundamental question has two possible controversial answers,
abortion laws vary considerably between jurisdictions, ranging from outright
prohibition to public funding of abortion.\ [#continuum]_

These differences are not because pro-choice supporters support murder, it is
because prenatal development is a continuum, with no clear defining feature
distinguishing an embryo from a fetus.

Late-term abortion is usually prohibited even in countries where
early-term abortion is considered a simple routine remedy for "accidents".
So where is the limit between early-term and late-term?
When does a human start to be a human?
At the beginning of pregnancy (the first day of the last menstrual period, when the mother's body starts to release an egg)?
At the moment of fertilization (when the father's spermatozoid enters into the mother's ovum)?
Or in week 4, when the the blastocyst implants in the wall of the uterus?
Or in week 6, when we can detect its first heartbeat?
Or in week 11, when doctors change its name from embryo to fetus?
Or in week 14, when the sex of the fetus can be identified?
[#heartbeat]_ [#embryo]_

James Elgin Gill (born on 20 May 1987 in Ottawa, Ontario, Canada) was the
earliest premature baby in the world, until that record was broken in 2004. He
was 128 days premature (21 weeks and 5 days' gestation) and weighed 1 pound 6
ounces (624 g). He survived and became a healthy adult. (`via Wikipedia
<https://en.wikipedia.org/wiki/Preterm_birth#Notable_cases>`__)

What the Church says
====================

The :term:`Roman Catholic` church teaches clearly and without any doubt: a human
exists from the moment where the father's cell enters into the mother's cell.

- https://www.vaticannews.va/de/papst/news/2020-10/papst-polen-abtreibung-lebensschutz-generalaudienz.html

Pope Francis asks: Is it legitimate to take out a human life to solve a problem?
Is it permissible to contract a hitman to solve a problem?

- https://www.nytimes.com/2019/05/25/world/europe/pope-abortion-sick-fetus.html

These thoughts make sense, but they don't answer our fundamental question.

It's about more than abortion
=============================

Raising a child means a fundamental change in the private life of a young woman.
A human civilization should not give other people the right to decide about your
private life.

We are not only discussing about some "right to decide", it is also about "who
should pay for your mistakes"?  We love to talk about our rights, but we also
need to talk about our responsibilities.  Young people happen to enjoy moments
of pleasure without worrying about the consequences of their acts.  Then they
get pregnant, oops! and in the end *we* are the ones they ask to fix their
problem.  Young people usually don't have enough money to pay the full medical
costs of an abortion.

"I do not believe that just because you are opposed to abortion, that that makes
you pro-life. In fact, I think in many cases, your morality is deeply lacking if
all you want is a child born but not a child fed, a child educated, a child
housed. And why would I think that you don't? Because you don't want any tax
money to go there. That's not pro-life. That's pro-birth. We need a much broader
conversation on what the morality of pro-life is." (Sr. Joan Chittister, from a
2004 interview with Bill Moyers) via `patheos
<https://www.patheos.com/blogs/messyinspirations/2020/11/dont-be-confused-about-catholic-teaching/>`__

About unwanted pregnancies
==========================

Who decides whether a child is "unwanted"?
It can be a complex question.
Even if it is clear that the mother does not want her child.
Maybe somebody else wants that child.  Maybe God wants it.
Maybe the mother will change her mind later and regret her decision.
A therapist can help a pregnant woman to find an answer to that question.
But who is going to pay the therapist?
Who is responsible for teaching therapists?

Many things in life are "unwanted".
A pregnant woman who does not want her child.
A young man who doesn't want to live any longer.
A man in his best years whose wife remains disabled after an accident.
I we permit the right to abort unborn children, shouldn't we also permit the
right to suicide, euthanasia and death penalty?

My suggestion
=============

See also :doc:`/sc/procare`.

I believe that the best solution would be (1) to give every mother a realistic
way to leave her child to public care (i.e. being freed from any duty as a
mother) if she decides so,  and *then* (2) to say that abortion, even very early
abortion, should never be financed by public money unless the life of the mother
is in danger.  Abortion should be neither financed nor prohibited. Let people
decide for themselves.

Yes, this solution means that a woman who got pregnant without wanting it, would
innocently have to suffer during nine months under the natural inconveniences
related to pregnancy. An unwanted pregnancy would be for the mother like an
accident that needs nine months until you are fully recovered. Even in case of
violation.  It's of course a pity to see a woman suffer innocently, and we are
tempted to say "We have the medical skills to avoid her these nine months of
suffering, so let's use our knowledge and stop her suffering". But the right to
life of the unborn child is more important than their mother's right to avoid
suffering.

Yes, my solution means that rich people can maybe afford the luxury of abortion
where poor people won't.  Life will always be easier for rich people than for
poor people.  It's not a government's business to change this.

Unfortunately (1) is not sufficiently implemented in most countries.  Without
(1), (2) perverts into a system where males are privileged over females. Some
pro-life activists unfortunately demand to implement (2) without demanding to
implement (1). In cases of conflict, individual humans should have priority over
moral law systems.

In a talk to health care workers, Pope Francis mentions real-life cases where a
health care worker is faced to an individual decision. `Papst über Abtreibungen:
„Sich nicht zum Komplizen machen"
<https://www.vaticannews.va/de/papst/news/2021-10/papst-abtreibung-krankenhaus-pharmazeuten-audienz-lebensschutz.html>`__.

If I was being asked by a pregnant woman for help with abortion, I would have to
first try everything for helping her using (1). And if this turns out to be
impossible, I would have a case of conflict and ask advice from a priest.
Ultimately I would help her because I believe that individual humans have
priority over moral law systems.


Further reading
===============

- "Kerala teenage girl delivers baby in her bedroom with help of YouTube video,
  parents find out two days later after they hear baby’s cry. The girl did not
  receive any outside assistance during the entire childbirth procedure and she
  cut the umbilical cord herself using YouTube videos as a guide, after being
  instructed to do the same by her boyfriend." (`opindia.com 2021-10-28
  <https://www.opindia.com/2021/10/kerala-teenage-girl-umblical-chord-youtube-video-boyfriend/>`__)

- ‘Give me my baby’: an Indian woman’s fight to reclaim her son after adoption
  without consent. Anupama S Chandran’s newborn child was sent away by her
  parents, who were unhappy that his father was from the Dalit caste.
  (`theguardian.com 2021-12-09
  <https://www.theguardian.com/world/2021/dec/09/give-me-my-baby-an-indian-womans-fight-to-reclaim-her-son-after-adoption-without-consent>`__)



.. rubric:: Footnotes

.. [#cfam] Taken from a `Petition on the right to life in
  international law <https://c-fam.org/right-life-international/>`__
  by radical c-fam.org. The petition collected 95586 signatures world-wide.

.. [#citizengo] Taken from a petition
  `Poland: Keep Standing for Life!
  <https://www.citizengo.org/en-row/node/183235>`__

.. [#continuum] Originally copied from Wikipedia articles about
  `Fetus <https://en.wikipedia.org/wiki/Fetus>`__ or
  `Abortion debate <https://en.wikipedia.org/wiki/Abortion_debate>`_.

.. [#embryo] medicinenet.com, `Embryo vs. Fetus: Differences between Stages Week
  by Week
  <https://www.medicinenet.com/embryo_vs_fetus_differences_week-by-week/article.htm#what_is_an_embryo>`__,
  (undated, seen 20190912).

.. [#heartbeat] `How Early Can You Hear Baby’s Heartbeat on Ultrasound and By Ear?
  <https://www.healthline.com/health/pregnancy/when-can-you-hear-babys-heartbeat>`_

.. .. [#invitro] Yes, there are exceptions to this rule, e.g. in-vitro
              fertilization.

.. [#VanceMorgan] `Idolatry and Abortion
   <https://www.patheos.com/blogs/freelancechristianity/idolatry-and-abortion>`__
   by Vance Morgan on Patheos, 2020-02-02).
