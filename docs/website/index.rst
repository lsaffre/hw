==================
About this website
==================

This website is made using a technology based on
:term:`git`, :term:`atelier`, :term:`Python` and :term:`Sphinx`.

How to request a change
=======================

Anybody can submit change requests. Unless they are trivial or merely cosmetic,
your suggestions will be discussed by the core team.

For new content, write your text in any format and send it to us, together with
explanations where this content should come.

To request modification of existing content, just describe the changes in some
way that makes sense.  Make sure to copy the URL of the page you are talking
about. One possibility is to copy and paste the content from the website into a
LibreOffice document, enable change tracking, start editing, and then sending us
the document. Note that we do not use proprietary software like Microsoft Word.


How to edit Sphinx files directly on GitHub
===========================================

The source code of this website is hosted as a *source code repository* on
GitLab: https://gitlab.com/sinod2023/www/

We can give write permission to the code repository to certain team members so
that they can edit Sphinx files directly on GitHub.

- If you do not yet have your personal GitLab account, create one (click on the
  button "Sign in/Register" on the `GitLab website <https://gitlab.com/>`__).

- Tell the name of your GitLab account to Luc.

- Communicate with the other team members to decide which file(s) you are going
  to edit. We use to meet in our Jitsi room `jitsi.eesti.ee/sinod
  <https://jitsi.eesti.ee/sinod>`__.

- Open the GitLab web interface, either directly via `this link
  <https://gitlab.com/sinod2023/www/>`__, or when you are on the website, by
  clicking the button with the GitLab logo in the upper right corner (it's a
  fox).

- Using the GitLab web interface, navigate to the file you want to edit.  For
  each html web page there is one source file ending with :file:`.rst`.
  Relative URLs on the website correspond to the file name paths in this
  repository. Estonian files are under :file:`docs`, English files under
  :file:`endocs`, Russian files under :file:`rudocs`.

- When you are on a source file ending with :file:`.rst`, click the button
  "Edit" or "WebIDE".

- Don't hesitate to ask for help!


The technology used for this website
====================================

TODO: Some documentation exists on
https://dev.lino-framework.org/writedocs
but needs to be reviewed

.. glossary::

  git

    A source versioning system used by software developers.
    See https://en.wikipedia.org/wiki/Git

  Python

    A programming language. See https://www.python.org

  Sphinx

    A program for building static websites.

    https://www.sphinx-doc.org

  atelier

    A program I wrote for my professional work as a software developer.

    https://atelier.lino-framework.org/
