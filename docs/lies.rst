=====================
Cultivating lies
=====================

One of the challenges of the :term:`information revolution` are :term:`legally
correct lies <legally correct lie>`, :term:`disinformation` and :term:`fake
news`.

.. glossary::

  legally correct lie

    A published :term:`disinformation` that is difficult or impossible to prove
    wrong because it does not contain any flagrant lie.

    It is of course legally allowed to ignore certain aspects of reality when you
    publish something.  If it would be a crime to publish incomplete information,
    nobody would dare to publish anything.  The right to emphasize some chosen
    aspects of reality and to ignore certain others is part of our human rights.


  disinformation

    False or misleading information that is spread deliberately to deceive.

    https://en.wikipedia.org/wiki/Disinformation

  fake news

    A piece of news that looks reliable but actually is :term:`disinformation`.

    https://en.wikipedia.org/wiki/Fake_news

  information revolution

    The revolutionary economic, social and technological changes caused by
    advances in information technology.

    https://en.wikipedia.org/wiki/Information_revolution


"78% of job applicants lie and 66% of hiring managers don't care!"  (according
to `an internal research
<https://www.checkster.com/are_you_hiring_charlatans>`__ led by Checkster, a
provider of web-based talent decision tools who offers help with "weeding out
charlatans in the hiring process" and "guard against unethical behavior in your
team".

Instagram influencer Katie Sorensen caused much trouble because she proclaimed
what she :term:`knew <to know>` to be true: `The Karen Who Cried Kidnapping. How
one unsuspecting craft-loving mom got tangled in an influencer’s viral yarn.
<https://www.elle.com/culture/career-politics/a39589245/the-karen-who-cried-kidnapping/>`__

See :doc:`liar_king` for my collection of examples.


TODO:

- https://www.forbes.com/sites/forbescoachescouncil/2023/10/10/five-actions-companies-can-take-to-stop-the-spread-of-dangerous-misinformation
