==========
About love
==========

The :term:`Good News` calls me to "love God, and to love my neighbour as
myself" (`Mt 22:37-39 <https://www.bibleserver.com/ESV/Matthew22%3A37-39>`__).

- *Loving God* means that I seek him everywhere, that I talk to him
  (:term:`prayer`), that I constantly ask how he wants me to act in any given
  situation, that I love the whole world because he created it and gave it to us
  as a gift, that I care for that gift, that I share it with all other living
  beings.

- *Loving my neighbour* means that I behave towards them as I would like them to
  behave towards me, that I consider all humans as my siblings and am benevolent
  towards them, that I try to build bridges and to fill abysses between "us" and
  "them".

- *Loving myself* means that I care for my body and my soul.  It includes
  working for education, wisdom and success.  It includes caring for my
  :term:`private property` and increasing my wealth.

Note the ordering: first comes God, then your neighbour ("as important as the
first"), and last comes you (potentially just because Jesus assumed it as
obvious).

See also `What does it mean to be a Christian?
<https://blog.youversion.com/2021/06/what-does-it-mean-to-be-a-christian/>`__.

What is love?
=============

The word "love" is obviously used for a wide range of different meanings. When
you compare my statements in the introduction with what you hear in movies and
talk shows, you might even get the impression that we are using a same word for
things that are completely different.

These aren't different *meanings*, only the *contexts* are different.  Love
applies to a wide range of different contexts, but the meaning remains the same.

Anecdote: When I was a Sunday school teacher in Vigala, I once entered the local
youth centre on a weekday evening. Many children were there, most of them from
non-Christian families who laughed at Christians.  One boy, maybe 12 years old,
came to me and asked "Hey, do you love me?". I knew him, and I knew that he was
making fun, but I answered "Yes, of course". My intention had been to add more
explanations,  but before I could say anything else,  he had already run away to
tell his friends that I am gay and pedophile.

The entertainment industry tends to focus on the :term:`family` context
(marriage, partnership, flirting and other situations around the making and
breaking of couples and generations living together as families), but keep in
mind that the family is just one of several other contexts where love is
important.  I see the following contexts:

- individual (self-love)
- familiar (the couple and its children)
- non-binding groups (all kinds of volunteer activity groups)
- legally binding groups (national, municipal, ...)
- economically binding groups (neighbourhood, work team, ...)

In his `triangular theory of love
<https://en.wikipedia.org/wiki/Triangular_theory_of_love>`_, Robert Sternberg
describes (in the 1980s) three components of love: intimacy, passion, and
commitment.  Julius W. Anderson
`wrote a book about this theory
<https://onlinelibrary.wiley.com/doi/abs/10.1002/9781119085621.wbefs058>`_ in
2016, where he introduces it as follows: "Within this theory, individuals love
each other to the extent that they experience and evidence these three
components, and different combinations of the components will yield markedly
different kinds of love. The triangular theory allows for eight types of love:
non‐love, liking, infatuation, empty, romantic, companionate, fatuous, and
consummate."
It seems to me that they focus on the familiar context.


About neighbourly love
======================

.. glossary::

  neighbourly love

    Love towards your neighbour.  Where "neighbour" does not mean the guy who
    lives next door but every individual human you happen to meet in your life.

Neighbourly love means to love our fellow humans as much as ourselves, i.e. that
we assign the same priority to their needs as to our needs. It means that we see
the other as an enrichment and not as an obstacle.

Neighbourly love is more than tolerance, charity or altruism. It doesn't mean
that we continue to believe that actually "we" are right and let "them" have
"what they want" because we are tolerant, generous or selfless.

Neighbourly love doesn't mean that we are indifferent, "give in" and let the
other side "win" just because we are fed up of discussing with them.


Love, and then do as you want
=============================

https://christianhistoryinstitute.org/study/module/augustine/

Liking others makes you more likeable. Being a good friend is the best way to
make good friends. That's what I also conclude from `7 Easy Ways to Become More
Likable
<https://www.psychologytoday.com/us/blog/social-influence/202201/7-easy-ways-become-more-likable>`__.


Love in a couple
================

The :term:`couple` is just one of these contexts.

`7 Clues That You Are In A Truly Loving Relationship
<https://medium.com/hello-love/7-clues-that-you-are-in-a-truly-loving-relationship-faa1f4988964>`__

Loving your enemies
===================

"You might prefer to get rid of the parts of your soul that carry unwanted
thoughts and feelings. But is it possible that, with God's help, they might
become your greatest allies? Remember: every part of you is valuable and worthy
of your compassion, and God loves and wants to do something beneficial through
each one." -- Alison Cook in `Loving Your Inner Enemies
<https://www.alisoncookphd.com/loving-your-inner-enemies/>`__


Quotes
==========

- L'amour, ce n'est pas se regarder l'un l'autre, c'est regarder ensemble dans
  la même direction. -- Antoine de Saint-Exupéry
