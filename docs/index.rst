===========
Human World
===========

Welcome to the **Human World** website, a collection of my personal opinions
about God and the world.

Try also the automated Google translation in `Estonian
<https://hw-saffre--rumma-net.translate.goog/?_x_tr_sl=en&_x_tr_tl=et&_x_tr_hl=en-US&_x_tr_pto=wapp>`__,
`German
<https://hw-saffre--rumma-net.translate.goog/?_x_tr_sl=en&_x_tr_tl=de&_x_tr_hl=en-US&_x_tr_pto=wapp>`__
or `French
<https://hw-saffre--rumma-net.translate.goog/?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=en-US&_x_tr_pto=wapp>`__.

Quick entry points:
:doc:`save the world <plan>` | :doc:`synodality </sc/index>` | :doc:`software </fs/index>`


Latest blog entries
===================

.. blogger_latest::

See all blog entries in :doc:`/blog/2023/index`, :doc:`/blog/2022/index` ...

Sitemap
=======

.. toctree::
    :maxdepth: 1

    talks/index
    me/index
    website/index
    Blog <blog/2023/index>
    /misc

.. toctree::
    :hidden:

    copyright
    blog/index
    feedback
    support
    blessme
