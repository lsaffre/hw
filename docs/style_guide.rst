==============
My style guide
==============

"Be content to appear stupid or clueless in extraneous matters. Don't wish to
seem knowledgeable — and if some regard you as important, distrust yourself." --
`Epictetus <https://en.wikipedia.org/wiki/Epictetus>`__

Don't say "To be honest". It sounds as if usually you weren't.

Don't say "Does that make sense?".

Don't say "I think X" -- be more precise: "I believe X" or "I am afraid X" or
"I hope X"

Don't say "Needless to say" because why then would you say what follows?

Don't say "In my opinion" because everything you write on your blog is your
opinion.

Don't say "Without any doubt", "It is scientifically proven" or "The Bible
clearly says". See :doc:`/talks/convictions`.
