===============
About the bible
===============

.. glossary::

  Bible

    The :term:`Holy Scripture` of :term:`Christianity`. The historic document
    used by all Christians to cultivate their faith.

  Old Testament

    The part of the Bible that was written before :term:`Jesus Christ`.

  New Testament

    The part of the Bible that was written after :term:`Jesus Christ`.


Christians have been discussing for centuries about whether at all we need our
own canonical :term:`Holy Scripture`, and which texts should be part of it.
Until today we don't completely agree on that `biblical canon
<https://en.wikipedia.org/wiki/Biblical_canon>`__.

Jesus's teachings were originally propagated orally and have been written down
by several authors.

  Jesus never wrote down a single word, nor did his original followers. It
  wasn't until 40 years after his death that his teachings were first recorded
  on paper in the Gospel of Mark. -- Stephen Mitchell (in his book *Jesus, What
  He Really Said and Did*)

Some of these texts were selected and collected (canonized) into the
:term:`Bible`. Some other texts, although they are known, didn't make it into
the canon. Christians regard the Bible as their :term:`Holy Scripture`. The
different Christian :term:`denominations <denomination>` have slightly differing
opinions about which books are part of the Bible or not\ [#canon]_, but these
differences are minimal if you compare them to the Bible as a whole.

The Bible gives answers to existential questions about :doc:`the meaning of life
<meaning>`. It answers these questions using stories and images. For example
`Luke 10:25-37 <https://www.bibleserver.com/EU.ESV/Lukas10%2C25-37>`__. A story
is not a prescription saying "This and nothing else is the answer".

The Bible is not a law book. While it contains ancient legal texts of the Jewish
people and letters witnessing the life of early Christian communities, these
texts are to be read as background information to help us to understand the
:term:`Gospel`.

The Bible is not a cookbook but "the compost pile that provides material for new
life", as Walter Brueggemann wrote (via `Pete Enns
<https://peteenns.com/the-bible-cookbook-or-compost-pile/>`__). In case you
don't know how important a compost pile is for a garden, please read about
permaculture.

The Bible is a powerful tool, but it is not fool-proof.  You need to get
training and learn how to use it.  It is not a user manual to life.  It is not a
collection of ready-to-use solutions for your everyday life problems.

The Bible contains wisdom that has been developed and selected by many
generations of humans.

The Bible teaches us that God doesn't want us to always agree with
:term:`Scripture <Holy Scripture>`. [#korpman]_

The Bible can be perceived as the :term:`Word of God` when reading it together
with other people.

The Bible is more than a book because it is the product of many cultures and
generations of humans. That's why the Bible deserves being referred to as a
:term:`Holy Scripture`.



.. rubric:: Footnotes

.. [#canon] To get an idea of the complexity, see e.g. `Development of the
   Christian biblical canon
   <https://en.wikipedia.org/wiki/Development_of_the_Christian_biblical_canon>`__

.. [#korpman] Mathew J. Korpman in `When It's Okay to Ignore Jesus
   <https://www.patheos.com/blogs/biblicallyliterate/2020/07/when-its-okay-to-ignore-jesus/>`__ where he refers to his book
   `Saying No to God: A Radical Approach to Reading the Bible Faithfully <http://www.quoir.com/saying-no-to-god.html>`__
