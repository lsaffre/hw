========================================
Why do Christians say that Jesus is God?
========================================

Can't we simply believe in Jesus Christ as a human who was born by a divine plan
as the long awaited Messiah announced by Jewish scriptures? Why do Christians
believe that *Jesus is God*, not just a divine revelation? Why did Christians
have to define the concept of :term:`Trinity`?

.. glossary::

  Trinity

    The idea that the one God can be seen and worshipped in three persons: the
    :term:`Father`, the :term:`Son` and the :term:`Holy Spirit`.

My short answer to these questions: because the game wouldn't work without these
concepts. When you say that you believe in Jesus Christ, you cannot add "but
*not* in a Holy Spirit" or "but *not* in a Father in Heaven".

The Gospel of John is especially clear in describing Jesus as much more than a
simple human. `John 8:21-24 <https://www.bibleserver.com/ESV/John8%3A21-24>`__:
So he said to them again, “I am going away, and you will seek me, and you will
die in your sin. Where I am going, you cannot come.” So the Jews said, “Will he
kill himself, since he says, ‘Where I am going, you cannot come’?” He said to
them, “You are from below; I am from above. You are of this world; I am not of
this world. I told you that you would die in your sins, for unless you believe
that I am he you will die in your sins.”

During several centuries after the historical events around Jesus' death and
resurrection, humans who were touched by the Gospel had hard discussions about
how to translate the Gospel into human language.  The biblical canon and the
creeds and the ancient writings of early Christian scholars are a result of
these discussions. It's not me who is going to reinvent that wheel.

.. glossary::

  Father

    One of the most specifically Christian names for :term:`God`.

  Son

    Title given to :term:`Jesus Christ` when seeing him as the divine Son of
    God. :term:`God` seen as a human.

  Holy Spirit

    :term:`God` seen as a divine :term:`spirit`
    who is sent to us by :term:`Jesus Christ` and
    helps us to understand
    and spread the :term:`Gospel`.




Change history:

- :doc:`/blog/2019/1022`
- :doc:`/blog/2019/1104`
