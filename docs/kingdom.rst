========================
About the Kingdom of God
========================

What do I mean when I talk about the :term:`Kingdom of God`? Good question.


I will start with my suggestion of a definition.

.. glossary::

  Kingdom of God

    The :term:`visible <visible world>` seen as a "kingdom" where :term:`divine
    law` applies. An early term for the :term:`common heritage of mankind`.


  Kingdom of Heaven

    Seems that this is just a synonym for the :term:`Kingdom of God`.

  divine law

    An unwritten set of laws that apply "by nature" or "in reality". It is not
    written into any book.  We ignore most of it, though we sometimes forget
    this.


.. glossary::

  commons

    https://en.wikipedia.org/wiki/Commons

  res communis

    Term derived from Roman law that preceded today's concepts of the
    :term:`commons` and :term:`common heritage of mankind`. It has relevance in
    international law and common law.

    https://en.wikipedia.org/wiki/Res_communis

  common heritage of mankind

    The territorial areas and elements of humanity's common cultural and
    natural heritage.

    https://en.wikipedia.org/wiki/Common_heritage_of_humanity

International law seems to say that the :term:`common heritage of mankind`
should be held in trust for future generations and be protected from
exploitation by individual :term:`nation states <nation state>` or
:term:`corporations <corporation>`.
