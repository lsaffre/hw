=========
Copyright
=========

The content on this website is written and maintained by **Luc (Johannes)
Saffre** as the copyright holder. I am the responsible publisher. 

Everything on this website is meant as a starting point to inspire other people.
You may publish copies or derived work of any content on this website without
asking for permission, provided that you respect the terms of the `license
agreement <http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1>`__.

.. raw:: html

  <p xmlns:cc="http://creativecommons.org/ns#" ><a rel="cc:attributionURL"
  href="https://hw.saffre-rumma.net">This work</a> by <a rel="cc:attributionURL
  dct:creator" property="cc:attributionName"
  href="https://hw.saffre-rumma.net">Luc Saffre</a> is
  licensed under <a
  href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1"
  target="_blank" rel="license noopener noreferrer"
  style="display:inline-block;">CC BY 4.0<img
  style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
  src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
  style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
  src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>


If you feel that something on this website
infringes your rights or the interest of your employer or customer, please send
me at least a warning mail and make sure to get my return receipt before filing
a law suit. This is in order to save both you and me a lot of administrative
work.  Failing to try to find a solution *à l'amiable* with me might influence
the outcome of the law suit.
