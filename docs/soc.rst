==================
Season Of Creation
==================

.. contents::
   :depth: 1
   :local:


What is the Season of Creation?
===============================

The Season of Creation is a "season of increased prayer and effort on behalf of
our common home."\ [#lsm]_

Season of Creation activities are coordinated by an ecumenical steering
committee, which was formed to help Christians around the world fulfill the
purpose of the season. As the urgent need to solve the environmental crisis
continues to grow, Christian churches have been called to strengthen their
united response. The Season of Creation ecumenical steering committee came
together to provide resources to empower Christians to respond to our faith,
each in the way of his or her own denomination, during this shared season of
reflection and action.\ [#soc]_

(`source <https://seasonofcreation.org/about/>`__)

The Season of Creation is a time to renew our relationship with our Creator and
all creation through celebration, conversion, and commitment together. During
the Season of Creation, we join our sisters and brothers in the ecumenical
family in prayer and action for our common home.

Ecumenical Patriarch Dimitrios I proclaimed 1 September as a day of prayer for
creation for the Orthodox in 1989. In fact, the Orthodox church year starts on
that day with a commemoration of how God created the world.

The World Council of Churches was instrumental in making the special time a
season, extending the celebration from 1 September until 4 October.

Following the leadership of Ecumenical Patriarch Dimitrios I and the WCC,
Christians worldwide have embraced the season as part of their annual calendar.
Pope Francis made the Roman Catholic Church’s warm welcoming of the season
official in 2015.

In recent years, statements from religious leaders around the world have also
encouraged the faithful to take time to care for creation during the month-long
celebration.

The season starts 1 September, the Day of Prayer for Creation, and ends 4
October, the Feast of St. Francis of Assisi, the patron saint of ecology beloved
by many Christian denominations.

Throughout the month-long celebration, the world's 2.2 billion Christians come
together to care for our common home.


Loomise aeg Eestis
==================

Eestis on palju inimesi ja ühendusi, kes küll ei nimeta ennast kristlasteks,
kuid teevad tegelikult seda, mida Jumal tahab: nad hoolitsevad looduse ja
inimlikuse eest.

Nende inimeste ja ühenduste eest me palvetame sel kuul erinevates kohtades.

..
  vabatahtlike kes mõtlevad sel
  teemal kaasa ja näitavad eeskuju. Mõned neist küll ei nimeta ennast usklikuks,
  aga „“

..
  Ja mul on veel lambaid, kes ei ole sellest tarast, neidki pean ma  juhtima; ja
  nad kuulevad minu häält ning siis on üks kari ja  üks karjane. (`Jh 10:16
  <http://www.piibel.net/#q=jh%2010%3A16>`_).

  Mitte igaüks, kes mulle ütleb: „Issand, Issand!”, ei saa  taevariiki; saab vaid
  see, kes teeb mu Isa tahtmist, kes on taevas. (`Mt 7:21
  <http://www.piibel.net/#q=Mt%207%3A21>`__)

Eestpalved
==========

Õnnista Eestis tegutsevaid inimesi, kes hoolitsevad loodus- ja inimkaitse eest.

Õnnista poliitikuid ja ettevõtjaid, et nad leppiksid kokku õigeid reegleid, mis
võimaldavad kõikidele inimeste elada rahus Maa peal nii täna kui ka tulevikus.



Sõnavara
========

.. list-table::

  - * Season of Creation
    * Loomingu Aeg
  - * A Home for All? Renewing the Oikos of God
    * Kodu kõigile? Uuendamas Jumala *oikost*


Palve: Issand, kes lõi Maad ja kõik, mis seal elab, Sa kutsud meid hoolitseda
Sinu loodu eest. Eestis on palju vabatahtlikud, kes mõtlevad sel teemal kaasa ja
näitavad eeskuju. Mõned neist küll ei nimeta ennast usklikuks, aga „“ (Jht :21).
Palun õnnista kõiki ühendused, kes hoolitsevad loodus- ja inimestekaitse eest.
Õnnista ka meie poliitikuid ja ettevõtjaid, et nad leppiksid kokku õigeid
reegleid, mis võimaldavad kõikidele inimeste elada rahus Maa peal nii täna kui
ka tulevikus.


Renewing the Oikos of God. Amid crises that have shaken our world, the global
Christian family was awakened to the urgent need to heal our relationships with
creation and with each other during the ecumenical Season of Creation. During
the 2021 Season of Creation, from 1st September through 4th October, thousands
of Christians on six continents get together for a time of restoration and hope,
a jubilee for our Earth, and to discover radically new ways of living with
creation. More than ever before, the global Christian family will unite to pray
and take action for our common home. https://seasonofcreation.org

Resources:

- Roman-Catholic: https://laudatosimovement.org/seasonofcreation/


Laudato Si' liikumise eesmärk on inspireerida ja mobiliseerida katoliku kiriku
inimesi hoolitseda meie ühise kodu eest ja saavutada kliima- ja majanduslik
õiglust.




A prayer for our earth
======================

| All-powerful God, you are present in the whole universe
| and in the smallest of your creatures.
| You embrace with your tenderness all that exists.
| Pour out upon us the power of your love,
| that we may protect life and beauty.
| Fill us with peace, that we may live
| as brothers and sisters, harming no one.
| O God of the poor,
| help us to rescue the abandoned and forgotten of this earth,
| so precious in your eyes.
| Bring healing to our lives,
| that we may protect the world and not prey on it,
| that we may sow beauty, not pollution and destruction.
| Touch the hearts
| of those who look only for gain
| at the expense of the poor and the earth.
| Teach us to discover the worth of each thing,
| to be filled with awe and contemplation,
| to recognize that we are profoundly united
| with every creature
| as we journey towards your infinite light.
| We thank you for being with us each day.
| Encourage us, we pray, in our struggle
| for justice, love and peace.
| (LS 246)

A Christian prayer in union with creation
=========================================

| Father, we praise you with all your creatures.
| They came forth from your all-powerful hand;
| they are yours, filled with your presence and your tender love.
| Praise be to you!
|
| Son of God, Jesus,
| through you all things were made.
| You were formed in the womb of Mary our Mother,
| you became part of this earth,
| and you gazed upon this world with human eyes.
| Today you are alive in every creature
| in your risen glory.
| Praise be to you!
| Holy Spirit, by your light
| you guide this world towards the Father’s love
| and accompany creation as it groans in travail.
| You also dwell in our hearts
| and you inspire us to do what is good.
| Praise be to you!
| Triune Lord, wondrous community of infinite love,
| teach us to contemplate you
| in the beauty of the universe,
| for all things speak of you.
| Awaken our praise and thankfulness
| for every being that you have made.
| Give us the grace to feel profoundly joined
| to everything that is.
| God of love, show us our place in this world
| as channels of your love
| for all the creatures of this earth,
| for not one of them is forgotten in your sight.
| Enlighten those who possess power and money
| that they may avoid the sin of indifference,
| that they may love the common good, advance the weak,
| and care for this world in which we live.
| The poor and the earth are crying out.
| O Lord, seize us with your power and light,
| help us to protect all life,
| to prepare for a better future,
| for the coming of your Kingdom
| of justice, peace, love and beauty.
| Praise be to you!
| Amen.
| (LS 246)



.. rubric:: Allikad

.. .. [#LS] Laudato Si' : https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html

.. .. [#LW] Celebration Guide of the Lutheran World : https://www.lutheranworld.org/content/resource-season-creation-2021-home-all

.. [#soc] https://seasonofcreation.org/about/

.. [#lsm] https://laudatosimovement.org/seasonofcreation/
