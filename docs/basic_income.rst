==========================
Unconditional Basic Income
==========================

.. contents::
   :depth: 1
   :local:

Basic idea
==========

Let's introduce a world-wide :term:`Unconditional Basic Income`.

.. glossary::

  Unconditional Basic Income

    A decent but honest income, paid by a national government to each citizen.

    A periodic cash payment unconditionally delivered to all on an individual
    basis, without means-test or work requirement.

A world-wide :term:`Unconditional Basic Income` would have multiple advantages:

- reduce mass migrations
- reduce influence of lobby work of :term:`greedy giants <greedy giant>`
- reduce the administrative effort of tax collection


"People will get lazy"
=========================

One argument of opponents is that the UBI will make people lazy. They will
increasingly drink coffee and alcohol, smoke Marijuana, play computer games,
watch movies, surf around, chat with their friends, stay in bed, walk around,
waste their time...

I agree that we don't want people to become lazier.

But it won't happen. First of all there's nothing wrong with staying in bed when
you are tired or ill, there's nothing wrong with reading or watching or hearing
stories that might teach you something, there's nothing wrong with chatting with
your friends and neighbours. There's nothing wrong with doing things that don't
give you immediate benefit. As long as you love, do as you please.

Addiction
=========

Addiction is a subtopic to be discussed.  When somebody has lost control over
their life for medical reasons, it would be wrong to give them money in the hope
they use it wisely. People suffering from an addiction will get different types
of support.

Addiction means that you use or do something more often than is good for you.
Addiction is caused by frustration, depression, hopelessness, and lack of
education or appropriate information about the dangers.

I don't believe that addiction will increase when people no longer need to worry
about their basic income.

Further reading
===============

- https://basicincome.org/
- :doc:`/blog/2020/0715`
- 2020-06-03 `The Unheard Results of The Finnish Basic Income Experiment
  <https://www.ubie.org/the-unheard-results-of-the-finnish-basic-income-experiment/>`__

- 2021-05-31 `Wie sinnvoll ist ein Grundeinkommen wirklich?
  <https://www.quarks.de/gesellschaft/wie-sinnvoll-ist-ein-grundeinkommen-wirklich/amp/>`__

- `To fight child labor, eliminate poverty, give adults fair wages, pope says | National Catholic Reporter
  <https://www.ncronline.org/news/justice/fight-child-labor-eliminate-poverty-give-adults-fair-wages-pope-says>`__

- `The Evolution of Ukraine’s Income Support Program
  <https://basicincome.org/news/2022/04/the-evolution-of-ukraines-income-support-program>`__

- `Tackling Poverty: The the power of a universal basic income
  <https://actionnetwork.org/user_files/user_files/000/076/443/original/Full_Paper_Lansley_Tackling_Poverty.pdf>`__

- For those who understand German:
  Watch `Friedrich Liechtenstein für Bedingungsloses Grundeinkommen <https://m.youtube.com/watch?v=gRZtC0wU8O8>`__
