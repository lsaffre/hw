====================
About Greta Thunberg
====================

.. raw:: html

  <a title="European Parliament from EU [CC BY 2.0 (https://creativecommons.org/licenses/by/2.0)],
  via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Greta_Thunberg_au_parlement_europ%C3%A9en_(33744056508),_recadr%C3%A9.png">
  <img align="right" width="256" alt="Greta Thunberg au parlement européen (33744056508), recadré"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Greta_Thunberg_au_parlement_europ%C3%A9en_%2833744056508%29%2C_recadr%C3%A9.png/256px-Greta_Thunberg_au_parlement_europ%C3%A9en_%2833744056508%29%2C_recadr%C3%A9.png"></a>

`Greta Thunberg <https://en.wikipedia.org/wiki/Greta_Thunberg>`__ is a
fascinating girl and I fundamentally agree with her message.

I also agree with the beginning of a text that anonymously circulated on social
media platforms in 2019 (`my source
<https://caldronpool.com/alan-jones-brutally-schools-child-climate-protesters-youre-the-first-generation-to-require-air-conditioning-and-televisions-in-every-classroom/>`__):

  To all the school kids going on strike for climate change, you’re the first
  generation who’s required air conditioning in every classroom. You want a TV
  in every room and your classes are all computerised. You spend all day and
  night on electronic devices.

  More than ever you don’t walk or ride bikes to school, but you arrive in
  caravans of private cars that choke suburban roads and worsen rush-hour
  traffic. You’re the biggest consumers of manufactured goods, ever. And update
  perfectly good, expensive, luxury items to stay trendy. Your entertainment
  comes from electric devices.

  (I deliberately removed the remaining paragraphs of my source because I
  classify them as hate speech.)

Yes, Greta's words are those of a angry furious youngster, she happens to
exaggerate or to simplify the facts. But I know some adult managers who do that
when they are furious.

Maybe she got famous only because her shouting at respectable politicians is so
*entertaining*.

But I perceive her message as follows : members of the parliaments get a big
salary for doing an important job.  Their job is to represent *us*, the citizens
of their country.  But they sometimes fail to do their job. Sometimes --maybe
far too often-- they represent only a very small part of us, namely those who
are wealthy enough to pay others to fight for their interests.  This is a
flagrant offense against democracy, and as such it deserves reproach and rebuke.

Of course it is an individual judgment whether you say "far too often" or "at
least sometimes".  Do they just fail *sometimes* to do their job? Or do they
*completely* fail to do it?

My message to her: Hey, Greta, thanks for your work so far. You are not alone.
Go on and get constructive.
