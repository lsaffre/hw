.. _copyright_bug:

==================================
There are two aspects of copyright
==================================

The laws about what we commonly call "copyright" are actually about two very
different things.


- One part is the right to claim **authorship** of your spiritual
  work.  For example when you quote somebody else's work in your own
  book, then you must mention the original author, otherwise you get
  guilty of `plagiarism <https://en.wikipedia.org/wiki/Plagiarism>`_.
  I'll call this the author's right to **honour** for their work.

- The other part is the right to distribute **copies** of your work.
  For example if you write a book in the hope of becoming rich by
  selling millions of copies, then you don't want somebody else to
  harvest the money that grew from your seed.  I'll call this the
  author's right to **revenue** for their work.

Side note: Reclaiming to be honoured does not exclude humbleness. Even the most
humble author who just wants to share their thoughts without any plan to get
famous would feel dishonoured if he discovered evident plagiarism of their work.

The **honour** and the **revenue** are two important but very different reasons
why people do creative work. Both desires, the one to be honoured and the one to
get revenue for your efforts are valid motivation to do efforts. But they are
different by nature.

Having two concepts in a single word can hide their difference and therefore
have a profound impact on our thinking and discussing.

Many languages translate the word "copyright" by their equivalent of "author
rights" (German *Urheberrechte*, French *droits d'auteur*). The mere English
*word* "copyright" is misleading.  If the English language was a software
project, I would report this as a bug.

.. rubric:: Related work

`Glyn Moody <https://en.wikipedia.org/wiki/Glyn_Moody>`_ describes these two
motivations using other words. He writes: "Textual works that are chiefly
designed to convey knowledge are different in important ways from those mainly
created for entertainment—novels, plays, poems, etc. Factual productions are
generally written by academics and researchers as a way of disseminating their
discoveries or ideas. They are not, primarily, written to gain money, unlike
novels and other works of entertainment.  The creators of factual works are
typically paid through recognition by their peers and (sometimes) the wider
public, which helps to advance their careers and ultimately leads to indirect
financial rewards such as more ready access to grants and a higher
salary."[#Moody]_

Moody calls them "factual" and "entertainment" works, but I do not agree that
the desire to "convey knowledge" is the prominent motivation for writing factual
works.  I don't believe that authors of novels and poems would do their work
mainly for revenue.  The difference is not in the content of the work, the
difference is in the motivation of the author.  The desire to "convey knowledge"
(others say "share knowledge") is based on the desire for "honour".  That's why
I prefer to call them *honour-motivated* and *revenue-motivated*.


.. [#Moody] `Open access: All human knowledge is there—so why can’t
            everybody access it?
            <http://arstechnica.com/science/2016/06/what-is-open-access-free-sharing-of-all-human-knowledge/>`_
            by Glyn Moody (UK) - Jun 17, 2016 7:30pm EEST
