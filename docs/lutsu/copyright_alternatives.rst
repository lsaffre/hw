=================
How it will start
=================

..
  We probably agree that :term:`intellectual work` is valuable and should be
  honoured.  But how?

:doc:`The Lutsu manifesto </lutsu/index>` turns :term:`intellectual property`
into public good and thus removes an established method of making money, namely
the sale of usage rights such as license fees. Therefore the production costs,
including the salary of authors and researchers, must be covered by other
approaches. Where does the money come from?

There are several ways to get there. I like the following one.

First of all, **the end of a particular way of making money is nothing new**. It
happens all the time. Cigarettes, analog cameras, diesel engines, are markets
that have been lucrative in their time and have become useless more or less
quickly. Selling usage rights for intellectual property is a market that is
going to die sooner or later.

I imagine that some countries will form a :doc:`Lutsu club </lutsu>` and
**refuse to protect intellectual property from being copied**. Citizens of those
countries would have the legal right to freely copy and reuse anything that has
been published anywhere in the world, including in countries that are not member
of the Lutsu club.

These countries would operate a **public commons service** to manage the
revenues from public commons.  They will probably define a series of *public
commons taxes*. I guess that such taxes won't be based simply on usage because
we cannot say that the one who reads a book or watches a movie about solidarity
is the only one to benefit from it.  We might even say that "using" some
published content makes it more valuable and should be rewarded by a reduced
taxation. No, I guess that the public commons tax will be a more general tax,
based on income, wealth or whatever criteria is used in each country.

Content producers in the Lutsu countries would receive a part of the collected
taxes. A public commons service will distribute the income in a transparent way.
They might set up a system for measuring the "merits" of content providers. For
example they might ask the individual citizens to "vote" for their favourite
content providers. They might employ experts of different areas who would
evaluate content.

Content producers located in non-member countries might not like this. They
might try to protect their "property". They might try to prevent "their" content
from "leaking" into the free countries, for example by manipulating the
Internet, or by other means.  They might declare war against the Lutsu club
countries. But there's an easier solution: they can just open a branch office in
some Lutsu country and apply for financial contribution.  And they will receive
a transparently allocated contribution that might eventually become bigger than
their revenue from sales of copyrights in the other countries.

All basic activities of content providers would continue as now. :term:`Private
corporations <private corporation>` that currently rely on their property rights
will convert to foundations or other form of :term:`public corporation` and
start raising funds like political parties or charity and religious
organizations.  They may reduce the costs for the fundraising work by
collaborating with others.
