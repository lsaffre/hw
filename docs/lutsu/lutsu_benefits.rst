==========================
Benefits of the Lutsu idea
==========================

The Lutsu idea will increase quality.

For example Linus Thorvalds, the author of the Linux operating system, chose to
publish it as Free Software.  30 years after this fundamental an irreversible
choice he was asked  `in May 2021 by David Cassel
<https://thenewstack.io/linus-torvalds-on-why-open-source-solves-the-biggest-problems/>`__:

  To the big question -- does he have any regrets about choosing the GPLv2
  license? -- Torvalds answers "Absolutely not... I'm 100% convinced that the
  license has been a big part of the success of Linux (and Git, for that
  matter). I think everybody involved ends up being much happier when they know
  that everybody has equal rights, and nobody is special with regards to
  licensing."
