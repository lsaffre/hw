===================
The Lutsu manifesto
===================

Here is my draft of a manifesto that could start the :doc:`Lutsu project
<index>` if it would find a relevant number of supporters to sign it.  Before
even actually calling for signatures, a group of competent people should review
the text in order to make sure that it says what we want and does not say
anything we don't want.

Text of the manifesto
=====================

We, the signers of this manifesto, declare that we want a world where published
content is no longer considered a :term:`private property` for which the "owner"
has a "right" to decide how it may be used or not. Any *published content*
should always and for everyone be free to use, study, share and extend.

By **published content** we refer to publications formulated as text, picture,
sound, movie, software source code or any other media.  It covers publications
of any investment type, ranging from spontaneous postings in an Internet forum
to books, songs, movies, scientific reports or patents.

The laws currently called "copyright" mix up two aspects that we should learn to
differentiate.  The author's *authorship right* is very different from the
*ownership right*.

The **authorship right** is the right to get honoured, identified and
acknowledged as the creator of your work. This right should become more
important and get more attention.  Authors should get protected more efficiently
against malicious or distorting quotation of their publications, defamation,
plagiarism or abuse of their reputation for other purposes. We need
international standards and infrastructure for protecting content users more
efficiently against disinformation, providing more reliable information about
the publisher, managing changes, version control, feedback, contributing and
derivative work of published content.

The **ownership right** is the right to control how others may use your work.
This right is no longer needed. It should get withdrawn. Publishing content
should no longer give you any right to control how others use your work, as long
as they respect your authorship right.

It might be difficult to start a dialogue as long as the two concepts are melted
together in a same word.

We don't deny that there are good historic reasons for the idea of considering
published content as the "property" of an "owner" who automatically has "all
rights reserved". But in the digital era this idea shows :doc:`serious design
flaws <copyright_issues>`. We need to stop wasting our resources into the
maintenance of an obsolete legal infrastructure.

We are aware that the transition won't be easy. There are many challenges.
Governments must review their laws. Existing copyright holders must review their
established methods of generating income.
We believe despite these challenges that a transition is possible and
:doc:`will have enormous benefits <lutsu_benefits>`.

We refuse criminal acts against existing laws. We don't call to ignore laws, we
call our governments to change them and to start moving towards a new horizon.

These changes will cause changes in the way of how content providers are being
rewarded. Already now we know several alternative ways to reward authors and
publishers. See :doc:`copyright_alternatives`.

.. **By signing this manifesto** we declare that we do our best to realize the
  principal necessity of this fundamental change and to carefully develop
  transition plans.  Signing this manifesto does not make the signer co-responsible
  for other signers of this manifesto.

Remarks
=======

Above text is my draft of a "manifesto" text that I suggest for a world-wide
campaign.

The name is because I wrote and presented this idea for the first time in `Lutsu
talu <https://lutsu.ee/>`__, in July 2018.

A bit later I started a `petition on wemove.eu
<https://you.wemove.eu/campaigns/abolish-the-notion-of-intellectual-property>`__
about this. The petition got 6 signatures. I learned that I am probably not the
right person to turn this idea into a serious project. But the idea continues to
haunt me and now and then I review the project description, which has evolved
significantly since then.

Maybe something is wrong with my idea. If you understand what's wrong, thanks
for trying to explain it to me. Or feel free to use it and make it grow, remove
what's wrong and add what's missing... either on your own or together with me.


..
  History of this document is part of the public Git repository which currently
  hosted at https://github.com/lsaffre/lutsu/commits/master/docs/manifesto.rst You
  can sign by introducing a pull request. We are open to suggestions and
  eventually adapt the text accordingly.

..
  Signing this manifesto does not have any legal commitment other than your
  agreement to have your name and --at your choice-- additional information for
  contacting and identifying you to be published in this repository. You may
  revoke your signature at any moment, but the fact you once signed and later
  revoked your signature will be visible forever. Changes to this manifest will
  be done only if all signers agree.
