====================================
Intellectual property is an illusion
====================================

I suggest to remove the concept of :term:`intellectual property` from our
legislation.

Observations
============

- The result of intellectual work is immaterial, intangible, just an idea.

- Information and knowledge is for humans a basic need. It is like the air and
  the earth and the sun: everybody needs it, and nobody may spoil it.

- When we consider published knowledge as common wealth of humanity,
  then it is a morally questionable activity to prevent others from using it.

All these "things" can currently be protected as :term:`intellectual property`,
which is a form of :term:`private property`.

They can have an owner --a legal or natural person-- and can be sold to another
owner. The owner is free to decide how these things are to be used.  Using them
without permission is considered equivalent to theft. The rules of this game are
formulated as copyrights, patents, trademarks, ...


My suggestions
==============

Publishing an intellectual work should be an irrevocable act of sharing the
result of your work with others and giving them permission to use and re-share
your work, provided they respect your right of being identified as the author.

An idea should be considered common property as soon as it has been spoken out.
For example, imagine that you have an idea about how to solve a common problem.
You explain it to your neighbour.  The neighbour writes an article about it and
publishes that article in a scientific newspaper.

Where is the border between a spoken sentence and a recorded video clip?  A
single spoken sentence can be worth more than a movie that took millions of
euros to produce.

A classical answer to this question is that resources as such have no value, the
revenue belongs to those who make a resource usable.  We call this activity
exploitation.

One problem with this classical view is that making knowledge usable is never
the work of a single entity. A software product is the result of the
collaboration of many humans, including developer and the end user.  When an end
user asks a question to a support provider, they actually increase the value of
the software product because they provide the valuable information that some
part of the product can be optimized. So who is the "customer" and how is the
"provider"?

Another problem with this classical answer is that exploiting a resource
potentially bears enormous risks, which are often unpredictable.

TODO
====

2024-05-24 : `Vatikan: Gegen unbegrenzten Schutz geistigen Eigentums
<https://www.vaticannews.va/de/vatikan/news/2024-05/vatikan-schutz-geistiges-eigentum-ballestrero-genf-konvention.html>`__

2024-05-24 :
`WIPO Member States Adopt Historic New Treaty on Intellectual Property, Genetic Resources and Associated Traditional Knowledge
<https://www.wipo.int/pressroom/en/articles/2024/article_0007.html>`__

2024-07-10 :
`Holy See welcomes ‘significant’ new treaty on intellectual property
<https://www.vaticannews.va/en/vatican-city/news/2024-07/holy-see-intellectual-property-wipo-balestrero.html>`__
