=======================
About intellectual work
=======================

.. glossary::

  intellectual work

    When somebody invests time and energy in order to develop and then publish
    an idea.

    This definition includes all application domains which can be artistic,
    scientific, educational, entertaining or commercial.  It also includes all
    types of carrier medium: most commonly used media are text, images, sound,
    video and source code.

    Intellectual work is :term:`creative work` which does not result into a
    material object. It is just an idea or a collection thereof.

  creative work

    When somebody creates something new.

    The result of creative work can be *material* and produce a unique object.
    This object is owned by its creator who can sell it or use it otherwise to
    earn money.  And occasionally this can produce huge financial benefit to the
    creator.


The costs of developing and publishing an idea vary largely with the number of
people who collaborated in the work, the time and money they invested, their
skills, etc.  A blog post of a guy describing his feelings after a concert is
obviously less worth than the report of a five year study about a new drug
against cancer.
