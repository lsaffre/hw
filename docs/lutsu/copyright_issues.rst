=============================================
Issues with the current copyright system
=============================================

The copyright system that is currently applied world-wide has some serious
issues.

- It is rather a lottery than a system for making a living: you may get famous
  and rich if you are lucky, but the majority of authors never see a just reward
  for their efforts.

- The legal infrastructure to maintain this system is expensive and causes
  increasing costs for our governments. Protecting copyright owners against
  theft and regulating the use and sharing of digitally published content has
  become a Sisyphean task, causing absurd laws and requiring hordes of human
  resources in public administrations.

- It has become counter-productive for creativity. It causes increased
  administrative work for publishers and distributors, leading to dependencies
  that hinder creative work and directly influence production workflows.

- It leads to conflicts of interest between individual humans and corporations
  where the former usually are defeated by the latter because of their different
  nature.

Some examples where copyright is being misused by :doc:`private corporations
</greedy_giants>`, are documented on `Wikipedia
<https://en.wikipedia.org/wiki/Copyright_misuse>`__.

The `TRIPS Agreement <https://en.wikipedia.org/wiki/TRIPS_Agreement>`__, obliges
WTO members to comply with certain standards to protect :term:`intellectual
property` and continues to live despite serious `criticism
<https://en.wikipedia.org/wiki/TRIPS_Agreement#Criticism>`__.

`The Case Against Patents
<https://www.jstor.org/stable/41825459?seq=1#page_scan_tab_contents>`__ by
Michele Boldrin and David K. Levine, The Journal of Economic Perspectives, Vol.
27, No. 1, pp. 3-22, (Winter 2013), commented in
`Comments by Karsten Gerloff
<http://blogs.fsfe.org/gerloff/2012/09/06/notes-from-boldrinlevine-2012-the-case-against-patents/>`__
and
`A question of utility
<https://www.economist.com/international/2015/08/08/a-question-of-utility>`__ in
The Economist (Aug 8th 2015).

"Wir sehen jeden Tag, dass Menschen in ressourcenschwachen Ländern an Covid-19
sterben. Es fehlt an vielem: an klinischem Sauerstoff, Schutzkleidung für das
Gesundheitspersonal, Medikamenten, Tests und schützenden Impfstoffen. Die
Knappheit dieser Ressourcen wird unter anderem auch durch die Monopolisierung
und eine künstliche Einschränkung ihrer Verfügbarkeit durch Patente und andere
geistige Eigentumsrechte befördert."  `Deutschland muss jetzt einer
Patentlockerung für Vakzine zustimmen
<https://www.tagesspiegel.de/politik/omikron-welle-als-wendepunkt-deutschland-muss-jetzt-einer-patentlockerung-fuer-vakzine-zustimmen/27899054.html>`__

`Open access: All human knowledge is there—so why can't everybody access it?
<http://arstechnica.com/science/2016/06/what-is-open-access-free-sharing-of-all-human-knowledge/>`__
by Glyn Moody, June 2016.

Wikipedia has two articles that overlap partly:
`Opposition to copyright
<https://en.wikipedia.org/wiki/Opposition_to_copyright>`__ and
`Copyright abolition
<https://en.wikipedia.org/wiki/Copyright_abolition>`__

The topic appears occasionally in controversial Internet discussions that are
useless because they don't differentiate between recognition and ownership:

- `Does copyright law limit creativity?
  <https://www.quora.com/Does-copyright-law-limit-creativity>`__

- `Do copyright and intellectual property laws harm creativity and innovation?
  <https://www.quora.com/Do-copyright-and-intellectual-property-laws-harm-creativity-and-innovation>`__

- `Does copyright help or hinder innovation?
  <https://www.quora.com/Does-copyright-help-or-hinder-innovation>`__


Tiit Aleksejev, chairman of `Estonian Writer's Union
<http://www.ekl.ee/?lang=en>`__, suggested (`Sirp, September 2022
<https://sirp.ee/s1-artiklid/c7-kirjandus/laenutustasust/>`__) that increasing
the rental fee for book in Estonian national libraries from 10 to 30 eurocent
would be helpful. I believe that it wouldn't. Everybody should pay for the work
of writers, not only those who actually read books. The usefulness of a book is
quasi unrelated to its popularity. The value of the work of writing books does
not depend on how many people actually read the result. There are books that
require years of research but are being read only be some experts or will become
popular only much later, and there are relatively "useless" but entertaining
books that were written by their author as a weekend activity.  A good book can
increase quality of life in my country because it inspired a few people.

The heirs of Astrid Lindgren had a law suit against those of Wolfgang Franke who
wrote the German text of the theme song that became famous with the `Pippi
Longstocking 1969 TV series
<https://en.wikipedia.org/wiki/Pippi_Longstocking_(1969_TV_series)>`__. (Source
in German: `Astrid-Lindgren-Erben gewinnen Urheberrechts-Streit
<https://www.stuttgarter-nachrichten.de/inhalt.hey-pippi-langstrumpf-astrid-lindgren-erben-gewinnen-urheberrechts-streit.1067fbf1-8441-47aa-ac4b-abc10777d810.html>`__).
The whole story would be irrelevant if copyright was just about honour and not
about revenue.

`Vantablack <https://en.wikipedia.org/wiki/Vantablack>`__ is a patented coating
used to colorize surfaces. Its patent owner,
the British company Surrey NanoSystems, granted artist
`Anish Kapoor <https://en.wikipedia.org/wiki/Anish_Kapoor>`__
exclusive rights to use it in artistic applications, causing other
artists to get angry about this monopoly over a substance.
(Sources:
`nature.com 2016-04-26 <https://www.nature.com/articles/nmat4633>`__,
`artforum.com 2016-02-28 <https://www.artforum.com/news/artists-angered-as-anish-kapoor-receives-exclusive-rights-to-vantablack-58410>`__,
`dezeen.com 2017-08-16 <https://www.dezeen.com/2017/08/16/nanolab-releases-singularity-black-paint-rival-anish-kapoor-vantablack-design-news/>`__)
