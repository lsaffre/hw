================
Common knowledge
================


**Common knowledge** is knowledge that has been acquired and
formulated by more than one author. Such knowledge should *by default*
not be permitted to become private property.

An author who quotes other authors should get only the *honor* part of their
work, not the *revenue* part (compare :doc:`/lutsu/copyright_bug`). The
*revenue* part, the right to share this knowledge, should be a human right,
freely granted to everybody.

For example when an end user of a software asks a question about how to use this
software, then she actively contributes to the knowledge pool around that
software.

Note the important relation between common knowledge and private data. Public
data should be usable, private data should be protected [#CCC]_. Disclosing
confidential data to the public is a criminal act which cannot be undone.


.. [#CCC] "Öffentliche Daten nützen, private Daten schützen" is a
          slogan used by the CCC (`Hackeretik
          <https://www.ccc.de/de/hackerethik>`__)


`"Weapons of Math Destruction" by Cathy O'Neil
<http://pyfound.blogspot.com.ee/2017/01/weapons-of-math-destruction-by-cathy.html>`__

`EU citizens might get a ‘right to explanation’ about the decisions
algorithms make
<http://fusion.net/story/321178/european-union-right-to-algorithmic-explanation/>`__
