==========================================
A just salary for authors and distributors
==========================================

- The current system of copyright laws was born with the `Licensing of the Press
  Act <https://en.wikipedia.org/wiki/Licensing_of_the_Press_Act_1662>`__ in 1662
  and the `Statute of Anne <https://en.wikipedia.org/wiki/Statute_of_Anne>`__ in
  1710 (see `History of copyright
  <https://en.wikipedia.org/wiki/History_of_copyright>`__).

- This system has some problems. :doc:`copyright_issues`

- The antagonists of the free and open culture\ [#most]_ actually "hijack"
  copyright laws for publishing free and open content using `copyleft
  <https://en.wikipedia.org/wiki/Copyleft>`__.

- The current copyright system relies on the axiom that published content
  becomes the :term:`private property` of its author and may be used only with
  their permission.

- The :doc:`Lutsu manifesto <manifesto>` suggests a fundamental review of
  this axiom.


.. toctree::
    :maxdepth: 1

    manifesto
    current
    copyright_bug
    copyright_issues
    copyright_alternatives
    noip
    work
    lutsu_benefits
    ck
    ethical

.. rubric:: Footnotes

.. [#most] For example by `Creative Commons <https://creativecommons.org/>`__,
  the `Wikipedia Foundation <https://wikimediafoundation.org/>`__ or
  the `Open Knowledge Foundation <https://okfn.org/>`__.
  The work of these movements is important because it shows the feasibility of alternative systems.
