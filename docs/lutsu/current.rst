============================
The current copyright system
============================

The current international copyright system is based on the idea that
:term:`published content` is automatically considered as :term:`private
property`.

.. glossary::

  published content

    Any result of an intellectual work that is intentionally published as text,
    picture, sound, movie, software source code or any other form of
    information storage.

The term :term:`intellectual property` appeared first in the 19th century and
became common in the majority of the world only in the late 20th century.

.. glossary::

  intellectual property

    Intangible creations of the human intellect that are legally recognized as a
    private property. (inspired by `Wikipedia
    <https://en.wikipedia.org/wiki/Intellectual_property>`__)

The term encompasses:

- **artistic works** like literature, pictures, music and movies and
  more subtle things like designs, slogans, symbols.

- **scientific works** like discoveries, inventions, software, methodologies,
  industrial standards.

- **identities**, i.e. :term:`legal persons <legal person>` as such with their
  name and their reputation.

- **trademarks**, i.e.

Intellectual property: the genial idea to turn :term:`published content` into a
tradable resource. The most profitable resource in the world because it is
unlimited (unlike material resources). Intellectual property is the aorta of
every :term:`greedy giant` in the digital era.

But intellectual property gets more and more complex:

- `Who owns Einstein? The battle for the world’s most famous face
  <https://www.theguardian.com/media/2022/may/17/who-owns-einstein-the-battle-for-the-worlds-most-famous-face>`__

- Some corporations have managed to trademark individual colors (`Can a
  corporation "own" a color?
  <https://thehustle.co/can-a-corporation-trademark-a-color/>`__)

Regulating intellectual property requires a huge arsenal of laws and
institutions.

- The *European Union Intellectual Property Office* (`euipo.europa.eu
  <https://euipo.europa.eu>`__).

- `International Trade Administration <https://www.trade.gov/protect-intellectual-property>`__


TODO:

- https://orrenprunckun.com/2018/08/20/regulating-intellectual-property/
