===================
Forms of government
===================


.. glossary::

  democracy

    A form of government in which the citizens of a territory have the authority
    to elect their governing legislation.

  meritocracy

    A political system in which economic goods and/or political power are vested
    in individual people based on talent, effort, and achievement, rather than
    wealth or social class. (Wikipedia
    <https://en.wikipedia.org/wiki/Meritocracy>`__)

  political party

    A :term:`public corporation` that unites humans of similar :term:`faith` in
    order to develop real-world solutions for reaching our common goals.
