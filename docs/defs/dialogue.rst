===================
The art of dialogue
===================

.. glossary::

  dialogue

    A written or spoken conversational exchange between two or more partners

Dialogue is "the embrace of different points of view — literally the art of
thinking together" -- (:term:`William Isaacs` via `spectrum.mit.edu
<https://spectrum.mit.edu/winter-2001/the-art-of-dialogue/>`__)

"Problems between managers and employees, citizens and elected officials, and
nation and nation often stem from an inability to conduct a successful
dialogue." -- (:term:`William Isaacs` via `spectrum.mit.edu
<https://spectrum.mit.edu/winter-2001/the-art-of-dialogue/>`__)

Dialogue **requires a vocabulary**, a common set of concepts and definitions.
This is why **polarization** (the process of identifying and defining the
opposing camps of a :term:`controversial question`) is not a sign of division
but a necessary step towards dialogue.

Dialogue is not always useful. "It is less helpful (or necessary) for solving
routine, or familiar or well-understood problems." (:term:`William Isaacs` in
`Accessing Genuine Dialogue <http://www.dialogos.com/wp-content/uploads/2020/10/dialogos-watercooler.pdf>`__,
The Watercooler, July/August 2012).

TODO:

- https://en.wikipedia.org/wiki/Nonviolent_Communication

- https://www.inc.com/jeff-haden/why-emotionally-intelligent-people-embrace-power-of-partial-favor-backed-by-science.html

- `Words and Phrases to Avoid in a Difficult Conversation
  <https://hbr.org/2021/06/words-and-phrases-to-avoid-in-a-difficult-conversation>`__
  (Harvard Business Review, James R. Detert, June 21, 2021) names six useful formulations to avoid:
  Don't assume your viewpoint is obvious;
  Don't exaggerate;
  Don't tell others what they *should* do;
  Don't blame others for your feelings;
  Don't challenge someone's character or integrity;
  Don't say "It's not personal".

- "Wenn man nicht will, dass es knallt, aber auch nicht stehen lassen will, was
  der mühsame Onkel so sagt, dann kann man ihm viele kritische Fragen stellen.
  Das wirkt oft entschärfend." -- "Aber dann habe ich ihm noch nicht die Meinung
  gegeigt." -- "Aber warum muss der denn wissen, wie Sie die Dinge sehen? Er
  wird die Dinge deswegen nicht so sehen wie Sie. Viel interessanter ist doch,
  ob man ihn durch geschicktes Fragen dazu bringt, selbst etwas zu sehen. Oder
  etwas zu begründen, das er sonst nie begründen muss, sodass er dadurch
  vielleicht den festen Stand verliert." (republik.ch, `Jemanden überzeugen? Das
  kann man sich meistens sparen
  <https://www.republik.ch/2023/12/23/jemanden-ueberzeugen-das-kann-man-sich-meistens-sparen>`__)
