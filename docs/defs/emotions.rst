========
Emotions
========

.. glossary::

  emotion

    A psychological state brought on by neurophysiological changes, variously
    associated with :term:`thoughts <thought>`, :term:`feelings <feeling>`,
    :term:`behavioural responses <behavioural response>`, and a degree of
    :term:`pleasure` or displeasure. (`Wikipedia
    <https://en.wikipedia.org/wiki/Emotion>`__)

    Examples of emotions:
    anger, disgust, fear, happiness, sadness, surprise,
    amusement, awe, contentment, desire, embarrassment, pain, relief, sympathy,
    boredom, confusion, interest, pride, shame ,
    contempt, relief, triumph.

    Emotions are expressed in face, voice or body. They are non-verbal. They can
    propagate from one human to another without need of giving them a name.

    The Component Process Model (CPM) of emotion defines five crucial elements
    of emotion: :term:`cognitive appraisal`, bodily symptoms, action tendencies,
    expression and feelings.

  behavioural response

    Something you do or say.

  thought

    Something you think.

  feeling

    Something you feel.

    The subjective experience of emotional state once it has occurred.

    A subjective representation of a :term:`emotions <emotion>`, private to the
    individual experiencing them.

  pleasure

    The experience of feeling well, being satisfied, enjoy something.

  cognitive appraisal

    The subjective interpretation made by an individual to stimuli in the
    environment.


.. glossary::

  neuroticism

    A :term:`temperament` characterized by a tendency to experience frequent and
    intense negative affect (e.g., anxiety, sadness, rage). A shared
    vulnerability for various :term:`emotional disorders <emotional disorder>`,
    such as anxiety, depression, and trauma-related disorders. (Inspired by
    :term:`Barlow202108`)

    https://www.psychologytoday.com/us/basics/neuroticism

  emotional disorder

    General term for miscellaneous disorders such as anxiety, depression, and
    trauma-related disorders.

  temperament

    In psychology, temperament broadly refers to consistent individual
    differences in behaviour that are biologically based and are relatively
    independent of learning, system of values and attitudes.

    https://en.wikipedia.org/wiki/Temperament

  personality trait

    Trait-based personality theories, such as those defined by Raymond Cattell,
    define personality as traits that predict an individual's behavior. On the
    other hand, more behaviorally-based approaches define personality through
    learning and habits. Nevertheless, most theories view personality as
    relatively stable.  (https://en.wikipedia.org/wiki/Personality)

    https://www.psychologytoday.com/us/basics/personality

Attitudes
=========


.. glossary::

  pride

    The fact of being proud.
    See :ref:`pride`.

  arrogance

    A personality quality of extreme or excessive :term:`pride` or exaggerated
    :term:`self-confidence`.

  self-confidence

    Confidence in yourself.

  gratefulness

    todo

  attitude

    todo

  honour

    todo

  fear

    An intensely unpleasant emotion in response to perceiving or recognizing a
    danger or threat. -- `Wikipedia <https://en.wikipedia.org/wiki/Fear>`__

  shame

    todo

  humility

    The quality of being humble.
    An accurate appraisal of one's characteristics combined with a low
    self-focus.
    A recognition and subsequent submission of oneself in relation to God.

    Inspired by :doc:`/blog/2022/0225_1440`, `Wikipedia <https://en.wikipedia.org/wiki/Humility>`__

    Being aware of your intellectual limitations and the fallibility of your
    beliefs [Sgam20220713]_

Psychologists have recently linked intellectual humility to a host of benefits:
showing more persistence in the face of failure, holding less polarized beliefs
and attitudes, and being received as warm and friendly by others. [Sgam20220713]_
