====
Hope
====

.. glossary::

  hope

    Something you hope for. Also the attitude of hoping for something.
