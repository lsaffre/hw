==========
Detachment
==========

The virtue or skill of :term:`detachment` means to be able to detach (take a
distance) from your own opinions and its differences with those of other people.

.. glossary::

  detachment

    The indifference to worldly concerns and the freedom from bias or prejudice
    (inspired from `Merriam-Webster <https://www.merriam-webster.com/dictionary/detachment>`__)

.. glossary::

  holy indifference

    A total openness to :term:`God's plan` in one's life.  In other words,
    whatever God wills for me, I will strive with all of the energy of my will
    and the proposition in my intellect to conform my will to God's will. It
    does *not* mean :term:`apathy` -- `Ignatius of Loyola
    <https://en.wikipedia.org/wiki/Ignatius_of_Loyola>`__ (via `fatherbroom.com
    <http://fatherbroom.com/blog/2013/03/ignatian-holy-indifference/>`__)

  apathy

    An resigned attitude that says "who cares", "I don't give a darn",
    "whatever".


One of the important functions of celebrating is to offer a spiritual place
where "the rule" says to everybody: "shut up now!" If we fail to obey this rule,
we spoil the celebration. One benefit of art of celebrating is to cultivate our
:term:`detachment` skills. detachment.
