==========
Bitterness
==========

.. glossary::

  bitterness

    A state of mind where you realize your inability to change something you
    don't like, and you fail to accept with humbleness that God's plan might be
    bigger than your :term:`convictions <conviction>`.


"It's terrible to arrive at old age with a bitter heart, with a disappointed
heart, with a heart that is critical of new things"
-- Pope Francis (via `la-croix.com <https://international.la-croix.com/news/letter-from-rome/bitterness-in-the-face-of-adversity-even-in-the-church/15171>`__)
