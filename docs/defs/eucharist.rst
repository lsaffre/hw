=========
Eucharist
=========

.. glossary::

  Eucharist

    (from Greek: *εὐχαριστία*, lit. "thanksgiving"), also known as *Holy
    Communion* and the *Lord's Supper* among other names, is a central Christian
    rite that commemorates Jesus' `Last Supper
    <https://en.wikipedia.org/wiki/Last_Supper>`__.

    Roman Catholics call it "Holy Communion", Protestants call it "Lord's
    Supper" (Abendmahl, armulaud), Orthodox people call it "Divine Liturgy".
    (via `Wikipedia <https://en.wikipedia.org/wiki/Eucharist>`__, `Encyclopedia
    Britannica <https://www.britannica.com/topic/Eucharist>`__)

According to the New Testament it was instituted by :term:`Jesus Christ`. When
giving his disciples bread and wine during a `Passover
<https://en.wikipedia.org/wiki/Passover>`__ meal, he commanded them to "do this
in memory of me" while referring to the bread as "my body" and the cup of wine
as "the blood of my covenant, which is poured out for many".

It is considered a `sacrament <https://en.wikipedia.org/wiki/Sacrament>`__ in
most churches, and as an *ordinance* in others.

A membership contract renewal
=============================

One meaning of the :term:`Eucharist` was to confirm communion with the church:
receiving the consecrated host from the priest confirms that the church accepts
you as a member. Taking it and swallowing it confirms that you accept to be a
member. It is like a weekly renewal of a membership contract. And it is in
public because all other participants of the mass can see it. This dimension of
the Eucharist made sense in times where

- knowing who's a member and who's not could decide about your life
- many people were illiterate
- legal protection was given only to some people, not to everybody

Some parts of the Roman Catholic church still hold on this meaning. Which is why
they do not accept Lutherans to their Eucharist. This difference causes
counter-productive reactions. My own family members avoid coming to a Holy Mass
with me because they feel offended by not being allowed to communion.

Inspired by :doc:`/blog/2022/0703_2124`
