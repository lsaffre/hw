===========
Clericalism
===========

.. glossary::

  clericalism

    The belief that a priest or other clerical person is somehow less subject to
    doing mistakes or having weaknesses.

    A social phenomenon in which elites exercise domination over members and
    structures in religious institutions. (`Wikipedia
    <https://en.wikipedia.org/wiki/Clericalism>`__)

  laity

    https://en.wikipedia.org/wiki/Laity

  clergy

    https://en.wikipedia.org/wiki/Clergy

  elitarism

    The belief of being better than those who don't belong to your group.



Note that clericalism might be more common among laypersons than among priests.
The Church is sometimes seen as a *service provider* who provides faith training
and coaching.



TODO:

- `Papst: „Den Glauben nicht eigenen Interessen unterordnen!“
  <https://www.vaticannews.va/de/papst/news/2021-11/papst-franziskus-angelus-katechese-glaube-interessen-ehrlichkeit.html>`__
