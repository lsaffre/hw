====
Duty
====


.. glossary::

  duty

    What you are asked to do.

    We all have three types of duties: towards ourselves, towards other humans
    and towards God.


  work-to-rule

    A *modus vivendi et operandi* where you take care of not doing more than
    what you are asked to do.

  quiet quitting

    An application of :term:`work-to-rule`, in which employees work within
    defined work hours and engage solely in activities within those hours.
    (Wikipedia <https://en.wikipedia.org/wiki/Quiet_quitting>`__)

    Do what you're asked to do, and nothing more.  (Zoe Williams, `Get off the
    hamster wheel! How to quiet quit absolutely everything
    <https://www.theguardian.com/money/2022/sep/30/how-to-quiet-quit-absolutely-everything>`__,
    2022-09-30).
