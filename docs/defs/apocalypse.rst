==========
Apocalypse
==========

An apocalypse (Ancient Greek: ἀποκάλυψις apokálypsis, from of/from: ἀπό and
cover: κάλυψις, literally meaning "from cover") is a disclosure or revelation of
great knowledge. (via `Wikipedia <https://en.wikipedia.org/wiki/Apocalypse>`__)



.. glossary::

  escatology

    A part of theology concerned with the :term:`end of times` (via `Wikipedia
    <https://en.wikipedia.org/wiki/Eschatology>`__).

  end of times

    The final events of history, the ultimate destiny of humanity.
