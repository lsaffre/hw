=============
About spirits
=============

I suggest the following definitions.

Spirits have no body, they exist only in the :term:`invisible world`.
Spirits are not directly visible, but we can observe their effects.

.. glossary::

  spirit

    An idea that exists (grows, lives) in more than one human heart.

    Spirits propagate (are shared) using a :term:`medium`.

  medium

    Something capable of storing and representing (symbolizing) a
    :term:`spirit`.

    Most modern spirits use written texts as their medium. In that case we can
    also call them a :term:`shared idea`. Besides these we also have
    :term:`unwritten laws <unwritten law>`.

  shared idea

    A :term:`spirit` that is formulated as a written text.

    Every :term:`corporation` is a shared idea, formulated by a legally binding
    definition.

    Every religion is a shared idea formulated by its :term:`Holy Scripture`

  unwritten law

    A :term:`spirit` that is not formulated as a written text.

    It is obviously difficult to write about unwritten laws.

  inspiration

    A thought about a concrete situation, an answer to a question, perceived by
    a human as coming from outside of his brain.

  divine inspiration

    An :term:`inspiration` coming from :term:`God` and perceived as such by that
    human. Some texts say that the :term:`Word of God` *came to* an individual
    human.
