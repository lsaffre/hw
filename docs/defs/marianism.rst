=========
Marianism
=========

Catholics are sometimes being ridiculed by other Christians for their
`veneration of Mary
<https://en.wikipedia.org/wiki/Veneration_of_Mary_in_the_Catholic_Church>`__. I
can understand those who laugh at us.

Jesus' mother, Mary, plays an important role in the Gospel. She agreed to get
pregnant without being married, and this in a time where this was almost sure to
mean her death. She raised up that boy who later became the saviour of the
world.  We know the important role of a mother for the social and personal
development of a child. I think it's okay that we talk to Mary when praying.  Or
would you refuse to talk to the mother of your best school friend?

I can confirm from own experience that the Rosary is a powerful prayer. You can
pray it alone, in your family or in big groups. You can pray it when walking,
swimming, waiting for a bus.  You can pray it in your bed, in your kitchen, at
school or at church.  You can pray it when you are tired or ill and have no
energy for finding your own words.


.. glossary::

  Marianism

    Over-stressing the importance of Jesus's mother Mary as the primary
    intercessor in :term:`prayer`.

I have heard terms like "Queen of Heaven", "immaculate conception", or
"perpetual virginity" and I must say that I don't understand why they are still
being used.



My notes after reading `Medjugorje message expresses Pope Francis’s 'Marian
Classic'
<https://cruxnow.com/news-analysis/2020/08/medjugorje-message-expresses-pope-franciss-marian-classic/>`__:
Pope Francis, like is predecessor Benedict, acknowledges that "simple believers"
are often a better guide to the faith than academic theologians. Hope in the
maternal compassion of Mary has sustained individual people as well as whole
peoples through their darkest moments. Francis' skepticism arises from a more
pastoral concern not to see those simple believers hoodwinked by people who
manipulate Marian faith for their own ends. He is suspicious of some of the
claims made by fervent Marian devotees. In November 2013 he declared that Mary
"is not a postmistress, delivering messages every day," in the context of
answering a question about Medjugorje.

Note that I have never been in Lourdes or Medjugorje so far. I fear that I would
feel disgusted by hypertrophied Marian devotion. On the other hand I can imagine
that I would change my mind if I would go there for real.
