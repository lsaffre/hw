.. _pride:

===========
About pride
===========

The word :term:`pride` is used for two very different emotions and attitudes:
*arrogant* pride and *grateful* pride.  Pride is always a form of joy, but the
question is: what makes you rejoice?

The :term:`attitude` behind a proud behaviour can be either :term:`arrogance` or
:term:`gratefulness`.

For example, when you managed to do something that you didn't believe to be able
to do, then you are relieved and *gratefully* proud. But when you rejoice
because you managed to show to your opponent that you are better than them,
that's *arrogant* pride.

Grateful pride won't vanish when you see that others are better than you.

Arrogant pride seems to be invisible to the one who is proud.

But why then does the word "pride" exist? Why don't we just say either
"arrogance" or "gratitude"? We do use these words as well. So there must be a
reason why the word pride exists. Maybe we use it when we don't know (or don't
want to know) which kind of pride it is.

Pride (more precisely its arrogant meaning) is considered a deadly sin.
Johannes Hartl describes it well (in German):
https://youtu.be/kZIYvD3i1rc

The opposite of pride is :term:`humility`.

.. rubric:: Pages referring to this

.. refstothis::
