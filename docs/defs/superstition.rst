============
Superstition
============

It seems that a considerable number of Christians pray that God helps them
finding a parking space (see e.g. `Praying for Parking Is Evil
<https://www.beliefnet.com/faiths/2007/01/praying-for-parking-is-evil.aspx>`__).
I call this a form of :term:`superstition`.

.. glossary::

  superstition

    Believing in something that is against :term:`scientific evidence`.

I refuse to believe that God tweaks his own rules.
He has created the visible world. Why would he break the rules he designed?



:term:`Superstition <superstition>` is a natural behaviour for every human.  We
share it with most higher animals.

You know the story about those mice in a cage with an automatic feeder.  The
feeder gave food only when the mouse was *slow* enough on its way towards it.
Those mice, when they discovered that running an extra round before getting to
table made it work, started superstitious behaviour resembling liturgical acts.



Paul Oldenburg tells a story about a man in WW2 who was convicted to get shot
after having dug his own grave.
https://www.youtube.com/watch?v=zhWe69oh7ec
