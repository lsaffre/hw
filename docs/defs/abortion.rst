========
Abortion
========

.. glossary::

  abortion

    The medical act of removing an unborn child from it's mother's womb before
    birth with the side effect of killing the child.

See :doc:`/sc/procare` and :doc:`/abortion`.
