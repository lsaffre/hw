=========
Knowledge
=========


.. glossary::

  information

    An impulse being received and registered by our senses.

  learning process

    A process where you receive incoming signals, sort and filter them, combine
    them with existing knowledge to produce a new piece of information, which is
    finally stored into your memory so that it becomes :term:`knowledge`.

    In a community it is analogue: the community observes reality, "digests" it
    and "updates" (review, optimize, reformulate) or rearrange some
    :term:`knowledge element` in their :term:`teachings`.



  knowledge

    The synthesis of what we have learned.

    Used to make decisions without further asking.

"Information might lead to knowledge but is not the same as knowledge, nor does
it replace knowledge. Knowledge depends on having a context into which
information can be accurately placed; context is one of the keys.  Knowledge is
what you understand thoroughly, are familiar with from a variety of angles, can
explain, and can, if asked, teach to someone else."
-- Gina Barreca, `Psychology Today <https://www.psychologytoday.com/us/blog/snow-white-doesnt-live-here-anymore/202111/whats-the-difference-between-knowledge-and>`__


Pages referring to this
=======================

.. refstothis::

  knowledge
  information
  learning
