===================
The flicflac (mind)
===================

Did you ever experience what I call a :term:`flicflac`? Of course! But how do
*you* call it?

.. glossary::

    flicflac

      When your mind, in trying to find your personal answer to a
      :term:`controversial question`, quickly changes back and forth between two
      or more options.

I call it "flicflac" until I find a better name for it. I also thought about
calling it "mind toggle". My final choice was inspired by `this article on
Wikipedia <https://de.wikipedia.org/wiki/Flickflack>`__.

"Which car should we buy? The Citroen or the Mazda?" I remember how my wife and
I have been doing :term:`flicflac` about this question back in 2019 before
finally deciding for the Mazda. We had been looking for a new car to buy for
several weeks. Actually there were a few more options that made it into this
last phase before our decision. And I remember how, when talking to the
respective sellers, I honestly believed that their car was about to win the
race. Salesmen, especially those who sell bigger objects like cars or real
estate, know this effect well. Even your signature on the contract is only
*almost* final when legal regulations sometimes allow for a period of changing
your mind after the deal.

A flicflac can be more or less visible. Sometimes it happens only in your own
brain, and nobody will ever know that you were about to make a completely
different choice. In other situations, especially in group decisions, the word
:term:`flicflac` might be useful for describing what happened.

Sometimes the one who does a flicflac is being frowned upon or even accused to
be a traitor. But I think that flickflacs are a normal part of life. There is
nothing wrong with having them. No need to feel ashamed. And if others are
irritated, this doesn't mean that you made something wrong. It is rather these
others who are wrong in accusing you.

A flicflac indicates that the question itself is based on insufficient
understanding or uncomplete knowledge.
