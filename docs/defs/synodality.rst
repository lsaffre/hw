==========
Synodality
==========

The term :term:`synodality` has been coined by the :term:`Roman Catholic` church
under Pope Francis as her way of "living and operating" together, which
differentiates it from any other existing human institution.

I can imagine that other communities, including non-religious communities, will
declare to be "synodal" in the future, if we generalize the definition of the
word into something like this:

    Synodality it is the awareness of being *together* and *on our way* as a
    community.

    Together: We cannot refuse anybody who declares to share our goal. When our
    interpretations of that goal conflict with each other, we cannot exclude
    anybody from our community. We must find a consensus and a modus vivendi
    until

    On our way : We are not yet there: we know that we will never be perfect.
    Our teachings, traditions and rules require continuous maintenance, which
    includes research, up­dates, quality control and deployment.

    Synodality is the way of living and operating together of a :term:`synodal
    community`.

On this page I try to formulate how I understand synodality.


..
  : nobody can leave. Once you understood
  the Gospel, you cannot *not* follow Jesus. You are with us, whether you want it
  or not, whether *we* want it or not.



.. glossary::

  synodal community

    A community of individual persons (and potentially child communities) that
    lives and operates together according to the :term:`rules <synodal rule>`
    defined in the community's :term:`teachings <synodal teachings>`. It has a
    publicly declared goal and an appointed :term:`leader <synodal leader>`.
    Every human who publicly declares to adhere to this goal (and gets approved
    as such by the community) is a member. A member can leave the community by
    simply declaring so, but the community cannot kick out a member they have
    previously approved.

  synodal government

    A way of government based on :term:`synodality`.

  synodal teachings

    The set of documents published and maintained by a given :term:`synodal
    community`.

  synodal leader

    A natural person that has authority of publishing :term:`synodal teachings`.

  synodal rule

    A short statement that is considered true by all members of the
    :term:`synodal community` who emits it.

  synodal directive

    A suggested :term:`synodal rule`.



The principles of :term:`synodality` can potentially be extended to
non-religious groups as well. These groups can be of any size, ranging from
embryonal groups and families (:term:`oikoi <oikos>`) to national governments
and multinational corporations.

A :term:`synodal government` has several strategic advantages over a pure
democracy:

- it provides methods to raise the voice of the weak and the poor in order to
  protect us from a hypertrophy of the strong and rich.

- it provides methods to potentially raise the voice of a little seemingly
  foolish amateur against those who are considered venerable and trustworthy.
  This protects us from desperate reactions caused by experiences of
  helplessness.
