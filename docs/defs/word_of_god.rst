===========
Word of God
===========

The :term:`Catholic Church` uses the expression :term:`Word of God` at several
places of the :term:`Holy Mass`. The :term:`Ordo Lectionum Missae` specifies
that two of the four Bible readings are to be concluded with the words "Verbum
Dei" (translated to "(This is) the word of God" or "Acclamons la Parole de Dieu"
or "Wort des Lebendigen Gottes" or "See on Jumala Sõna"). And the other
participants of the mass are then to answer "Deo gratias" ("Thanks be to God" or
"Louange à toi Seigneur" or "Dank sei Gott dem Herrn" or "Tänu olgu Jumalale").

Don't misunderstand this formulation as a form of :term:`biblicism`. The
:term:`Word of God` is far more than what is written in the :term:`Bible`. Like
a *love story* is not the same as *love*, the *Bible* is not the same as *the
Word of God*. Don't mix up the photograph of a cat with the cat. The Word of God
cannot be formulated as a collection of texts in human language. We can hear it,
but we cannot grasp nor reproduce it.



.. glossary::

  Word of God

    (1) The assumed immutable eternal message revealed by God to all humans
    through :term:`Jesus Christ`.

    (2) In Jewish tradition, a synonym for their :term:`Holy Scripture`.

Note expressions like "the word of the Lord came to Jonah" (`Jonah 1:1
<https://www.bibleserver.com/ESV/Jonah1>`__), which rather describe a
:term:`divine inspiration`, i.e. a concrete message perceived by a particular
human as coming from God.

.. glossary::

  divine plan

    The assumed plan God has for humanity and the whole Universe.

  God's plan

    Another word for God's :term:`divine plan`

  Will of God

    Something God wants to happen as part of his :term:`divine plan`.


Related reading
===============

- `When the Bible is Not the Word of God
  <https://www.patheos.com/blogs/freelancechristianity/when-the-bible-is-not-the-word-of-god>`__
  (Vance Morgan)

- `Kraft der kleinen Worte
  <https://liturgie.ch/liturgieportal/eucharistiefeier/wortverkuendigung/114-wort-des-lebendigen-gottes>`__
  (Gunda Brüske)
