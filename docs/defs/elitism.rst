=======
Elitism
=======

.. glossary::

  elitism

    Exaggerated differentiation between a small "elite" of "experts" and a
    bigger "crowd" of "followers".

    See also :term:`clericalism`.

It is normal for every institution to make a difference between "experts"
(specialists, vocational workers) and "simple" members who don't interact with
the institution in their everyday life. But this differentiation needs to be
balanced.  Expert teams must take care of remaining accessible to every simple
member who might want to join their circle.

For Christians there is only one ultimate expert and teacher: :term:`Jesus
Christ`. We have a huge collection of valuable --and less valuable--
documentation about his teachings.   A selection of these has been canonized as
our :term:`Holy Scripture`. It is only normal that we need intermediate experts
and teachers who "digest" these sources and help us to understand them.

But no faith expert can ever claim that they understand faith perfectly.  Faith
institutions provide interpretations of the Bible for a set of concrete
situations.  And already here we see that different institutions can come to
contradicting results.  That's normal because the :term:`Good News` is beyond
human reason.

  At that time Jesus declared, “I thank you, Father, Lord of heaven and earth,
  that you have hidden these things from the wise and understanding and revealed
  them to little children; 26 yes, Father, for such was your gracious will. (`Mt
  11:25-27 <https://www.bibleserver.com/ESV/Matthew11%3A25-27>`__)

Faith needs life-long training, and we need institutions as trainers because the
object of our training is so complex.  We pay professional trainers to help us
with our psychological and health issues. Why shouldn't we pay professionals for
helping us with our faith issues?

But would you pay a psychologist for making your wife happy without you?
Would you pay a doctor for running each day a mile in the hope to improve your health?
Why would you pay a priest for praying in your place?

Elitism leads church professionals to exaggerate the importance of "proper"
church meetings. Faith training is much more than assisting such meetings.

Elitism leads simple church members to see their church institution as a service
provider who does, once per week, the training work they fail to do during the
week.
