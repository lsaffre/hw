=========
Biblicism
=========

My definition
=============

.. glossary::

  biblicism

    When you misuse the Bible for judging other people instead of judging
    yourself.

    .. A group of Christian teachings based on the assumption that the
      :term:`Bible` is an :term:`inerrant <Biblical inerrancy>`, "infallible" and
      "authoritative" expression of the divine :term:`law system`.

Some biblicists refer to themselves as **biblical literalists**. But when
:term:`biblical literalism` is used as more than a hermeneutic technique, it is
indeed just an euphemism for :term:`biblicism`.

The :term:`Catholic Church` officially renounced from biblicism during the
:term:`Second Vatican Council` in 1965.

Why biblicism exists
====================

Biblicism is an expression of our desire to "give a name" to God, to understand
Him.

Biblicism can be our answer to the healing effect of reading the :term:`Bible`.
When you discover how much these texts help you to get a healing glimpse into
the :term:`Good News`, then you are tempted to conclude that the texts
themselves are the source of your salvation.

Biblicism is partly caused by an old vocabulary problem. See :doc:`word_of_god`.

Systemic biblicism
==================

Protestant Christian denominations suffer from systemic biblicism because
Luther replaced the Pope by the Bible.

.. glossary::

  systemic biblicism

    The fact that church institutions without a world-wide apostolic leader have
    only "the Bible" as authority and are doomed to assume that the Bible is the
    :term:`Word of God`.

If I had been living 500 years ago, I would fully agree with Luther.
On the other hand I can imagine that
if Luther would see how the Catholic Church has learned since then,
he would be the first to return to the :term:`Catholic Church`.

Examples of biblicism
=====================

The screenshot on the right is an example of extreme biblicism. Note how the
Bible is listed even before God.

.. image:: bible_idolatry.png
  :width: 50%
  :align: right

A biblicist explains (`here
<https://www.gotquestions.org/Biblicist-Biblicism.html>`__) : A proper view of
Scripture is that the Bible *is* the ultimate authority. "All Scripture is
breathed out by God and profitable for teaching, for reproof, for correction,
and for training in righteousness" (`2 Timothy 3:16
<https://www.bibleserver.com/ESV/2%20Timothy3%3A16>`__).  (...) Since the Bible
is "God-breathed," it is in fact authoritative, infallible, and internally
consistent. It must be allowed to have the final word on everything.  How could
Scripture be profitable for "training in righteousness" if it cannot be trusted
as absolutely true and consistent?" My reaction: How can you believe that a text
is "God-breathed" just because it claims to be so? And when you read "All
Scripture", how can you exclude other scriptures than those that were selected
into the canon several hundred years after the text you quote was written?  And
when you read "profitable", how can you extend this into "in fact authoritative,
infallible, and internally consistent"?

Another scaring example of :term:`biblicism` is the following text, which was
not written 500 years ago, no, it was published in October 2020 when Donald
Trump was trying to become president for another time. Evangelical author
Grayson Gilbert writes:

  The Democratic Party has built its entire platform off of two major issues:
  abortion and sexual anarchy. I will state at the beginning of this that no
  Christian can support the Democratic Party. I have no qualms expressing such.
  I find it hard to believe that professing Christians could even find this
  topic worthy of debate. Per Scripture's own stance on the evil of abortion and
  sexual licentiousness, these issues really are about as black and white as
  they can get. There is simply no room within the historic Christian faith to
  disagree. What we are dealing with then is a simple rejection of these issues
  as matters of primacy. Scripture is quite clear that the shedding of innocent
  blood is abominable (Pro. 6:17). --`You Can't Be a Christian and Support the
  Democratic Party
  <https://www.patheos.com/blogs/chorusinthechaos/cant-be-a-christian-support-democratic-party/>`__

I didn't waste my time to read this text until the end (enough other people did
this because it was a few weeks before the elections).  My reaction was to just
quote what pope Francis writes about it in paragraph 86 of his `Fratelli Tutti
<http://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20201003_enciclica-fratelli-tutti.html>`_):

  I sometimes wonder why (...) it took so long for the Church unequivocally to
  condemn slavery and various forms of violence. Today, with our developed
  spirituality and theology, we have no excuses. Still, there are those who
  appear to feel encouraged or at least permitted by their faith to support
  varieties of narrow and violent nationalism, xenophobia and contempt, and even
  the mistreatment of those who are different. Faith, and the humanism it
  inspires, must maintain a critical sense in the face of these tendencies, and
  prompt an immediate response whenever they rear their head.


Seventh-Day Adventist Ulli Scherhaufer explains why his church is the closest to
the "biblical truth". He develops an impressive theory about how humanity "fell"
away from the "pure teaching" given to Moses on the Sinai, how God started to
"restore" it through Martin Luther and how this restoration evolved through
Lutherans, Baptists, Methodists and finally the Seventh-Day Adventist teachings.
He explains that Jesus himself criticizes a faith that relies on traditions, and
calls us to walk in the light and search for "the truth that is contained only
in the Bible". https://www.dasgeheimnis.de/web/religionen.htm


Biblicism is the opposite of the Gospel
=======================================

The Jewish faith culture assumes that the :term:`Holy Scripture` was the
:term:`Word of God`, revealed to Moses and the prophets, a revelation of divine
law, translated into human language and carved into stone. For Jewish faith the
divine law was carved into stone and later into scripture.

But already the Old Testament contains hints that one day God will pour out his
spirit into our hearts, change our "hearts of stone" into "hearts of flesh".

Jesus brings a revolution to this understanding of what a :term:`Holy Scripture`
means to our :term:`faith`.  He says "Loving God and your neighbour is more
important than obeying the scriptures.  "The Sabbath is there for man, not man
for the Sabbath." The scriptures are made by humans, they are *not* the Word of
God, *I* am the Word of God."

Jesus sheds a new light on the role of Scripture for our faith.

And Jesus died on the cross before a single word of his message was written
down.  It took a few generations for the :term:`Church` to understand that the
world is not going to an end immediately and that they need to somehow write
down this message.

Unlike other religions, Christianity is not based on a set of documents
written by their central single teacher.  The central authority of Christian
religion is a person, not a book.

Biblicism misses an important aspect of the Gospel.
It focuses on the theology of atonement, which is
an answer to the question "How can God be merciful and just at the same
time?"
But atonement is only a side effect of the Gospel.
The Gospel is more than atonement.

Biblicism inverses a basic point of the Gospel, which calls us to never *fully*
trust in any Holy Scriptures.



.. For the :term:`Catholic Church` the Bible is
  the historic trace of God's
  revelation in Jesus.

Reasonable people cannot accept biblicism
=========================================

History gives us many sad examples of humans who fight against and even kill
each other because of their differing interpretations of the Bible. This shows
that the Bible is only an *image* of the truth and not the truth itself. You may
read the Bible thoroughly and still miss the :term:`Good News`. You may live in
the light of the Good News without ever having read the Bible.

Biblicists can seriously hurt other people because they are convinced that they
have a holy duty


How to recognize biblicism
==========================

Biblicism refuses to recognize the plurality of the biblical canon.

Biblicism says that the Bible has been "written by God himself".

Biblicism is when you worship the Bible instead of worshipping God.

Biblicism makes you refuse to accept :term:`scientific evidence` when it
conflicts with something you read in the Bible. Refusing to see
:term:`scientific evidence` as a more trustworthy tool than a :term:`Holy
Scripture` is a form of :term:`superstition` and leads to unrealistic results.

In :doc:`/letter5` I try to imagine what God might say about biblicism.

I believe that this understanding of the Bible is a misconception. The only
authority regarding ethical questions is :term:`God`, and as Christians we must
not forget that :doc:`God's name </talks/name_of_god>` remains "holy": beyond
our imagination and unreachable to our mind. Consequently we cannot fit God into
a collection of texts. The Bible is illustrative and inspiring, but not
authoritative.


For those who disagree
======================

Jason D. Bradly  has some interesting thoughts to share for people who believe
to know the Bible quite well: `Biblical Illiteracy Is a Big Problem--for
Christians
<https://www.patheos.com/blogs/jaysondbradley/2019/02/bible-literacy>`__. Some
highlights: "Saying that reading the Bible cures biblical literacy is kind of
like saying that malnourishment is cured by eating. The truth is that a lot of
people are malnourished because of *how* they eat." "[Y]ou need to be careful
not to read Scripture in a way that merely supports or champions your position."
"If you think the Bible always agrees with you, that’s one of the biggest signs
that you’re biblically malnourished." "The problem is that the people who should
be reading [the Bible], usually don't. And when they do, they're scouring it for
confirmation and not inspiration."

If you don't agree with my views, then here are some questions I want to ask
you.

Consider that `John 1:14
<https://www.biblegateway.com/passage/?search=John+1%3A14&version=NIV>`_ says
that the Word was made *flesh*, not it was made *book*. And `2 Cor 3:6
<https://www.biblegateway.com/passage/?search=2+Cor+3%3A4-6&version=NIV>`_ says
"He has made us competent as ministers of a new covenant--not of the letter but
of the Spirit; for the letter kills, but the Spirit gives life."

.. The confusion of both terms has deep roots. The Old Testament often uses
  :term:`Word of God` when referring to the :term:`Holy Scripture` because the
  Jewish faith did not make a difference.

How do you explain that a same text can have different effects on different
readers depending on their personal history and the social and emotional
context.

Do you think that my refusing the Bible as an authority is a form of
:term:`individualism`?

How do you explain statements like "It is by Jesus' own teachings that we come
to learn how to evaluate and discern when to apply or ignore those same
teachings. (...) it's not so much about our freedom to ignore Jesus, but rather
quite the opposite: our necessity to become better disciples of Jesus, paying
closer attention to the principles that undergird his continuing spirit filled
will, rather than puppets who merely echo his historical words."[#korpman]_

`Vi La Bianca
<https://medium.com/@Vi_LaBianca/please-stop-making-these-3-arguments-to-christians-part-1-ed79c67dadb0>`__
has some interesting questions for biblical literalists.

Matthew Distefano names `7 Reasons the Bible is NOT the Inerrant Word of God
<https://www.patheos.com/blogs/allsetfree/2021/01/7-reasons-the-bible-is-not-the-inerrant-word-of-god>`__:

- The Bible Never Claims to Be
- The Bible Says Christ Is
- The Bible is Human
- The Bible Contains Errors
- There Are Different Bibles
- The Bible Has Been Edited ... A lot!
- Jesus Didn’t Have a Bible

I have no problem with designating the Bible as "fully trustworthy" and "our
highest authority for faith and life" (found `here
<https://scriptureunion.global/who-we-are/aims-belief-and-working-principles/>`__).
These statements are normal because the Bible is our :term:`Holy Scripture` and
the only written definition of our faith. The statement that the texts of the
Old and New Testament are "God-breathed, since their writers spoke from God as
they were moved by the Holy Spirit" is a bit useless because this applies also
to many other texts.

Some Eric English quotes taken from
`Biblical Inerrancy Does NOT Glorify God
<https://www.patheos.com/blogs/unenlightenment/2021/03/biblical-inerrancy-does-not-glorify-god/>`__

- "Biblical inerrancy is one of the most sensitive issues within the Christian
  culture. Those who adhere to its doctrine cannot fathom a world where the
  Bible can be from God and not inerrant at the same time. Inerrancy is the
  cornerstone doctrine for evangelicals. All theology for evangelicals is built
  upon this premise."

- "It is not the purpose of the Bible to be a metaphysical treatise on truth."

- "Most progressives care more about the
  truth of Scripture above and beyond the doctrines some traditions assert."

- "The doctrine of inerrancy takes away from the message of Scripture because it
  necessitates that some interpretations be false for the sake of preserving the
  doctrine instead of the message.""


Related terms
=============

.. glossary::

  Bible idolatry

    When you worship the :term:`Bible` instead of worshipping God.

    This was the name I gave to :term:`biblicism` before I knew that name.

  biblical literalism

    A methodology used in law, history and theology for interpreting biblical
    and philosophical texts and other content (like pictures, sculptures, sound
    or movies).

  biblical inerrancy

    The idea that the :term:`Bible` is inerrant, i.e. does not "fail".

    I don't understand why the Roman-Catholic church still uses this concept.


Pages referring to this
=======================

.. refstothis::
    biblicism
    biblical inerrancy
    biblical literalism
    Bible idolatry


.. rubric:: Footnotes

.. [#korpman] Mathew J. Korpman in `When It's Okay to Ignore Jesus
   <https://www.patheos.com/blogs/biblicallyliterate/2020/07/when-its-okay-to-ignore-jesus/>`__
