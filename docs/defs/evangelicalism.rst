==============
Evangelicalism
==============

Evangelicalism is a trans-denominational movement within Protestant Christianity
that emphasises the absolute authority of the Bible.
It started in the Anglosphere in 1738
before spreading further in the 19th, 20th and early 21st centuries.
It separates from mainline Christianity.
It has the disadvantage of being a temptation to :term:`biblicism`,
:term:`comminorism` and :term:`elitarism`.


.. glossary::

  evangelicalism

    The teaching that the Bible is God's authoritative revelation to humanity
    and gives clear instructions, that the essence of the :term:`Gospel` is the
    salvation by grace solely through faith in Jesus's :doc:`atonement
    </atonement>`, and that an experience of being "born again" is important for
    receiving salvation.

  evangelical

    Related to :term:`evangelicalism`.

  exvangelical

    A human who turned away from :term:`evangelicalism` and eventually from
    Christian faith as a whole.

  devangelical

    Another term for :term:`exvangelical`.


Eddie Chu describes himself as *a "DEvangelical" liberated from the narrow,
judgmental, and exclusivist culture*. He wrote (2019-08-28):

  I am saddened to observe that some Devanglicals also stop identifying
  themselves with Christianity. This is sad because Evangelicals are but a
  branch of Christianity and they by no means fully represent Christianity.

  I have learned (...) as an Evangelical the supremacy of Jesus Christ, the
  presence of the indwelling Holy Spirit, the love of the Father, the importance
  of prayers, the need to study the Bible, the joy of sharing my faith, the call
  to serve others as Jesus teaches, and the centrality of the Cross. Through the
  evangelistic efforts of Evangelicals, I was liberated from a totally
  self-absorbed life to a new life in Christ.

  There is, however, an Achilles' heel in the Evangelical teachings. Many
  well-meaning Evangelical preachers, Bible study leaders, and adult Sunday
  school teachers told me during my spiritually formative years that
  Evangelicals had the only "right" view of the Bible. I was taught to view
  other Christian branches such as Roman Catholics, Anglicans, the United Church
  (in Canada), and other denominations and "mainline" churches with suspicion to
  say the least and, at times, with hostility. Seventh Day Adventists? They’re
  too legalistic. Roman Catholics and Orthodox? They shouldn’t pray to Mary and
  the saints. Mormons? Forget it. Difficult to discern back then, but obvious
  now in hindsight, was how the teaching had made me feel spiritually superior
  to other Christians who were not Evangelicals. Further, I learned to regard
  all other non-Christian religions as driven by the devil to mislead people to
  Hell.


"Those of us who are not Catholics–believing that the Bible, not a human leader,
makes the most reliable authority;"
-- Gene Veith `on Patheos
<https://www.patheos.com/blogs/geneveith/2020/10/on-the-popes-endorsement-of-same-sex-civil-unions/>`__
