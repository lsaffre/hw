===================================
Comminorism
===================================

Don't use the Bible for threatening

When you work as a faith teacher, you can be tempted to get attention of your
listeners by threatening. The same can happen to engaged non-professional
followers of a religion who believe that "their" religion is the only and
universally true teaching about God.

This temptation exists not only in Christianity but in every religion.  The
Bible, like any :term:`Holy Scripture`, can be misused for threatening.  I call
this attitude :term:`comminorism`.

.. glossary::

  comminorism

    The practice of using the Bible for threatening instead of using it to
    spread the :term:`Good News`.

It is me who invented the word :term:`comminorism`. It comes from Latin
*comminor* (to menace) and is inspired by the German word "Drohbotschaft"
("threatful message"), which is used since 1995, together with its opposite
"Frohbotschaft" (German for :term:`Good News`) by
`KirchenvolksBegehren <https://de.wikipedia.org/wiki/Kirchenvolks-Begehren>`__,
an Austrian grass-root movement within the catholic church who works against
comminorism. See also a sermon by Franz Alt in Die Zeit 2019-01-02 :
`Frohbotschaft statt Drohbotschaft
<https://www.zeit.de/2019/02/christentum-jesus-christus-nazareth-aramaeisch/komplettansicht>`__.

Comminorism is opposite to spreading the :term:`Good News` because Jesus saved
the world from the idea of a God who punishes us for our sins. More about this
in :doc:`/talks/cross`.


.. Jesus refers to comminorism as the leaven (sourdough) of Pharisees (`Mt 16:6
   <https://www.bibleserver.com/ESV/Matthew16%3A6>`__). A few decades after Jesus,
   James writes "Let no one say when he is tempted, “I am being tempted by God,”
   for God cannot be tempted with evil, and he himself tempts no one." (`James 1:13
   <https://www.bibleserver.com/ESV/James1%3A13>`__)


My mother used to say "Kleine Sünden bestraft der Liebe Gott sofort" in
situations that might foster this belief.  But she said it with a twinkle in her
eyes, like a joke, as if to remind us that we should not take this too serious.

Seems that my mother is not the only one to know that sentence:
Nane Jürgensen wrote
`Gilt „Kleine Sünden bestraft der Liebe Gott sofort“ für Sie?
<https://www.keine-tricks-nur-jesus.de/2013-11/gilt-kleine-snden-bestraft-der-liebe-gott-sofort-fr-sie.html>`__.

Yet another example: :doc:`/blog/2020/1025`.

Comminorism can cause allergic reactions on people who otherwise would gladly
follow the :term:`Good News`, but who turn away in disgust. Mehta describes a
situation that happened in a comminoristic school:  `A Christian School
Apologized After a Guest Speaker Wasn't Anti-LGBTQ Enough
<https://friendlyatheist.patheos.com/2021/03/29/a-christian-school-apologized-after-a-guest-speaker-wasnt-anti-lgbtq-enough/>`__

Christianity is currently learning to get rid of comminorism. This should not
lead us to deny the scaring passages of the Bible: :doc:`/scaring`.

Comminorism is a side-effect of :term:`scriptural faith`. "One of the great
liberating moments of my life was when I was given the opportunity (...) to
read, analyze, critique, and appreciate the Bible as literature. I had spent the
first eighteen years of my life with this book looming over me, forced to read
it in its entirety every year and to memorize significant portions of it, not as
perhaps the most influential book ever assembled, but rather as the inspired and
literal :term:`Word of God`.  Studying it as literature in college turned what
had been a lifelong burden into a voyage of discovery." -- Vance Morgan, `When
Christianity becomes an Angry and Fearful Faith
<https://www.patheos.com/blogs/freelancechristianity/when-christianity-becomes-an-angry-and-fearful-faith>`__

.. salvation can happen only by formal
   submission under the authority of their choice (e.g. the Pope or the
   :term:`Bible`).  The Roman-Catholic church started only in X to review her dogm
   "Extra ecclesiam ...".

Pages referring to this
=======================

.. refstothis::
    comminorism
