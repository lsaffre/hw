===========
Creationism
===========

.. glossary::

  Creationism

    The belief that the universe and lifeforms were created as they exist today
    by divine action, and that any explanation can be true only if it is
    mentioned in the :term:`Bible`.
