===================
The Daniel syndrome
===================

The :term:`Daniel syndrome` is one of my linguistic creations.

.. glossary::

  Daniel syndrome

    A psychological condition that makes you decide or behave, in certain
    situations, in a way that others find weird or even unacceptable, because
    you are disturbed by something that doesn't disturb them. They don't
    understand why you make your life more complicated than necessary.

Examples of Daniel syndrome include the person who refuses to receive medical
help because they believe that something is wrong with our health care system.
Or me trying to avoid using content that is considered :term:`intellectual
property` by its owner.

The name is inspired from `Daniel 3:1-14
<https://www.bibleserver.com/ESV/Daniel3%3A1-14>`__. Daniel was a successful and
trusted high-ranked employee of king Nebuchadnezzar. One day his boss the king
had the idea to build a statue of himself and demanded that every citizen should
follow a given rite to show reverence to this statue. And Daniel refused to
follow that rite, because his religious conviction demanded him to not believe
in other gods than JHWH. But refusing to follow a rite that has been installed
by your boss is a very stupid thing to do, especially when you have a well-paid
job and your boss is the king. He probably knew that refusing would be the end
end of his career or even his life. We can assume that his friends advised him
to stop being stupid, and that Daniel himself probably had serious doubts
regarding whether to be reasonable or to follow his religious conviction. And
still he refused to perform the rite. From the stories before this we can assume
that Daniel was a reasonable and even wise person. How can a reasonable and wise
person be so stubborn, unrelenting, uncompromising and :term:`resistant to
advice`?

Georges Brassens suggests "Mourir pour des idées? D'accord, mais de mort lente."
