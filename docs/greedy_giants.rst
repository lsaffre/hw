=================
The greedy giants
=================

The world is being ruled by :term:`private corporations <private corporation>`.

It's amazing to watch them do their jobs.
They bring clean water and electricity into our houses.
They bring bananas to tables in regions where no banana grows.
They produce cars, telephones, computers, music, movies.
And we enjoy it.  We are grateful for it. We want it to last.

But we start to realize that :term:`corporations <corporation>` sometimes do their job more
efficiently than what would be useful. And that they are doing it
*independently*. We are like the `sorcerer apprentice
<https://en.wikipedia.org/wiki/The_Sorcerer%27s_Apprentice>`__ realizing that
the spirits he called are out of control.
Let's realize this fully. And then don't panic.

There are two types of corporations, and we often fail to  `differentiate
<https://en.wikipedia.org/wiki/Discernment_of_Spirits>`__ them: private and
public.

Every :term:`private corporation` is --by definition and in full accordance with
common laws-- an *immortal, ruthless, unscrupulous and greedy* :term:`legal
person`.

.. glossary::

  greedy giant

    A :term:`private corporation` of a certain size, especially when it gets
    more powerful than a :term:`national government`.

Knowledge means power. In the digital era knowledge becomes more important than
ever before.

On the other hand it would be a waste of energy if every government would manage
their own knowledge pool about things that apply for other governments as well.
Health care and education are two examples of areas where much knowledge applies
internationally.

Some national governments concludes agreements with other governments and
collaborate.

Every national government also concludes contracts and agreements with private
corporations. The government is the customer and the private corporation is the
provider. The customer should be the king, i.e. decide what they want the
provider to do. But reality is different.

In reality the governments are rather like the sorcerer apprentice (see
:doc:`/stgg`)

Quote from `techcrunch.com 2021-11-28
<https://techcrunch.com/2021/11/28/digital-regulation-must-empower-people-to-make-the-internet-better>`__

  As the European Parliament considers new regulations aimed at holding Big Tech
  platforms accountable for illegal content amplified on their websites and apps
  through packages like the Digital Services Act (DSA), it must protect
  citizens' ability to collaborate in service of the public interest.

  Some of the proposed requirements could shift power further away from people
  to platform providers, stifling digital platforms that operate differently
  from the large commercial platforms.

  Big Tech platforms work in fundamentally different ways than nonprofit,
  collaborative websites like Wikipedia. All of the articles created by
  Wikipedia volunteers are available for free, without ads and without tracking
  our readers’ browsing habits. The commercial platforms’ incentive structures
  maximize profits and time on site, using algorithms that leverage detailed
  user profiles to target people with content that is most likely to influence
  them. They deploy more algorithms to moderate content automatically, which
  results in errors of over- and under-enforcement.
