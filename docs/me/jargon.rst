==============
Private jargon
==============

.. glossary::

  Vigala

    The *kihelkond* where I lived permanently with my family between 2006 and
    2018. In 2018 we moved *de facto* to Tallinn because our youngest daughter
    changed from the village primary school to the TMKK.  Ly and I plan to move
    back there when our children have become independent. Seen like this, we are
    kind of living in exile when we are in Tallinn.

  Misjonikoor

    See https://misjonikoor.eelk.ee


  Covid pandemy

    TODO: start a page about it.
