.. _about_me:

========
About me
========

See my `business card <https://luc.lino-framework.org/about/index.html>`__ for
basic information about me. In this section I will tell you even more about me.


.. contents::
  :local:

I speak about myself
=======================

I am the most interesting human in the world. For *me*. Because I am the
only human whose heart I can read directly.

If you perceive this as arrogant or egocentric, then I ask you to forgive me.

I am the only human I can write about without offending others. Who, if not me,
is allowed to speak about myself?

I perceive speaking about myself as a form of gratefulness. Being grateful
includes telling others about it.  Telling others about me is one of my ways of
glorifying God, who made me.

Speaking about myself requires courage. It is dangerous because others are
likely to laugh about me.

I hope that speaking about myself encourages you to speak about yourself as
well.

..
  Others might perceive you as arrogant or
  egocentric. See also `„Liebe spricht für sich – nicht von sich“
  <https://www.vaticannews.va/de/papst/news/2021-11/papst-franziskus-herz-jesu-predigt-messe-rom-uni-gemelli-krank.html>`__

Speaking about yourself helps you to become more self-aware. Like every human I
happen to have wrong images of myself. Showing my pictures to others invites my
friends to correct me if I am wrong.

..
  To give you just one example: my wife also
  can read this blog and I can tell you that she isn't shy with giving critical
  feedback.

.. And then people might want to know more about who I am.


.. _about_me_blogger:

I am a blogger
==============

I started blogging in 1996. My first blog entry was `Eine Reise ins Internet
<https://luc.saffre-rumma.net/blog/1996/1018.html>`__. Note that it was posted
on October 18th, the Feast of `Saint Luke the Evangelist
<https://en.wikipedia.org/wiki/Luke_the_Evangelist>`__, who is my patron.

Blogging is writing a diary, but instead of keeping it secret, you do it in
public.  It is a kind of spiritual exercise: What do I want to say, knowing that
anybody might read it at any moment, maybe somebody whom I really didn't expect
to read it, maybe many years later? For me it's an interesting challenge.

Of course this hobby teaches me to think about confidentiality, copyright,
privacy and other topics.  As a blogger I must be careful to respect other
people's privacy.

During more than 25 years I have gathered some experience, but of course I am
not a master. I never blogged with the goal of getting many followers. I blog
because it helps me to digest my everyday experiences.  Writing a personal blog
is one of my ways to :term:`pray <prayer>`.

As a professional software developer I am trained in the art of using a computer
as a tool for thinking about complex topics. I sometimes call it "thinking
through your fingers". It helps me because human brains actually manage quite
poorly with the cognitive task of keeping in mind lists of things.

.. I am not good in accurate listening, I am better in *fuzzy listening*.

I recommend reading the book `Free Culture
<https://lessig.org/product/free-culture/>`__ by Lawrence Lessig. "Lawrence
Lessig could be called a cultural environmentalist. One of America’s most
original and influential public intellectuals, his focus is the social dimension
of creativity: how creative work builds on the past and how society encourages
or inhibits that building with laws and technologies."

I am an analyst/programmer
==========================

I love using computers as a tool for thinking. And --unlike many IT specialists,
as it seems-- I love humans as well.

My focus of interest is how to transfer knowledge from a human brain to a
computer, and from the computer to another human brain.

I stumbled into this vocation quasi against my will. I did not choose this
vocation, it chose me.  My plan at the age between 16 and 20 was to become a
biologist. I said "computers are nice tools for playing around, but I want to
something useful". After my failure in biology, my next plan during one year was
to become a basic school teacher. I fully realized my vocation only after
discovering the world of :doc:`Free Software </fs/index>`.

.. _potatoes:

I will rather grow potatoes than write non-free software
========================================================

I believe that software should be free, and I try to avoid using non-free
software as much as possible. I will not collaborate in any proprietary software
product. I refuse to publish any content other than under a `free license
<https://en.wikipedia.org/wiki/Free_license>`__.

There is no Windows computer under my roof. My family occasionally complains
about this, but I use my veto right as long as I am the main income generator.
And actually they manage quite well.

This is because I believe in something I named :doc:`the Lutsu project
</lutsu/index>`.  It is a :doc:`Daniel effect </defs/daniel>`, believe it or
not.

I am a Christian
=================

I classify myself as a Christian, more precisely a :term:`Roman Catholic` one.
See :doc:`/talks/christian`


I am a scout
============

I joined the scout movement at the age of 15, I was a cob scout leader (7-12
years old boys) for seven years, one year a leader of 15-18 yo boy scouts, and
then, during a few more years, helped organizing trainings for young scout
leaders.

I am no longer active in any scout group, but dare to say that the principles of
the Scout Promise and Scout Law remain active in my heart.  I believe that this
world would be more human if there were more scouts and less people who don't
care about these principles.

 "The World Movement of Scouting is in a very unique position to help the
 different peoples and cultures of the world find common ground from among their
 best traditions and beliefs. By this Scouting can help promote better world
 citizenship and world peace." -- Bryce Hall via `scoutwiki.org
 <https://en.scoutwiki.org/Duty_to_God_%28Scout_Oath%29>`__

I am an immigrant
=================

I moved from Belgium to Estonia at the age of 30. During the last twenty years I
have never regretted this decision. I plan to die here (not immediately of
course, but I have no plan to move back to Belgium or to some other country
during my lifetime). I don't see any need for changing my nationality. I will
always remain a Belgian living abroad, I will never become an Estonian.

I feel well integrated, mostly because I am married to an Estonian. I love the
Estonian people and their culture.

It took me about two years to learn the Estonian language to a satisfying level.
I will never speak it without accent, though.

I am married
============

I am married with an Estonian woman. How we found each other, that's another
story and would deserve a page on its own. Read also :doc:`our wedding
</talks/wedding>` and what I say :doc:`about marriage </talks/marriage>`.

I am a visionary dreamer
========================

https://www.thebalancecareers.com/visionary-leadership-4174279

I have political views
======================

The `politicalcompass.org` website classified me as `Economically rather Left and Socially moderatedly Libertarian
<https://www.politicalcompass.org/analysis2?ec=-8.63&soc=-4.26>`__.
I believe that finding ways for global sustainable development is more important than keeping the consumer society running.
If given the choice between an economic crisis and a climate catastrophe, I'd chose the former.
I believe that we need some fundamental changes in the way we rule this world.
Since September 2021 I am member of the social-democrat party of Estonia.
I happen to :ref:`talk about politics <talks.politics>`.

I love singing
==============

I received a more than average education in solfeggio and singing, but never
consider becoming a professional.

I love to sing alone and with others. But just for fun, I don't love to work
hard for it. I am not an "academic" style singer.


I try to be honest and open-minded
==================================

But note that I say "I *try*", not "I *claim*". Nobody is truly honest or
open-minded all the time. I happen to fail. I happen to have an illusionary
image of myself. Your :doc:`feedback </feedback>` can help me to become more
honest and open-minded.

I believe that the :term:`Gospel` calls us to develop an open mind and to be
honest. I have communication problems with people who refuse to be open-minded.
Yes, these people exist.  They obviously don't call themselves "closed-minded",
but they won't follow me in :doc:`/talks/openminded`.

.. _me.resistant:


I am resistant to advice
========================

Some of my friends say I am :term:`resistant to advice`. Which is at least
partly true.

.. glossary::

  resistant to advice

    A cynical compliment that actually means "who does not listen to advice from
    other people". (de: beratungsresistent)

Yes, I try to do the things I believe that they need to be done. Which sometimes
differs from the things that other people advise me to do.

With every incoming advice it makes sense to ask first "Who gives it? Where does
it come from? How competent is this person in the current context?"  This can
avoid you wasting time with triaging useless information. Not everybody is
competent everywhere. You recognize a tree by its fruit. Know your friends.

On the other hand this first filter shouldn't be waterproof, it should feature
holes for exceptions.

And when an advice makes it through the first filter --either because you trust
its source to be reliable or because it found an exceptional entry hole--, your
next important question is to ask yourself "What does it mean to me?"

See also :doc:`/defs/daniel`.


.. _unvaccinated:

I am not vaccinated
===================

I won't get vaccinated against Covid by my free will as long as the vaccination
campaign helps :term:`greedy giants <greedy giant>` to get even more power over
those who care for the poorest. I see this is a symbolic act of solidarity.

This does not mean that I refuse to trust in :term:`science` as a tool for
observing reality. We :term:`know <to know>` that vaccination is an efficient
weapon against viruses. And we :term:`know <to know>` that it would be foolish
to rely on vaccination as the only or most important thing we can do.  And we
:term:`know <to know>` that vaccination fails if big parts of the population
remains unvaccinated. We also :term:`know <to know>` that most unvaccinated
people are so because they can't afford it. :doc:`/blog/2021/1222_1215`

I refuse the reproach that my choice is irresponsible, reckless, out of egoism
or refusal to limit my liberty. By remaining unvaccinated I actually *reduce* my
liberty, compared to those who are vaccinated. For example I cannot travel
freely, I cannot eat in restaurants or enjoy social events.
(:doc:`/blog/2021/1215_2000`)

I also see that :doc:`even Wikipedia cannot provide unbiased information
</blog/2021/0903>` because :doc:`science serves those who pay for it
</blog/2021/0904>`.

I do agree --at least theoretically--  with the reproach that unvaccinated
people are more dangerous to their fellows (:doc:`/blog/2021/1217_2238`). But if
we apply this level of simplification to traffic accidents, then a vaccinated
car driver is *much more* dangerous to their fellows than an unvaccinated
bicycle driver. Another point to consider here is that the few antivaxxers in
rich countries are much less dangerous than the many people in poor countries
who would agree to get vaccinated if they had the money to pay it.

See also :doc:`/talks/vaccinate`

.. _me.words:

My words happen to cause harm
=============================

My words can be offending or simply wrong. Every now and then, both in real life
and on this website, it happens that I hurt other people or even cause harm
because I lack prudence when choosing my words.

I regret when such accidents happen, but I also believe that they are
*accidents* and not my intention. I try to to avoid them, to learn from them, to
become more prudent. But I don't plan to stop speaking. You can help me to learn
by telling me when I made yet another mistake.

.. _me.more:

More about me
=============

.. toctree::
    :maxdepth: 1

    /talks/wedding
    /talks/marriage
    /talks/church
    /talks/christian
    /talks/openminded
