=====
Credo
=====

The Apostles Creed
==================

I believe in God, the Father Almighty, Creator of heaven and earth. I believe in
Jesus Christ his only Son, our Lord. He was conceived by the power of the Holy
Spirit and born of the Virgin Mary. He suffered under Pontius Pilate, was
crucified, died and was buried. He descended into hell. On the third day he rose
again. He ascended into heaven, and is seated at the right hand of the Father.
He will come again to judge the living and the dead. I believe in the Holy
Spirit, the holy catholic Church, the Communion of Saints, the forgiveness of
sins, the Resurrection of the body and life everlasting. Amen.

See also `Wikipedia <https://en.wikipedia.org/wiki/Apostles%27_Creed>`_.



Here is my self-written statement of the beliefs that guide my actions.

  Planet Earth is an irreplaceable gift, given to all living beings as the only
  available place to live.

  All humans are responsible for managing this gift because as a species we have
  cognitive intellectual skills that go beyond those of other animal species.

  This responsibility is given to humanity as a whole, no individual human can
  grasp it for them alone, nor escape nor be exempted from it.

  Managing this gift means that we must develop ways for living together on this
  planet in a sustainable way.

  Every human has a unique combination of assets, experiences and skills,
  leading to a unique set of responsibilities and rights and a unique role
  in their community, their country and on the whole planet.
