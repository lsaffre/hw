======================
What is free software?
======================

.. contents::
  :local:

Some illustrations
==================

The sandwich bar
----------------

Imagine that you are the owner of a well-running sandwich bar in a
little town.  And as your sandwich bar gets more and more customers,
you decide to engage an assistant to help you.  And because your
assistant is trustworthy and capable, you delegate more and more
responsibilities to him.

Then imagine that you need to move to another town and designate your
assistant to become the manager of your sandwich bar.  The manager
continues to send you regular financial reports about your sandwich
bar.

Now imagine that after some time you discover that the manager of
your sandwich bar would refuse to show the invoices he received
and issued, and the statements of your bank accounts, saying "My
financial reports are enough, I don't want you to see the
underlying detailed bookings because it is my privacy how I
realized the results."

How would you react?  You would probably fire this manager, maybe even
file a lawsuit against him.

That's how enterprises are run.  The managers do the work of ruling,
and they write regular activity reports to the owners. The managers
get a salary, and the owners get the profit.  The owners continue to
control their enterprise because they can fire their managers at any
moment.

That's how enterprises are run, and that's also how a computer works.
The computer hardware are the rooms of your sandwich bar, the
operating system is the manager. You communicate with your manager
using the keyboard and the monitor.

The point of this story is that if you fire such a manager in a
real-world enterprise, why do you run a closed-source software on your
computer?

And although this story was written for enterprises, it applies to
public institutions as well except for some vocabulary. Replace
"owner" by "director" and "profit" by "honour".

I hope that this picture opens your eyes and that you start to feel
with me why it is important to wake up.

Other images
-------------

- Compare a family living in a *rented* house versus a family living in a
  *owned* house.

- Imagine the government of your country would sell all its real estate. The
  buildings where public administrations and the parliament do their work. And
  all the streets

- Imagine there was only one car manufacturer on this planet.  Would you
  be worried about quality and security?


Vocabulary
==========

Don't mix up :term:`free software` with :term:`open-source software`. There are
business models where they sell the right to consult the source code without
permission to redistribute it.  They call themselves :term:`open-source
software` but they're not :term:`free software`.

Don't mix up :term:`free software` with :term:`freeware`. The "free" in
:term:`free software` means "free of bondage", not "free of costs".


.. glossary::

  free software

    Software that gives its users the freedom to *run* it, to *see the source
    code*, to *modify* it, and to *distribute* both the original software and
    the adapted versions.

    https://en.wikipedia.org/wiki/Free_software

  open-source software

    Software with publicly available source code.

    https://en.wikipedia.org/wiki/Open-source_software

  freeware

    Software that you can use for free, i.e. without paying any money.

    https://en.wikipedia.org/wiki/Freeware

  FOSS

    Abbreviation for "Free and Open-Source Software", means the same as
    :term:`free software`. Has become popular in contexts where the
    differentiation from :term:`free software` might not be obvious.


It's not about short-term freedom
=================================

Richard Stallman's `Free Software Definition
<https://en.wikipedia.org/wiki/The_Free_Software_Definition>`_ states
that users of free software get the freedom to look at source code, to
change it and to share their modifications.

The problem with this formulation is that normal software users don't
want that freedom.  Normal users want to be able to phone their friend
and ask "How do you do when you want to format this document in two
columns instead of one?"  And they want well-written documentation and
a series of books about their software so that they can look up
themselves how to solve their problems.  That's the kind of freedom
they want.  And that's the kind of freedom they are more likely to get
when they use wide-spread software products owned by some
international worldwide corporation.

When we define freedom as "the power or right to act, speak, or think
as one wants" [#oxford1]_, then there is no need for free software
because proprietary software does not violate this freedom more often
than free software.

So free software isn't about this kind of freedom.

But freedom also means "absence of subjection to foreign domination or
despotic government" or "the state of not being imprisoned or
enslaved".  [#oxford2]_ These meanings are more difficult to grasp
because they are less related to our everyday activities.

Think for example about a woman who enjoys wearing clothes which make
her more attractive to men.  There are probably not many such women in
countries like Afghanistan.  Most women in Afghanistan probably can't
even imagine that they are missing some part of life's joys. If you
asked one of them whether she would like to wear western clothes, you
will probably get a negative answer. So they are free, aren't they?

If you agree that women in Afghanistan are free, then proprietary
software is for you. Don't read on.


It's not about money
====================

A text titled "Unavoidable Ethical Questions About Open Source"
[#scu_questions]_ published by the Santa Clara Univerity in Silicon
Valley expresses common arguments against free software.  The text is
formulated as a series of "ethical" questions. I perceive these
questions as "wrong" because they are drifting away from the important
point.  However we need to answer such questions, so here they are,
together with my answers:

- **Is human knowledge advanced by full and free access to all
  information, allowing engineers and developers to correct and
  improve on already existing systems?** -- Answer: Yes.  "[T]he free
  sharing of useful information benefits the human community.  This
  sounds so obvious as to not merit discussion, but when this point is
  applied to real life, it seems to stir up a lot of controversy."
  [#cook]_

- **Or does a lack of strong protection for IP discourage innovation by removing
  the financial incentive for developing it?** -- Answer: Removing property
  right on something removes indeed the "financial incentive" for developing and
  maintaining it. And that's what we need. We need to disconnect software from
  the interests of a particular group of humans so that the other incentives for
  doing the work can grow: usefulness, solidarity. Innovation is done by
  creative humans who act because they love their work and not because they get
  money for it. Many modern slaves do some job just because they are getting
  money in return of the lifetime and energy they spend for working on some
  project which is not their's. These are not usually the ones who bring
  innovation.

- **Classically, free speech is understood as a right, but is this a useful way
  to think about open source software? Is there anything in the nature of
  software that would give people a right to it in the same way that we have a
  right to speech?** -- Writing software is substantially the same activity as
  writing a prosa text. Prohibiting to share software is like prohibiting to
  share texts. Making humans write proprietary software by paying them a salary
  violates their basic human right for free speech.

- **Is it fair to expect software developers to create and distribute their
  intellectual product without restrictions while we do not expect the same from
  other inventors or producers?** -- Free software does not mean volunteer work.
  There are many people who earn their living by writing free software.  The
  free software industry shows that proprietary licensing is not the only way of
  making money using software and that there are better methods to pay software
  developers for their work.

- **The Vatican document “Ethics in Internet” argues that “use of the new
  information technology... needs to be informed and guided by a resolute
  commitment to the practice of solidarity in the service of the common good.”
  Flowing from this view, the document says that “cyberspace ought to be a
  resource of comprehensive information and services available without charge to
  all, and in a wide range of languages. The winner in this process will be
  humanity as a whole and not just a wealthy elite that controls science,
  technology, and the planet's resources.” Is this view applicable to software
  as well?** -- Answer: Yes. The Vatican's document takes a beautifully clear
  position against proprietary software.  Software is stored knowledge about how
  a given job can be done. This is information. It is comprehensive only in the
  source code form.

- **(...) But to others, "sharing" software is like having to consent to its
  theft because the sharer is giving away someone’s work product, which is the
  result of sweat and ingenuity and which has monetary value, as well. Will open
  source inculcate the virtues of friends or of thieves?** -- Answer: Yes, using
  proprietary software without permission is theft because the copyright holder
  refuses to share. Using free software is never theft because the author has
  publicly given their permission to share their work.  Note that the "monetary
  value" is usually not owned by those who gave their "sweat and ingenuity" but
  by their employer, usually a :term:`legal person` that does neither sweat nor
  have ideas.

While my above answers --hopefully-- give satisfying answers to the
given arguments [#jesuits]_, all these thoughts are rather misleading
regarding to our original question about why software must be free.
Free software is definitively not about money, neither about the price
of the product nor about the wage of the author.


It is about power
=================

Free software is not about *money* and not about *comfortability*, it
is about *power*.  The real issue with proprietary software is that
its copyright holder effectively has power over its users.  Non-free
software makes the users surrender control over their computing to
someone else.\ [#watercutter]_

Vendors of proprietary software are skillful in finding ever and ever
new tricks and whole technologies whose ultimate goal is to bind you
to their product (`vendor lock-in
<https://en.wikipedia.org/wiki/Vendor_lock-in>`_).  They hide this
fact more or less successfully, but it is a necessary part of the
game.



.. rubric:: Footnotes

.. .. [#lewis1]  Inspired by `The New New Thing: A Silicon Valley Story Paperback
   <https://www.amazon.com/The-New-Thing-Silicon-Valley/dp/0393347818>`__
   by Michael Lewis (January 2014),
   via `What Is a Business Model? <https://hbr.org/2015/01/what-is-a-business-model>`__.


.. [#cook] `Cody Cook: Open Source Jesus <http://www.cantus-firmus.com>`_

.. [#scu_questions] `Unavoidable Ethical Questions About Open Source
                    <http://www.scu.edu/ethics/publications/submitted/open-source.html>`_

.. [#jesuits] It is funny that catholic monks seem to be more
              concerned about money than about anything else.

.. [#oxford1] First meaning of freedom according to `Oxford dictionary
              <http://www.oxforddictionaries.com/definition/english/freedom>`_

.. [#oxford2] See most other meanings of freedom according to `Oxford
              dictionary
              <http://www.oxforddictionaries.com/definition/english/freedom>`_

.. [#watercutter] Angela Watercutter in `Why Free Software Is More
                  Important Now Than Ever Before
                  <https://www.wired.com/2013/09/why-free-software-is-more-important-now-than-ever-before/>`__
                  (wired.com 2013-09-28)
