===========================
Why software should be free
===========================

.. toctree::
   :maxdepth: 1

   intro
   whatisfs
   sovereignty
   challenges
   foolproof
   brave
   limux
   solutions
   links
