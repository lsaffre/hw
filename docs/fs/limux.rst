============================
The LiMux project in München
============================

- February 2014 `How Munich rejected Steve Ballmer and kicked
  Microsoft out of the city
  <http://www.techrepublic.com/article/how-munich-rejected-steve-ballmer-and-kicked-microsoft-out-of-the-city/>`_
  (Nick Heath)


- February 2017 `Nahendes LiMux-Aus: Open-Source-Szene trauert,
  Microsoft jubelt
  <https://www.heise.de/newsticker/meldung/Nahendes-LiMux-Aus-Open-Source-Szene-trauert-Microsoft-jubelt-3627759.html>`_

- October 2017 `LiMux-Ausstieg durch die Hintertür?
  <http://gruene-fraktion-muenchen.de/limux-ausstieg-durch-hintertur-2/>`__

- January 2025 `Nach LiMux-Aus: München startet erstes Open-Source-Sabbatical
  <https://www.heise.de/news/Nach-LiMux-Aus-Muenchen-startet-erstes-Open-Source-Sabbatical-10266582.html>`_
