===================
Miscellaneous links
===================


.. toctree::
   :maxdepth: 1

   rms


More unclassified notes
=======================


http://www.investigate-europe.eu/en/


Der Radiobeitrag `Gefährliche Abhängigkeit von Microsoft
<http://www.inforadio.de/programm/schema/sendungen/digitalesleben/201711/185851.html>`_
(inforadio.de, Sa 25.11.2017) erklärt mit verständlichen Worten,
weshalb es so gefährlich ist, wenn ein Software-Hersteller als
Platzhirsch die Szene dominiert.

"As software becomes increasingly integrated into our lives, it's crucial to
ask: Who controls that software? Because, in reality, this question transcends
to a deeper one: who controls YOU through that software, determining what you
are/aren't allowed to do in your own life? Free software is not about nice
features, convenience or cost, it's about control, ethics, and freedom." --
Jason Self (`source <https://mastodon.social/@jxself/111565473218173550>`__)
