===========
Being brave
===========

The world of `free software
<https://en.wikipedia.org/wiki/Free_software>`_ and the world of
`proprietary software
<https://en.wikipedia.org/wiki/Proprietary_software>`_ are two
separate universes.  They are substantially different.

Free software is to computers what democracy is to society.  Free software means
democracy, proprietary software means dictatorship and slavery.  Proprietary
software means that you are either a slave or a master, free software means that
there is no master and no slave.

With the increasing impact of software on our daily lives,  free software is the
only choice if we want sustainable peace on Earth.

The most important advantage of Free software is to avoid vendor lock-in.  This
is still very difficult to sell. The breakthrough of free software is not yet
achieved. Normal managers still prefer a proprietary system over a sovereign
system because that's cheaper and more comfortable. We all tend to value comfort
over sovereignty.

Choosing free software means that you need to be brave because you leave the
mainstream.  Many people believe that managers should be rather careful than
brave.  I don't fully agree with this.  Here are some thoughts to hopefully
encourage you.

Being brave does not mean to be careless. There are situations where brave
decisions need to be done. Freedom from vendor lock-in is one of them.

Some people just shrug when I tell them that avoiding vendor lock-in is more
important than their comfort, or that :ref:`I will rather grow potatoes than
accepting to write non-free software <potatoes>`. My Lino project exists only
because I am crazy enough to believe that open solutions will become the norm
some day. I am realistic enough to see that this day might not be very soon.
