================
Richard Stallman
================

"While few many agree totally with his viewpoints, and with the
techniques that he uses to express his thoughts, there is often an
underlying current of logic to Stallman's thoughts that deserves
further consideration." (`Stallman celebrates 30 years of Free
Software by decrying SaaS
<http://www.techrepublic.com/blog/australian-technology/stallman-celebrates-30-years-of-free-software-by-decrying-saas/>`__)


