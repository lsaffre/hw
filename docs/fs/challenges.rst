==========
Challenges
==========

Choosing free software is actually *not* difficult,
but many people *believe* that it is.
This can become a self-fulfilling prophecy,
causing miscellaneous problems.

Here are some examples of the challenges you are going to face:

- Psychological effects ("Under Windows it would work better") ranging
  from personal reproaches for your decision to measurable
  side-effects.

- When using free software, it is more difficult to get professional
  support, trained human resources, specialized hardware.

- The end-users themselves use proprietary software at home and
  are reluctant to change their habits.

- Free software cannot serve as capital.  While this is the inevitable
  condition for remaining free, it also reduces the motivation of
  investors whose primary goal after to make money.

- A widely spread attitude which I use to label "Uncle Tom's
  Advice"[#uncletom]_ is to say "Come on!  Stop making your own life
  complicated, we are all slaves of somebody, just let Microsoft or
  Google or Apple be your master and enjoy life!"[#eager]_.

..
  short-term versus long-term
  
  all kinds of companies of private law to invest into free software.
  A good manager takes care of not wasting time or money into projects
  that are "useless" in regard to their primary goal, namely to create
  benefit for the owners.  And the chances of success of an enterprise
  increase with the competence of its managers.  And successful
  enterprises tend to grow in size and power. So one might come to the
  conclusion that free software projects have no chance of becoming
  big and powerful.


.. rubric:: Footnotes

.. [#uncletom] `Uncle Tom <https://en.wikipedia.org/wiki/Uncle_Tom>`_
   was a slave who accepted his destiny.

.. [#eager] A good example of this opinion is Michael Eager's blog
            entry `Free Software Foundation vs Microsoft
            <http://www.embedded.com/electronics-blogs/open-mike/4440107/Free-Software-Foundation-vs-Microsoft>`_.
            Later (in a `closed thread on LinkedIn
            <https://www.linkedin.com/grp/post/43875-6037641137475301379>`_)
            he made it even more clear: "Not sure what you (or FSF)
            mean by "draconian".  IP [intellectual property] laws
            apply to Microsoft, Coke, Intel, Ford, and LinkedIn, among
            many others. What is draconian about them?  Can you trust
            Intel to build chips correctly, Coke to bottle soda, Ford
            to build cars, and Linked in to host messages, without
            knowing all of the details of how this is done?  Do you
            drink Coke, drive a car, use a computer, eat at
            MacDonald's?"
