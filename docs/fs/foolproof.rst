========================================
Free software is less foolproof
========================================

Somebody on Reddit asked `What is Ubuntu Pro?
<https://www.reddit.com/r/Ubuntu/comments/tk9aou/what_is_ubuntu_pro/>`__ and got
several useful answers, but also one shining example of the opposite:

  One year later and it still doesn't work properly I enabled a bunch of pro now
  my Wi-Fi won't connect despite using the correct passwords all I get is a
  message saying password or encryption keys are required to access the wireless
  network blah blah blah sorry Linux but you're never gonna be mainstream!
  Uninstalled

Yes, proprietary software developers invest more time into making their products
foolproof. They care for the fools.
