===========================
Introduction
===========================

In this article I try to explain why software should be free, why you should
avoid proprietary ("non-free") software, why your organization should choose
`Linux <https://en.wikipedia.org/wiki/Linux>`_ rather than `Windows
<https://en.wikipedia.org/wiki/Microsoft_Windows>`_ or `OS X
<https://en.wikipedia.org/wiki/OS_X>`_ as the operating system on your
computers.

I also try to explain why this choice is currently not the mainstream and why
you should nevertheless consider it.

Most experts agree that software :term:`vendor lock-in` is a problem and
:term:`Free Software` a solution.  The controversial question is how serious the
problem and how realistic the solution is. On this page we explain why we
believe that the problem is serious and the solution realistic.

Short answer to the question in the title: Software should be free because the
more your everyday life depends on computers, the more it is important to ask
who controls these computers: who decides when to upgrade, which features to
drop and which to develop, how to implement security controls, how to distribute
the costs.

This document focuses on software, but there are even more fundamental reasons
why software should be free: because it is a form of :term:`published content`
(see :doc:`/lutsu/index`) and because proprietary software is a
:term:`collective sin` that cannot lead to a sustainable modern human
civilization.


Proprietary software is easier to use
=====================================

An often-heard argument against free software is "For me as a non-expert,
software products like MS Windows, MS Office and Google Maps are easier and
cheaper to use than Linux, LibreOffice and OpenStreetMap".

Yes, proprietary software is easier to use and sometimes cheaper than free
software. And as an individual you will *of course* choose the easiest way,
which is the mainstream, the way chosen by your friends.

But as the leader of an organization you are assumed to think further than an
individual human. The captain of a ship must not only care whether the engine
works optimally, he must also check whether the ship move into the right
direction.

.. Having more rights means to have more responsibilities.

..
  Using proprietary software is like choosing to live in a *rented* house rather
  than in an *owned* house.


Tangible problems of proprietary software
=========================================

There are a few aspects of software business you might care about even if you
are not an IT expert:

As an :term:`end user` you actually are an important contributor in the software
development process.  When you have a problem with a :term:`software product`,
then its owners should be *thankful* that you take the time of explaining them
your problem. Every support request increases the value of their product.
Despite this, most :term:`software developers <software developer>` make you pay
for getting support. In a certain way it should be the opposite.

Who decides which features to include or remove in a :term:`software product`?
Who decides which versions they continue to maintain?
The owner of course.
The owner also decides about the price to pay.
Using a :term:`software product` makes you depend on its owner.
The owner has motivation to bind you even further to their product.

"Proprietary software comes with major barriers such as high costs and vendor
lock-in, making OSS more appealing as a viable alternative for digital
government services." (Tony Blair Institute, `Open-Source Software: Three
Considerations for Digital-Government Transformation
<https://www.institute.global/insights/tech-and-digitalisation/open-source-software-considerations-digital-government-transformation>`__)

At the world-wide level there is an increasing concentration of power towards a
few software giants. National governments are worried. For example the German
Federal Government `agreed in 2021
<https://www.bundesregierung.de/breg-de/service/gesetzesvorhaben/koalitionsvertrag-2021-1990800>`__
that software development should be generally commissioned as open source. The
parliament confirmed this `in December 2023
<https://www.bundestag.de/presse/hib/kurzmeldungen-983744>`__. Some activists
discovered that in 2023 the government continues in an "obscene" way to spend
most of their budget for proprietary rather than free software (sources:
`netzpolitik.org
<https://netzpolitik.org/2023/digitale-souveraenitaet-milliarden-fuer-oracle-microsoft-und-co-statt-fuer-open-source>`__,
`computerwoche.de
<https://www.computerwoche.de/a/bundesregierung-zahlt-milliarden-an-oracle-und-microsoft,3698126>`__).
