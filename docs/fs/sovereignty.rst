========================
Sovereignty and software
========================

If you care about the sovereignty of the information systems in your
enterprise or organization, then you ask questions like the following:

- Who will help us when something goes wrong? Who is responsible for
  maintaining that software, for giving support to its end-users?

- Who is going to decide whether to switch to some newer technology?
  Or to discontinue some existing product and push us to migrate to a
  newer one?

- Who is going to analyze our needs, decide how to cope with new
  challenges, which new functionalities should be implemented and
  which not?

- How competent is the partner I chose? What is their motivation? How
  is the price-quality ratio? Do they have concurrents?  Do I have a
  possibility to chose another partner in case I am not satisfied?


.. Eric S. Raymond describes the democratic ("bazaar") and monocratic
   ("cathedral") models in his book `The Cathedral and the Bazaar
   <https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar>`_.

