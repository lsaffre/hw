=========
Solutions
=========

Yes, choosing free software *does* make our life more complicated.
You need to consider the challenges and decide bravely how to react to them.
This will require measurable additional effort.
You need to find trustworthy *partners* because you cannot do everything
yourselves.

But your
sovereignty is inalienable [#rousseau]_ and it is *worth the effort*.

And there is a growing list of organizations devoted to :term:`Free Software`
[#wikipedia1]_. Many developers feel that free software is fundamentally better
than proprietary software [#fsf1]_. Public administrations are increasingly
aware of the important strategic advantages that only Free Software can give.
See `Free Software in public administrations`_ below.



Free Software in public administrations
=======================================

The `Public money public code <https://publiccode.eu/>`__ campaign explains why
:term:`Free Software` is important for public administrations: "Software created
using taxpayers' money should be released as Free Software. We want legislation
requiring that publicly financed software developed for the public sector be
made publicly available under a Free and Open Source Software licence. If it is
public money, it should be public code as well. Code paid by the people should
be available to the people!"

The European Union is more diplomatic regarding proprietary software by
using the neutral term :term:`Open-source software` (OSS). The term :term:`Free
Software` can indeed cause allergic reactions if you don't fully agree with the
Free Software Foundation and their chairman Richard Stallman. But their
`Guidelines for creating sustainable open source communities
<https://joinup.ec.europa.eu/collection/open-source-observatory-osor/guidelines-creating-sustainable-open-source-communities>`__
show the importance of :term:`Free Software` after removing the polemic parts.
Some excerpts:

  Public administrations should not merely reuse OSS (i.e. be consumers) but
  rather be active members and contributors to the communities that exist around
  this software.

  The sustainability of OSS communities is not a one-off investment. Once you
  either successfully join or launch an OSS community, it is important for your
  public administration and your steering committee to keep nurturing and growing
  the community behind your software.

  In the long run, your community's sustainability will rely on the following
  key factors: a clear governance structure, the vibrancy and health of the
  community, continuous commitment of the public administration’s political
  hierarchy to the project, sustainable funding, and the maturity of your
  software.

  Transparency is at the heart of successful open source communities. For this
  reason, as your community evolves and grows over time, its governance should
  remain clear and transparent. This will help you to attract new members, make
  it easier to promote your software, and ensure the commitment of key community
  contributors.


Sustainably Free Software
=========================

The question with Free Software is: when a software product belongs to
everybody, who is going to care for it?

While :term:`Free Software` gains popularity because of its technical and
strategical advantages, its administrative and commercial challenges become more
visible. Having the source code free and open is not enough.  A software
product is more than its :term:`source code`, it also consists of developer
resources, user documentation, training material, configuration files,
installation tools, promotional documentation and a legal infrastructure.

Managing and controlling all these things for a software product with
more than 25 users is more work than a single person can reasonably do.  There
will be a community around the product, which is more than the developers who
write the source code.

The legal entity that controls the community of a software product is what I
call the :term:`product carrier`. And a product carrier, like every
organization, needs a business model.

As long as that business model is based on the idea of increasing the profit of
a limited group of humans (the owners or shareholders), we will remain in the
vicious circle that leads to vendor lock-in or hijacking. A
permissive Free Software license leaves the door open to such endeavour.
An example is explained in `2021-12-12 Why I no longer use the MIT license for
new projects
<https://dev.to/ender_minyard/why-i-no-longer-use-the-mit-license-for-new-projects-348m>`__

Even the *GNU Affero General Public License* (AGPL), probably the most
protective Free Software license there is, cannot fully protect a
software product from getting hijacked. An example is what's currently happening
with WebKit Internet browsers (explained e.g. in 2021-11-28, Bozhidar Batsov,
`Firefox is the Only Alternative
<https://batsov.com/articles/2021/11/28/firefox-is-the-only-alternative/>`__)


If we want Free Software to be *sustainably* free, we must consider the whole
product as a common good. And we must govern the product as such. We must
realize that software development is a *res publica*, a common issue, and as
such needs to be governed in a transparent and democratic way. It becomes a
public infrastructure, similar to roads, railways, water distribution systems,
school buildings. That's why Free Software belongs to the competence of public
authorities.


.. rubric:: Footnotes

.. [#wikipedia1] See `List of free and open-source software organizations
   <https://en.wikipedia.org/wiki/List_of_free_and_open-source_software_organizations>`__

.. [#fsf1] See `Why Open Source misses the point of Free Software
   <https://www.gnu.org/philosophy/open-source-misses-the-point.html>`__




An International Data Agency
============================

Jens Berger suggests an International Data Agency, an organization with
universal authority who distributes the profit gained by the companies who turn
data into money.

`Jens Berger (2017-01-03) : Das Daten-Kartell – Warum fordert niemand
eine Vergesellschaftung der Daten?
<http://www.nachdenkseiten.de/?p=36456>`__


.. rubric:: Footnotes

.. [#rousseau] The idea that the sovereignty of a state is inalienable
   was first formulated by Jean Jacques Rousseau in his book `Of the
   Social Contract, or Principles of Political Law
   <https://en.wikipedia.org/wiki/The_Social_Contract>`__
