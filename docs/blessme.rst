===============
Please bless me
===============

Besides sending an occasional :doc:`feedback <feedback>` and :term:`praying <to
pray>` for me and for this project, you may formulate a :term:`benediction` and
send it to me as an email. It can be written in any language.

Until now I have not received any formal benediction, so you have a chance to be
the first. I will probably wait to have received a reasonable number of formal
benedictions before publishing them here.

Unless you prefer to remain anonymous, I will share your benediction on this
website together with your full name and the date. This will increase the
measurable value of my work and help other people to trust it.
