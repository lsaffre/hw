===============================
Public money, public activity
===============================

Let us change our laws so that public administrations must use
:term:`undisclosable management` and :term:`undisclosable accounting`.

.. glossary::

  undisclosable accounting

    A form of accounting where every transaction and its underlying documents is
    published in the periodic declaration.

  undisclosable management

    A form of management where confidential communication is allowed only in
    well-refined circumstances.


.. contents::
   :depth: 1
   :local:


Examples
========

A :term:`legal person` making business with a public administration agrees to
the condition that contracts, invoices and financial transactions with that
public administration will be published.

Every letter and email sent to a public administration is automatically
published by default. Confidential 


Why
===

Public administrations should not have any right to :term:`privacy`. See
:doc:`/talks/dangerous_rights`.

As citizens we have the right to information about how our money is being spent,
how much some decision or project actually costs. This includes access to the
raw data so we can do our own analysis.

This will reduce corruption, avoid public administrations to enter into secret
agreements.,  increase quality of services, reduce disenchantment with politics,
increase citizen participation, increase trust in democracy, reduce conspiracy
theories...

How
===

This change doesn't need any international concertation, every nation will do it
when they are ready for it.

Any active contracts with a non-disclosure clause can continue, but the
non-disclosure clause becomes void.  It becomes void *by national law*, not
because one of the partners decided so.  If one of the partners decides to end
the contract prematurely because the national laws changed, they may do so
according to the contract's terms. Existing contracts that mention such
confidential information can be changed before the new law gets active.

Exceptions
=============

Price offers and drafts don't need to be public because that would disturb the
free market.

Salaries don't need to be public because this would touch the privacy of humans.

Certain public administrations will receive special permission to remain secret.
For example those involved in security (e.g. police or army) or the payment of
salaries to the clerks. But even these administrations will have a duty to
report upon request to authorized auditing administrations.
