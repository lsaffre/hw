=================
About law systems
=================

The values of a culture are statements and rules that define what its members
perceive as good, important and valuable. We use them consciously and
subconsciously in our social and political life. They influence our daily
choices and priorities.

Every :term:`law system` describes the values of a given :term:`culture`.

.. glossary::

    law system

      A system of laws, i.e. of written rules.

    common law

      A :term:`law system` issued and maintained by a national government or a
      supernational body.

    natural law

      An unwritten rule imposed by nature.

      See `Wikipedia <https://en.wikipedia.org/wiki/Natural_law>`__.


As an edge case there are **cultures without written laws**, for example
**historic** cultures like the `Celts <https://en.wikipedia.org/wiki/Celts>`_,
or **ideal** cultures like the :term:`Church` or :term:`marketism`.

There is a `list of contemporary national legal systems
<https://en.wikipedia.org/wiki/List_of_national_legal_systems>`__.
A whole science, called `comparative law
<https://en.wikipedia.org/wiki/Comparative_law>`__, does nothing but
studying the differences and similarities between these law systems.

Besides the civil law systems we have different `religious law systems
<https://en.wikipedia.org/wiki/Religious_law>`__. Some of them can be
classified as `Christian law systems
<https://en.wikipedia.org/wiki/Canon_law>`_.
