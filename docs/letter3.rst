======================
Third letter from God
======================

My dear children,

the Corona virus is not your enemy. Your enemy are some of your laws and beliefs
that do not fully comply with my :term:`plan <God's plan>`. Please review them.
The Corona crisis shows that you can do it.

I love your selfishness as much as your altruism. Selfishness may motivate you
to do great things. Your concepts of private property and free market are great
ideas. But please stop giving selfishness more power than it deserves. Stop
making institutions out of it. Stop allowing corporations to be selfish.
Corporations are no humans, they are a product of human imagination. They cannot
have the same rights as humans. You cannot exempt them from responsibility and
you cannot give them the right of a protected privacy.


I want justice. Please stop granting limited responsibility combined with
unlimited profit to the wealthy among you. You know that every project bears a
potential risk of causing damage. When a project turns out to be harmful, the
wealthy among you, not the poor, must pay for repairing the damage.

Natural resources are mine, not yours.  Please
make clear that they are common resources and cannot be the private property of
anybody. I mean them as a gift to *all* humans, to be managed together
and not by some selfish group of them.

Come on! Stop saying that what I ask is too much! You can do it.  Don't believe
that you can continue as before when the Corona crises has calmed down. I just
want you to manage my property in a sustainable way. I won't get tired of
reminding you my plan.

Your Father in Heaven

(May 2020. See also :doc:`/blog/2020/0502`)

.. Last edited at 11:20 : Change paragraph sequence. Removed "published
   intellectual work" because it looks too strange here. Needs another letter.

   13:20 : changed wording order in first sentence fr readability. Added "Stop
   making institutions out of it."
