=========================
Facebook versus Wikimedia
=========================

Why I praise Wikimedia and despise Facebook.
Why you should become more active on Wikimedia and less active on Facebook.

Can we consider Wikimedia "social media"?
=========================================

Before comparing WM and FB, we need to discuss whether they are comparable.
Because indeed not everybody thinks the same about `What Is Social Media?
<https://www.lifewire.com/what-is-social-media-explaining-the-big-trend-3486616>`__.

But I claim that they both are social media.
Social media is any website that helps you to satisfy your social needs. Sharing
interesting information with others and getting their feedback is a social need.
"Social media is widely marketed as an interconnected environment that enables
content sharing, collaboration, and shared dialogue. This would seemingly appeal
to anyone with an inclination for political debate, which requires careful
consideration of a variety of viewpoints, evidence, and opinions. However,
social media does not currently live up its this potential to contribute to
healthy democratic debate." (`Inside the social media echo chamber
<https://www.brookings.edu/blog/techtank/2016/12/09/inside-the-social-media-echo-chamber/>`__)


Why you should prefer Wikimedia over Facebook
=============================================

.. list-table::

  * - FB incites you to publish **private content** (i.e. something you
      want to share only with a restricted group of friends).
      It hides the risk that such a post might inadvertently leak to others.

    - WM does not allow you to publish private content.

  * - FB helps you to bring **your ego** in the centre of attention.
    - WM helps you to restraint from being egocentric.

  * - FB makes you forget your **responsibility regarding copyright**. When you
      share something on FB, FB makes it difficult for you to also specify your
      sources.

    - WM reminds you your responsibility regarding copyright. You cannot share
      content without providing appropriate information about your sources.

  * - FB **neglects history** and focuses on the present, the recent past and the
      the near future.
    - WP makes sure that **nothing gets lost** in history.

  * - FB teaches you a **superficial worldview**.
    - WM teaches you to go **beyond the first impression**.

  * - FB makes you **consume** without giving back.
    - WM makes you **produce** useful content and become **creative**.

  * - FB appropriates part of your copyright. When creating an account on FB,
      you give them an
      irrevocable permission to use any content you post on their platform.

    - WM protects your copyright. You are free to decide which license to use
      for your work. For example you may prefer to not allow commercial usage,
      which means that
      if somebody wants to make money with your work, they would have to
      negotiate commercial conditions with you.

  * - FB makes it difficult for others to use your work.
      If your friend would download a picture you posted on FB,
      and then would -after asking your permission- publish it somewhere else,
      then FB could sue your friend for copyright infringement because they add
      invisible information to
      your file so that the copy on their site becomes their property.

    - WM makes it easy for others to use your work.  If for example somebody
      wants to use one of your pictures for printing a book, they don't even
      need to ask your permission (provided they meet your conditions). You will
      get free publicity and reputation by being cited.

  * - FB is a **for-profit corporation**, i.e. a :term:`greedy giant`.

    - WM is a **non-profit foundation**, i.e. it is legally protected against
      becoming a :term:`greedy giant`.


Why people nevertheless prefer Facebook
=======================================

.. list-table::

  * - FB has a sufficiently good mobile app.
    - WM doesn't have a good mobile app.

  * - FB permits quick achievements that make you feel successful.
    - WM has a higher learning curve than FB.

  * - FB allows you to have friends and followers.
    - WM has no clear notion of friends or followers.

  * - FB allows you to share spontaneously.
    - WM requires you to think before you share.

  * - FB enforces real user names. Connects every user account to a real person.
    - WM respects privacy and discourages real user names.
