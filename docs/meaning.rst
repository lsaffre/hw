===================
The meaning of life
===================

What is the meaning of life? Does *my* life make sense? This question is maybe
the most basic of all :doc:`existential questions <existential_questions>`. It
has always been asked by humans, and no religious or political teaching can give
a definitive and reasonably clear answer.

We might differentiate between the meaning of life *in general* and the meaning
of *my* life. The former is at political or community level while the latter is
at individual level.  But in the end they are weaved into each other, we cannot
really separate them.

A logical answer is "The meaning of life is to reproduce itself". This statement
is indeed true. We have a hard-wired instinct that makes us struggle for life.
There are people who live happily without asking any further questions. Blessed
are those who don't worry.

I think that people start asking further when they encounter some obstacle in
their life. Some accident, some crisis. Something that makes them consider to
escape from their frustrating or useless life by committing suicide or by using
narcotic drugs. At least for me it was like this.

At this point the logical answer didn't help me. Because it is a self-referring
statement. It works only as long as you don't ask "Why then does this
reproduction machinery of life exist?"

Douglas Adams humorously describes in his *Hitchhiker's Guide to the Galaxy* how
a distant people in a distant galaxy builds a supercomputer to find the answer
to the "Ultimate Question of Life, the Universe, and Everything". After 1,5
Million years of computing it announces the answer, which is simply 42. Adams
concludes that the problem is the question, not the answer.

Does it make sense to live? Yes, this question is somehow absurd because any
system "makes sense" only *for some observer* who looks at it from the outside.
Some time ago it was possible to ask whether a given slave made sense.  Nowadays
slavery has been abolished.  Because I am a free human, my life makes sense only
if humanity as a whole makes sense. And humanity as a whole makes sense only if
the Universe makes sense.

Life, the Universe and everything makes sense only if there is some world beyond
the visible world. This hypothetical world must be invisible to us, i.e. not
detectable by any scientific method.  It has neither a clear definition nor a
name because  *by definition* it lies beyond our thinking.  It is bigger than
anything we can conceive.

All religious systems do basically the same thing: they assume a world beyond
the :term:`visible world`. Only this assumption can give a meaning to
everything.

.. glossary::

  invisible world

    The part of reality that we cannot see directly using :term:`scientific
    method`.

  visible world

    The world that humanity can observe and analyze using :term:`scientific
    method`.
