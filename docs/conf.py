# -*- coding: utf-8 -*-

from rstgen.sphinxconf import configure
configure(globals())

project = 'Human World'
import datetime
copyright = '2018-{} Luc Saffre'.format(datetime.date.today().year)
html_title = "Human World"

extensions += ['sphinxcontrib.youtube']
extensions += ['rstgen.sphinxconf.blog']
extensions += ['rstgen.sphinxconf.complex_tables']
# extensions += ['lino.sphinxcontrib.actordoc']
# extensions += ['sphinxcontrib.taglist']
# extensions += ['sphinx.ext.inheritance_diagram']

extensions += ['rstgen.sphinxconf.sigal_image']
sigal_base_url = 'https://sigal.saffre-rumma.net'

# html_short_title = "⌘"

intersphinx_mapping['sinod'] = ('https://sinod.katoliku.ee/en/', None)
# intersphinx_mapping['hw'] = ('https://hw.saffre-rumma.net/', None)
intersphinx_mapping['cg'] = ('https://community.lino-framework.org/', None)

templates_path.append(".templates")

if html_theme == "insipid":
    html_sidebars = {
        '**' : ['languages.html',
            'globaltoc.html',
            'separator.html',
            'searchbox.html',
            'indices.html',
            'links.html'
            ]}


# extensions += ['sphinxcontrib.bibtex']
# bibtex_bibfiles = ['/home/luc/private/lucde/docs/references.bib']
# bibtex_encoding = 'utf-8'

# rst_prolog = """
# .. raw:: html
#
#     <p style="font-size: 0.8em; text-align:right;">
#     <em>This website expresses my personal opinions.
#     You can change my mind by giving your <a href="/feedback">feedback</a>.</em>
#     </p>
# """

html_use_index = True
