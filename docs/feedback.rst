========================
Your feedback is welcome
========================

Your feedback is welcome via email to **luc** (at) **saffre** (dash) **rumma** (dot) **net**

To avoid wasting our time, I ask you to read the following before writing your
email.

This website is just my private hobby project. Nobody pays me for doing it. I am
doing this by my own will, independently, with love, in my spare time, sometimes
eagerly and sometimes lazily.

I am running this website independently, but not alone. Many people contributed
their feedback and encouraged me. You might be one of them. Thank you in
advance.

Legally spoken this website is my :term:`intellectual property`: I am the
responsible publisher, the one who would get punished if I would accidentally
publish something that conflicts with the laws. See also :doc:`copyright`.

But I do not consider this website as my :term:`private property`. It wouldn't
exist without those who gave me their feedback. It is just my personal reaction
to my social interactions with other humans.

This website is not finished and may evolve as long as I live. It is not even
*designed* to get done before I die. It tries to be as transparent as possible,
but it isn't fully transparent. For example there are moments when I just lack
time or motivation for writing down everything.

If I don't respond to your mail within 3 days, there are chances that there was
a technical issue. Try sending your mail again.

Besides sending an occasional feedback, you might want to help me more
systematically. See :doc:`support`.
