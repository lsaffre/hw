===================================
Limited Liability -- Limited Profit
===================================

Here is my favourite idea for fixing the bug described in :doc:`stgg`.

Our problem is that we grant :term:`private corporations <private corporation>`
*unlimited rights* to profit while demanding only *limited responsibility* for
the risks. We grant limited *liability* without demanding limited *profit*.

My solution is theoretically simple: we just need to make sure that every
:term:`private corporation` will get `nationalized
<https://en.wikipedia.org/wiki/Nationalization>`_ when its *investment-to-profit
ratio* exceeds a certain threshold. IOW when we can say that the investors have
made enough profit.

This nationalization won't harm the corporation's activity.
We just change the recipient of the profit.
It is just a new strategy of distributing the profit.

Nationalization is imposed by the state because --of course-- the owners of an
expanding business don't want to give away their shares.  It is a simple
obligation, as obvious as a yearly tax declaration.  Failing to do it in time
simply leads to increasing fines and ultimately criminal prosecution of the
responsible managers.

It is a form of expropriation. Of course the owners don't like to get
expropriated (unless they bought for speculation, which is an edge case). But
they have no choice. Expropriation happens when some public interest becomes
more important than the private interest. The public interest of subduing the
giant is more important than the private interest of its owners.

Of course the gory details are to be regulated by clear legal rules. There must
be a balance between giving to investors what they deserve and keeping the
corporation's activity alive. For example who decides when the investors have
made "enough profit"?

Implementation note: Google, Amazon, Microsoft and Facebook are registered in
the United States. But only the parts that actually exist in the US will go to
the US. The Belgian headquarter remains private as long as the Belgian
government doesn't decide to nationalize it in turn.

Inspired (also) by
:doc:`/blog/2021/1123_1500` and
:doc:`/blog/2021/1123_2212`.


What's wrong with the idea I describe in :doc:`/lllp`? Why don't we start doing
it?

Yes, I know that reality is more complex than theory, but why don't we
:term:`repent <to repent>` in theory, at political level, i.e. decide that we
change our direction and want to start moving towards a new horizon?

An example of how things go: "A company aims to power the world for millions of
years by digging the deepest holes ever. And it utilizes a nuclear fusion
technology." (`2022-06-28 interestingengineering.com
<https://interestingengineering.com/innovation/company-power-world-dig-deepest-holes>`__)

An approach that maybe goes in the right direction: In March 2019 `OpenAI
<https://en.wikipedia.org/wiki/OpenAI>`__ shifted from nonprofit to
"capped-profit" in order to attract capital. They created OpenAI LP ("Limited
Partnership") as a hybrid of a for-profit and nonprofit. The profit is limited
to 100x: if you invest a million, you will get "only" the first hundred millions
of profit in return. (`techcrunch.com
<https://techcrunch.com/2019/03/11/openai-shifts-from-nonprofit-to-capped-profit-to-attract-capital/>`__)
