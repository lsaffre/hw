===================
The Liar King Award
===================

Here is my collection of candidates for the Liar King Award, a yet to be created
award that honours brilliant examples of propaganda, :term:`fake news` and other
ways of :doc:`cultivating lies <lies>`.

- Example of a :term:`legally correct lie`: "Wing 101 is free to use for any
  purpose and does not require a license to run." (`download page of Wing IDE
  <https://wingware.com/downloads/wing-101/7.2/binaries>`__). No, it is not free
  to use *for any purpose* because its source code is not published under a free
  software license. It is free only as in *free beer*, not as in *free speach*.
  Don't tell me that you have never heard about :term:`free software`. The whole
  page deliberately ignores a part of reality.

- :doc:`/blog/2021/0326`.

- :doc:`/blog/2021/0921`
