======================
About sexual disorders
======================

I suggest that the Church should review her usage of the word "disorder" when
speaking about :term:`queer` people.

I suggest the following definitions:

.. glossary::

  disorder

    Something that is out of order and disturbs your social life.

  extraordinarity

    Something that is out of order and does not disturb your social life.

Queer people have an *extraordinary* sexual orientation or identity. Something
with your sexual orientation or identity is out of order, it is extraordinary.
We can say that being :term:`queer` means that you are "out of order": Your
sexual orientation developed into the *wrong* direction, it makes you choose a
partner of the *wrong* sex. Where *wrong* means different from the reason why
nature developed sexual drive. It is a form of *impotence* or *disability*
because you aren't able to have biological children (unless you somehow manage
to have at least a short moment of sex with a "real" partner).

None of above means that being queer hurts other people or that it disturbs your
social life.

An extraordinary sexuality can become a disorder only if you fail to integrate
into your environment. In a homophobe society, homosexual people can develop a
disorder. Homophobia hurts innocent people. That's why :term:`homophobia` is a
:term:`sin`.

Note that pedophilia, unlike queerness, is a :term:`disorder` (see e.g.
`Pedophilia Is a Mental Health Issue. It's Still Not Treated as One
<https://www.vice.com/en/article/y3zk55/pedophilia-is-a-mental-health-issue-its-still-not-treated-as-one>`__
Shayla Love, vice.com, August 2020). Unlike queerness it can lead humans to
*hurt* other humans. There were cultures where pedophilia was considered normal:
`Pedophilia in Ancient Greece and Rome
<https://www.thecollector.com/pedophilia-ancient-greece-rome/>`__

There is no valid reason to feel guilty about being queer, and there is no valid
reason to reject queer people from society. Homosexuality is an exception
to the rule and an exciting phenomena at social, ethic and scientific level and
an enrichment for humanity.

Living as a homosexual has sometimes been difficult because many human
civilizations were afraid of the phenomenon.  Homosexuality is a challenge
because it confronts our society with the question "What to do with those who
are different?". It is only a few generations ago that humankind started to
understand that homosexuality is actually a gift to us all because it helps us
to realize the miracle of sexuality.

The :term:`UN` are working hard on this.  Article 16 of the :term:`UDHR` was
written in a time when most people didn't worry about homosexual people.  I
guess that one upon a time they will optimize the formulation to make things
clear.  This process takes time because not yet all nations are ready to embrace
homosexual people as a gift. But we are on our way. In November 2018, the UN
Human Rights Office launched a special series of articles to commemorate the
70th anniversary of the :term:`UDHR` where it addresses the issue:

https://www.ohchr.org/EN/NewsEvents/Pages/DisplayNews.aspx?NewsID=23927&LangID=E




- They are deviations from norm that fails to satisfy
  the original biological purpose of sexuality.

- They are not by themselves a source of negative psychological or social
  effects.

- There is no valid reason
  to feel guilty about being homosexual or transgender, and there is no valid
  reason to reject disordered humans from society.

- They are exceptions to the rule and a valuable phenomena at social, ethic and
  scientific level and an enrichment for humanity.

There are enough known cases of homosexual individuals who managed to marry a
human of opposite sex, had biological children and were happy ever thereafter
until the end of their life.

There are cases where a homosexual orientation could successfully be changed at
least partly into a heterosexual orientation.

Another way to manage homosexuality is to live a celibate life.  Becket Cook, in
his book `Change of Affection: A Gay Man's Incredible Story of Redemption`
(2019), shares his experience as a gay who became Christian.  This didn't make
him heterosexual but helped him to find a meaningful way of life.  See also
:doc:`/blog/2020/1210`.

Homosexual humans sometimes refrain from openly identifying as such due to
:term:`homophobia`.

Homosexual behaviour has also been documented and can be observed in non-human
animal species.


In 2017 there were still humans who seriously considered suicide as the only
solution for their sexual disorder.

In 2017 there were still humans of moral authority who publicly declared that
homosexuality is a :term:`sin`.

A sin is
