==========
About sins
==========

.. contents::
   :depth: 1
   :local:


A :term:`sin` is --theoretically-- any choice that displeases God. If it is
unavoidable, then it is not a sin because there is no choice. For example an
accident you caused is not a sin (unless you were imprudent). The problem is
that we never know whether something pleases God or not. So we can only make
assumptions.  We assume that God loves every creature and therefore never likes
when any living creature gets harmed.  So we can assume as a rule of thumb for
recognizing a sin that it *harms* somebody.  If your choice causes harm to a
creature, even a pet, then it is a sin.

We suggest the following definition:

  A :term:`sin` is a choice that causes harm to somebody.



Do Christians focus too much on sins?
=====================================

Keith Giles formulates well why many people turn away from church. He writes
(`source
<https://www.patheos.com/blogs/keithgiles/2019/08/behold-the-lamb-of-god-who-keeps-reminding-us-of-our-sins/>`__):
"It's quite disturbing to me that so many Christian churches today are
continually fixated on how sinful we all are. They constantly remind us that we
are unworthy and that our sins are filthy and that this keeps us separated from
God."  In another blog post (`Seven reasons why Jesus was not sacrificed for
your sins
<https://www.patheos.com/blogs/keithgiles/2020/08/7-reasons-why-jesus-was-not-sacrificed-for-your-sins>`__)
he goes further and writes that "One of the lynchpins of Penal Substitutionary
Atonement Theory [PSA] is that Jesus had to die on the cross to fulfill God's
requirement for a worthy sacrifice that could atone the sins of mankind once and
for all". In a theological fight against the idea of atonement he gives a list
of Bible verses that turn it *ad absurdum*. I personally think that Keith is
getting a bit too zealous there. Which is an example of how the Bible can be
interpreted in very :term:`controversial <controversial question>` ways.
Theology is an eternal journey in boggy lands!


See also
========

- :doc:`scaring`
- :doc:`/talks/cross`
- :doc:`/sc/sins`

External links
==============

Jews and Christians illustrate the :term:`original sin` in the story of Adam and
Eve in Eden where they disobey God and eat the forbidden fruit from the tree of
the knowledge of good and evil. Theologians have characterized it in many ways,
ranging from "a slight deficiency", or a "tendency toward sin yet without
collective guilt", referred to as a "sin nature", to something as drastic as
total depravity or automatic guilt of all humans through "collective guilt".
(via `Wikipedia <https://en.wikipedia.org/wiki/Original_sin>`_)

Gene Veith writes about :term:`original sin` (in `The Doctrine of Original Sin as an Essential
American Conviction
<https://www.patheos.com/blogs/geneveith/2020/12/the-doctrine-of-original-sin-as-an-essential-american-conviction/>`__):
"Among the characteristics of those religious dissidents who settled at Plymouth
was a strong belief in original sin--that is, a sense that evil is innate,
pervasive, internal, and systemic, that human beings have a tendency to
aggrandize themselves and to harm each other."
