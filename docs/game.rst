==============================
Saving the world: a human game
==============================

Reality is obviously not simple. But don't we all have these moments where we
sit with friends in a pub or in a sauna, believing honestly, at least for a
moment, that we *do* understand the world, that we *do* see what's wrong and how
to fix it?  And then we eagerly develop our :doc:`plan for saving the world
</plan>`.

Of course such ideas are **too big for me**. They are beyond what I can
reasonably expect to achieve during my lifetime. When hearing such ideas, you
may be tempted to classify them as a case of :term:`PacktSchonMalAn`.
Yes, feel free to laugh at me.
But laughing about something is not yet a solution.

See it as a game.  A typically human game.
It started with `that apple in Eden <https://www.bibleserver.com/ESV/Genesis3>`_.
There are people who understand this apple story as :doc:`the beginning of a big
problem </blog/2020/0311>`, but I prefer to read it positively: we are God's
children playing the If-I-Was-God game, trying to help our father in Heaven to
save his world. I :term:`believe <to believe>` that God loves our childlike
attempts.

Let's play the game. Let's dream.
Dreams are by nature unrealistic.
So don't be shy when your dreams sound crazy.
Formulating them might be the
needed first step into the right direction.
And if our dreams are good, God will care for making them happen.
