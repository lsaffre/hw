======================================
Stop mixing up corporations and humans
======================================

Our laws and moral systems often fail to differentiate between humans and
:term:`corporations <corporation>`.

Corporations are :term:`legal persons <legal person>` and as such they have
rights like owning things, trading with goods and services, entering into
contract with others or introducing a law suit against them.

The :term:`UDHR` (art. 17) says that "everyone has the right to own property
alone as well as in association with others". And everybody is equal before the
law, right? This idea is one of the unwritten bases of liberal capitalism. But
it is wrong. Because it adds "as well as in association with others". Every
human has the right, but not every association. The :term:`UDHR` mixes up humans
and corporations.

Another example is :term:`Fratelli Tutti`.

Corporations are not humans. They are just ideas, born out of human minds,
registered in some public database, personified as :term:`legal persons <legal
person>`. They are part of the spiritual world.

We can consider corporations as :term:`spirits <spirit>`.
A spirit is an idea that exists outside of a human brain.

Unlike humans, corporations can be incredibly powerful and are virtually immortal.
They survive the lifetime of individual humans.

Learning to make the difference is important because some of the *egoistic
instututions* we created (I call them :term:`greedy giants <greedy giant>`) are
out of control and "want" to rule the world. If we want to avoid a catastrophe,
we need to act without mercy. Every human deserves mercy, but no corporation
does.
