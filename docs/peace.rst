The Great Peace
===============

"The Great Peace towards which people of good will throughout the
centuries have inclined their hearts, of which seers and poets for
countless generations have expressed their vision, and for which from
age to age the sacred scriptures of mankind have constantly held the
promise, is now at long last within the reach of the nations."  (from
`The Promise of World Peace
<http://www.bahai.org/documents/the-universal-house-of-justice/promise-world-peace>`_)

- `Pierre Teilhard de Chardin
  <https://en.wikipedia.org/wiki/Pierre_Teilhard_de_Chardin>`__ called
  it the `Planetization of Mankind
  <http://aromagosa.easycgi.com/christianhumanism/Teilhard/stepsetization.htm>`_

- `Pope Francis <https://en.wikipedia.org/wiki/Pope_Francis>`_
  introduces it in his second encyclical letter `Laudato si'
  <https://en.wikipedia.org/wiki/Laudato_si'>`_

While the ideas of "World peace", "Great Peace" or "Kingdom of God"
are quite universally accepted by most humans, they inevitably raise
the question "Yes, but how should this world peace look like?  How to
get there?"


