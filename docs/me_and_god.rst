=====================
Welcome to Christians
=====================

When I was 17 years old, my best friend died in a traffic accident.  I mourned
about one year.  Does life make sense, does *my* life make sense, if it can end
every day without warning?
