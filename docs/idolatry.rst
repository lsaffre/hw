==============
About idolatry
==============

.. glossary::

  idolatry

    When you worship something instead of worshipping :term:`God`.

We humans understand only a fragment of the world we are living in.  Our ability
to summarize and simplify the enormous data input streams coming from the
outside world is a key feature of life itself.  The recent progress in
artificial intelligence can give us an idea of the amazing work done by our
brains in seemingly trivial situations of everyday life.

Simplifying things is vital.
You cannot operate successfully if you discuss and
doubt about every aspect of life again and again.
That's
why nations have a :term:`constitution`,
that's why companies have a :term:`mission statement`,
that's why Catholics have a :term:`creed`.

.. glossary::

  mission statement

     A concise textual description of a corporation's fundamental purpose. It
     answers the question "Why does our corporation exist?" It formulates the
     corporation's purpose for both its members and the public. -- Adapted from
     `thebalancesmb.com
     <https://www.thebalancesmb.com/mission-statement-2947996>`__ and `Wikipedia
     <https://en.wikipedia.org/wiki/Mission_statement>`__

  constitution

    The :term:`mission statement` of a nation.

  creed

    A statement of the shared beliefs of a community in the form of a fixed
    formula summarizing core tenets. -- Adapted from  `Wikipedia
    <https://en.wikipedia.org/wiki/Creed>`__

Only the top management board may review a company's mission statement.
Only the government may change a nation's constitution.
Only the pope may initiate changes in Church teachings.

For the common people these things are "holy": you must learn them by heart, you
must honour them, you must follow them, you must not touch (criticize) them.
"Holy", in this context, means "beyond any doubt" and "it makes no sense to
discuss about this".

Monotheistic religions say that *only God is holy*. (Yes, Christians have the
concept of :term:`Trinity` but that's another topic, they still classify as
monotheistic.)

And the one and only God is described as "jealous".

`Simone Weil <https://en.wikipedia.org/wiki/Simone_Weil>`__ defines idolatry
as "the error that attributes a sacred character to what is not sacred"
and describes it as "the crime that is most widespread in every time and every
country." (via [#VanceMorgan_])

"Idolatry has everything to do with thinking that you know :doc:`God's name
</talks/name_of_god>`." (`Stanley Hauerwas
<https://stanleyhauerwas.org/naming-god-the-burning-bush-the-cross-and-the-hiddenness-of-the-revealed-god/>`_)

Examples of idolatry include
:term:`nationalism`,
:term:`traditionalism <traditionalist>`,
:term:`technologism`,
:term:`egoism`,
:term:`liberalism`,
:term:`communism`,
:term:`marketism`,
:term:`capitalism`, ...

Idolatry is one of the key topics of the :term:`Good News`
Jesus says
"I am the way, the truth and the life" and
"You cannot serve two masters".

Note that Christians aren't more immune against idolatry than other people. They
have their own forms of it: :term:`Bible idolatry` and :term:`Church idolatry`.
How true! "Not everyone who says to me, 'Lord, Lord,' will enter the kingdom of
heaven" (Matthew 7:21)



.. glossary::

  nationalism

    Over-stressing the importance of a national identity.

  technologism

    Over-stressing the importance of technology.

  egoism

    Over-stressing the importance of your own person.

  liberalism

    A political and moral philosophy focused on liberty, consent of the governed
    and equality before the law.

  communism

    An ideology whose ultimate goal is the establishment of a society based on
    the :term:`communistic ideal`.

  capitalism

    An ideology whose ultimate goal is the establishment of a society based on
    the :term:`capitalistic ideal`.

  communistic ideal

    The idea that the means of production should be owned in common by all
    members of a society and not by individual private actors.

  capitalistic ideal

    The idea that the means of production should be owned by individual private
    actors and not in common by all members of a society.

    This includes the protection of private property, capital accumulation, wage
    labor, voluntary exchange, a price system and competitive markets. --
    adapted from `Wikipedia <https://en.wikipedia.org/wiki/Capitalism>`__

  marketism

    The idea that the :term:`free market` should be the ultimate authority to
    regulate all human relationships.

"We human beings are incurable idolaters, more than happy to pattern ourselves
after someone or something else rather than to take on responsibility for
ourselves. Anything can be an idol--a person, an idea, a group, whatever is
raised to such importance that all other matters, including even basic moral
guidelines and principles, fade into the background." [#VanceMorgan]_

"The best cure for the disease of idolatry that I am aware of begins with
embracing the complexity, ambiguity, uncertainty, and provisional nature of
everything that we believe and think we know. Idolatry is always an attempt to
establish certainty where it is both unwarranted and unearned."\ [#VanceMorgan]_

.. rubric:: Footnotes

.. [#VanceMorgan] `Idolatry and Abortion
   <https://www.patheos.com/blogs/freelancechristianity/idolatry-and-abortion>`__
   by Vance Morgan on Patheos, 2020-02-02).
