=================
Mistakes are good
=================

A **mistake** is when you did something wrong.  You did or decided or said
something, and afterwards you *regret* it.  Or you *failed to* do or decide or
say something that you should have done, decided or said, and now you regret
that you haven't.

A **weakness** or
**vice** is a variant of mistake that occurs repeatedly as a practice,
behaviour, or habit.

A normal and first reaction when you discover that you made (or have) a mistake
is to feel ashamed. You try to *not* tell it to anybody.  "I am not going to
show others how stupid, weak, unreliable... I am!"  You hide your mistake.   And
if you can't hide it, you explain to yourself and to others that it actually
isn't a mistake.  Such reactions shows that we tend to believe that mistakes are
*bad*.

But there is another possible type of reaction. You can say: "Oh, a mistake! How
did that happen?  What can I learn from it?  How can I avoid it next time?". You
believe that mistakes are *good*.

Of course I am not saying that mistakes are "pleasant". Unpleasant things can be
good, and pleasant things can be bad.

.. Or, if your mistake is unhidable, you *apologize*.  That word comes
   from Greek *ἀπολογέομαι* ("to speak in one's defense").
   https://en.wiktionary.org/wiki/apologize
   You admit that it was your mistake and that you are sorry, and

The problem with considering mistakes as something bad is that it "deactivates"
an important aspect of mistakes: They are a fundamental part of our learning
process. We need them to grow, to become better.

Nature shows us how important mistakes are. They are the cause of random
mutations that arise in the genome of an individual organism, which is a
requirement for evolution.

Considering mistakes as something good helps you to be honest with yourself and
to grow.  It's easier to say "Indeed, I made a mistake!" when you know that
mistakes are not a shame.

Social contacts are easier with people who discovered that mistakes are actually
a good thing. If you don't believe in forgiveness, you develop a tendency to
become defensive, combative, to justify your actions, and it can be difficult to
converse with you. (compare Nedra Glover Tawwab, via `Relationship expert on why
it's hard to talk to some people
<https://www.hindustantimes.com/lifestyle/relationships/relationship-expert-on-why-it-s-hard-to-talk-to-some-people-101661261071552.html>`__,
Hindustan Times, August 2023.

How other people formulate it:

- "Shaming yourself is not constructive. Shame is toxic. It's not how God thinks
  of you, and it's not how God wants you to think of yourself."  says
  relationship counselor Alison Cook and gives helpful strategies to "fight"
  your shame\ [#cook]_.  She chose the words "to fight it", but I guess she
  actually meant "to welcome and embrace it".

- Considering mistakes as something good is a basic message of the **deliberate
  practice** concept, which invites to "a life-long period of deliberate effort to
  improve performance in a specific domain"\ [#ericsson]_.

- Every mistake opens a new door. "Fear of making a mistake (...) undoubtedly
  causes upheaval and stress.  But as surely as a door closes, another one
  opens. The objective is not to stay mired in the loss, but to look for the new
  door that is opening.  They are always there if we learn to look for them."
  [#schwartz]_

- "Die Fehler gehören zum Leben wie der Schatten zum Licht." -- Ernst Jünger


Considering mistakes as something good is a basic element of the :term:`Gospel`.
It is summarized in the creed as "I believe in the forgiveness of sins".
Christians dare to look at their sins because they believe that God forgives
them. Yet another example of how human science confirms what Jesus has been
telling us for two thousand years (:ref:`science_confirms_gospel`).




.. rubric:: Footnotes

.. [#cook] Alison Cook, Examples of Shame and 4 Strategies to Fight It, July 2020,
   (`link <https://www.alisoncookphd.com/examples-of-shame-and-4-strategies-to-fight-it/>`__)

.. [#ericsson] K. Anders Ericsson, Ralf Th. Krampe, and Clemens Tesch-Romer. The
   Role of Deliberate Practice in the Acquisition of Expert Performance.
   Psychological Review 1993, Vol. 100. No. 3, 363-406
   (`pdf <http://graphics8.nytimes.com/images/blogs/freakonomics/pdf/DeliberatePractice(PsychologicalReview).pdf>`__).

.. [#schwartz] Mel Schwartz.  What is a Mistake? Psychology Today, May 2010
   (`link <https://www.psychologytoday.com/us/blog/shift-mind/201005/what-is-mistake>`__).
