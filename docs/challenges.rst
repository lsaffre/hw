==============================
The big challenges of humanity
==============================

- Letting money rule the world
- Let entertainment absorb human energy
- Ignoring the role of faith culture
- :doc:`lies`

Subtopics
=========

.. toctree::
    :maxdepth: 1

    lies
