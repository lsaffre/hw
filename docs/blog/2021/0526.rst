:date: 2021-05-26

=======================
Wednesday, May 26, 2021
=======================

Yesterday 19:15 pm. We left Jaani church a bit later than usual because today we
sang, for the first time, a few more songs after the "official" part of the
prayer. I had previously asked the other singers to remain if they want and to
feel free to leave at any moment, even in the middle of a song. I liked this
because it reminds the prayers in Taizé and creates an ambience of never-ending
prayer. Prayer should be a continuous background process in our everyday life.

As we left the church, which is located in the center of Tallinn, there was a
group of four girls, aged somewhere between 15 and 20, sitting on the grass,
singing songs and having obviously fun. I considered talking to them, but
withheld myself because they might misunderstand my approach as sexually
motivated.

Today 6:39 am. Google guessed right: they computed out that I'd find the following
article interesting: `More millennials follow horoscopes than believe in an
all-knowing God
<https://amp.kansascity.com/news/nation-world/national/article251638838.html>`__.
Written by Mike Stunson, published by "The Kansas City Star".

It's 6:50 am. Iiris is one of the 71000 followers who are currently watching
`Ranboo <https://www.twitch.tv/ranboolive>`__ playing "Life is Strange".

Now it's 7:50 am. Iiris left to school.

The article about millennials following horoscopes rather than believing in God
contains a `broken link
<https://www.arizonachristian.edu/wp-content/uploads/2021/05/CRC_AWVI2021_Release03_Digital_01_20210512-.pdf>`__
regarding its source, but seems to be based on a report titled `American
Worldview Inventory 2020-21
<https://www.arizonachristian.edu/culturalresearchcenter/book/>`__ published by
the `Cultural Research Center
<https://www.arizonachristian.edu/culturalresearchcenter/>`__,  a service of the
`Arizona Christian University <https://www.arizonachristian.edu>`__, who provide
"a biblically-integrated, liberal arts education equipping graduates to serve
the Lord Jesus Christ in all aspects of life, as leaders of influence and
excellence" and who "exists to educate and equip followers of Christ to
transform culture with the truth"
(https://www.arizonachristian.edu/about/mission/). What is a
"biblically-integrated, liberal arts education"?
