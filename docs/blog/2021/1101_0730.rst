:date: 2021-11-01 07:30

==================================
Interview with two boys in the bus
==================================

Monday, November 1, 2021 (07:30)

Today I went to town with the morning bus. There were two boys, obviously
classmates going to school, maybe 10 years old. I had seen them several times
before in this bus. They had previously captured my attention because of how
they talk with each other during the travel. They don't  stare at their
smartphones, they really talk with each other, alternating with short moments of
silence, where they watch the outside world. Sometimes I had captured fragments
of what they talk about: nothing special, everyday topics of ten-year-old boys.
What's interesting, is the *way* they are living these ten daily routine minutes
together.

Today I disturbed their being together. I entered into their private room. I
stepped in front of them and said:

Boys, may I ask you a question? I ask because I see you now for the third time
and I have the impression that you are smart guys. My question is this: Do you
also have the feeling that the world is currently in a big kind of a mess, with
all those crises? Or would you rather say that everything is okay and there's no
reason to worry?

One of them said: Rather that answer with the crises.

Me: And do you actually worry?

Boy: Yes, a bit. Not very much because I just got through it (*põdenesin läbi*).


..
  Me: And then there's that climate crisis as well. Have you heard about it? I
  sometimes hear older people say that climate is more important for the youngest
  generations because they will have to live on this planet when we, the older
  generation, have already passed away.

  Me: If you were the president of Estonia, or more precisely the king of the
  whole world --yes that doesn't exist, but just imagine--, what would you tell us
  to do?

  Boy: I think that something needs to get invented against this.

  Me: Against what?

  Boy 1: Against this pandemic. Or for example a *maailmakoristuspäev*.

  Boy 2: For example I heard about a project where they clean the air in big cities.

  Me: A beautiful answer. Thank you!

I agree that I am not really gifted for running such interviews.

- My question wasn't neutral. I asked "do you *also* have the feeling".

- I have acoustic difficulties to understand what people, especially young
- people, say.

- I lack experience and time for getting children to speak about "big
  questions".

Despite these issues I believe that we *should* make the effort of encouraging
children to get aware of their own opinion, which might be different from what
"everybody else" says.

There are many good teachers in Estonia who would do this job much
better than I.

Let us pray that some experienced teacher feels the call of acting after reading
this story.

Follow-up: :doc:`1103_0730`.
