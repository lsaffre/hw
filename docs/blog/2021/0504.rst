:date: 2021-05-04

===================================
The seemingly almighty corporations
===================================

Tuesday, May 4, 2021.

I read a comment `Legal, illegal, ganz egal
<https://www.mz.de/amp/panorama/legal-illegal-ganz-egal-1865356>`__ in German
newspaper *Mitteldeutsche Zeitung*.  It speaks about the work of Austrian
privacy advocate `Max Schrems <https://en.wikipedia.org/wiki/Max_Schrems>`__ and
his successful work to protect us against the `Safe Habour
<https://en.wikipedia.org/wiki/International_Safe_Harbor_Privacy_Principles>`__
and `Privacy Shield
<https://en.wikipedia.org/wiki/EU%E2%80%93US_Privacy_Shield>`__ regulations.
And it speaks about how these noble ideas remain useless because even our
politicians and public administrations happily use the digital infrastructure
provided by :term:`greedy giants <greedy giant>`.

Oh my God. So many people, even those who see the danger, seem to believe that
there is nothing we can do about it, that "they" are simply too powerful. And
actually it is so simple: we just need to tackle the disease at its roots
instead of applying symptom remedies. We just need to realize that :term:`greedy
giants <greedy giant>` are nothing but ideas, ghosts that exists only within our
own law systems.  We just need to review some basic laws that pull out their
plug, and their seemingly omnipotent power will vanish. *Though an army encamp
against me, my heart shall not fear; though war arise against me, yet I will be
confident.* (Psalm 27)

Here is an example of why symptom remedies won't help: `Loop Giveaways: Das
Riesengeschäft mit echten Followern auf Instagram
<https://www.vice.com/amp/de/article/88najz/loop-giveaways-das-riesengeschaft-mit-echten-followern-auf-instagram?__twitter_impression=true&s=09>`__.
