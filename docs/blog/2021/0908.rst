:date: 2021-09-08

================================
trustWHO -- Who can we trust?
================================

Wednesday, September 8, 2021

A friend pointed me to “trustWHO”, a documentary film directed by Lilian Franck.
(spoiler: anti-vaxxers love it).

It lasts 1h25 and is an avalanche of impressive snippets that I would classify
as high-quality journalist work. Many snippets are taken in the real world, not
constructed. These meetings obviously happened really. I guess that the
production wasn't cheap and that the producers did it mostly out of personal
motivation. I watched only the first 44 minutes because I got saturated. It
repeats the same message again and again: something very evil is going on behind
the façade of the WHO. Obvious conflicts of interests, lack of control and
transparency, and the WHO answers basically that these conflicts of interests
are unavoidable and that they are actually quite well under control. I believe
that this film contains no made-up, tweaked or fake content. The question is
whether it covers all relevant aspects of the topic, whether it reflects truth
in an acceptably neutral way.  At one point somebody says that "they"
deliberately created a H1N1 epidemic when the predicted wave didn't happen. And
nobody speaks against this claim. Se non è vero, è ben trovato.

What alarms me is that the film is obviously underdocumented on Wikipedia. There
is a page about its author `Lilian Franck
<https://en.wikipedia.org/wiki/Lilian_Franck>`__, which has one sentence about
it: "In February 2018, Franck's film trustWHO,[11] a report on the influence of
privatised industry on the World Health Organisation, was released in German
cinemas nationwide." The only source mentioned on Wikipedia is the `imdb
<https://www.imdb.com/title/tt6818554/>`__, which provides the trailer and says
that it was published in 2016.

The producer's website `OVALmedia GmbH <https://www.oval.media/de/work/>`__ seem
to be ashamed of their work, you can't find it in their portfolio. Actually
their summary is `still there <https://www.oval.media/en/steps/trustwho/>`__,
just a bit hidden, and the embedded video frame says that it is not available.
It gives, by the way, a third date of publication: 2017.

On YouTube there is a `message from April 2020
<https://www.youtube.com/watch?v=VjQGyqVN5RM>`_ by producer and co-author Robert
Cibis, who says that Vimeo deleted the film from their platform, stating that
they do not support “Videos that depict or encourage self-harm, falsely claim
that mass tragedies are hoaxes, or perpetuate false or misleading claims about
vaccine safety. He replies that “trustWHO” has been "thoroughly researched for 7
years; it has been fact-checked and approved by lawyers, experts in the medical
field and even by key executives of the WHO itself."

But undoing a publication is of course impossible. You can watch the whole film
on `conspiracydocumentaries.com
<https://www.conspiracydocumentaries.com/health-drugs/trustwho/>`__ or on
`nutritruth.org
<https://www.nutritruth.org/single-post/who-exposed-dark-corrupt-and-anti-public-health>`__.

One website publishes the same old summary in July 2021 under `New Documentary on
WHO Exposes Widespread Corruption, Massive Funding by Bill Gates
<https://childrenshealthdefense.org/defender/trustwho-documentary-who-corruption-funding-bill-gates/>`__.
I left them a comment "But TrustWHO is not a "new" documentary, it is 5 years
old! How can I trust a news article that starts with a flagrant example of
propaganda rhetorics?"

Another site, `collective-evolution.com
<https://www.collective-evolution.com/2020/04/20/vimeo-bans-documentary-exposing-big-pharmas-influence-within-the-world-health-organization/>`__
summarizes some of above --in a quite biased way-- and claims to be "the most
comprehensive investigation into both sides of the vaccine debate." Which is of
course  a symptom for non-reliability. Ironically they also advertize their new
course 'Overcoming Bias & Improving Critical Thinking.'

I guess that updating the Wikipedia article would be a waste of time, my change
would quickly get reverted because all these sources can be considered
`unreliable <https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources>`__: their
publishers have no "reputation for fact-checking and accuracy".  For a primary
research to become reliable, it must get reviewed (with a positive result) by at
least one existing reliable source. Becoming reliable requires a lot of
professional research.  Professional research means that somebody pays your
salary. Money rules the world.  Wikipedia confirms this rule.

If you like conspiracy theories, consider the fact that OVALmedia is obviously
getting quite famous among conspiracy fans. Vimeo maybe overcautiously decided
to remove it, and the producer used this move to stage a censoring story.

Anyway... these observations make me doubt whether :doc:`my plan for saving the
world </plan>` is complete. Something is missing when even a :term:`public
corporation` like the WHO can get jeopardized. Yes, a reliable world government
could manage it all. But how can we protect that government from getting
jeopardized? And  who is going to pay the salaries of all those clerks?

Okay my God, it is not me who is going to save the world! I'll leave that to
you.
