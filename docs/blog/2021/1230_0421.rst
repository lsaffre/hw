:date: 2021-12-30 04:21

===================================
Thursday, December 30, 2021 (04:21)
===================================

1:12
How can we achieve true joy and happiness?

Desmond Tutu: Joy is the reward really of seeking to give joy to others.
When you are caring, compassionate,
more concerned about the welfare of others than of your own,
you suddenly feel a warm glow in your heart.
Because you have wiped the tears from the eyes of another.

1:17

Dalai Lama to Desmond Tutu (in 2015): "Perhaps according your tradition we may
meet in Heaven in the presence of God. You as a Christian good practitioner, you
go first, you may some other day help me bring together."

https://etv.err.ee/1608431381/eesmark-leida-roomu-raskel-ajal
