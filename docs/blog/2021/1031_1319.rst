:date: 2021-10-31 13:19

=======================================
How volatile and vain human things are!
=======================================

Sunday, October 31, 2021 (13:19)

Today I could not attend a :term:`Holy Mass` because we are in :term:`Vigala`. I
also missed the third consultation meeting, which took place yesterday in
Tallinn. Looking forward to Raphael's report.

But I attended the Lutheran service in `Maarja church
<https://et.wikipedia.org/wiki/Vigala_kirik>`__. I also participated in the
*armulaud*. I always do this when I am in Vigala.  The Lutheran liturgy is quite
similar to a Holy Mass. Yes, I know that according to CIC this is not a
replacement. But for me, when I visit other people and they invite me to their
meal, it is natural to eat together with them.

Lutherans celebrate *usupuhastuspäev* today, so the sermon today was about the
day when Martin Luther published his `ninety-five theses
<https://en.wikipedia.org/wiki/Ninety-five_Theses>`__. The picture somehow
reminded me the ten "nuclei" of our questionnaire.

Note that today morning, after waking up, I had read `Unser Sonntag: Das
Tripelgebot der Liebe
<https://www.vaticannews.va/de/kirche/news/2021-10/unser-sonntag-radio-vatikan-evangelium-leo-skorczyk-oktober-20214.html>`__,
so I had received at least the Gospel and a homily.

We also visited my wife's grandparents at their grave. My wife did not attend
the service (Lutherans are more relax regarding dominical duty), she used this
hour for tidying up the grave.  After service I joined her. She had just
finished her work. She lighted two candles for her grandparents while I sang
some songs. This ceremony has become a kind of family ritual for us. First I
sang `Christe Lux Mundi <https://www.youtube.com/watch?v=BHHk3PZiiP4>`__ and `In
manus tuas <https://www.youtube.com/watch?v=qGXbpqWT6bU>`__. And then I had an
idea:  `Ach wie flüchtig, ach wie nichtig
<https://de.wikisource.org/wiki/Ach_wie_fl%C3%BCchtig,_ach_wie_nichtig>`__, a
song that I haven't sung very often during the last 20 years, I remember it from
my time in Belgium where it was being sung here and there. I had to look up the
text from Internet. I sang all the 13 verses at the grave. Here I quote only
three of them:

| 1. Ach wie flüchtig,
| Ach wie nichtig
| Ist der Menschen Leben!
| Wie ein NEBEL bald entstehet
| und auch wieder bald vergehet,
| so ist unser LEBEN, sehet!
|
| 8. Ach wie nichtig,
| Ach wie flüchtig
| Ist der Menschen Wissen!
| Der das Wort kunt prächtig führen
| Und vernünfftig discurriren,
| Muß bald alle Witz verlieren!
|
| 9. Ach wie flüchtig,
| Ach wie nichtig
| Ist der Menschen Dichten!
| Der, so Kunst hat lieb gewonnen
| Und manch schönes Werck ersonnen,
| Wird zu letzt vom Todt erronnen !

My wife and
