:date: 2021-10-10

========================
About boldness and pride
========================

Sunday, October 10, 2021

Boldness is easily perceived as arrogance or hate speech. Or both.

See :doc:`/talks/parrhesia`.
