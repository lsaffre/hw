:date: 2021-02-22

================================
Christians can't be nationalists
================================

Monday, February 22, 2021

"The United States will never be able to get to the mark of a Christian nation.
No nation on earth is capable. If it were then we would not need Jesus.  Jesus
did not come to earth to save nations; He came to save souls."  -- John D. Gee
in `Is Christian Dominionism Biblical?
<https://www.patheos.com/blogs/thoughtsofamoderncelticwanderer/2021/02/is-christian-dominionism-biblical>`__.

In above post I also heard for the first time about :term:`dominionism`.

.. glossary::

  dominionism

    A group of Christian political ideologies that seek to institute a nation
    governed by Christians based on their understandings of biblical law.
    -- from `Dominion theology <https://en.wikipedia.org/wiki/Dominion_theology>`__.

I wasn't aware that such an absurd thing this still exists.  Yes, yet another
word to describe what Christianity should *not* be mistaken for.

Nationalism is by nature not Christian.  Which doesn't mean that Christians
don't love the nation into which we happened to be born.  Of course we are
grateful and happy when we can live there in peace, and of course we worry when
we can't. And of course we should actively --whatever our talents are-- and
lovingly help our nation to become better. Loving your country, your people,
your nation is natural and human.  Nationalism is different from loving your own
nation in that it says that you shouldn't care about *other* nations.
Nationalists love their own nation more than other nations. That's not
Christian. 
