:date: 2021-12-22 12:15

====================================
How to get out of the health crisis
====================================

Wednesday, December 22, 2021 (12:15)

"As long as access to vaccines isn't favoured everywhere, variants of the virus
will continue to emerge. The fifth wave and Omicron are no surprise: they are
the result of political choices. The fact of not having allowed for a world-wide
vaccination and of having prevented the southern countries to vaccinate, has
widely favoured virus circulation and appearance of variants. Less than 5% of
the population in countries with low income and less than 50% of the world
population have received a first dose of the vaccine. Sweden has received nine
times more doses from Pfizer than all low income countries together. As long as
these vaccinal injustices persist, we won't get out of the health crisis. To get
over this pandemic, we need to revoke the patents so that the poorer countries
can produce their vaccines."

These words were spoken by Jérôme Martin, co-founder of OTMeds (Observatoire de
la transparence dans les politiques du médicament), during an interview
published by Reporterre on 2021-12-16 (`Pour en finir avec la pandémie, levons
les brevets vaccinaux
<https://reporterre.net/Pour-en-finir-avec-la-pandemie-levons-les-brevets-vaccinaux>`__)

Jérôme Martin is not alone, and his ideas are not new. On 2019-11-05 he was one
of the 39 signers of an open letter to the French government where they demand
rules to ensure transparency regarding prices and information about medical
products. (`Lettre ouverte au gouvernement sur la transparence dans les
politiques du médicament
<https://www.comede.org/lettre-ouverte-au-gouvernement-sur-la-transparence-dans-les-politiques-du-medicament/>`__)

Yet another confirmation for my to stay :ref:`unvaccinated <unvaccinated>`.

And I am writing this while having first symptoms of a potential omicron
infection (scratchy throat and dry cough). They started quite suddenly yesterday
at noon.
