:date: 2024-01-04

==================================================
What the European Meeting means to me
==================================================

Written between 2024-01-04 and 2024-01-17

When I first heard about the idea that the traditional European Meeting of the
Taizé community might come to Tallinn, I said "Hey guys, are you crazy? Estonia
is not ready for this!" And now the brothers dare to do it. And they tell me "We
will be happy to meet you, listen to your vision of society and the Churches and
collect your suggestions in areas that will contribute to providing participants
with a fruitful experience..."

The European Meeting is to me like the worm that ate Jonah's tree (`Jonah 4:7
<https://www.bibleserver.com/ESV/Jonah4%3A7>`__).

In July 2023 I started a "communion strike".  Now I realized that the word
"strike" was inappropriate. It is not a strike because I don't ask anything
concrete and I don't blame anybody particular.  I now describe my action as an
"abstention" or an "atoning sacrifice for the sins of the Church". My
`declaration <https://belglane.saffre-rumma.net/arvamus/streik/>`__ is written
in Estonian.

The trigger of my action was a pastoral letter signed by the members of the
*Council of Estonian Churches* who asked the Estonian Parliament to stop a
planned law change that would allow same-sex marriages. Quite contrastingly,
shortly before this pastoral letter, I had published `a manifest
<https://belglane.saffre-rumma.net/arvamus/kave/>`__ saying "As a Christian I
believe that we should be among the first ones to welcome this law change".  My
problem is not whether same-sex couples may marry or not, it's about whether
this question is part of the Gospel or not. I believe that writing a biased
pastoral letter about a controversial question was a systemic mistake.

.. My document was signed by 85 individuals. This small number
  shows the dramatic situation in Estonia. Both Christians and non-Christians in
  Estonia are convinced that Christianity teaches that homosexual people should
  feel ashamed about it! On the other hand, 85 was actually much more than I had
  expected. As such this manifest was an encouraging experience for me. But it
  came too late.

But the discussion about same-sex marriages was only a trigger, only the tip of
an iceberg.

I believe that we should "not fight against evil but fight for what is good".
But now I add "And we should not waste your energy for useless projects".
Estonian church institutions are useless and even counterproductive. I have
given 20 years of my life to these organizations, but now I wipe the dust from
my sandals and leave them alone. That's what I basically say with my action.

Two examples of traumatic real-life failures I had with the church in Estonia:

From September 2021 to March 2022 I worked with full-time motivation as the
Synodal Contact Person to suggest a synthesis of what people in Estonia have to
say at the Synod on Synodality. The result of this work is a 10 pages report
that summarized what I had heard during six months of active listening. I wrote
it together with 3 theologians, one Lutheran, one Anglican and one Catholic. The
document furthermore was signed by 10 individuals who declared that this was a
good synthesis. But the synodal team did not even read his report fully and sent
a synthesis to Rome that had been written by a single person and the content of
which had not been discussed. I cannot blame them because I was their appointed
leader. But I failed as their leader and quit my post quietly.

Since 2015 I had been part of a group who organized "Bible Reading Camps" for
children in Estonia. In June 2020 the leader of this group asked me to retire
from this work because I did not comply with their principles.  Indeed the
Scripture Union relies on the authority of the Bible and I am often quite
fervent in criticizing what I call :term:`biblicism`.

My allergy against :term:`biblicism` started here in Estonia somewhere around
2014. But I never perceived it as a "fight" against "evil", I rather saw it as
my contribution to help the church on her journey.

Christians in Estonia live in a bubble. Most Estonians know that Christians
follow the ten commandments, but have never heard about the beatitudes.  Most
schools in Estonia teach a picture of the Church that is obsolete by at least
500 years. Many Estonians imagine the Church as what Daniel Vaarik labelled a
"truth institution": "Truth institutions were obscenely convinced that they are
always right."  Christians in Estonia resemble more the Pharisees than the
apostles in their fervent fight for traditional moral values.  (Excerpts from
section 2 of `Synodality in Estonia
<https://hw.saffre-rumma.net/dl/2022/talgud/v18.pdf>`__)

The Meeting invites me to reconsider my verdict about Estonian church
organizations and my position within the church.

My visions and hopes for the coming year of preparation for this event:

- I guess that EELK as the most motivated group will dominate the meeting, and
  the other confessions will therefore have more difficult to join. That's
  reality in Estonia. There is currently no non-EELK community in Estonia
  organizing common prayers. The reason of being of the `laudate.ee
  <https://www.laudate.ee>`__ website is to NOT be controlled by EELK in order
  to make it easier for other confessions to join.

- Maybe the brothers manage to meet with the ministry of education in order to
  explain why the European Meeting is interesting for all schools, not only the
  Christian ones. Maybe we can get a derogation from the rule that announcements
  made by religious communities are not allowed in schools.

- Maybe the meeting can help Christians in Estonia to get out of the bubble by
  collaborating with people who don't define themselves as "Christian" and yet
  live the :term:`Gospel`: nature activists, choirs, folklore groups, artists,
  non-profit organizations, ...

My visions for the church and society in general:

- See section 3 of `Synodality in Estonia
  <https://hw.saffre-rumma.net/dl/2022/talgud/v18.pdf>`__

- :doc:`/pmpa`

- Help our political leaders to understand that usage of proprietary software in
  Estonian schools should be prohibited. Estonian laws prohibit "propaganda"
  work of religious groups in schools, but allow indoctrination by using
  proprietary software.

The 47th European Meeting of the Taizé community is a huge gift for both
Christians and non-Christians in Estonia. I am curious to see how many will
realize this.
