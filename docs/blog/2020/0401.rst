:date: 2020-04-01

========================
Wednesday, April 1, 2020
========================

"The COVID-19 crisis is also an opportunity to make us understand that if we
don't want to live more -and more severe- crises like this, we need to change
our society model. Some little cosmetic changes won't be enough. What we need is
to ban the egoistic homo oeconomicus who cares only about maximizing their
profit, and to welcome the homo postcoronavirus who is attentive and knows about
his inter-dependencies with other humans and the natural world, of which he is
just a branch among others. This birth won't be without throes. This upheaval
will have to face the conservatism of the privileged, the anguish of the
fearful, the misunderstanding of the poorly informed. (...) We observe today
that activities that are most useful to humanity (nurses, cashiers, grocers,
garbage men, bus drivers, ...) are paid the lowest salary while some other
highly paid professional activities are useless or even harmful (traders,
advertisers and other parasites)" (Alain Adriaens,
www.objecteursdecroissance.be)
