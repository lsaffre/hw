:date: 2020-02-21

==============================
Practising religion in Estonia
==============================

Friday, February 21, 2020.

Yesterday (Thursday, 20 February 2020 from 19:00-22:00) I had the honour of
speaking about faith in Estonia at a public event, together with six
representatives of other religions.

Everything was in English, as the event was organized by the magazine `Estonian
World <https://estonianworld.com/author/estonianworld/>`__.
It was the first event of
their project "Estonishing Life", a series of public events about Estonian
culture targeted at foreigners who live in Tallinn.
This first event had been `announced
<https://estonianworld.com/life/a-new-english-language-event-to-discuss-religion-in-estonia/>`__
as taking "a closer look at different religious communities and their activities
in Estonia". It took place in Vurle café, Telliskivi 60/2, I-Hoone.
The room was filled by about 50 visitors; about half of them were Estonians.

Excerpts from the `Facebook event page
<https://www.facebook.com/events/234849997514866/permalink/241660023500530/>`__:

  Irina Pärt represented the Estonian Russian Orthodox Church (she is a member of
  Nõmme Orthodox Church in Tallinn). Irina works as a senior researcher at the
  faculty of theology at the University of Tartu and has published extensively on
  the history of Old Believers and the Russian Orthodox Church. She is the author
  of two books on the subject that have been published in English: “Charisma and
  Tradition in Russian Orthodoxy” and “Old Believers, Religious Dissent and Gender
  in Russia, 1760-1850”. Irina is also a cofounder of Saint John School in
  Tallinn. Image may contain: 1 person, child, outdoor and close-up

  The Estonian Jewish community was represented by Ilja Smorgun.
  Ilja holds a PhD in information society technologies. He has been previously
  working as the usability specialist at the Estonian Information System’s
  Authority (Riigi Infosüsteemi Amet), where he was designing new digital services
  for the Estonian State Portal, eesti.ee. Currently, he is the associate
  professor of interaction design at Tallinn University and is managing the NGO
  Estonian Jewish Education Centre.

  Luc Saffre shed light on his experiences at the Estonian Lutheran church.
  Luc was born and grew up in Belgium (Eupen), and at the age of 32 married an
  Estonian woman and moved to Estonia almost 20 years ago. They have two daughters
  and live in Vana-Vigala, a small village between Tallinn and Pärnu. Luc grew up
  in a Catholic family, but here in Estonia he is more actively engaged in the
  Lutheran church. He also has good friends in Baptist and Methodist church. “I
  perceive the differences between religions and Christian theologies rather as
  enriching than separating. My faith is influenced by the spirituality of the
  Taizé community – their prayer style is my preferred one. I classify myself as a
  convinced reasonable Christian,” he says. In his daily life, Luc works as senior
  software developer in a small Estonian company owned by himself and his wife.

  Estonian Buddhist community was represented by Priit Rifk from the Buddhist
  Drikung Kagyu Ratna Shri Center in Tallinn. It is a Tibetian Buddhism centre in
  Tallinn, where regular practice evenings, meditation weekends, courses for
  beginners and longer summer retreats are organised. Various meditations taught
  at the centre originate from Drikung Kagyu ancient system of teaching. Drikung
  Center in Estonia is open to everybody who wishes to calm down one’s mind and
  develop selfless and compassionate attitude. Priit himself has been a member of
  the Drikung Kagyu for almost ten years. His interest in Buddhism began more than
  15 years ago and there were not too many Buddhist centres publicly known in
  Estonia back then. He did find Drikung Kagyu, and was really happy to have a
  chance to learn Buddhism from the ancient lineage and from those teachers who
  have deep knowledge of not only the philosophical aspect, but also the practical
  point of view. Priit considers himself a young practitioner who still has a lot
  to learn.

  The Estonian Muslim community was represented by Dr. Kazbulat Shogenov. Kazbulah
  is the president of the board at the Estonian Islamic Centre Foundation
  (Tallinn's mosque). He works as a doctor of geosciences and a research scientist
  at the Tallinn University of Technology (TalTech). Kazbulah is also the head
  coach of the Estonian Taekwondo National team and the president of the Kabardian
  Cultural Society of Estonia “Elbrus”.

  Aleksander Sarapik represented the Estonian Apostolic Orthodox Church at our
  event. He is a high priest and the development director of the Estonian Orthodox
  Church. He received his masters degree in theology from the Hellenic College
  Holy Cross Greek Orthodox School of Theology in Boston and was consecrated as a
  high priest in 2008. Sarapik also holds the military rank of a colonel
  lieutenant in the Estonian Defence Forces and has been serving as a principal
  chaplain for the Estonian Defence League since 2016. He has participated in
  military missions in Bosnia and Herzegovina twice. Furthermore, Sarapik has
  worked for the Estonian Ministry of Culture, as a coordinator of foreign
  relation for the Tiigrihüpe (Tiger Leap) programme, as well as a specialist in
  the Ministry of Finance, Ministry of Social Affairs and Ministry of Foreign
  Affairs; as a social and cultural advisor for the Tallinn City Council, and
  leading the youth collaboration project at the Estonian Red Cross. Aleksander
  Sarapik has been giving lectures on different topics since 1983 – karate, youth
  projects and defence of the country to name a few, as well as about his work in
  the church and as a chaplain. Last, but not least – Sarapik has received
  numerous honour medals and awards for his services.

  The evening started with a general overview by Ringo Ringvee on the religious
  landscape in Estonia. Ringo is a historian of religion. His academic interest is
  in the relations between the state and religions. Besides, Ringo is also a poet
  and a DJ! Back in 2011, Ringo also wrote an article in The Guardian on religious
  beliefs in Estonia: `Is Estonia really the least religious country in the world?
  <https://www.theguardian.com/commentisfree/belief/2011/sep/16/estonia-least-religious-country-world>`__


It is fun to note that the distribution of speakers was not statistically
representative.  Orthodox church was represented by two theologians (who by the
way were sitting at the left-most and the right-most seat) while all other
Christian denominations were so-to-speak on my amateur shoulders.

For me this event was a nice occasion to talk about the :term:`Good News` and to
make publicity for both Lutheran and Catholic church and --of course-- for the
Taizé prayers in Tallinn.
