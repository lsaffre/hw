:date: 2020-09-24

=============================
Why would one vote for Trump?
=============================

Thursday, September 24, 2020.

There seems to be still a number of US citizens who say openly that they are
going to vote for Trump.  How can a president like Donald Trump have a chance of
getting re-elected?   How is it possible that a person like Donald Trump is
still being discussed as a candidate?  How is such a thing possible?

My answer: Because he represents a powerful idea.

When an idea that has the unfortunate destiny of being represented by a person
like Donald Trump, and when this idea nevertheless makes it into the US
presidential election final sprint, then this idea must be *very* powerful.

The name of this powerful idea is "Better a republic than a democracy".
**Voting for Trump is a cry of despair saying "Something is wrong with our
democratic system. I prefer being ruled by a human dictator if it only protects
me from being ruled by a post-liberal system of inhuman corporations."**

Once more I say :doc:`/stgg`. And it seems that I should translate my old idea
of `credocracy <https://luc.saffre-rumma.net/reden/kredokratie.html>`__.

My above thoughts were triggered after reading on `reddit
<https://www.reddit.com/r/PoliticalHumor/comments/iymdp3/should_we_laugh_or_cry/>`__
in the r/PoliticalHumor channel:

  "Saw a woman wearing a "Vote" t-shirt. The fact that I know who she is voting
  for tells you everything about the substance of our political divide."

Of course the discussion turned into a battle. Some quotes:

- It annoys me to no end when they claim to be all about democracy and
  patriotism while playing their "wink and a nod" crap over making it more
  difficult to vote. The thing about closing so many polling sites as "cost
  saving measure" is particularly irksome. What? We can spend 3 trillion dollars
  to protect Wall Street, but we can't spend a couple of billion dollars to make
  sure our polling places are open, well manned, and stocked with supplies to
  protect our democracy?

- "We'Re a RePuBLiC NoT a DeMocRAcy" -- "Exactly. They don’t want a democracy.
  They'd be happy to never see an election again." -- "They don't want a
  Republic as it is commonly defined either. They want an authoritarian dictator
  who believes the things they believe."

- "This thread has been locked by the moderators of r/PoliticalHumor
  New comments cannot be posted."
