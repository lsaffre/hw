:date: 2019-10-14

========================
Monday, October 14, 2019
========================

An Estonian clergy wrote:

  We must not interpret the :term:`Bible` in a too narrow way. We must interpret
  it in its full scope, without reducing nor reinterpreting its message.  We
  must not remove or reinterpret the parts that seem inappropriate to modern
  humans, because that would leave only a pitiful shadow of the :term:`Holy
  Scripture`. The revelation given to Moses and the prophets is to be taken as
  it has been passed on to us. (...)
  The valid interpretation is the one Jesus taught his disciples and which those
  passed on to us, and which the church has taught throughout the centuries without
  change.  This is the right interpretation, any different interpretation is
  wrong or distorted.

  (Arne Hiob, 10 October at 20:15 on Facebook group "Kiriku sõbrad")

That's the teaching of a :term:`Bible fetishist <Bible idolatry>` (somebody who
takes the Bible for the Word of God).

Questions I'd ask to Arne:

- Does "interpretation" mean how to apply it to concrete questions like
  legislation about LGTB people, divorce, abortion?

- Which church has taught throughout centuries without any change in their
  interpretation? The Roman Catholic one? The Estonian Lutheran church?

- Do you agree that there are humans who studied the Bible thoroughly and
  who come to opposite results regarding above questions?

- Do you agree that Jesus says (Mt 10:14-15) that those who refuse to
  welcome his disciples are worse than the people of Sodom and Gomorra?

Actually I even translated these questions and asked him on that same group:

  Milline kirik on õpetanud evangeeliumit muutumatult läbi sajandite? Kas mõne
  usuõpetuse "tõlgendus" tähendab, kuidas seda õpetust rakendada konkreetsetes
  küsimustes nagu nt. Maa kuju, orjapidamise, LGTB inimeste või abielu lahutamise
  suhtes? Kas on olemas inimesi, kes jõuavad või on jõudnund nendes küsimustes
  tulemustele, mis on kiriku tulemustega vastuolus? Kas nende hulgas leidub ka
  inimesi, kes õppisid Piiblit tõsiselt? Kellel on kohtupäeval hõlpsam põli:
  "Soodoma- ja Gomorramaa" või "Need kes Jeesuse õpetust vastu ei võta"? (`Mt
  10:14-15 <http://www.piibel.net/#q=Mt%2010%3A14%2D15>`_) Kas Sõna sai lihaks või
  raamatuks? (`Jh 1:14 <http://www.piibel.net/#q=Jh%201%3A14>`_)

But don't expect me to translate his answers here.  Enough philosophy for today!
(I hope!)
