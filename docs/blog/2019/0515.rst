:date: 2019-05-15

========================
The Free Software rebels
========================

Wednesday, May 15, 2019.
I watched `a report on ARTE
<https://www.arte.tv/de/videos/077346-000-A/software-rebellen/>`__ about the
"Free Software rebels".

.. raw:: html

   <iframe allowfullscreen="true" style="transition-duration:0;transition-property:no;margin:0 auto;position:relative;display:block;background-color:#000000;" frameborder="0" scrolling="no" width="720" height="406" src="https://www.arte.tv/player/v3/index.php?json_url=https%3A%2F%2Fapi.arte.tv%2Fapi%2Fplayer%2Fv1%2Fconfig%2Fde%2F077346-000-A%3Fautostart%3D1%26lifeCycle%3D1&amp;lang=de_DE&amp;mute=0"></iframe>

Most of the "rebels" still seem to "respect" spiritual property. They just
complain when some big private knowledge owners finds yet another way of
binding humans and exploiting them until the situation gets unveiled and law
makers decide to interact.

Does really nobody see that we can simply stop this fightings?  We can simply
refuse to "respect" any form of published content as something "private". There
is no valid reason to maintain a concept which has become obsolete where
digital sharing is as natural as earth, wind and fire.

The report ends with the following question: Die Logik des Teilens durchsetzen
und dabei jeder Gemeinschaft die volle Kontrolle über ihr Wissen zurück geben,
um die Probleme des Klimawandels besser angehen zu können. Wäre das nicht die
wahre Herausforderung dieser Revolution des Teilens?
