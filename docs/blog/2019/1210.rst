:date: 2019-12-10

==========================
Tuesday, December 10, 2019
==========================

This morning I wrote a :doc:`/letter2`.  Seems that it was triggered by a Bible
sharing I had yesterday evening with Kaimo, Aivar and Kristo. It's already the
second one. Please don't misunderstand me when I call these texts "letters from
God". There is nothing supernatural going on here.  Of course these letters are
just a product of my mind, I am the legal author.  It's just me who imagines God
talking to us.
