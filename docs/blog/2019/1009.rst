:date: 2019-10-09

==========================
Wednesday, October 9, 2019
==========================

Luxembourg is the first country in the world to introduce really free public
transport. They plan to completely abolish the ticket system in 2020.


2019-10-09
https://epl.delfi.ee/uudised/kasutud-andmed-tallinn-lukkab-sadade-miljonite-piiksutamistega-kogutud-valideerimisandmed-sahtlisse?id=87686501

2019-01-30
https://www.bbc.com/worklife/article/20190128-the-cost-of-luxembourgs-free-public-transport-plan

2019-03-01
https://www.citymetric.com/transport/luxembourg-s-free-public-transport-sounds-great-it-isn-t-4458
starts with a factual mistake in the first paragraph : Estonia did not introduce
free nationwide :term:`public transport`, it's free only in Tallinn and only for
citizens of the capital, so they continue to have a full ticketing system, and
an expensive one.

2018-12-05
https://www.theguardian.com/world/2018/dec/05/luxembourg-to-become-first-country-to-make-all-public-transport-free
