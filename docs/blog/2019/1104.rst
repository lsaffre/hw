:date: 2019-11-04

========================
Monday, November 4, 2019
========================

After reading `The shack <https://en.wikipedia.org/wiki/The_Shack>`__ I
fundamentally reviewed my opinion regarding the question why Christians need to
believe that Jesus is God (see :doc:`/deity`). 
I no longer feel like :doc:`a bull in a china shop </blog/2019/1022>`.
