:date: 2023-02-21

==========================
Tuesday, February 21, 2023
==========================

Speech recognition
==================

Already in December, both my daughters had recommended me to listen to a podcast
called `The Magnus Archives
<https://youtube.com/playlist?list=PLSbuB1AyaJk8zTF3nE2KRxuixG_A5gBKJ>`__ for
when I can't sleep. My problem is that he is talking too fast for me. More
precisely my English speech recognition mechanism is too slow for him. It seems
that for falling asleep I need German podcasts.

But today I found the subtitle button and am amazed by how good speech
recognition works. I had to find out myself that the bar mentioned at minute 6
of the first episode is `The Albanach
<https://www.visitscotland.com/info/food-drink/the-albanach-p1721601>`__.

Speaking about speach recognition: The friends where I live here in Eupen have
an Alexa, and when they want to turn on the radio, they simply say "Alexa, spiel
BRF 1".  And to turn it off they say "Alexa, Stopp". Yesterday I tried "Alexa,
jetzt reicht's" and it worked...


multiple_blog_entries_per_day
