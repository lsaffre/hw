:date: 2022-05-06 15:00

===========================
Friday, May 6, 2022 (15:00)
===========================


Today, virtually nobody in the West grows up being encouraged by parents,
teachers, or society to become a dockworker, a train driver, or a utility
repairman (...). --
`Dockworkers Worldwide Are Trying to Stop Russia’s War
<https://foreignpolicy.com/2022/05/05/dockworkers-stop-russia-war-ukraine-blockades/>`__


Microsoft files are still based on the proprietary format deprecated by ISO in
2008, which is artificially complex, and not on the ISO approved standard. This
lack of respect for the ISO standard format may create issues to LibreOffice,
and is a huge obstacle for transparent interoperability.  -- The Document
Foundation, `2022-05-05
<https://blog.documentfoundation.org/blog/2022/05/05/libreoffice-733-community/>`__
