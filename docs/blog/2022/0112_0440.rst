:date: 2022-01-12 04:40

===================================
Wednesday, January 12, 2022 (04:40)
===================================

Yesterday I started to understand that my report needs a few fundamental
changes. It is still too long and not yet clear enough.  I plan to start a
separate document with information *about* our report, which formulates the
lessons we learned while elaborating it and the rules and considerations that
lead to it.

I will maybe change technology: the main document won't be a odt/pdf file but an
rst/html file (or a series of rst/html files). A printable odt/pdf document will
be generated from this only when the diocesan discernment phase has been done
(and for bigger intermediate meetings).

Discernment can never happen alone. The fact that you are convinced about
something does not mean that you are right.

When we hear about some concrete answer or formulation or plan, we have an
instinctive ("automatic") judgement regarding whether it reflects the truth.
This judgement is fundamentally binary: either "good" or "bad". This binary
judgement between good and bad is of existential importance. When you are
walking alone in a forest and suddenly hear some noise, then reacting quickly,
without rational thinking, whether to escape or to stay. Every moral question
can finally be reduced to the old binary decision: escape or stay?

For every binary judgement there is --of course, and this confirms the
binaryness-- always a third possible answer, which is "undecided" or "I do not
yet know whether this is good or bad". Answering "I don't know" does not mean
that a decision is useless. German language knows the expression  "to sit like a
rabbit in front of the snake".

Our instinctive, emotional, unreflected judgement starts as a feeling and grows
into a conviction the more we think about it. A conviction is of same nature as
a feeling, their difference are the amount of reflection and reasoning and
discussion that has been considered.  Every new detail can potentially make a
conviction "flip".

The SC will make a few fundamental things clear in its teachings about the
Gospel.

It will explain the Gospel in a new light. This will cause some fundamental
changes in certain moral or ethical rules.

A top-level change is that moral or ethical rules can never be imposed. The
Church gives directives, but leaves to each community the freedom of following
them or not. "Kannste so machen, ist dann halt kacke" (Lars Niedereichholz)

So how to formulate them? Estonia is not going to provide final answers because
these answers will need world-wide discussion and several generations of
discernment. But the big direction is clearly visible now.

Historic teachings about the Gospel and their synodal answer

Christ Saviour

Jesus saves the world from sin

You get redeemed if you believe in Jesus

guilt, repentence

Inimene on patune ja vajab päästmist

- No salvation outside the *Church* --> No salvation without the *Gospel*.

It is important to believe in the Gospel because an innate natural instinct
teaches its opposite.

In certain situations the Gospel calls us to the opposite of what an egoistic
individualistic attitude
