:date: 2022-01-27 14:33

==================================
Thursday, January 27, 2022 (14:33)
==================================


I received a very interesting response from a friend who also had spoken with
two other friends. He writes:

Unfortunately I disagree both with the form and the foundation of  :doc:`your
report </sc/draft>`.

First you say that you have also asked other persons for their feedback. But I
don't know to whom you sent it and their reactions, and whether they agree or
not. And hence I don't know whether you have really taken into account their
opinions and reactions when you wrote your report.

I also note that you just say your own personal opinions, most of which have
never been discussed in any of our meetings. Also you don't mention anywhere the
opinions, reflections and anecdotes of the other participants of our sessions.

Finally it is an extremely political presentation, which I don't share.

When reading your document, I no longer wished to participate in the synodal
meetings because, as you indicate, this is the report you plan to submit in the
name of the meetings I organized.  I refuse being associated with this text
because it reflects in no way what I feel (and probably what the other
participants felt), what has happened and what has been said and what we have
discussed. Furthermore, I understood that the document we planned to write will
not be transferred. It makes no sense to loose one's time to participate in
meetings that lead to nothing.

.. rubric:: My reactions

I am so relieved that you finally answered! I was starting to fear that you and
the two others would stop our dialogue!  Your message makes me hope that we will
remain friends who communicate also when their opinions differ. Thank you for
having done the work of formulating your reaction to my text, which is indeed
very provocative.

There is one misunderstanding: my draft is not my summary of the meetings that
you organized, it is my summary of all those to whom I managed to listen. And
also: my report won't have any priority over the other reports submitted to the
:term:`sinoditiim` until March 31st. It will be the :term:`sinoditiim`, not me
alone, who will have a big job of triaging and synthesizing all those reports.

I encourage you to continue your meetings and to introduce your own report. Keep
in mind that my report will only be one of the many reports that will be
discussed in April by the :term:`sinoditiim`. I hope that your report will be
among them.

Should I abstain from coming to your next meetings in order to not disturb you
by my presence. What do you think? If you prefer me to stay away, I will stay
away.
