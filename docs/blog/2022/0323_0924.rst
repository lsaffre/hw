:date: 2022-03-23 09:24

=============================================================
Eugen Drewermann about Putin, Covid-19, Jesus and peace
=============================================================

Wednesday, March 23, 2022 (09:24)

German church critic and peace activist  `Eugen Drewermann
<https://en.wikipedia.org/wiki/Eugen_Drewermann>`__  recently faced several
rather dramatic accusations against his person. Even if you don't speak German,
it is interesting to have a look at the non-verbal aspects of this interview
where a 82 years old man answers to every question with a calm and focussed
voice.

`Eugen Drewermann antwortet seinen Kritikern
<https://youtu.be/WMHqdemalY0>`_


If you understand German, you will be amazed by his rhetoric talent. He seems to
have a miraculous memory. Every sentence fits. His explanations are
straightforward and clear.

One of his messages is:

The only path to sustainable peace is disarmament. The NATO allies have been
doing the opposite during the last 30 years. It is no wonder that Putin ended up
doing such an unwise mistake.




..
  23:58 Das ist eine Ausgrenzung, die nur in Angst noch verständlich ist.

  In unserer Gesellschaft herrscht nur noch die Naturwissenschaft und die Medizin.
  Dann ist der Tod der Tod. Punkt.  Dann müssen wir Ärzte haben, die wie Götter in
  Weiß den Tod verhindern. Dann ist die [einzige] Hoffnung, dass wir möglichst
  lange leben. Wir können den Tod nicht einladen
