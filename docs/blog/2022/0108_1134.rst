:date: 2022-01-08 11:34

===================================
How the Synodal Church will operate
===================================

Saturday, January 8, 2022 (11:34)

Some unclassified answers to questions about :term:`apostolic governance` and
how the :term:`Synodal Church` will operate in real life.

It seems that the combination of the words "apostolic" and "government" is a
neologism. Should I call it "apostolic management"? Or rather "apostolic
government"? At the moment I seem to prefer "government", but it is used both to
"manage" projects and to "govern" bigger institutions (including the whole
church).

Most appointments are legally expressed as a contract similar to a work
contract, which is publicly visible. But there can be oral appointments as well.

The Pope will physically live at varying places where an existing community
agrees to host him.

The Synodal Church will legally be a non-profit organization that lives from
donations. The Roman Catholic church will be the first sponsor. It will accept
donations even from non-synodal organisations (though clearly stating that
accepting a donation does not mean a :term:`benediction`).

The Synodal Church will use its money partly for paying wages to its appointed
workers. It can also appoint part-time workers or volunteer workers and pay them
money for supporting their work, legally formulated as service sales or cost
refunds.

The Synodal Church will use its money also for acquiring and maintaining
material goods of all kind, including real estate objects, web servers, clothes,
food, infrastructure for distributing water, internet or electricity, roads,
cars, ships, air planes, ...

One activity of the SC will be to issue :term:`benedictions <benediction>`,
which it can revoke at will.
