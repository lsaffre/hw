:date: 2022-02-06 06:51

================================
New agenda without the vision
================================

Sunday, February 6, 2022 (06:51)

Two members of the team helped me :term:`to repent` regarding the vision in my
draft: it is no longer needed. The Pope doesn't need to do this step because
most things I imagine as a result of it do already exist. For example, the
Church has many "small research teams", just look at the Laudato Si' and
Fratelli Tutti movements, or `Stiftung Weltethos
<https://www.weltethos.org/>`__, `Wir sind Kirche
<https://www.wir-sind-kirche.de>`__ (`We are Church
<https://www.we-are-church.org>`__), `WFC
<https://www.worldfuturecouncil.org>`__. Or instead of creating yet another
organization that tries to unite all Christians, the Roman Catholic church can
work on joining the `WCC
<https://en.wikipedia.org/wiki/World_Council_of_Churches>`__.

So I plan to abandon this "vision" as the starting point. Which probably causes
big parts of my "draft report" to become pointless.

I started a new page :doc:`/sc/talgud/1`.

See :doc:`0207_0523`.

I stumbled into the following articles at Kolleegium:

- https://kolleegium.ee/jaanuar-2022/vestlusringis-koos-isa-wodekiga-traditsiooni-moiste-katoliku-kirikus/
- https://kolleegium.ee/jaanuar-2022/romano-guardini-liturgia-kui-mang/
- https://kolleegium.ee/aprill-2021/meil-pole-siin-jaadavat-linna-palverandav-jumalarahvas-katoliiklikus-vaates-ii-osa/
