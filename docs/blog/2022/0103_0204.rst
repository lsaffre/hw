:date: 2022-01-03 02:04

===============================
Monday, January 3, 2022 (02:04)
===============================

I reviewed the paragraphs (86) to (90) according to what I described on
:doc:`/blog/2021/1230_0430`. These paragraphs are probably the most prophetic
parts of my report, the most difficult to formulate, and they are the part for
which I did not yet receive much feedback.
