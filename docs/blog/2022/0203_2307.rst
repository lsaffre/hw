:date: 2022-02-03 23:07

==================================
Thursday, February 3, 2022 (23:07)
==================================

A first response to :doc:`0203_0829`:

(1) Although there may be some support for the pope being the 'peategalane'
within the western Church, for the eastern Church, he is one of a number of
patriarchs who act together synodically.

(2) This hospitality model (n.b. Supper at Emmaus) may appeal to Francis, who
made an early decision to live in the pilgrim hostel.

(3) We would be greatly helped by this 'provisional' character of 'being on a
journey with'. The sense that papal views can't undergo change has put a brake
on dialogue through the Spirit with the World.

(4) As you observe in your previous draft, the indelibility of roles like
priesthood, husband, wife, baptized Christian does not always align well with
human experience, or with the freedom in which the Spirit calls and calls again.
The Spirit may send the desert fathers into the wilderness or a pope to retire
to Castel Gandolfo an to prayer.

(5) Accounting isn't a strength of mine, but I'm well aware that the costs of
not preventing disease or environmental degradation should be accounted for in a
wholistic system.

(6) While I understand that you want to keep the definitions succinct, I've not
come across the term 'a faithful' in the singular in english before and your
definition of a sin makes no reference to God at all, so why use a 'theological'
term?

(7) The essential crunch comes when a general teaching is expressed in a more
concrete form by each individual ecclesial community and enforced by rules. Will
not this shift our attention from the Pope and his praying synodical community
to his appointee in the Roman Catholic Church and the leadership structures of
other participating churches?

(9) I wouldn't agree with the two words 'and unforgiving'.

(10) If each Christian could speak from the heart about their faith, without
checking with the apostolic creeds and ecclesiastical websites to see if their
views were orthodox, they would express their belief differently. This is a
blessing, not an obstacle. Without the flexibility of the churches, our faith
wouldn't have survived for two millenia. Witness, for instance, how important
the experience of substitutionary atonement is to evangelical Christians,
although there's not a word about it in the catholic creeds. If what we say
sound feeble and confused, its surely because we are voicing second-hand
experiences, rather than speaking from the heart. The Holy Spirit gives the
gifts of many tongues and the interpretation of them.

(12) I can imagine that the group might well omit particular examples, not only
because they will divide us, but also because we are tasked with discussing
synodality and its always the specifics that divide us because they belong to a
particular era, area or culture.

(13) We get too easily into sins, especially other peoples sins, while turning a
blind eye to the collective sin of our particular culture, nation or church. Sin
is more fundamental than sins, both in the harm that we cause collectively, but
also in the way that Sin, as St Paul taught, underlines our dependence on divine
grace for that reason is what the Exultet calls a 'happy fault'.

(14) Some of these are more than organisations: they are 'prophetic movements'
(for example the environmental movement in our time).
