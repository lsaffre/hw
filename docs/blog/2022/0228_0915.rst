:date: 2022-02-28 09:15

=================================
Empathy and loyalty isn't enough
=================================

Monday, February 28, 2022 (09:15)

Alison Cook gives quite clear definitions of the words "empathy" and "loyalty":

"Empathy is the ability to feel with another person. It’s not feeling sorry for
them. It’s stepping inside their skin and feeling what they feel. It’s an
uncanny gift. Empathy is the oxygen of healing."

"Loyalty is showing support through the bad and the good. It’s seeing the worst
in someone you love and sticking with them. At its best, loyalty is similar to
faithfulness. It’s the fabric of safety and trust.""

https://www.alisoncookphd.com/the-empathy-trap/

She then explains that an empathic and loyal person also needs courage.
