:date: 2022-03-13 04:31

===================================
First change requests to version 11
===================================

A letter to the :term:`sinoditalgutiim`.

Yesterday I released version 11 of "my" :doc:`report </sc/report>` and
received first feedback.

We are approaching our deadline
===============================

Some members of the :term:`sinoditalgutiim` accepted to be in the mailing list
because I assured them that I won't write more than one message per week. I am
afraid that I will have to repent about this because we are approaching our
deadline (March 31st). If this is getting too much for you, feel free to
silently ignore my mails or ask me to remove you from the mailing list.

To those who stay with us:

- Take care to not send any email to sinod@lino-framework.org unless you want
  everybody else to read it. You can answer privately to me by sending your
  email to luc@saffre-rumma.net. And of course you can call me on the phone or
  write instant messages any time.

- Don't be shy when giving feedback. If you disagree or feel uncomfortable with
  some formulation, then please let me know. Don't worry for me or our
  friendship. I am :ref:`resistant to advice <me.resistant>`.

- Speak spontaneously. Don't hold back your feedback until you have read the
  whole document or until you are really sure about your reaction.  I give you
  permission to contradict yourself or to change your mind while you are
  speaking. Collect your reactions while reading and send them to me when you
  get interrupted.

- In about two weeks I will ask each of you whether you agree with our document.
  I guess that there will be four classes of contributors:
  Those who disagree,
  those who agree publicly,
  those who agree incognito
  and those who are undecided.

Should we speak about burnout and depression?
=============================================

Unless the team objects, I plan to **remove item (58)** ("Several participants
witnessed the experience of having abandoned, at some point in their life...
independently of whether they are “religious” or not.")

This item was --as I imagined-- the summary of what at least three of my friends
had told me. But after asking for their confirmation I found that only two of
them confirmed my summary. And these two are not part of our team because they
are neither Estonian nor do they live in Estonia. Hence we cannot say that this
is something we have observed here in Estonia.

Should we speak about "presynodal" language?
============================================

One team member wrote: I'm not comfortable with the term ‘pre-synodal language’.
It feels to me a bit like the term ‘unamerican’ -- kind of daring anyone to
disagree with the principle of synodality. Every age has struggled with how to
express the ineffable in words. I feel that the use of ‘presynodal’ is
judgemental and divisive. I would therefore delete Step 5 (items 88-93)

I answered: Needs further discussion. I use "presynodal" (not "unsynodal") in
analogy with "`preconciliar
<https://www.merriam-webster.com/dictionary/preconciliar>`__"...

I fully agree that there is something missing. I plan to insert the following
after (65):

  People have different entry points to the Church. It is important to *also*
  welcome people who are love historic buildings and see them as a part of the
  Gospel,  who prefer a "magical" Bible, a "clerical" church or a church that
  serves as a "truth institution". And local pastors are the first "church
  officials" to welcome them. Pastors must have a freedom to choose their
  priorities when talking to "their" sheep. A synodal church will provide space
  for political diversity and unite all baptized in the Eucharist.

Thanks in advance for your prayer and -maybe- your feedback!

Luc

Created on Sunday, March 13, 2022 (04:31).
