:date: 2022-01-20 20:26

==================================
Thursday, January 20, 2022 (20:26)
==================================

Today I invited yet another person to join the :term:`sinoditalgutiim`, and of
course she would like to get a link before deciding wheter to join the team. I
promised here a link, but then wrote her:

  aitäh, et sa helistasid. Varsti saadan sulle linki, aga eilse päeva jooksul
  juhtus veel nii palju, mida ma pean seedima ja oma veebisaidil kajastama... ja
  täna tuli mulle lisaks veel kaks tööülesannet sisse, mida ma muidugi teen
  esimesena...  nii et hetkel pole mõtet, et ma sulle midagi lugeda annan. Aga
  ma ei unusta sind, saadan linki siis kui see on valmis.

Regarding priorities. Yes, my family and my salaried work have priority over my
volunteer work for the Church. I have no plans of abandoning `TIM and Lino
<https://www.saffre-rumma.net>`__. Unlike  many other software developers: `The
Great Resignation is here. What does that mean for developers?
<https://stackoverflow.blog/2022/01/10/the-great-resignation-is-here-what-does-that-mean-for-developers/>`__
