:date: 2022-02-15 12:00

===========================================
"The Church teachings are without mistakes"
===========================================

Saturday, January 15, 2022 (12:00)

I had a private meeting with a friend who told me "Luc, you have a
post-enlightenment image of the Church!" ("sul on valgustusjärgne pilt
Kristlusest"). Yes, I hope that I do! I hope that my image includes what the
Church has learned after the `Age of Enlightenment
<https://en.wikipedia.org/wiki/Age_of_Enlightenment>`__. But he didn't mean it
as a compliment, he rather seemed to say that we will never find a consensus on
my draft. He mentioned `Friedrich Schleiermacher
<https://de.wikipedia.org/wiki/Friedrich_Schleiermacher>`__ who seems to have
claimed that the Church teachings are without mistakes. Which is indeed in
flagrant opposition with my statements that mistakes are a necessary part of our
learning process and that we must realize and acknowledge them in order to
learn.

Despite our very different views our meeting was very friendly, we agreed to
remain in contact, and I later even invited him to join the
:term:`sinoditalgutiim`.
