:date: 2025-02-16

=====================================
Bishop Philippe spoke to US newspaper
=====================================

Sunday, February 16, 2025

It seems that bishop Philippe Jourdan spoke unusually clear words about the law
that finally regulated `same-sex marriage in Estonia
<https://en.m.wikinews.org/wiki/Estonian_parliament_passes_legalization_of_same-sex_marriage>`_.
The article `From 6 to 6,000 - Meet the bishop of the growing diocese of Estonia
<https://www.pillarcatholic.com/p/from-6-to-6000-meet-the-bishop-of>`__) was
published in October 2024 but I read it only now.

Here is my excerpt of what Philippe answered to their questions: "It's difficult
to talk with the government (...) because sometimes the government is not very
much in favor of our way of seeing things. (...) We work a lot together on
relations with the state, especially when there are laws on marriage or on life
that are not very good. There we go, Lutherans, Catholics, Orthodox, Baptists
and other denominations, visiting politicians giving a common witness of faith.
(...) With regards to the government, God works miracles, but we do not
(laughs). Last year there were elections and now we have the most progressive
government in this country, and they wanted to pass a gay marriage law almost
immediately. (...) [I]n the end the parliament passed this law, but it was clear
that the Christian churches are very united on this issue, that we are all in
favor of marriage as a union between a man and a woman. Certainly, we did not
achieve what we wanted, which was not allowing this law to pass, but we clearly
made known the position of the Church and that the Church defends this not
because we are conservative and old, but because we have a clear and rational
position on these points. I believe that in the long run this will bear fruit."

I said "it seems" in my first sentence because I suspect newspapers in general
and this one in particular of sometimes reflecting other people's words in a
biased way. This one is called `The Pillar
<https://en.m.wikipedia.org/wiki/The_Pillar>`__, and *nomen est omen*. Another
reason for my "it seems" is my hope that Philippe is actually more open-minded
than this. It's a common issue with faith questions that the answer is right
before you but you don't see it, and that people get you wrong when you talk
about it. Otherwise the article gives an easy-to-read introduction to the
Church in Estonia.

Yes, the topic of same-sax marriage is a frightening one because it requires
subtle but deep changes in the teachings of the Church.  Such changes cause a
lot of work and require much time and energy, and it's easy to burn one's
fingers. Software developers say `here be dragons
<https://en.wikipedia.org/wiki/Here_be_dragons>`__ when they need to change
something in a piece of complex old source code.

But there are more important topics. Rather than trying to explain why we
shouldn't call it a "marriage" when a same-sex couple lives together, you'd
better explain to our government "the need to accelerate decarbonisation and
fight climate change, the need of highly indebted countries to retain fiscal
space to invest in poverty eradication, social services and global public goods,
and the need to get back on track towards reaching crucial Sustainable
Development Goals" (as the Pontifical Academy of Social Sciences wrote last
Friday in their opening of a workshop on `Tax Justice and Solidarity
<https://www.pass.va/en/events/2025/tax_justice.html>`__). And the Vatican's
Secretary of State, in his speech at that opening, is even clearer: "The current
global tax system is no longer fit for purpose. It favours multinational
corporations, deepens social inequalities and makes it easier for the wealthiest
to consolidate their power."  (Source: `Vatikan ruft zu globaler Steuerreform
auf
<https://www.vaticannews.va/de/vatikan/news/2025-02/vatikan-aufruf-global-steuerreform-gerechtigkeit-solidritaet.html>`__)

Dear Philippe, it is true that people get afraid when they hear that things need
to change, that we need to repent. But isn't this the key point of the Gospel?
Stop being afraid. Francis gives clear signals. Your job as the bishop is to
*lead* your flock, not to *follow* a small subset of it. The Church is growing
in Estonia *despite* your mistakes, which consist in cultivating a clerical and
introvert church that is obsolete by at least 50 years. Your flock is growing
because Francis does *not* make these mistakes, because God wants the Gospel to
get announced to *everybody*.  Do not fear. Trust in the Pope and help your
flock to do so as well.



Feedback
========

Our contemporary hope for marriage
----------------------------------

Stiiv Knowers reacted to above blog post as follows:

The predominant image of marriage in the new testament is of Christ and his
bride the Church. This image depends on a cultural concept of marriage that has
disappeared in the west - of a passive and dependent bride passed from her
parental family to her husband who will take on the responsibility of caring for
her and that the bride will obey her husband.

Our contemporary hope for marriage however, is that romantic love will play a
far greater rôle than it did hitherto, and that the partners will share domestic
responsibilities and work for equal pay, with a safety net provided by the state
for women who have maternal responsibilities for children both within the
marriage and, if the marriage fails, outside it. I think that the ‘bride of
Christ’ image has very little light to shed on such a view of marriage. A
marriage of equals requires a different image - like the image of the ‘body of
Christ’ in which the cells of the body play individual and complementary parts.

It’s interesting that when the Church defines ‘traditional’ marriage today, it
speaks about ‘the union of one man with one woman’, but omits the traditional
words ‘for life’. Yet the ‘for life’ part is founded on the clear words of Jesus
recorded in the gospels. It seems to me that this suggests that the Church has
already accomodated itself to divorce and remarriage despite our Lord’s
teaching. In contrast to their clear teaching about marriage, the gospels have
nothing to say about same sex attraction.

Above all, the Church should encourage personal truthfulness as the seedbed for
discipleship. It is very wrong to encourage people to deceive themselves about
their own nature or to act out a rôle in order to fit in with an acceptable
template of what a holy lifestyle should look like. What’s needed is a
sensitivity to the many different ways in which God has created human beings and
loving support for them as they discover how to follow Christ within their
God-given nature.
