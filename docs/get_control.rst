=====================================
Get control over private corporations
=====================================

:term:`Private corporations <private corporation>` have become bigger and more
powerful than the national governments that are supposed to control them. This
is a fundamental problem for humanity because
private corporations are :doc:`big egoists <big_egoists>`.
Their hypertrophy seriously prevents us from working at the actual challenges.
We should not tolerate any longer that private corporations grow out of control.

What can we do?
=================

International laws need new measures to regulate the functioning and
collaboration of juridical persons. We need a board to formulate and maintain a
worldwide binding definition of these responsibilities. We need legislation that
enforces private corporations to assume their responsibilities.

Private corporations should be deprived of certain rights they currently benefit
from:

- The right of privacy and having secrets. See :doc:`whistleblowing`.
- The right of intellectual ownership. See :doc:`/lutsu/noip`.
