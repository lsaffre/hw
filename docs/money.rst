=======================
We need more than money
=======================

One thing to make clear is the role of money in our world. There are
two major political schools here.  The liberal school states that
money is enough for regulating our problems and consequently that
governments or other human organisations should not use any other mean
of regulation.  This includes the idea that laws may not contain any
ethical judgement.  It is time to understand that we need more than
money.  The challenge is of course *how* to do it.
