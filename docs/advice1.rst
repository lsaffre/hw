================================================
When your partner doesn't agree that he is wrong
================================================

What can you do when your partner is doing something wrong and does not change
their mind, despite your explanations?

Here is a rule of thumb that I apply -more or less successfully- in my own
marriage or in professional or amateur teams.

This rule applies only when both partners have the same degree of
responsibility.


Short answer: give in.

Because when you are in such a situation *and* remember this advice, then you
are obviously more able to detach from your emotions than your partner. Make
sure that your partner received all your explanations. If needed, tell your
partner that you give in only because he refuses to do so. But give in.

Do as your partner wants, even if you clearly feel he is wrong. Because making
him avoid the mistake would need more energy than letting things happen and
letting him learn from the mistake.

This does not mean that you subordinate to your partner. You act like this
because you decided to trust him. It is your decision.

After giving in regarding some decision, do your best (not more and not less) to
sustain your partner's decision. And as life goes on, be especially attentive to
signs that show that your partner was right, and use every little occasion to
tell him that you saw this sign, and that you continue to trust him.

And when your partner says -once again!- something that irritates you, maybe
even humiliates you before others, then your first rule is: Shut up! Keep quiet!
Let him have the last word. If needed, leave the room and have a walk and do
something else. Discuss about hot topics in private emails, not before others.

Let other team members know that certain topics are "hot" between you. There is
no need to hide this from them. Find funny summaries and stories about your past
disputes and your past reconciliations. Celebrate every little reconciliation
and report to the other team members.

Realize that your fighting just expresses your longing for self-esteem. Every
human needs the feeling of being accepted and of doing something useful. You are
the person who can give this feeling to your partner. Be generous.
