====================================
The coloured glasses behind our eyes
====================================

Humans must not look directly into the sun because this can damage our retina.
In 1912, a sun eclipse caused alone in Germany 3000 cases of damaged retinas.
(`source
<https://www.welt.de/wissenschaft/article4051648/Wenn-die-Sonne-gefaehrlich-ins-Auge-geht.html>`__)

Christian and Jewish faith says that we cannot see God directly
(`1 Timothy 6:16 <https://www.bibleserver.com/ESV/1%20Timothy6%3A16>`__).
The book Exodus describes how Moses talks with God "as with a friend, from face
to face" (`Exodus 33:11 <https://www.bibleserver.com/ESV/Exodus33%3A11>`__). A
bit later in the same chapter, Moses asks God "Let me see your face", and God
answers "No human can see my face and remain living", but then explains a plan
to "lay his hand over Moses" to protect him from seeing his face, and then to
remove his hand so that Moses can at least see his back.
(`Exodus 33:19-23 <https://www.bibleserver.com/ESV/Exodus33%3A19-23>`__)
(also inspired e.g. by `this
<https://www.bibelkommentare.de/index.php?page=qa&answer_id=379>`__)

The manifestations of :term:`rationalism` invite us to remove these glasses.

I believe that fully removing those glasses behind your eyes is impossible
because we need to make selections and apply filtering.  The amount of data
needed to digest the full reality is far bigger than any human brain can handle.
Yes, we use only some percentage of our brain power, but there *is* a definitive
physical limit.  Even if some individual human potentially manages to increase
this percentage from let's say 10 to 90 percent, the result would still be just
a fraction of what's actually needed.

We can say "let's learn to switch those glasses". Let's develop a red, a green
and a blue filter and look at everything using those three colours in turn. That
will result in a more realistic image of reality.  That's a good idea, and it is
the basic idea of multicultural politics, one of the basic strategies of the
European Union, and --believe it or not-- one of the principles of the
:term:`Synodal Church`.
