=========================
Subdue the greedy giants!
=========================

We have a bug in our legal system, in the laws that rule our "western-liberal-
capitalistic" civilisation. Many people are trying to understand the problems
humanity is facing in this era, and to suggest solutions. Here are my two cents.


.. contents::
   :depth: 1
   :local:


A screaming injustice
======================

Every human activity bears a potential risk.  No investor would dare to engage
in any bigger project if they wouldn't be protected from the risk.  The limited
liability concept makes sure that the worst case scenario for an investor is
that they loose the money they invested into that particular project. And no
wise investor puts all their capital into a single project.  This is one of the
basic principles of :term:`capitalism`.

But what happens when it turns out, sometimes decades later, that some activity
has dramatically negative effects? For example by causing health issues?
[#asbest]_ Or, more difficult to detect, indirectly contributes to complex
world-wide climate changes or pandemics, resulting in damage that goes beyond
anything we can reasonably account for? What if the damage exceeds the capital?
Who is going to pay the bill?

Answer: those who suffer. The victims. Some group of humans.
Sometimes those who made the profit are part of this group.
Though the wealthier ones usually suffer less than the poorer ones.

If the victims are lucky, they get help from their governments or some
charitable organization (all of which are :term:`public corporations <public
corporation>`).

But don't expect the corporations that made the profit to lend a helping hand!
Because that's not part of the game. Basically, and by law, it is not their
business. Their business is to be greedy egoists.
After all they are :term:`private corporations <private corporation>`.


..
  It can happen that they help (1) if they can use it for increasing their profit
  or (2) if their government *forces* them to to help, which  usually demands
  considerable administrative costs.


Who is to blame?
=================

How to change it?  Where is the bug?  Where is the :term:`sin`? Who is guilty?

I don't blame those who happen to be wealthy.  A considerable part of donations
for charitable organizations comes from wealthy individuals who read Carnegie's
`Gospel of Wealth <https://en.wikipedia.org/wiki/The_Gospel_of_Wealth>`__. There
is nothing wrong with being skillful and clever for using your opportunities and
talents. It would be wrong to *not* use them.

It is not a *conspiracy*. A conspiracy means that a group of humans "conspire"
("unite their spirits") in order to plan something.  We are standing in face of
something far bigger and far more dangerous than a conspiracy.

We cannot blame the private corporations for being greedy because that's their
reason of being. We cannot blame their managers for doing their job. If a
courageous manager would feel guilty and "waste" some capital of the owners for
"useless" purposes like "mercy" or "solidarity", this manager would soon get
replaced by somebody who "knows" their "duty".

Neither can we blame *capitalism*, money or the free market. Capitalism is
important because it provides a way to transform ideas from mere words of a
written text into money, which gives them "substance" and power.  Money turns
ideas into reality, money is a door between the :term:`invisible world` and the
:term:`visible world`.

Neither can we blame the *limited liability* concept as such. Limited liability
is important because humans should get protected from carrying more
responsibility for their mistakes than they can reasonably bear. Humans need
mercy. Mercy is an unwritten human right.

We *cannot blame anybody*. There is no culprit.  It is simply a bug in our
system. We need to fix it all together. It is a :term:`collective sin`.  Let us
recognize this sin, let us stop hiding it away,  let us confess our
:term:`collective sin`, let us repeat and realize that our way of governing the
earth is a screaming injustice.

How to fix it
=============

This bug was hidden as long as :term:`private corporations <private
corporation>` remained relatively small. The bug has become active and dangerous
only in the digital era where corporations have at their disposal new ways of
communication and data processing, giving them a chance to become more powerful
than the :term:`nation state` that controls them.

Here is my suggestion for fixing the bug: Let us subdue private corporations.
Let us make clear by law that every :term:`private corporation` will get
`nationalized <https://en.wikipedia.org/wiki/Nationalization>`_ when certain
conditions are met:

- when it has **generated enough profit** (i.e. when its investment to profit
  ratio exceeds a certain threshold),

- when it has **gone out of control** (i.e. when its size has become bigger or
  more powerful than the government who regulates it),

..
  A third condition might be when it **deserves death** for ethical reasons. We
  are allowed to "kill" a legal person without mercy. Mercy is a reserved right
  for humans, it does not apply to legal persons.

Theoretically it is easy because these are *our* laws. *We* developed
them.  As the developers of these laws, we are able and responsible for
upgrading and changing them.

:term:`Private corporations <private corporation>` are neither a :term:`natural
law` nor tangible objects, but :term:`legal persons <legal person>`, i.e. a
product of our own law systems.

Nationalization is a form of expropriation. Expropriation is justified when some
public interest becomes more important than the private interest. The public
interest of subduing a private corporation is more important than the private
interest of its owners.

Nationalization won't stop the corporation's activity. It just changes the
recipient of the profit. It is just a new strategy of distributing the profit.

There are private corporations who did an equivalent step by their own decision
and with success. For example `The Salt Lake Tribune
<https://en.wikipedia.org/wiki/The_Salt_Lake_Tribune>`__ went nonprofit in 2019
(via `Sarah Scire
<https://www.niemanlab.org/2021/11/now-nonprofit-the-salt-lake-tribune-has-achieved-something-rare-for-a-local-newspaper-financial-sustainability/>`__).

This measure is inspired by the Bible, which describes laws like the `Sabbatical
year <https://en.wikipedia.org/wiki/Shmita>`__ and the `Jubilee
<https://en.wikipedia.org/wiki/Jubilee_(biblical)>`__, designed to limit wealth
hypertrophy and balances between liberty and equality (compare `Liberty vs.
Equality <https://www.iwp.edu/articles/2019/02/25/liberty-vs-equality/>`__)


Challenges
==========

Reality, of course, is less easy than theory.

Of course owners of an expanding business won't like to get expropriated because
--of course-- nobody wants to give away what they consider their property. But
they will have no choice. Nationalization is imposed by the state.  It is a
simple obligation, as obvious as a yearly tax declaration.  Failing to do it in
time simply leads to increasing fines and ultimately criminal prosecution of the
responsible managers.

Of course the gory details are to be regulated by clear legal rules. For example
who decides when the investors have made "enough profit"? There must be a
balance between giving to investors what they deserve and keeping the
corporation's activity alive.

..
  Implementation note: Google, Amazon, Microsoft and Facebook are registered in
  the United States. But only the parts that actually exist in the US will go to
  the US. The Belgian headquarter remains private as long as the Belgian
  government doesn't decide to nationalize it in turn.


We don't want to *stop* private corporations --most of them are doing good work
most of the time--, but we must *subdue* them.  We must remind them that *we*
are the masters, not *they*.

The effect will be yet another example of `beating swords into ploughshares
<https://en.wikipedia.org/wiki/Swords_to_ploughshares>`__. Nothing new, but
still a revolution each time it happens.





Why we don't do it
==================

If the solution is theoretically simple, why don't we start doing it? Why don't
we :term:`repent <to repent>`? To "repent" in this context would mean to decide,
at least in theory, at political level, that we want to change our direction and
start moving towards a new horizon.

First obstacle: My explanations above are obviously naive and simplified.  Maybe
they are fully wrong, after all I'm suggesting a fundamental change in a complex
system. It needs feedback and help from experts. Who would pay such experts?
Definitively not a :term:`private corporation`.

Second obstacle: Most of those who understand it have no reasonable motivation
to fix it because that would deprive them from a convenient source of income.
And most of those who would benefit from it have neither the competence nor the
power to defend it.

These two obstacles form a vicious circle. But as with any vicious circle there
is hope to break out of it.  My hope is that the present call gets enough
attention so that a significant number of individual humans find the courage to
unite with those who are ready to make the first step.

My call waits here for other people --maybe you-- to take it and work on it,
with or without my help.

History of limited liability
============================

The world's first modern limited liability law was enacted by the state of New
York in 1811. The modern world is built on two centuries of industrialisation.
Much of that was built by equity finance, which is built on limited liability.
Limited liability corporations are the key to industrial capitalism.
(`economist.com <https://www.economist.com/finance-and-economics/1999/12/23/the-key-to-industrial-capitalism-limited-liability>`__,
`Wikipedia <https://en.wikipedia.org/wiki/Limited_liability>`_)

Windfall taxes
==============

An example of how governments can force private corporations to help are
windfall taxes.

A **windfall tax** is a higher tax rate on profits that result from a sudden
"windfall gain" to a particular company or industry, often as the result of a
geo-political disturbance, war or natural disaster that creates unusual spikes
in demand or interruptions to supply. (via `Wikipedia
<https://en.wikipedia.org/wiki/Windfall_tax>`__)

Example: `Financial Times: EU targets €140bn from windfall taxes on energy
companies <https://www.ft.com/content/c936d529-4223-4983-980c-0e4251ed1297>`__

Platform boards
===============

Aline Blankertz (`blog.wikimedia.de`) suggests to force the big platforms to
install "platform boards" where major decisions would happen in a transparent
way. `Wie wir gemeinsam die Macht der Plattformen bändigen
<https://blog.wikimedia.de/2024/11/01/wie-wir-gemeinsam-die-macht-der-plattformen-baendigen/>`__.

Related publications
====================

Inspired (also) by
:doc:`/blog/2021/1123_1500` and
:doc:`/blog/2021/1123_2212`.

See also :doc:`/talks/dangerous_rights`.

See also :doc:`lllp`

Footnotes
=========

.. [#asbest] As an example, consider how much profit has been generated since
  the mid 19th century thanks to the discovery of `asbestos
  <https://en.wikipedia.org/wiki/Asbestos#Industrial_era>`__ for construction and
  fireproofing and compare this to the costs generated
  after `discoverying its toxicity
  <https://en.wikipedia.org/wiki/Asbestos#Discovery_of_toxicity>`__.
  A similar example is the case of a  `Teflon factory in Rumilly (Haute-Savoie, France) <https://reporterre.net/Scandale-des-PFAS-35-ans-apres-les-dechets-de-Tefal-contaminent-toujours>`__.
