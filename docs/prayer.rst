============
About prayer
============

.. contents::
  :local:

Praying is the opposite of working. It is `a relaxation technique`_

Praying is social training.


A relaxation technique
======================

Praying is a relaxation technique.
It protects you against burnout.
It helps you to "get out of the hamster wheel".
`Relaxation techniques: Breath control helps quell errant stress response
<https://www.health.harvard.edu/mind-and-mood/relaxation-techniques-breath-control-helps-quell-errant-stress-response>`__




.. raw:: html

  <a
  href="https://commons.wikimedia.org/wiki/File:Phodopus_sungorus_-_Hamsterkraftwerk.jpg#/media/File:Phodopus_sungorus_-_Hamsterkraftwerk.jpg"><img
  src="https://upload.wikimedia.org/wikipedia/commons/e/e7/Phodopus_sungorus_-_Hamsterkraftwerk.jpg"
  width="30%" align="right" style="padding-left:1em"
  alt="Phodopus sungorus - Hamsterkraftwerk.jpg"></a><br>By <a
  href="https://de.wikipedia.org/wiki/User:Doenertier82" class="extiw"
  title="de:User:Doenertier82">Doenertier82</a>, <a
  href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons
  Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a
  href="https://commons.wikimedia.org/w/index.php?curid=642841">Link</a>





No time for prayer?
===================

Getting out of the hamster wheel is theoretically easy: simply reserve some time
for yourself.

When I invite people to come to some prayer meeting, they usually answer with a
more or less direct variant of "I am overwhelmed with projects and appointments!
Why would I add year another meeting to my calendar?"

- I have at least three prayer meetings per week in my calendar. When I add the
  time I spend for transportation, that's more than one man-day per week. And I
  don't miss this time because these hours give me back more energy than they
  require.

- Check how much time you spend for entertainment. The entertainment industry
  wants you. And they are the most efficient and wily time thieves in the world.
  When you manage to reserve some time for yourself, you usually spend it for
  useless experiences that only *pretend* to get you out of the hamster wheel. I
  am often amazed to see how much money and energy people are willing to invest
  into relaxing. The cheapest and most efficient way to relax is a prayer
  meeting. I have no proof for this claim.


Glossary
========

.. glossary::

  to pray

    To speak with :term:`God` as a child speaks with her father or mother. You
    tell God about your observations and emotions, you listen what he answers,
    and you do your best to play your personal role in God's :term:`divine
    plan`.

  prayer

    A spiritual practice that includes any form of communicating with
    :term:`God`. It can express as meditation, reading, speaking, singing,
    dancing, walking, painting, writing, building and others.  Prayer can be
    practised individually or in groups.

Quotes
======

"You will never enjoy the sweetness of a quite prayer unless you shut your mind
to all worldly desires and temporal affairs." -- `St. Norbert of Xanten
<https://en.wikipedia.org/wiki/Norbert_of_Xanten>`__ (1075--1134)

"We should always pray as if acting was useless, and act as if prayer wasn't
enough" -- `Thérèse of Lisieux
<https://en.wikipedia.org/wiki/Th%C3%A9r%C3%A8se_of_Lisieux>`__ (1873--1897)

| Kaz: "Why do gods always want to be worshipped in high places?"
| Inej: "It's men who seek grandeur. The Saints hear prayers wherever they are spoken."
| Kaz: "And answer them according to their moods?"
| Inej: "What you want and what the world needs are not always in accord, Kaz. Praying and wishing are not the same thing."
| -- Leigh Bardugo, Crooked Kingdom (p. 180)
