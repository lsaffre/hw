===========================
Capitalism versus communism
===========================


":term:`capitalism` and :term:`communism` are opposing political and economic
ideologies that never go together." -- Adapted from `differencebetween.net
<http://www.differencebetween.net/miscellaneous/difference-between-communism-and-capitalism>`__

"Great developers share a lot, e.g. by writing a blog.  For many top developers,
their sharing mindset came before their success, and was the direct cause of it,
not the result of it." -- Adapted from `The most successful developers share
more than they take
<https://stackoverflow.blog/2020/05/14/the-most-successful-developers-share-more-than-they-take/?utm_source=Iterable&utm_medium=email&utm_campaign=the_overflow_newsletter>`__
(Ben James, 2020-05-14)
