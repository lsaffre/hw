================
About traditions
================

Traditions are good and important. They are more than a collection of unwritten
rules that you must follow if you belong to some culture. They are the memory of
a culture.

Traditions are enforced "automatically" or by "nature itself". Many corporations
act as carriers of traditions, but none of them is a unique actor.  Unlike laws
(written rules controlled and enforced by a government) they have no unique
corporation that represents them.

These observations can lead humans to believe that traditions are divine, made
by God.  But I don't agree with this. Traditions are made by humans, not by God.
No tradition is eternal. (TODO: give arguments)

While traditions can last much longer than human-made laws, they are not static.
(TODO: give examples)

Don't cling to a mistake just because you spent a lot of time to make it
-- `Aubrey De Grey <https://en.wikipedia.org/wiki/Aubrey_de_Grey>`_


.. glossary::

  traditionalism

    An attitude of over-emphasizing the value of traditions and not wanting them
    to change.

  traditionalist

    Related to :term:`traditionalism`.

  conservatism

    An attitude of clinging to the past, wanting to conserve old "traditional"
    values.

  conservatist

    Related to :term:`conservatism`.

.. glossary::

  progressivism

    An attitude of under-estimating the value of traditions.
    Promoting changes in a system without considering the dangers they imply.

  progressivist

    Related to :term:`progressivist`.

  Church idolatry

    When you worship the :term:`Church` instead of worshipping :term:`God`.

  rationalism

    An attitude of preferring to base your decisions on reason and
    :term:`scientific evidence` rather than on :term:`faith` or emotional
    response. 
