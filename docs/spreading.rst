==========================
About spreading the Gospel
==========================

When you believe to have found a solution for a problem that preoccupies
humankind as a whole, then of course you want to propagate your solution.

Jesus asked his disciples to spread his teachings to all peoples.

Jesus "proclaimed the gospel of God", he said "The time is fulfilled" and "The kingdom of God has come near".
He told people to "Repent" and to "believe this good news".
(`Mk 1:14-15 <https://www.bibleserver.com/ESV/Mark1%3A14-15>`__).

.. glossary::

  evangelism

    The act of spreading the Gospel

Some quotes I liked:

  Evangelism is not a *result of* spiritual
  maturity, evangelism is a *pathway to* spiritual maturity.
  -- Ben Ellis (:doc:`/blog/2020/0419`):
