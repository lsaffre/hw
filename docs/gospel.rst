=======================
About the word "Gospel"
=======================

This page contains linguistic considerations about the word "Gospel".  For the
message of Christianity see :doc:`/talks/good_news`.

The word **Gospel** is the English translation of the greek term *εὐαγγέλιον*
(euangélion).

Biblical authors used this expression to designate Jesus' teachings as a whole.
Jesus "proclaimed the gospel of God". He said "The time is fulfilled" and "The
kingdom of God has come near". He told people to "Repent" and to "believe this
good news" (`Mk 1:14-15 <https://www.bibleserver.com/ESV/Mark1%3A14-15>`__).

When the Bible was written down, the word *εὐαγγέλιον* designated an
**announcement of victory**.  Wars were still quite common at that time,
disputes between two political leaders used to be settled by sending troops of
soldiers into battles. These battles were followed with more concern and
excitement than football matches because their outcome had quite existential
consequences for the people. People had a vivid picture when they heard
*εὐαγγέλιον*.

Christians later used the word *Gospel* also to designate the texts about
Jesus's life\ [#oxford]_. The difference is usually clear from the context.

.. glossary::

  Good News

    A more explicit word for the :term:`Gospel`.

  Gospel (literal genre)

    A text that describes Jesus' life, death and resurrection. Usually one of
    the four books of this genre that are part of the :term:`Bible` (Mark,
    Matthew, Luke and John).


The English Wikipedia has `a lot of talk
<https://en.wikipedia.org/wiki/Talk:Gospel>`__ about these words:

- `The Gospel <https://en.wikipedia.org/wiki/The_gospel>`__ (violates the
  general Wikipedia rule of avoiding `definite and indefinite articles
  <https://en.wikipedia.org/wiki/Wikipedia:Article_titles#Article_title_format>`__).
- `Gospel <https://en.wikipedia.org/wiki/gospel>`__
- `Gospel (disambiguation) <https://en.wikipedia.org/wiki/Gospel_(disambiguation)>`__

I would suggest:

- rename existing **Gospel** to **Gospel (literal genre)**
- rename existing **The Gospel** to **Gospel (message)**
- create a redirect from **Gospel** to **Gospel (disambiguation)**
- tidy up

Another topic is that `Good News
<https://en.wikipedia.org/wiki/Good_News>`__ redirects to `Good news
<https://en.wikipedia.org/wiki/Good_news>`__, which mentions `The Gospel
<https://en.wikipedia.org/wiki/The_gospel>`__ somewhere near the end under
"Other uses". Quite offending.


Nijay Gupta suggests at least three "dimensions" of what Christians mean when
they talk about "the Gospel": It is (1) the person of Jesus Christ and/or (2)
the Christian world view and/or (3) the mission of Christianity.\ [#gupta]_

.. rubric:: Footnotes

.. [#oxford] It "originally meant the Christian message itself, but in the 2nd
             century it came to be used for the books in which the message was set out." ---
             Cross & Livingstone (2005). The Oxford Dictionary of the Christian Church.

.. [#gupta]  Nijay Gupta in `Why Is It So Hard to Define “The Gospel”? A Short
  Reflection and a Case Study
  <https://www.patheos.com/blogs/cruxsola/2020/04/why-is-it-so-hard-to-define-the-gospel-a-short-reflection-and-a-case-study/>`__,
