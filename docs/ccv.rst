==========================
Continuous Cascaded Voting
==========================

I suggest a change in our implementation of democracy.  I call it "Continuous
Cascaded Voting" because it is a change in the voting system with two main
aspects:

- Voting becomes *continuous*: every voter can vote or change their vote at any
  time.  You simply log in at the parliament's website, select some citizen as
  getting your voice, and hit submit.

- Voting becomes *cascaded* : citizens are invited to vote for somebody they
  know personally and consider trustworthy, independently of whether that person
  is a candidate for the voted seat. The vote of your friend will then count
  twice, their voice plus your voice. This idea is not new, it has also been
  described as `Liquid democracy
  <https://en.wikipedia.org/wiki/Liquid_democracy>`__.

Explanations
============

The system is not limited to parliaments. It can also be used for municipalities
or for a local scout group. The **board of deciders** is called the parliament,
city council or committee, depending on the context.

Voting remains private because every vote is considered confidential data. You
cannot see *who* voted for you, but you can see *how many* people voted for you.
When you know that 1000 people voted for you, you know that your own vote will
have a weight of 1001 voices.

There are no "candidates".  Every member of the group is a potential candidate.
If you want to work as a board member, you must simply convince your friends to
vote for you.  Electoral campaigns will become useless, you will rather focus
continuously on your personal relationships and what you say to other people.

When you have a certain number of voices, you are asked whether you agree to
take a seat in the board. If you have enough voices but do not want to actually
work as a board member, you simply select some other member.

As a board member you can potentially get notified at any moment that you lost
your seat because some other member has collected more voices than you and will
therefore replace you. The replacement itself will become effective only after a
configurable amount of time in order to avoid excessive fluctuation. For
example, a parliament seat could be configured to last at least two years, and
even after these two years you would still have a notice period of three months.
And if you are lucky, your voices would even recover before the end of your
notice period.


Advantages
==========

This system **reduces the risk of having public opinion manipulated** by lobby
work, sponsoring, publicity or propaganda because citizens are encouraged to
vote for people they know personally.

This system **brings a natural solution to situations where a board of deciders
is locked because of a lack of consensus**, e.g. when a :term:`controversial
question` splits opinions into to camps. As soon as the lock becomes tangible,
people will start to discuss in public, think, and change their mind about whom
to vote, which will lead to changes in the composition of the board of deciders.

What's next?
==============

Estonia might be a pioneer for developing this idea because the country is
relatively small and has relatively good IT infrastructure.

The system could be tested in all kinds of associations if there would be a
website where they can configure their situation and register their members.
