=========================
About names and languages
=========================

We humans love to give names to everything around us. We love giving names to
every species of living beings, to the places we inhabit, to the objects and
products we invent and produce, to our activities, and even to our ideas, myths,
projects and corporations.

Our capacity and desire of giving names was an important step in the history of
Mankind.  Yumal Harari (Sapiens) describes it as the cognitive revolution. The
Bible describes it as follows:

  Now out of the ground the Lord God had formed every beast of the field and
  every bird of the heavens and brought them to the man to see what he would
  call them. And whatever the man called every living creature, that was its
  name. (`Genesis 2:19 <https://www.bibleserver.com/ESV/Genesis2%3A19>`__)

God does not assign names to concepts, he leaves that job to us.

Living and operating together as humans needs communication. Communication needs
a human language. Human language is mostly just a vocabulary. A vocabulary is a
set of definitions. A definition is a name that has been assigned to a concept
(it gives a meaning to a name).

A good definition provides a complete, concise, courteous, clear, concrete,
consistent, correct and coherent\ [#7c]_ description of its meaning. It must be
concise because all participants of a dialogue need to be able to re­member it.
Using unclear, vague, incomplete or incoherent definitions can promote disorder.

`Plain language <https://en.wikipedia.org/wiki/Plain_language>`_ is the art of
explaining things so that your "audience can find what they need, understand
what they find the first time they read or hear it, and use what they find to
meet their needs."\ [#plain]_

Different contexts or audiences can have different vocabulary. For example the
word "heart" has a different definition when surgeons speak about their work
than when lovers speak about their emotions. Humans are able to switch contexts:
a surgeon is able to understand a text message "My heart is worried" from her
husband or his wife even when it arrives while he or she is working with
colleagues.


Diversity of languages
======================

There are many different languages on Earth, which can become a political a
challenge when a group of humans migrates to a place where they speak another
language.

Human languages are translatable to each other, and humans are able to learn a
new language.

For example, imagine a family with a linguistic particularity.  They would use
the word 'ragle' to designate that thing we use to gather around when having
meal. They would be surprised or even offended when you would call their 'ragle'
a table. They find that 'Children, meal is ready, come to *ragle*!' sounds much
more beautiful.  If some circumstance would require you to live in that family
for a few years, you would say "Why not" and eventually get used to that little
oddity in their vocabulary.

There is no "perfect" language. Every language is just an incomplete reflection
of reality. A language is obviously influenced by the :term:`world view` of its
speakers. Every human language has its historically grown oddities and bugs,
which can lead to misunderstandings.

For example, imagine you would have to live in a family where they  gave
different names to each individual chair, depending on the time of the day.  In
the morning they call it a "blair", in the evening a "plair". Or another family
where they use a single word "thing" for everything: for the table, the door,
the chairs, the forks and the knives. Or yet another family where they decided
to not speak at all about tables, chairs, doors, knives and forks. You would
perceive these models as unrealistic or unpractical.

Human communication is more complex than plain language.  For example, imagine
a family where they would have a rule saying that I must knock three times at
the kitchen door and praise it by saying "What a beautiful door you are" before
sitting down at their table.

Languages are more or less adapted to a given usage field. When you live in a
village in northern Alaska,  the `Inuit <https://en.wikipedia.org/wiki/Inuit>`__
language  is probably more practical than English.


Some Eskimo languages like Yupik and Inuit have

https://en.wikipedia.org/wiki/Eskimo_words_for_snow
It seems that there are 46 Somali words for "camel"
http://itre.cis.upenn.edu/~myl/languagelog/archives/000457.html

Note that the opposite direction (whether a language influences the world view
of its speakers) is a controversial hypothesis known as `Whorfianism
<https://en.wikipedia.org/wiki/Linguistic_relativity>`__.

Every computer user believes to know what a "folder" is. But actually the
"folders" on your computer  are more like "directories" than like "folders":
https://itsfoss.com/folder-directory-linux/


An example to illustrate that names can represent a lot of money: The name
"Supermac" given to an Irish fast-food chain had triggered a long-lasting legal
battle because their big concurrent McDonald's sued them  for trademark
infringement and claimed the name would confuse consumers in European markets.
In January 2019, the European Union Intellectual Property Office (EUIPO) ruled
in Supermac's favor.  Irish Independent newspaper called a "David vs. Goliath"
victory. (Source: `independent.ie
<https://www.independent.ie/irish-news/tasty-result-for-supermacs-with-david-and-goliath-big-mac-win-37716672.html>`__
via `Wikipedia
<https://en.wikipedia.org/wiki/Big_Mac#2019_EUIPO_trademark_revocation>`__)

.. rubric:: Footnotes

.. [#7c] 7C of communication see e.g. https://www.managementstudyguide.com/seven-cs-of-effective-communication.htm

.. [#plain] https://www.plainlanguage.gov/about/definitions/
