=====================
How to save the world
=====================

Here are my suggestions for saving the world. Please don't follow them blindly.
I think that these steps are needed if we want to live in sustainable peace on
this planet. I honestly :term:`believe <to believe>` in these ideas, but I am
just another little amateur philosopher trying to help God with saving the
world. All this is :doc:`just a game </game>`, feel free to laugh at me. And
then let my ideas inspire you. Maybe you are the one who is going to realize
something that is going to actually save the world. And of course I'd love to
hear your :doc:`feedback </feedback>`.

If the world were an IT system and I were one of its :term:`key users <key
user>`, then I would submit the following *change requests*.

- copyright -- :doc:`/lutsu/index`
- liability  -- :doc:`/stgg` / :doc:`/lllp`
- transparency -- :doc:`/pmpa`
- democracy -- :doc:`/ccv`
- solidarity -- :doc:`/basic_income`
- peace -- :doc:`/talks/nato_ukraine`
- generally -- :doc:`mixup` / :doc:`ptsf`



.. toctree::
    :maxdepth: 1
    :hidden:

    /lutsu/index
    /stgg
    /lllp
    /pmpa
    /ccv
    /basic_income
    /mixup
    /ptsf

.. Backlog

  - Ensure that public administrations cannot enter into secret agreements.
    :doc:`pmpa`.

  - Return control over private corporations back to the public institutions that
    create them. :doc:`stgg` and :doc:`lllp`.

  - Review copyright laws to abolish the notion of :term:`intellectual property`.
    Consider all published content as :term:`common property`. This will reduce
    fake news, propaganda and disinformation (see :doc:`lies`). This will open the
    door to a new reformed education system. It will remove the Sisyphean task of
    protecting "intellectual property" from our shoulders. :doc:`/lutsu/index`.

  - Introduce a world-wide Unconditional Basic Income.
    This will increase general health condition and reduce forced work and other
    forms of modern slavery. :doc:`basic_income`.

  - Declare all natural resources as :term:`common property`.

  - Create a world government for managing common
    property (including published content and natural resources)
    in a sustainable way.

  - :doc:`/ccv`
