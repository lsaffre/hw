==========
Principles
==========

We **support** the ideas of

- democratic societies
- multicultural governments
- international cooperation
- civil rights

We are **careful** about certain ideas:

- The freedom of speech and the press is important but must not
  pervert into irresponsible speech. (responsibility of speech)
  
- The free market must not pervert into *laissez-faire* economic
  liberalism.
  
We **condemn** principles like
  
- hereditary privilege and absolute monarchy

