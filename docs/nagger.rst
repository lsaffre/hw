=============
Nagger
=============

Some perceive me as a nagger, and they are partly right. *to nag* means to
continuously remind or complain (to someone) in an annoying way, often about
insignificant or unnecessary matters (source: `wiktionary
<https://en.wiktionary.org/wiki/nag>`__).

You might perceive my matters as insignificant or unnecessary, but I don't.

Religion

.. toctree::
    :maxdepth: 1


Politics

.. toctree::
    :maxdepth: 1
