======================
About miracles
======================

What do I mean when I speak about :term:`miracle`?

.. glossary::

  miracle

    An observation that we perceive as positive and good without being able to
    fully explain its causes. An observation for which there is no
    :term:`scientific <science>` explanation.

I sometimes hear Christians refer to miracles as "super-natural". I perceive
this as an irritating linguistic mistake. I believe that miracles are natural.
Saying that a miracle happened does not mean you are :term:`superstitious
<superstition>`.

Our world is full of :term:`miracles <miracle>`. Miracles are something normal
because the world is far more complex than we can grasp.  There are constantly
happening things that we cannot explain. They are unexplainable simply because
we lack information about the causes. They don't need to be "super-natural" for
this. Miracles don't need to be super-natural.

Paul Oldenburg tells a story about a man in WW2 who was convicted to get shot
after having dug his own grave.
https://www.youtube.com/watch?v=zhWe69oh7ec
