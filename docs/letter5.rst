===================
You and your bible!
===================

My dear children,

the :term:`Bible` is a very beautiful image of me.
It can help you to speak about me in a common language.
It is a powerful tool.
It is important for cultivating my kingdom and for explaining it to all humans.
Like every Holy Scripture it contains wisdom that has been developed and selected by many
generations of humans.
Some of you consecrate their whole life just to spread the Gospel of the Bible.
I am loving it.

You want to be close to me, you long for me, you have a deep desire to
understand my plans.  Your longing becomes visible in Holy Scriptures, national
constitutions and other systems for storing and sharing your common
knowledge. I am loving it.

We both know that your desire cannot get satisfied.
You want to understand my plans, but you don't.
Your brain is simply too small, it isn't able to store a full image of reality.
That wasn't possible when I created you. My apologies for the inconveniences.

You might perceive this limitation as frustrating
and waste your time trying to get around it.

But actually it is easy to live with this limitation:
simply follow *my word for you*.
Which includes that you refrain from worshipping *your image of me*.
It's not only easy, it is required.
There is no other way to achieve peace on Earth.

When I say "my word for you", please don't think that I am talking about the
Bible or any other Holy Scripture.
And don't think I am addressing only the religious among you.
Those who don't follow any religion can be more eager
in worshipping their image of me than those who follow some religion.

Many among you hear that the Bible is the "Word of God" and conclude that
it is a law book.
What a nonsense!
My word is much more than what you have managed to write down in your scriptures.
I talk to you directly in your heart,
your emotions, your thoughts and your inspirations.

Believe in me, and don't believe that your scriptures are all I have to tell you.
Your scriptures are written in human language.
I am eternal while human language changes constantly.
Your scriptures are not fool-proof.
They are not a user manual to life.
They are not a collection of solutions for your everyday life problems.

Your scriptures are beautiful images of me, and you need images to worship me.
But you need to learn how to use them properly and
how to make sure to worship *me*, not your image!

The general rule of thumb is easy:
imagine that I am the father and that all humans are my beloved children.


..
  It is actually easy to live with the limitation of not being divine, but it can
  be difficult to realize that it is easy. Your system architecture, your whole
  being, is based on your subconscious assumption that your image of reality is
  accurate.

  Reality often shows you that your image of it is not perfect.
  Your instinctive reaction is to ignore such signs.
  And indeed, that's the meaning of instincts:
  you can safely ignore the exceptions. At least when they confirm the rule.

  It can be painful to experience differences between reality and your image of
  it. Some of you love to use drugs to escape temporarily into an imaginary world
  where reality indeed corresponds to their image of it.  But this world actually
  does not exist.  There is no way to escape from reality.

  Sometimes your cultivated image of reality needs a fundamental change.
  These changes usually last generations and you usually recognize them only afterwards.

  There is no way to avoid hurting each other. Your habits, behaviours and
  strategies can be more offensive or more polite, they can reduce or increase the
  danger of hurting others, but in the end there is no way to completely avoid
  hurting others.

  That's why it is important to always be ready to forgive.

  When something went wrong and you hurt each other, there is no way but to
  forgive.  Forgiving starts with realizing your responsibility for *your*
  behaviour and your non-responsibility for *the other's* behaviour.

  My dear children, never give up loving those who hurt you, never give up showing
  your love for those whose image of me differs from yours. Enjoy the differences.

Your father in Heaven.


(September 2020)
