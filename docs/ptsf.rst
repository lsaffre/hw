===============================
Public transport should be free
===============================

I like the idea that :term:`public transport` should be free of charge for its
users, i.e. fully paid by the government.


.. contents::
   :depth: 1
   :local:


TODO:

- :doc:`/blog/2021/0831`
- :doc:`/blog/2019/1009`
- :doc:`/blog/2019/0915`
- https://www.unikims.de/blog/autoverkehr-kostet-die-kommunen :
  Der Autoverkehr kostet die Kommunen das Dreifache des ÖPNV und der Radverkehr
  erhält die geringsten Zuschüsse
