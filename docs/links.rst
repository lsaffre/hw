=====
Links
=====

Websites
========

.. glossary::

  PsychologyToday

    https://www.psychologytoday.com

    "Our goal is to constantly improve the service for potential clients, so
    they can be shown--as simply as possible--the alternatives open to them in
    their time of need." (`About <https://www.psychologytoday.com/us/about>`__)

    Site run by `Sussex Directories Inc. <https://sussexdirectories.com>`__ ("We
    provide directories that link professionals to potential clients in America,
    Canada and Europe.")

  CatholicAnswers

    https://www.catholic.com

    Looks as if it was under authority of the Vatican but isn't.


Short films
===========

.. glossary::

  FreedomWithin

    Short film `La Liberté de l'Interieur - Freedom Within <https://www.globalshortfilmnetwork.com/libert-linterieur-freedom-within-franais-p-261-c-8_28_121.html>`__
    (USA, 2006) by `Eric Albertson (1937--2009) <https://www.imdb.com/name/nm0016772/>`__

    Pages referring to this:

    .. refstothis::

      FreedomWithin

  ShortFilmUnspoken

    `UNSPOKEN (2018) - Award Winning Short Film About Depression
    <https://youtu.be/k269NkuKK7Y>`__

  PumbaTimon

    Watch `Pumba and Timon talking about the stars
    <https://www.youtube.com/watch?v=QZDk1cbKp7s>`__.

    | Pumbaa: Timon, ever wonder what those sparkly dots are up there?
    | Timon: Pumbaa, I don't wonder; I know.
    | Pumbaa: Oh. What are they?
    | Timon: They're fireflies. Fireflies that, uh... got stuck up on that big bluish-black thing.
    | Pumbaa: Oh, gee. I always thought they were balls of gas burning billions of miles away.
    | Timon: Pumbaa, with you, everything's gas.


Speeches
========

.. glossary::

  The Gospel-Shaped Life

    Tim Keller speaks about `The Gospel-Shaped Life <https://www.youtube.com/watch?v=-mu_CLg2Nfo>`__
    at The Gospel Coalition's
    New England 2012 Regional Conference
    titled "The Gospel-Shaped Life." (Plenary 1).
    49 minutes.

    Excerpts: The Gospel is news, not advice.  Advice is counsel that you get to
    help you get something accomplished. News is a report that something has
    been accomplished for you, it's already happened in history, and you must
    response to it. Essentially all religions besides Christianity (and to a
    great degree a big part of Christianity is formulated this way, though it
    shouldn't be) are all advice. Christianity is news, Gospel, Good News.
    Every other religion was founded by somebody, by a prophet or a sage or a
    figure, who came and said "Here is the way to find God". Only Christianity
    was founded by a man who said "I am God coming to find you".



Articles
========

.. glossary::

  Barlow202108

    2021-08-23 David H. Barlow, Andrew J. Curreri, Lauren S. Woodard,
    `Neuroticism and Disorders of Emotion: A New Synthesis
    <https://journals.sagepub.com/doi/abs/10.1177/09637214211030253>`__
    via `A New Way of Treating Anxiety, Depression, and Trauma
    <https://www.psychologytoday.com/us/blog/finding-new-home/202111/new-way-treating-anxiety-depression-and-trauma>`__.

  HaasMortensen201606

    `The Secrets of Great Teamwork
    <https://hbr.org/2016/06/the-secrets-of-great-teamwork>`__
    by Martine Haas and Mark Mortensen.

    Abstract: Collaboration has become more complex, but success still depends
    on the fundamentals. Today's teams are different from the teams of the past:
    They're far more diverse, dispersed, digital, and dynamic (with frequent
    changes in membership). But while teams face new hurdles, their success
    still hinges on a core set of fundamentals for group collaboration.

  Kalda20211031

    `Martti Kalda: Demokraatia kui usund. Elame kriisis – kus on pääsetee?
    <https://kolleegium.ee/oktoober-2021/martti-kalda-demokraatia-kui-usund-elame-kriisis-kus-on-paasetee/>`__
    based on "The World Religions: Old Traditions and Modern Transformations" by
    `Ninian Smart (1927-2001) <https://en.wikipedia.org/wiki/Ninian_Smart>`__





Books
=====

.. [Watzlawick1980] The Invented Reality: How Do We Know What We Believe We Know?, :term:`Paul Watzlawick` (editor)

Common sense suggests that reality can be discovered. In contrast,
constructivism postulates that what we call reality is a personal
interpretation, a particular way of looking at the world acquired through
communication. Reality is, therefore, not discovered, but literally invented.

Paul Watzlawick has contributed commentary, an introduction and an epilogue, and
two of the ten essays.

For educated readers, this is the first multidisciplinary presentation of a
subject of vital importance to the way we think and live.


.. [Andersen2018] Andersen, Jens (2018). Astrid Lindgren: The Woman Behind Pippi
  Longstocking. Translated by Caroline Waight. New Haven: Yale University Press.

`Review by irishtimes.com
<https://www.irishtimes.com/culture/books/astrid-lindgren-the-woman-behind-pippi-longstocking-review-1.3456925>`__


People
======

.. glossary::

  William Isaacs

    Founder and President of `Dialogos
    <https://www.dialogos.com/about/overview/>`__ and the author of `Dialogue and
    the Art of Thinking Together
    <https://www.amazon.com/Dialogue-Thinking-Together-William-Isaacs/dp/0385479999>`__
    (Doubleday, 1999).

  Joseph Bastin

    A friend of mine. I translated some of his speeches to English:

    | :doc:`/blog/2022/0403_1942`
    | :doc:`/blog/2021/1226_1907`
    | :doc:`/blog/2021/1127_0451`.

    He has an account on `Facebook <https://www.facebook.com/bastin.joseph>`__.


  Paul Watzlawick

    See https://en.wikipedia.org/wiki/Paul_Watzlawick


Classifications
===============


.. glossary::

  ICD

    `International Classification of Diseases
    <https://www.who.int/news-room/spotlight/international-classification-of-diseases>`__



Slogans
=======

.. glossary::

  PacktSchonMalAn

    `Es gibt viel zu tun -- Packt schon mal an
    <https://de.wikipedia.org/wiki/Liste_gefl%C3%BCgelter_Worte/P#Packen_wir%E2%80%99s_an!>`__
    is a parody of a slogan developed in 1974 by McCann Erickson (Hamburg) for
    Esso `[source] <https://www.slogans.de/slogans.php?BSelect%5B%5D=291>`__. The
    original text, which translates to "There's much to do -- let's get started"
    is transformed into "There's much to do -- get started (without me)".

Documents of the Roman Catholic church
======================================

.. glossary::


  Ordo Lectionum Missae

    The *Lectionary for Mass* of the :term:`Catholic Church`.

    https://en.wikipedia.org/wiki/Lectionary#Catholic_Mass_Lectionary_and_the_Revised_Common_Lectionary

    Felix Just, S.J., Ph.D. maintains a website
    `Catholic Resources <https://www.catholic-resources.org>`_
    where you can read the `full
    online text of the 1998/2002 USA Edition
    <https://www.catholic-resources.org/Lectionary/1998USL.htm>`__

    The German edition (published 1981) is here:
    https://www.sbg.ac.at/pth/links-tipps/past_ein/lektionar/


  Dei Verbum

    A dogmatic constitution on divine revelation produced and published by the
    :term:`Catholic Church` in November 1965 as one of the results of the
    :term:`Second Vatican Council`.

    `Full official text
    <https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19651118_dei-verbum_en.html>`__

    https://en.wikipedia.org/wiki/Dei_verbum

  Caritas in Veritate

    Enyclical letter by Pope Benedict XVI, (given 2009-06-29).

    https://www.vatican.va/content/benedict-xvi/en/encyclicals/documents/hf_ben-xvi_enc_20090629_caritas-in-veritate.html

  Laudato si'

    Encyclical letter
    `Laudato si' <https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html>`__
    of the Holy Father Francis
    on care for our common home.
    (24 May 2015)

  Fratelli Tutti

    Encyclical letter
    `Fratelli Tutti <https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20201003_enciclica-fratelli-tutti.html>`__
    of the Holy Father Francis
    on fraternity and social friendship.

.. rubric:: Citation sources

.. [Sgam20220713] Tyrone Sgambati, 2022-07-13, `How Do You Know if You’re Actually Humble?
  <https://greatergood.berkeley.edu/article/item/how_do_you_know_if_youre_actually_humble>`__

.. [CIC1983] `Codex Iuris Canonici
  <https://www.vatican.va/archive/cod-iuris-canonici/cic_index_en.html>`__ (Code
  of Canon Law)
