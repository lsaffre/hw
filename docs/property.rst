======================
About private property
======================

The concept of :term:`private property` is one of the basements of human
civilizations. Humans tend to care for the things they own, and to not care for
things owned by somebody else.

something ("to invest
into it"), we must give them a **right to govern** this something.

Basic concepts
==============

We generally accept the idea that humans can **own** certain **resources**. This
resource (the owned thing) can be as small as a pencil or as big as a piece of
land.


The owner of a resource is the sole responsible for governing it. He has the
right of deciding whether and how to make it available to others. He invests
care and energy to develop the resource, to repair it when needed, to maintain
it and share it efficiently. The owner has even the right to destroy the
resource.


We cannot expect them to make efforts unless they get some reward.

..
  They must have a chance to pursue their own profit and to satisfy
  their desire for :term:`owning <ownership>` things.


.. glossary::

  resource

    Something useful. Something that can be used by humans and which needs work,
    care and energy to be discovered, developed, maintained and used
    efficiently.

  property

    A :term:`resource` that is :term:`owned <ownership>` by somebody.

  ownership

    The fact of having been given the right to govern a resource, which includes
    managing its usage.

  private property

    Ownership of :term:`property` by non-governmental legal entities.

Article 17 of the `Universal Declaration of Human Rights
<https://www.un.org/en/universal-declaration-human-rights/index.html>`_ confirms
this when it says that "everyone has the right to own property".

Owning something does not only mean the *right* of using it but also the
*responsibility* of using it in a way that does not hurt others.


Types of resources
==================

Mobile or immobile goods are the most obvious forms of resources.

Money is the most flexible resource.

Territories and areas of the Earth are a particular form of resource.

The name and reputation of a human are a form of resource because
they represent a value.  Honour. Trustworthiness.

Trademarks and names of companies and organisations are
similar in nature to the name and reputation of a physical person.
They are the result of business activity.
As such they are a particular form of resource.

And then we invented the concept of *intellectual property*, the most
sophisticated form of a resource.



Shared ownership
================

Article 17 of the Universal Declaration of Human Rights adds "alone as well as
in association with others".  And article 23 (4) gets more concrete when it says
"Everyone has the right to form and to join trade unions for the protection of
his interests." A "trade union" is what I call a :term:`private corporation`.

Yes, humans can **collaborate**. This is another great thing with us. We can
create corporations ("unions"), which enable us to achieve things that need more
than one human life to get done.



The right to own is inferior to the right to survive
====================================================

`Thomas Aquinas <https://en.wikipedia.org/wiki/Thomas_Aquinas>`_ introduced the
following classification of resources (via `fellowdyinginmate
<https://www.patheos.com/blogs/messyinspirations/2020/11/dont-be-confused-about-catholic-teaching/>`__):

#. Resources needed to survive (both individuals and their families).
#. Resources required to fulfill social responsibilities, these being:

     - Education and support of the family.
     - Maintaining employment.
     - Helping your children in general.
     - Paying debts.

#. Surplus resources (superflua), remaining after situations (1) and (2) are met.


https://www.patheos.com/blogs/messyinspirations/2020/11/dont-be-confused-about-catholic-teaching/
