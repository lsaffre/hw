===========
About trust
===========

Let's talk about **trust**, or **confidence**, and **doubt**.

.. contents::
   :depth: 1
   :local:


Definitions
===========

.. glossary::

  to trust

    To trust in something means that you *assume* that this something is *right*
    despite the fact that you cannot ultimately verify your assumption.

  to doubt

    To doubt means to test your trust.
    Trust grows with every answered doubt.
    When your doubt is not answered, your trust gets lost.

The *something* that gets your trust can be a single person, a formal
:term:`teaching`, or a loosely defined set of :term:`beliefs <belief>`. I use
the word :term:`spirit` for referring to these potential lords who invite you to
trust them.

We all rely on trust
====================

Everybody :term:`trusts <to trust>` more or less strongly in various
:term:`spirits <spirit>`. Everybody does this more or less consciously.

Young children trust their parents --and the beliefs they show-- without any
:term:`doubt <to doubt>`. The more they grow, the more they dare to express
doubt.

A **blind trust** excludes the possibility of *doubt*.

Rule of thumb: when a :term:`spirit` demands blind trust, i.e. refuses your
expression of doubt, then this spirit is not trustworthy.

It makes no sense to doubt whether :term:`reality` is true, but you should
always be ready to doubt whether *your image of reality* matches reality.

"I don't trust in science"
==========================

There are people who refuse to trust in :term:`scientific evidence`.
They dare to express doubt about something that seems evident to :term:`common sense`.
Christians are well educated in this art.

Note that they do this usually in the context of a given :term:`controversial
question`. Even the most stubborn anti-vaxxer does not refuse the
:term:`scientific method` in general.

The :term:`Bible` is full of stories
and advice that encourages them to do so.   Some examples:

- Noah refused to give in to evidence when building his ship.

- Daniel refused to give in to evidence

- "It is better to take refuge in the Lord than to trust in humans." (Ps 118:8)

- "Trust in the Lord with all your heart, and do not lean on your own
  understanding. In all your ways acknowledge him, and he will make straight your
  paths."" (`Proverbs 3:5 <https://www.bibleserver.com/ESV/Proverbs3%3A5-6>`__)

- Mary refused to give in to evidence that getting pregnant before being
  married is dangerous. Joseph, her fiancé, refused to give in to the evidence
  that Mary had been sleeping with another guy.

I agree that we should not trust in science *blindly*.  Indeed :term:`scientific
evidence` does not mean that something is "definitively true" because
:term:`science` is just a product of human civilisation, and it is limited to
the :term:`visible world`. Furthermore, scientific research is limited by
available money. Pseudo-scientific methods can be abused by :term:`corporations
<corporation>` who want to present some theory as if it was :term:`scientific
evidence`.

But don't throw out the baby with the bathwater. God created humans with a
brain, senses and intellectual capacities. He wants us to observe and analyze
the :term:`visible world`. He wants us to give a name to everything.  Refusing
to accept :term:`scientific evidence` as a more trustworthy tool than for
example a :term:`Holy Scripture` is a form of :term:`superstition` and leads to
unrealistic results.

"I trust in science"
====================

Saying "I trust in science" is as valid as saying "I trust in books". Science is
a tool, not a :term:`spirit`.

See also
========

- :doc:`/blog/2021/0908`
