=============================================
Not everybody is ready for the Synodal Church
=============================================

On this page I try to summarize what I heard when listening to how people
reacted to the `questionnaire of the Synod
<https://sinod.katoliku.ee/en/nuclei/>`__ .
This is not a representative or systematic synthesis, it is what I personally heard.

My general impression is that quite some convinced Christians feel offended by
the vision of the Synodal Church that shines through in these questions.  Their
replies reveal a series of fears. The message "The church is synodal!" is for
them like that call "Liberté!" in the short film :term:`FreedomWithin`.

.. contents::
   :depth: 1
   :local:


1. Companions on the journey
============================

- *In the Church and in society we are side by side on the same road.*

Some reply: We need to hold together because we are a small community in a
dangerous world. Those who do not live according to the Church teachings about
sacraments, participation, mission, priesthood, homosexuality, transparency...
are not on our side, we need to protect ourselves against them.

My reply: The first theme of the questionnaire says one of the most important
characteristics of the :term:`Church`, which differentiates it from any other
human institution and makes it similar to a :term:`family`: nobody can leave.
Once you understood the Gospel, you cannot *not* follow Jesus. Once you have
become :term:`faithful`, you cannot go back. You are with us, whether you want
it or not, whether *we* want it or not. I see at least two consequences:

- All :term:`faithful` are part of the Church.  The Church must clearly signal
  that every :term:`faithful` is welcome, including those who come to different
  conclusions regarding certain "details".

- We will need to review our definition of "faithful" and "baptized". The rite
  itself obviously gives no guarantee. There are "baptized" people who actually
  didn't understand the Gospel and who follow something else. They received the
  sacrament by mistake. And there are people who follow the Gospel but refuse to
  get baptized because they have seen too much of the weed in the fields of the
  Church.


2. Listening
============

- *Listening is the first step, but it requires an open mind and heart, without
  prejudice.*

Some reply: Be careful: Listening to lies causes you to believe them in the end.

My reply: Sometimes we listen only in order to find arguments against "their"
opinion and to explain once more "our" opinion, which we mix up with the truth.
We must learn to not fear other humans as our "enemies". Our mind is like a
radar monitor. We constantly send out "listener signals" and then record the
echo we receive\ [#radar]_. Our mind is always "directed", turned into a given
direction. We see only the things we are looking at\ [#radar2]_. We need to
unlock our radar antenna and have it constantly turn in order to listen to every
direction.


3. Speaking out
================

- *All are invited to speak with courage and parrhesia, that is, in freedom,
  truth, and charity.*

Some reply: But only a fool speaks out everything without worrying about
diplomacy and how the enemy might use your words against you. Also "Do not give
dogs what is holy, and do not throw your pearls before pigs, lest they trample
them underfoot and turn to attack you." (`Mt 7:6
<https://www.bibleserver.com/ESV/Matthew7%3A6>`__)

My reply: :doc:`parrhesia`.

4. Celebration
==============

- *“Walking together” is only possible if it is based on communal listening to
  the Word and the celebration of the Eucharist.*

Some reply: The traditional Holy Mass fosters our identity and grows our faith.
We must not let other people tell us how to celebrate.

My reply: We must stop to claim that the traditional Roman Catholic way of
celebrating the Holy Mass is the only valid rite for celebrating.  On the other
hand we cannot prohibit the rite of the Old Mass as such.

This is a dilemma only as long as you see it as a dichotomy.  The logical
solution for this to extend our teachings about rites.  The Synodal Church will
embrace and document a wide set of rites. Not only the Old Mass and the Holy
Mass of the Catholic Church, but also the big treasure of rites that have
evolved in other church institutions. The SC won't *control* these rites (they
remain under the authority of their respective church institution) but evaluate
them and explain them in the synodal light.

Inspired (among others) by
`The Discalced Carmelite Sisters of Fairfield and Vatican document Cor Orans
<https://www.catholicworldreport.com/2021/11/20/the-discalced-carmelite-sisters-of-fairfield-and-vatican-document-cor-orans/>`__,
`Vatican's top liturgy official confirms restrictions on Latin Mass
<https://www.ncronline.org/news/vatican/vaticans-top-liturgy-official-confirms-restrictions-latin-mass>`__



5. Sharing responsibility for our common mission
================================================

- *Synodality is at the service of the mission of the Church, in which all
  members are called to participate.*

Some reply: As a lay person I must be careful of not instructing other people
regarding faith questions; teaching should be done by a professional. Beware of
the "stench effect": you can get used to live in a "stinking environment", you
can loose your faith when you are constantly surrounded by people who don't
believe.


6. Dialogue in Church and society
=================================

- *Dialogue requires perseverance and patience, but it also enables mutual
  understanding.*

Some reply: "We value perseverance and patience, but we must not waste our
energy into dialogue with people who refuse to accept the truth. We should
rather keep a clear distance from those who want to make us believe something
other than the truth."


7. Ecumenism
============

- *The dialogue between Christians of different confessions, united by one baptism,
  has a special place in the synodal journey.*

Some reply: "The purpose of *ecumenism* is to open a door to other confessions so
that lost faithful can turn back to the Roman Catholic church.  During Holy Mass
we welcome non-Catholic visitors as long as they behave adequately and follow
our rules. We shouldn't waste our time speaking with Christians who decided to
stay outside of the Roman Catholic church."


8. Authority and participation
==============================

- *A synodal church is a participatory and co-responsible Church.*

Some reply: "Church needs a clear hierarchy. Whenever humans do something
together, it must be clear who is the boss. The boss is responsible in the end.
If you are not the boss, then you must rather obey than feel co-responsible.
That's called discipline.  We don't believe in co-responsibility.  History shows
that communism has failed. Participation means communism. It is not a
sustainable way for living together.

My reply: While a well-regulated democratic government is probably the best management
system we can get in a world where faithful need to co-operate with faithless
people, the synodal management system has several strategic advantages over a
pure democracy:

- it provides methods to raise the voice of the weak and the poor in order to
  protect us from a hypertrophy of the strong and rich.

- it provides methods to potentially raise the voice of a little seemingly
  foolish amateur against those who are considered venerable and trustworthy.
  This protects us from desperate reactions caused by experiences of
  helplessness.

Inspired (among others) by `Debatte um Synodalen Weg: Was eine Kirchenrechtlerin
antwortet
<https://www.vaticannews.va/de/kirche/news/2021-11/debatte-um-synodalen-weg-was-eine-kirchenrechtlerin-antwortet.html>`__



9. Discerning and deciding
==========================

- *In a synodal style we make decisions through discernment of what the Holy
  Spirit is saying through our whole community.*

Some reply: "Common decisions must be the result of a well-organized democratic
process. We cannot allow corrupt leaders who do what they want, even if they
call it discernment."

10. Forming ourselves in synodality
===================================

- *Synodality entails receptivity to change, formation, and on-going learning.*

Some reply: "The Church must align to Jesus Christ alone, not to a multicultural
mix of ideologies. The mission of the Church is to *teach* others, not to
*learn* from others."


.. The Church no longer claims to teach the *only* valid teaching about God

..
  Teaching requires learning. While the Church --of course-- teaches what she
  has learned so far, we must never forget that her learning continues.



.. rubric:: Footnotes

.. [#radar] Reality is of course more complex because we
  also record incoming signals that are caused by something else than our
  listener signal. For example when the sun shines, the optical signals we
  receive are not an echo to our listener signal.

.. [#radar2]  "Still a man hears what he wants to hear and disregards the rest"
  (S&G).  Sometimes we perceive a movement outside of the focussed area, and our
  eyes then "automatically" redirect, turn into the new direction.
